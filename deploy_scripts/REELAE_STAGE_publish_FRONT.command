#!/bin/bash

echo -n "Are you sure you want to publish FRONTEND in DEV? (input 'yes/YES')"

read item
case "$item" in
   yes|YES)
       GitPath="~/reelae-web"
       KeyPath="~/.ssh/reelae"
       RemoteIP="167.99.224.85"
       User="root"

       ssh -i $KeyPath $User@$RemoteIP "cd $GitPath \
                                            && git reset --hard HEAD \
                                            && rm -rf node_modules/ \
                                            && git pull origin stage \
                                            && npm i \
                                            && npm run stage \
                                            && rm -rf /root/web-api/reelae-frontend/build/* \
                                            && rsync -a build/* /root/web-api/reelae-frontend/"
                                            ;;
   *) ;;
esac