import React, {Component, Fragment} from 'react';
import {Route, withRouter, Router, Switch} from 'react-router-dom';
import {connect} from 'react-redux';
import {history, userAuthToken} from './helpers';
import Auth from './containers/General/Auth/Auth';
import './App.sass';
import ForgotPassword from './containers/General/Auth/ForgotPassword'
import Loader from './components/UI/Loader';
import {PrivateRoute} from './components/Layout/PrivateRoute';
import {REGISTRATION, LOGIN, FORGOT_PASSWORD} from './constants/routes';
import {showLoader, hideLoader} from './store/actions/loader';
import {getDataByToken} from './store/actions/auth.action';
import Layout from './containers/General/Layout/Layout';
import * as routeConstants from './constants/routes';
import Modal from './components/UI/Modal/Modal';
import ModalHeader from './components/UI/Modal/ModalHeader';
import ModalBody from './components/UI/Modal/ModalBody';
import ModalRow from './components/UI/Modal/ModalRow';
import moment from 'moment';
import {logout} from './store/actions';

class App extends Component {

  state = {
    showAttention: true,
  };

  componentDidMount() {

    if (localStorage.getItem('registration')) {
        localStorage.removeItem('registration');
    }
    if (localStorage.getItem('user')) {
      let user = JSON.parse(localStorage.getItem('user'));
      if(user.institutionId === 0){
        this.props.logout();
      } else
      this.props.getDataByToken({refreshToken: this.props.auth.user.refreshToken}, this.props.auth.user.token);
    }

    if (!window.matchMedia('(min-width: 1366px)').matches) {
      document.body.className += 'non-responsive';
    }
  }

  hideAttention = () => {this.setState({showAttention: false})};

  render() {
    return (
      <Fragment>
        {!this.props.auth.loading &&
        <Router history={history}>
          <div>
            <Switch>
              <PrivateRoute exact path='/' component={Layout}/>
              <PrivateRoute path='/admin' component={Layout}/>
              <PrivateRoute exact path='/forum' component={Layout}/>
              <PrivateRoute exact path='/forum/:id' component={Layout}/>
              <PrivateRoute path='/submissions' component={Layout}/>
              <PrivateRoute path='/connect' component={Layout}/>
              <PrivateRoute path={routeConstants.GROUP_MEMBER} component={Layout}/>
              <PrivateRoute exact path={routeConstants.ASSIGNMENTS} component={Layout}/>
              <PrivateRoute exact path={routeConstants.ASSESSMENTS} component={Layout}/>
              <PrivateRoute exact path={routeConstants.RESOURCES} component={Layout}/>
              <PrivateRoute exact path={`${routeConstants.ASSIGNMENTS}/:id`} component={Layout}/>
              <PrivateRoute exact path={routeConstants.STAFF_ROOM} component={Layout}/>
              <PrivateRoute exact path={routeConstants.KEYWORD} component={Layout}/>
              <PrivateRoute exact path={routeConstants.NOTICE_BOARD} component={Layout}/>
              <PrivateRoute exact path={`${routeConstants.NOTICE_BOARD}/:id`} component={Layout}/>
              <PrivateRoute exact path={routeConstants.PROFILE} component={Layout}/>
              <PrivateRoute exact path={routeConstants.DASHBOARD} component={Layout}/>
              <PrivateRoute exact path={routeConstants.PINUP} component={Layout}/>
              <PrivateRoute exact path={routeConstants.MILESTONE} component={Layout}/>
              <PrivateRoute exact path={routeConstants.REGISTRATION_FLOW} component={Layout}/>
              <PrivateRoute exact path={routeConstants.PAYMENT} component={Layout}/>
              <PrivateRoute exact path={routeConstants.WORKSPACE} component={Layout}/>
              <PrivateRoute exact path={routeConstants.ASSIGNMENT_RESULT} component={Layout}/>
              <PrivateRoute exact path={routeConstants.ATTENDANCE} component={Layout}/>
              <PrivateRoute exact path={routeConstants.EVENTS} component={Layout}/>
            </Switch>
            <Route path={LOGIN} exact component={Auth}/>
            <Route path={REGISTRATION} exact component={Auth}/>
            <Route path={FORGOT_PASSWORD} exact component={ForgotPassword}/>
          </div>
        </Router>
        }
        {this.props.loader.show || this.props.auth.loading ? <Loader/> : null}
        {!this.props.auth.loading &&
          this.props.auth.user.id &&
          this.props.auth.user.institutionId &&
        !moment().isBefore(moment(this.props.auth.user.institutionExpireAt)) &&
        <Modal show={this.state.showAttention} noCross hideModal={this.hideAttention}>
          <ModalHeader>
            <h3 className="modal-title"
                style={{'fontSize': '32px'}}
            >Attention</h3>
          </ModalHeader>
          <ModalBody>
            <ModalRow>
              <p style={{
                'lineHeight' : '25px',
                'fontWeight' : '400'
              }}>Dear {this.props.auth.user.firstName} {this.props.auth.user.lastName}, your account has been
              locked for due to the fact that we were unable to charge your card.
              </p>
            </ModalRow>
          </ModalBody>
        </Modal>
        }
      </Fragment>
    );
  }
}

const mapStateToProps = ({loader, auth}) => ({loader, auth});

const mapDispatchToProps = dispatch => ({
  showLoader: () => dispatch(showLoader()),
  hideLoader: () => dispatch(hideLoader()),
  getDataByToken: (refreshToken, token) => {dispatch(getDataByToken(refreshToken, token))},
  logout: () => {dispatch(logout())}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
