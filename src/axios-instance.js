import axios from 'axios';
import store from './index';
import * as actions from './store/actions'

const instance = axios.create({
  baseURL: process.env.API_URL,
});

let isLoadingRefreshToken = false;

instance.interceptors.response.use(null, function (error) {
  if (error.response && error.response.status === 401) {
    const state = store.getState();

    if (!isLoadingRefreshToken) {
      isLoadingRefreshToken = true;

      store.dispatch(actions.refreshToken({refreshToken: state.auth.user.refreshToken}))
        .then(result => {
          if(result.data) throw new Error(result.data.message);
          return result;
        })
        .then(refreshedToken => {
          isLoadingRefreshToken = false;

          let currentLocalStoreUser = {
            ...JSON.parse(localStorage.getItem('user')),
            token: refreshedToken.accessToken,
            refreshToken: refreshedToken.refreshToken,
          };

          localStorage.setItem('user', JSON.stringify(currentLocalStoreUser));

          return {
            token: refreshedToken.accessToken,
            refreshToken: refreshedToken.refreshToken,
          }
        })
        .then(tokens => {
          store.dispatch(actions.getDataByToken(tokens.refreshToken, tokens.token))
        })
        .catch(err => {
          isLoadingRefreshToken = false;
          store.dispatch(actions.logout());
        });
    }

  }
  return Promise.reject(error);
});

export default instance;