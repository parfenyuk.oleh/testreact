import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import CommentItem from '../../UI/Layout/CommentItem';

const CommentBlock = ({user, comment, loadingReplies, auth, editMode, removeComment}) => {
    return (
        <div className='comments-block'>
            <p className='bolder' style={{marginBottom: '20px'}}>Comments ({user.item.repliesCount}):</p>
            {
                comment !== undefined && (
                    <Fragment>
                        {
                            comment.map((item) => {
                                let dateReplies = moment(item.createdAt).format('DD-MM-YYYY LT');

                                return <CommentItem
                                    comment={comment}
                                    user={auth.user}
                                    creatorId={item.creatorId}
                                    role={auth.user.role}
                                    key={item.id}
                                    imgSrc={item.avatar}
                                    firstName={item.firstCreatorName}
                                    lastName={item.lastCreatorName}
                                    date={dateReplies}
                                    className=''
                                    editMode={() => editMode(item.id, 'comment')}
                                    removeComment={() => removeComment(item.id)}
                                    style={{}}
                                >
                                    {item.message}
                                </CommentItem>})
                        }
                    </Fragment>
                )
            }

        </div>
    );
};

CommentBlock.propTypes = {
    user: PropTypes.object,
    date: PropTypes.string,
    modified: PropTypes.string,
    deleteForumItem: PropTypes.func
};

export default CommentBlock;
