import moment from 'moment';
import {Link} from 'react-router-dom';
import * as routeConstants from '../../../constants/routes';
import Card from '../../UI/Layout/Card';
import React from 'react';
import EmptyState from '../../UI/EmptyState';
import Preloader from '../../UI/Preloader';

export const List = ({list, cardClick, role, userId, isLoading}) => {
    return <div className='list'>
        {
            list.length ? (list.map((item) => {
                const date = moment(item.createdAt).format('DD-MM-YYYY');
                const activeUntil = moment(item.inactiveDate).format('DD-MM-YYYY');

                return <Card
                    userId={userId}
                    role={role}
                    key={item.id}
                    info={item}
                    createdAt={date}
                    activeUntil={activeUntil}
                    cardClick={cardClick}
                >
                    {item.description}
                </Card>
            })) : (
                <div className='emptyState-wrapper'>
                    {
                        isLoading ? <Preloader/> : <EmptyState/>
                    }
                </div>)
        }
    </div>
};