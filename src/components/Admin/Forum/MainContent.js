import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import Icon from '../../UI/Elements/Icon';
import {userType} from '../../../constants';
import Table from './Table';
import Reply from '../../UI/Layout/Reply';
import CommentBlock from './CommentBlock';
import {Link} from 'react-router-dom';
import * as routeConstants from '../../../constants/routes';
import ROLES from '../../../constants/roles';

const MainContent = ({
                         auth,
                         user,
                         date,
                         inactiveDate,
                         toggleEditFormModal,
                         deleteForumItem,
                         message,
                         handleChange,
                         saveComment,
                         isLoading,
                         removeComment
                     }) => {

    const assessmentAttachments = [
        {
            attachmentHeading: "string",
            url: "http://142.93.54.236:81/uploads/resource/Photo/Photo_a650a8e5-8e27-4b25-8f77-a69f79a88c67.png"
        },
        {
            attachmentHeading: "string",
            url: "http://142.93.54.236:81/uploads/resource/Photo/Photo_a650a8e5-8e27-4b25-8f77-a69f79a88c67.png"
        },
        {
            attachmentHeading: "string",
            url: "http://142.93.54.236:81/uploads/resource/Photo/Photo_a650a8e5-8e27-4b25-8f77-a69f79a88c67.png"
        }
    ];

    return (
        <Fragment>
            <div className='detail-forum__header'>
                <Link to={routeConstants.FORUM}>
                    <Icon name='left-arrow'/>
                </Link>
                <p style={{marginLeft: '20px'}}>{user.item.name}</p>
            </div>
            <div className='detail-forum__main'>
                <Table user={auth.user}
                       forumDetail={user.item}
                       role={auth.user.role}
                       date={date}
                       forum
                       inactiveDate={inactiveDate}
                       editMode={() => toggleEditFormModal(user.item.id, 'forum')}
                       deleteForumItem={deleteForumItem}
                       assessmentAttachments={assessmentAttachments}
                />
                <Reply
                    imgSrc={auth.user.avatar}
                    text={`${auth.user.firstName[0]}${auth.user.lastName[0]}`}
                    placeholderText='Type here to reply…'
                    className=''
                    message={message}
                    handleChange={handleChange}
                    onSubmit={saveComment}
                />
                <CommentBlock user={user}
                              comment={user.forumReplies}
                              auth={auth}
                              loadingReplies={isLoading}
                              editMode={toggleEditFormModal}
                              removeComment={removeComment}
                />
            </div>
        </Fragment>
    );
};

MainContent.propTypes = {
    user: PropTypes.object,
    date: PropTypes.string,
    modified: PropTypes.string,
    deleteForumItem: PropTypes.func
};

export default MainContent;
