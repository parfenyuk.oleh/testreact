import React from 'react';
import Modal from '../../../UI/Modal/Modal';

import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Input from '../../../UI/Input';
import Textarea from '../../../UI/Textarea';
import DatePicker from '../../../UI/Datepicker';
import Cell from '../../../UI/Cell';
import Button from '../../../UI/Elements/Button';
import SelectMaterial from '../../../UI/Elements/SelectMaterial';
import icons from '../../../../helpers/iconsLoader';
import DocumentLoader from '../../../UI/DocumentLoader';
import MultipleUpload from '../../../../containers/General/Upload/MultipleUpload';

const NewForumItem = ({show, fieldUpdated, saved, hideModal, handleChange, subject, subjectList, errors}) => {
    const documentText = 'document';

    if (subjectList !== undefined) {
        return (
            <Modal show={show} hideModal={hideModal}>
                <ModalHeader>
                    <h3 className="modal-title">New Forum</h3>
                </ModalHeader>
                {
                    errors.length > 0 && (
                        <div className="auth-errors" style={{marginBottom: '-50px'}}>
                            <ul>
                                {errors.map((item, key) => <li key={key}> <img src={icons.iconAttention} alt=""/> {item} </li>)}
                            </ul>
                        </div>
                    )
                }
                <ModalBody bigPadding overflow={true}>
                    <ModalRow flex>
                        <Cell column={7.5}>
                            <DatePicker placeholder={'Inactive date'}
                                        changed={(value) => { fieldUpdated(value.format('l'), 'inactiveDate') }}
                            />
                        </Cell>
                    </ModalRow>
                    <div>
                        <ModalRow>
                            <div className="select-wrapper">
                                <SelectMaterial placeholder="Choose subject" handleChange={handleChange} activeItem={subject} itemList={subjectList}/>
                            </div>
                            <Input placeholder="Title" changed={(e) => { fieldUpdated(e.target.value, 'name') }}/>
                            <Textarea placeholder="Description" changed={(e) => { fieldUpdated(e.target.value, 'description') }}/>
                            <MultipleUpload/>

                        </ModalRow>
                    </div>
                    <ModalRow button>
                        <Button
                            onClick={saved}
                            type="button"
                            size="xl"
                        >
                            Save
                        </Button>
                    </ModalRow>
                </ModalBody>
            </Modal>
        );
    } else {
        return (
            <div></div>
        )
    }
};

NewForumItem.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};

export default NewForumItem;
