import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import Icon from '../../UI/Elements/Icon';
import {userType} from '../../../constants';
import AvatarBlock from '../../../components/UI/Elements/AvatarBlock'
import File from '../../../images/doc_no_bg.svg';
import axios from '../../../axios-instance';
import {userAuthToken} from '../../../helpers';
import fileDownload from 'js-file-download';

const TableInfo = ({forumDetail, creatorId, role, date, inactiveDate, deleteForumItem, editMode, user, forum, assessmentAttachments}) => {

    const downloadFile = files => {
        let extension;

        if (files.url) {
            extension = files.url.split('.');
            extension = extension.slice(-1)[0].toLowerCase();
            console.log(files.url)
        }
        axios({
            url: files.url,
            method: 'GET',
            crossdomain: true,
            responseType: 'blob', // important
            headers: {
                Authorization: userAuthToken.token(),
                'Access-Control-Allow-Origin': '*',
            },
        }).then(response => {
            console.log(response.data);
            fileDownload(response.data, `${files.attachmentHeading}.${extension}`);
        })
    };

    return (
        <div className='info-table'>
            <div className='info-table__header'>
                <AvatarBlock
                    size='lg'
                    forum={forum}
                    createdAt={date}
                    activeUntil={inactiveDate}
                    imgSrc={forumDetail.avatar}
                    textAvatar={`${user.firstName[0]}${user.lastName[0]}`}
                >
                    {forumDetail.firstCreatorName} {forumDetail.lastCreatorName}
                </AvatarBlock>
                <div className='mode'>
                    {
                        (forumDetail.creatorId === user.id || userType.admin === role) && (
                            <Fragment>
                                <Icon name='edit-dark' onClick={editMode}/>
                                <Icon name='delete-dark' onClick={deleteForumItem}/>
                            </Fragment>
                        )
                    }
                </div>
            </div>
            <div className='info-table__main'>
                <p className='name'>
                    {forumDetail.name}
                </p>
                <p className='subject-name'>
                    {forumDetail.subjectName}
                </p>
                <p className='desscription'>
                    {forumDetail.description}
                </p>
                {assessmentAttachments &&
                <div className="forum-attachments">
                    {assessmentAttachments.map(files => {
                        return (
                            <div className="file-container">
                                <img src={File}
                                     alt={`${files.attachmentHeading}`}
                                     className="uploaded-file"
                                />

                                <p>{files.attachmentHeading}</p>
                                <div onClick={() => {
                                    downloadFile(files)
                                }}>
                                    <Icon name='download'/>
                                </div>
                                <div>
                                    <Icon name='delete-dark'/>
                                </div>
                            </div>
                        )
                    })}
                </div>
                }
            </div>
        </div>
    );
};

TableInfo.propTypes = {
    user: PropTypes.object,
    date: PropTypes.string,
    modified: PropTypes.string,
    deleteForumItem: PropTypes.func
};

export default TableInfo;
