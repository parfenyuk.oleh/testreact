import React from 'react';
import PropTypes from 'prop-types';
import Select from "../../UI/Select";

const MainHeader = ({title}) => {
  return (
    <div className="admin-main-header">
      <h1 className="page-title">Members</h1>
    </div>
  );
};

MainHeader.propTypes = {

};

export default MainHeader;
