import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Icon from '../../../components/UI/Elements/Icon';
import icons from '../../../helpers/iconsLoader';


const ADMIN_ITEM_BODY_SELECTOR = 'admin-main-sidebar-body';
const ADMIN_SUB_HEADER_SELECTOR = 'admin-main-sidebar-sub-header';


function openSubHandler({target}) {
    target.closest('.' + ADMIN_ITEM_BODY_SELECTOR).nextSibling.classList.toggle('open');
}

const MainSidebar = ({ showNewGroupModal,
                         removeGroupItem,
                         groups,
                         changeActiveGroup,
                         activeGroup
                     }) => {
    return (
        <div className="admin-main-sidebar groups">
            <div className="admin-main-sidebar-item">
                <div className={`${ADMIN_ITEM_BODY_SELECTOR}`}
                     onClick={openSubHandler}>
                    <span className="admin-main-sidebar-name">GROUPS</span>
                    <span className="admin-main-sidebar-count">{groups.length}</span>
                    <span className="admin-main-sidebar-add"><img src={icons.iconPlus} alt="" onClick={showNewGroupModal}/></span>
                </div>
                <div className="admin-main-sidebar-sub">
                    {
                        groups.map((item) => <div key={item.id}
                                                  className={activeGroup === item.id ? `${ADMIN_SUB_HEADER_SELECTOR} active` : `${ADMIN_SUB_HEADER_SELECTOR}`}
                                                  onClick={() => {changeActiveGroup(item.id, item.subjectId)}}>
                            <p>
                                <img src={icons.iconFolder} alt=""/>
                                {item.name}
                            </p>
                            <Icon name="delete-dark" onClick={() => {removeGroupItem(item.id)}}/>
                        </div>)
                    }
                </div>
            </div>
        </div>
    );
};

MainSidebar.propTypes = {
    showNewSubjectModal: PropTypes.func,
    showNewStudentModal: PropTypes.func,
    showNewTeacherModal: PropTypes.func
};

MainSidebar.defaultProps = {
    subject: []
};

const mapDispatchToProps = ({admin: {subject, members}}) =>({subject, members});

export default connect(mapDispatchToProps)(MainSidebar);
