import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from "../../UI/Elements/Checkbox";
import RoundAvatar from "../../UI/RoundAvatar";

const MemberTable = ({ checkedItems, handleInputChange, list}) => {
    return (
        <table>
            <tbody>
            {
                list.map(item => {
                        const text = `${item.firstName.charAt(0)}${item.lastName.charAt(0)}`;

                        return <tr key={item.id}>
                            <td>
                                <Checkbox name={`${item.id}`}
                                          checked={checkedItems.get(`${item.id}`)}
                                          onChange={(e) => {
                                              handleInputChange(e)
                                          }}/>
                            </td>
                            <td>
                                <RoundAvatar avatar={item.avatar} text={text}/>
                            </td>
                            <td>
                                {item.firstName} {item.lastName}
                            </td>
                            <td>
                                {item.email}
                            </td>
                        </tr>
                    }
                )
            }
            </tbody>
        </table>
    );
};

MemberTable.propTypes = {
    handleInputChange: PropTypes.func,
    list: PropTypes.array,
};

export default MemberTable;
