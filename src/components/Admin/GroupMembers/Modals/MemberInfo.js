import React from 'react';
import Modal from "../../../UI/Modal/Modal";
import ModalHeader from "../../../UI/Modal/ModalHeader";
import ModalBody from "../../../UI/Modal/ModalBody";
import ModalRow from "../../../UI/Modal/ModalRow";
import ROLES from '../../../../constants/roles';
import RoundAvatar from "../../../UI/RoundAvatar";

const MemberInfo = ({show, hideModal, user}) => {
  return (
    <Modal show={show} hideModal={hideModal}>
      <ModalHeader>
        <h3 className="modal-title">{user.role === ROLES.STUDENT ? 'Student information' : 'Teacher information'}</h3>
      </ModalHeader>
      <ModalBody bigPadding overflow>
        <ModalRow flex center>
          <RoundAvatar avatar={user.avatar} size={'normal'}/>
        </ModalRow>
        <ModalRow>
          <p className="admin-modal-line">
            <span className="admin-modal-label">Name:</span>
            <span className="admin-modal-text">{user.firstName} {user.lastName}</span>
          </p>
          <p className="admin-modal-line">
            <span className="admin-modal-label">Id:</span>
            <span className="admin-modal-text">{user.id}</span>
          </p>
          <p className="admin-modal-line">
            <span className="admin-modal-label">Email:</span>
            <span className="admin-modal-text">{user.email}</span>
          </p>
          <p className="admin-modal-line">
            <span className="admin-modal-label">Location:</span>
            <span className="admin-modal-text">{user.location}</span>
          </p>
          <p className="admin-modal-line">
            <span className="admin-modal-label">BIO:</span>
            <span className="admin-modal-text">{user.bio}</span>
          </p>
        </ModalRow>
      </ModalBody>
    </Modal>
  );
};

export default MemberInfo;
