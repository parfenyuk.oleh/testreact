import React from 'react';
import Modal from '../../../UI/Modal/Modal';

import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Input from '../../../UI/Input';
import Textarea from '../../../UI/Textarea';
import icons from '../../../../helpers/iconsLoader';
import SelectMaterial from '../../../UI/Elements/SelectMaterial';
import Button from '../../../UI/Elements/Button';

const NewGroupModal = ({show, fieldUpdated, saved, hideModal, handleChange, subject, subjectList, errors}) => {
    if (subjectList !== undefined) {
        return (
            <Modal show={show} hideModal={hideModal}>
                <ModalHeader>
                    <h3 className='modal-title'>New Group</h3>
                </ModalHeader>
                {
                    errors.length > 0 && (
                        <div className='auth-errors' style={{marginBottom: '-50px'}}>
                            <ul>
                                {errors.map((item, key) => <li key={key}> <img src={icons.iconAttention} alt=''/> {item} </li>)}
                            </ul>
                        </div>
                    )
                }
                <ModalBody bigPadding overflow={true}>
                    <div>
                        <ModalRow>
                            <Input placeholder='Group name' changed={(e) => { fieldUpdated(e.target.value, 'name') }}/>
                            <Textarea placeholder='Description' changed={(e) => { fieldUpdated(e.target.value, 'description') }}/>
                            <SelectMaterial placeholder='Choose subject' handleChange={handleChange} activeItem={subject} itemList={subjectList}/>
                        </ModalRow>
                    </div>
                    <ModalRow button>
                        <Button
                            onClick={saved}
                            type='button'
                            size='xl'
                            className=''
                        >
                            Save
                        </Button>
                    </ModalRow>
                </ModalBody>
            </Modal>
        );
    } else {
        return (
            <div></div>
        )
    }
};

NewGroupModal.propTypes = {
  show: PropTypes.bool,
  hideModal: PropTypes.func,
  fieldUpdated: PropTypes.func,
  saved: PropTypes.func
};

export default NewGroupModal;
