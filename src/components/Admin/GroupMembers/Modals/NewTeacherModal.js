import React from 'react';
import Modal from '../../../UI/Modal/Modal';
import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Button from "../../../UI/Elements/Button";
import icons from "../../../../helpers/iconsLoader";
import MemberTable from "../../../Admin/GroupMembers/MemberTable";
import SearchBar from "../../../Admin/GroupMembers/SearchBar";
import {compose} from "recompose";
import {withInfiniteScroll} from "../../../../hoc/withInfiniteScroll";


const NewTeacherModal = ({
                             show,
                             hideModal,
                             checkedItems,
                             handleInputChange,
                             list,
                             handleChange,
                             saved,
                             admin,
                             take,
                             skip,
                             onPaginated
                         }) => {
    return (
        <Modal show={show} hideModal={hideModal} className="modal-member">
            <ModalHeader>
                <h3 className="modal-title">Add new teacher to group</h3>
            </ModalHeader>
            <ModalBody overflow={true}>
                <ModalRow>
                    <SearchBar handleChange={(e) => {handleChange(e, 'newTeacher')}}/>
                    <div className="table-block">
                        <AdvancedList
                            checkedItems={checkedItems}
                            handleInputChange={(e) => handleInputChange(e, 'teacher')}
                            list={list}
                            lengthArray={list.length}
                            isLoading={admin.loading}
                            take={take}
                            skip={skip}
                            onPaginated={() => {onPaginated('teacher')}}
                        />
                    </div>
                </ModalRow>
                <ModalRow button>
                    <Button
                        type="button"
                        size="xl"
                        onClick={() => saved('teachers')}
                    >
                        Save
                    </Button>
                </ModalRow>
            </ModalBody>
        </Modal>
    );
};

const AdvancedList = compose(
    withInfiniteScroll,
)(MemberTable);

NewTeacherModal.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};

export default NewTeacherModal;
