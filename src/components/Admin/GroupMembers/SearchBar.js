import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from "@material-ui/core";
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles/colorManipulator';

const styles = theme => ({
    search: {
        position: 'relative',
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: 'white',
            color: 'black'
        },
        borderBottom: '1px solid #44566C',
        marginBottom: '20px',
        marginLeft: 0,
        width: '100%',
        color: 'black',
        [theme.breakpoints.up('sm')]: {
            marginLeft: 0,
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing.unit * 5,
        color: '#44566C',
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'black',
        width: '100%',
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 5,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
});

const SearchBar = ({ checkedItems, handleInputChange, list, classes, handleChange}) => {
    return (
        <div className={classes.search}>
            <div className={classes.searchIcon}>
                <SearchIcon />
            </div>
            <InputBase
                placeholder="Search students..."
                name="search"
                onChange={handleChange}
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput,
                }}
            />
        </div>
    );
};

SearchBar.propTypes = {
    checkedItems: PropTypes.bool,
    handleInputChange: PropTypes.func,
    list: PropTypes.array,
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SearchBar);
