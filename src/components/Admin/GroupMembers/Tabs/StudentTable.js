import React from 'react';

import {getRandomInt} from '../../../../helpers/randomColor';
import icons from '../../../../helpers/iconsLoader';
import TableHeader from "../../../UI/Table/TableHeader";
import Cell from "../../../UI/Cell";
import TableRow from "../../../UI/Table/TableRow";
import RatingLine from "../../../UI/RatingLine";
import AvatarBlock from "../../../UI/Elements/AvatarBlock";

const StudentTable = ({users, changeMemberHandler, deleteMemberHandler}) => {
  return (
    <div className='admin-table'>
      <TableHeader>
        <Cell column={1.5}>ID</Cell>
        <Cell column={3.5}>Name</Cell>
        <Cell column={5}>Subjects</Cell>
        <Cell column={2}>Rating</Cell>
      </TableHeader>
      {
        users.map(item => (<TableRow key={item.id}>
          <Cell column={1.5}>#{item.id}</Cell>
          <Cell column={3.5}>
            <AvatarBlock textAvatar={`${item.firstName[0]}${item.lastName[0]}`}
                         customInfo={<span>{`${item.firstName} ${item.lastName}`}</span>}
                         imgSrc={item.avatar}
            />
          </Cell>
          <Cell column={5}>
            UI  /  Branding  /  Interactive  /  Motion
          </Cell>
          <Cell column={2}>
            <RatingLine rating={getRandomInt(100)}/>
          </Cell>
          <Cell column={3} justify={'center'}>
            <button className={'button-empty-gray'}>ATTENDANCE</button>
          </Cell>
          <Cell column={1}>
            <img src={icons.iconEdit} alt="edit"
                 style={{marginRight: `20px`, cursor: 'pointer'}}
                 onClick={() => {changeMemberHandler(item)}}
            />
            <img src={icons.iconBin}
                 style={{cursor: 'pointer'}}
                 alt="delete"
                 onClick={() => {deleteMemberHandler(item)}}
            />
          </Cell>
        </TableRow>))
      }
    </div>
  );
};


export default StudentTable;
