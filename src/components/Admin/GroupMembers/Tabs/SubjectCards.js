import React from 'react';
import Card from '../../../UI/Card/Card';
import CardFooter from '../../../UI/Card/CardFooter';
import icons from '../../../../helpers/iconsLoader';
import CardBody from '../../../UI/Card/CardBody';
import RoundAvatar from '../../../UI/RoundAvatar';
import Button from '../../../UI/Elements/Button';
import ROLES from '../../../../constants/roles';
import Preloader from '../../../UI/Preloader';
import EmptyState from '../../../UI/EmptyState';

const SubjectCards = ({users, showMemberInfo, deleteMember, showMemberModal, showTeacherModal, isLoading}) => {
    console.log(isLoading);
    return (
        <div className='admin-cards group-main-block'>
            <div className='button-block' style={{display: 'flex', justifyContent:'space-between', marginBottom: '12px'}}>
                <div />
                <div>
                    <Button
                        type='button'
                        size='md'
                        className=''
                        style={{marginRight: '20px'}}
                        onClick={showTeacherModal}
                    >
                        ADD NEW TEACHER
                    </Button>
                    <Button
                        type='button'
                        size='md'
                        className=''
                        style={{}}
                        onClick={showMemberModal}
                    >
                        ADD NEW STUDENT
                    </Button>
                </div>

            </div>
            {
                users.length ? (users.map(user => {
                        return <Card key={user.id}>
                            <CardBody align={'center'} justify={'space-between'} clicked={() => {showMemberInfo(user.id)}}>
                                <RoundAvatar text={`${user.firstName[0]} ${user.lastName[0]}`}
                                             size='big'
                                             avatar={user.avatar}
                                />
                                {user.role === ROLES.TEACHER && <div className='admin-cards-teacher'>Teacher</div>}
                                <div>
                                    <p className='admin-cards--name'>{user.firstName} {user.lastName}</p>
                                    <p className='admin-cards--email'>{user.email}</p>
                                </div>
                            </CardBody>
                            <CardFooter justify='center' action={() => {deleteMember(user.id)}}>
                                <img src={icons.iconBin} alt='' />
                            </CardFooter>
                        </Card>
                    }

                )) : (
                    <div className='emptyState-wrapper'>
                        {
                            isLoading ? <Preloader/> : <EmptyState/>
                        }
                    </div>)
            }
        </div>
    );
};

SubjectCards.defaultProps = {
    users: []
};

export default SubjectCards;
