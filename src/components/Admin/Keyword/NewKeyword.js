import React from 'react';
import Modal from '../../UI/Modal/Modal';

import PropTypes from 'prop-types';
import ModalHeader from '../../UI/Modal/ModalHeader';
import ModalBody from '../../UI/Modal/ModalBody';
import ModalRow from '../../UI/Modal/ModalRow';
import Input from '../../UI/Input';
import Button from '../../UI/Elements/Button';
import icons from '../../../helpers/iconsLoader';

const NewKeyword = ({show, handleChange, saved, hideModal, errors}) => {
    return (
        <Modal show={show} hideModal={hideModal}>
            <ModalHeader>
                <h3 className='modal-title'>New keyword</h3>
            </ModalHeader>
            {
                errors.length > 0 && (
                    <div className='auth-errors' style={{marginBottom: '-50px'}}>
                        <ul>
                            {errors.map((item, key) => <li key={key}><img src={icons.iconAttention} alt=''/> {item}
                            </li>)}
                        </ul>
                    </div>
                )
            }
            <ModalBody bigPadding overflow={false}>
                <div>
                    <ModalRow>
                        <Input placeholder='Title' name='description' changed={handleChange}/>
                    </ModalRow>
                </div>
                <ModalRow button>
                    <Button
                        onClick={saved}
                        type='button'
                        size='xl'
                        className=''
                    >
                        Save
                    </Button>
                </ModalRow>
            </ModalBody>
        </Modal>
    );
};

NewKeyword.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};

export default NewKeyword;
