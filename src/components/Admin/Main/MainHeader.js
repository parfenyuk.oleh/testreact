import React from 'react';
import PropTypes from 'prop-types';
import Select from "../../UI/Select";
import PageHeader from "../../UI/PageHeader";

const MainHeader = () => {
  return (
    <PageHeader>
      <h1 className="page-title">Members</h1>
    </PageHeader>
  );
};


export default MainHeader;
