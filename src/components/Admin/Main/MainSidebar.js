import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import icons from '../../../helpers/iconsLoader';
import ROLES from '../../../constants/roles';
import Icon from '../../UI/Elements/Icon';

import {MODAL_TYPES, MODALS_TITLES }  from '../../../constants/modalsTypes';

import * as actions from '../../../store/actions/modals.action';

const ADMIN_ITEM_BODY_SELECTOR = 'admin-main-sidebar-body';
const ADMIN_SUB_HEADER_SELECTOR = 'admin-main-sidebar-sub-header';


function openSubHandler({target}) {
  target.closest('.' + ADMIN_ITEM_BODY_SELECTOR).nextSibling.classList.toggle('open');
}

function openSubListHandler({target}) {
  const closest = target.closest('.' + ADMIN_SUB_HEADER_SELECTOR);
  closest.nextSibling.classList.toggle('open');
  closest.classList.toggle('open');
}

const MainSidebar = ({
   modals,
   showModal,
   showNewSubjectModal,
   showNewStudentModal,
   showNewTeacherModal,
   changeTab,
   TABS,
   counts,
   subject,
   activeTab,
   auth,
   chosenSubject,
   archiveSubject,
   updateSubject,
   showNewAdminModal,
 }) => {
  return (
    <div className="admin-main-sidebar">
      {auth.user.role === ROLES.ADMIN && <div className="admin-main-sidebar-item">
        <div className={`${ADMIN_ITEM_BODY_SELECTOR} ${activeTab === TABS.ADMIN ? 'active' : ''}`}
             onClick={() => {
               changeTab(TABS.ADMIN)
             }}>
          <span className="admin-main-sidebar-name">Admins</span>
          <span className="admin-main-sidebar-count">{counts.admins}</span>
          <span className="admin-main-sidebar-add"><img src={icons.iconPlus} alt=""
                                                        onClick={showNewAdminModal}/></span>
        </div>
      </div>
      }
      {auth.user.role === ROLES.ADMIN && <div className="admin-main-sidebar-item">
        <div className={`${ADMIN_ITEM_BODY_SELECTOR} ${activeTab === TABS.TEACHERS ? 'active' : ''}`}
             onClick={() => {
               changeTab(TABS.TEACHERS)
             }}>
          <span className="admin-main-sidebar-name">Teachers</span>
          <span className="admin-main-sidebar-count">{counts.teachers}</span>
          <span className="admin-main-sidebar-add"><img src={icons.iconPlus} alt=""
                                                        onClick={showNewTeacherModal}/></span>
        </div>
      </div>
      }
      <div className="admin-main-sidebar-item">
        <div className={`${ADMIN_ITEM_BODY_SELECTOR} ${activeTab === TABS.STUDENTS ? 'active' : ''}`}
             onClick={() => {
               changeTab(TABS.STUDENTS)
             }}>
          <span className="admin-main-sidebar-name">STUDENTS</span>
          <span className="admin-main-sidebar-count green">{counts.students}</span>
          <span className="admin-main-sidebar-add">
              <img src={icons.iconPlus} alt="iconPlus"
                   onClick={showNewStudentModal}/>
          </span>
        </div>
      </div>
      <div className="admin-main-sidebar-item">
        <div className={`${ADMIN_ITEM_BODY_SELECTOR} ${activeTab === TABS.SUBJECTS ? 'active' : ''}`}
             onClick={openSubHandler}>
          <span className="admin-main-sidebar-name">SUBJECT</span>
          <span className="admin-main-sidebar-count">{subject.length}</span>
          {auth.user.role === ROLES.TEACHER &&
            <span className="admin-main-sidebar-add"><img src={icons.iconPlus} alt=""
                                                          onClick={showNewSubjectModal}/></span>
          }
        </div>
        <div className="admin-main-sidebar-sub">
          <div className={ADMIN_SUB_HEADER_SELECTOR} onClick={openSubListHandler}>
            <p>
              <img src={icons.iconFolder} alt=""/>
              Latest
            </p>
            <img src={icons.iconDropDown} alt=""/>
          </div>
          <div className="admin-main-sidebar-sub-body">
            {subject.filter(item => !!!item.archievedAt).map(item =>
              <div key={item.id}>
                <p onClick={() => {
                     changeTab(TABS.SUBJECTS, item)
                   }}
                   className={chosenSubject && item.id === chosenSubject.id ? 'active' : ''}
                >
                  {item.name}
                </p>
                <span>
                    <Icon name="edit-dark" onClick={() => {
                      updateSubject(item);
                    }}/>
                    <Icon name="delete-dark" onClick={() => {
                      archiveSubject(item.id)
                    }}/>
                  </span>
              </div>
            )
            }
          </div>
          <div className={ADMIN_SUB_HEADER_SELECTOR} onClick={openSubListHandler}>
            <p>
              <img src={icons.iconFolder} alt=""/>
              Archive
            </p>
            <img src={icons.iconDropDown} alt=""/>
          </div>
          <div className="admin-main-sidebar-sub-body">
            {subject.filter(item => !!item.archievedAt).map(item =>
              <div key={item.id}>
                <p onClick={() => {
                     changeTab(TABS.SUBJECTS, item)
                   }}
                   className={chosenSubject && item.id === chosenSubject.id ? 'active' : ''}
                >
                  {item.name}
                </p>
              </div>
            )
            }
          </div>
        </div>
      </div>
    </div>
  );
};

MainSidebar.propTypes = {
  showNewSubjectModal: PropTypes.func,
  showNewStudentModal: PropTypes.func,
  showNewTeacherModal: PropTypes.func
};

MainSidebar.defaultProps = {
  subject: [],
  chosenSubject: {}
};

const mapStateToProps = ({admin: {subject, members}, auth, modals}) => ({subject, members, auth, modals});

const mapDispatchToProps = dispatch => ({
    showModal: (modalType, titles, action) => {dispatch(actions.showModal(modalType, titles, action))}
});

export default connect(mapStateToProps, mapDispatchToProps)(MainSidebar);
