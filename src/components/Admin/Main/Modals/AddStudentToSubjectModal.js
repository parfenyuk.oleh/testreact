import React from 'react';
import Modal from '../../../UI/Modal/Modal';
import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Button from "../../../UI/Elements/Button";
import MemberTable from "../../../Admin/GroupMembers/MemberTable";
import SearchBar from "../../../Admin/GroupMembers/SearchBar";
import {compose} from "recompose";
import {withInfiniteScroll} from "../../../../hoc/withInfiniteScroll";
import ROLES from "../../../../constants/roles";


const AddStudentToSubjectModal = ({
                            show,
                            hideModal,
                            checkedItems,
                            handleInputChange,
                            list,
                            handleChange,
                            saved,
                            admin,
                            take,
                            skip,
                            onPaginated,
                            onSearch
                        }) => {

    return (
        <Modal show={show} hideModal={hideModal} className="modal-member">
            <ModalHeader>
                <h3 className="modal-title">Add new student</h3>
            </ModalHeader>
            <ModalBody overflow={'scroll'}>
                <ModalRow>
                    <SearchBar handleChange={(e) => {onSearch(e, ROLES.STUDENT)}}/>
                    <div className="table-block">
                        <AdvancedList
                            checkedItems={checkedItems}
                            handleInputChange={(e) => handleInputChange(e, 'students')}
                            list={list}
                            lengthArray={list.length}
                            isLoading={admin.loading}
                            take={take}
                            skip={skip}
                            onPaginated={() => {onPaginated('students')}}
                        />
                    </div>
                </ModalRow>
                <ModalRow button>
                    <Button
                        type="button"
                        size="xl"
                        onClick={() => saved('students')}
                    >
                        Save
                    </Button>
                </ModalRow>
            </ModalBody>
        </Modal>
    );
};

const AdvancedList = compose(
    withInfiniteScroll,
)(MemberTable);

AddStudentToSubjectModal.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};

export default AddStudentToSubjectModal;
