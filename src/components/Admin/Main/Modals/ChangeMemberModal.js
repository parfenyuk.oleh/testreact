import React,{Component} from 'react';
import Modal from '../../../UI/Modal/Modal';

import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Input from '../../../UI/Input';
import validate from "../../../../helpers/validator";
import icons from "../../../../helpers/iconsLoader";
import ROLES from "../../../../constants/roles";

class ChangeMemberModal extends Component {

  state = {
    email: {
      value: '',
      error: false
    },
    firstName: {
      value: '',
      error: false
    },
    lastName: {
      value: '',
      error: false
    },
    password: {
      value: '',
      error: false
    },
    confirm: {
      value: '',
      error: false
    },
    errors: []
  };

  saveHandler = () => {
    const {email, password, confirm, firstName, lastName} = this.state;
    const errors = [];

    email.error = validate('email',email.value,{isEmail: true});
    confirm.error = password.value.length !== 0 ? validate('password',password.value,{minLength: 6}) : '';
    password.error = validate('confirm', confirm.value,{isEqual: password.value});
    firstName.error = validate('First name', firstName.value, {isNotEmpty : true, maxLength: 50});
    lastName.error = validate('Last name', lastName.value, {isNotEmpty : true, maxLength: 50});

    if(email.error) {errors.push(email.error);}
    if(confirm.error) {errors.push(confirm.error);}
    if(password.error) {errors.push(password.error);}
    if(firstName.error) {errors.push(firstName.error);}
    if(lastName.error) {errors.push(lastName.error);}

    if(!errors.length){
      const {user} = this.props;
        user.email = email.value;
        user.firstName = firstName.value;
        user.lastName = lastName.value;
        user.newPassword = password.value;
        this.props.saved(user);
    }

    this.setState({
      email,
      password,
      confirm,
      errors});

  };

  fieldUpdated = (value, field) => {
    let state = {...this.state};
    state[field].value = value;
    this.setState({...state});
  };

  componentDidUpdate (prevProps){
    if(this.props.user.length && !prevProps.user.id || prevProps.user.id !== this.props.user.id){
      this.setState({
        email: {
          value: this.props.user.email,
          error: false
        },
        firstName: {
          value: this.props.user.firstName,
          error: false
        },
        lastName: {
          value: this.props.user.lastName,
          error: false
        },
      })
    }
  }

  render() {
    const {show, hideModal, user} = this.props;
    const {errors, email, password, confirm, firstName, lastName} = this.state;
    return (
      <Modal show={show} hideModal={hideModal}>
        <ModalHeader>
          <h3 className="modal-title">{user.role === ROLES.STUDENT ? 'Edit Student' :
            user.role === ROLES.ADMIN ? 'Edit Admin' : 'Edit Teacher'}</h3>
        </ModalHeader>
        <ModalBody bigPadding overflow={'scroll'}>
          {errors.length > 0 && <div className="auth-errors">
            <ul>
              {errors.map(item => <li><img src={icons.iconAttention} alt=""/> {item} </li>)}
            </ul>
          </div>
          }
          <ModalRow>
            <Input placeholder="First name"
                   changed={(e) => { this.fieldUpdated(e.target.value, 'firstName') }}
                   value={firstName.value}
                   className={firstName.error ? 'error' : ''}
            />
            <Input placeholder="Last name"
                   changed={(e) => { this.fieldUpdated(e.target.value, 'lastName') }}
                   value={lastName.value}
                   className={lastName.error ? 'error' : ''}
            />
            <Input placeholder="Email"
                   changed={(e) => { this.fieldUpdated(e.target.value, 'email') }}
                   className={email.error ? 'error' : ''}
                   value={email.value}
            />
            <Input placeholder="New Password"
                   type={'password'}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'password') }}
                   className={password.error ? 'error' : ''}
                   value={password.value}
            />
            <Input placeholder="Confirm new password"
                   type={'password'}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'confirm') }}
                   className={confirm.error ? 'error' : ''}
                   value={confirm.value}
            />
          </ModalRow>
          <ModalRow button>
            <button className="big-primary" onClick={this.saveHandler}>SAVE</button>
          </ModalRow>
        </ModalBody>
      </Modal>
    );
  };
}

ChangeMemberModal.propTypes = {
  show: PropTypes.bool,
  hideModal: PropTypes.func,
  fieldUpdated: PropTypes.func,
  saved: PropTypes.func,
  title: PropTypes.string,
  user: PropTypes.object
};

ChangeMemberModal.defaultProps = {
  show: true,
  hideModal: () => {},
  saved: () => {},
  user: {}
};

export default ChangeMemberModal;
