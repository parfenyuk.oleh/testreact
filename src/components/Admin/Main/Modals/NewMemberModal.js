import React,{Component} from 'react';
import Modal from '../../../UI/Modal/Modal';

import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Input from '../../../UI/Input';
import validate from "../../../../helpers/validator";
import icons from "../../../../helpers/iconsLoader";

class NewMemberModal extends Component {

  state = {
    email: {
      value: '',
      error: false
    },
    firstName: {
      value: '',
      error: false
    },
    lastName: {
      value: '',
      error: false
    },
    password: {
      value: '',
      error: false
    },
    confirm: {
      value: '',
      error: false
    },
    errors: []
  };

  saveHandler = () => {
    const {email, password, confirm, firstName, lastName} = this.state;
    const errors = [];

    email.error = validate('email',email.value,{isEmail: true});
    password.error =  validate('password',password.value,{minLength: 6});
    confirm.error = validate('confirm', confirm.value,{isEqual: password.value});
    firstName.error = validate('First name', firstName.value, {isNotEmpty : true, maxLength: 50});
    lastName.error = validate('Last name', lastName.value, {isNotEmpty : true, maxLength: 50});

    if(email.error) {errors.push(email.error);}
    if(confirm.error) {errors.push(confirm.error);}
    if(password.error) {errors.push(password.error);}
    if(firstName.error) {errors.push(firstName.error);}
    if(lastName.error) {errors.push(lastName.error);}

    if(!errors.length){
      this.props.saved({
        email : email.value,
        password : password.value,
        firstName : firstName.value ,
        lastName : lastName.value
      })
    }

    this.setState({
      email,
      password,
      confirm,
      errors});

  };

  fieldUpdated = (value, field) => {
    let state = {...this.state};
    state[field].value = value;
    this.setState({...state});
  };

  render() {
    const {show, hideModal, title} = this.props;
    const {errors, email, password, confirm, firstName, lastName} = this.state;
    return (
      <Modal show={show} hideModal={hideModal}>
        <ModalHeader>
          <h3 className="modal-title">{title}</h3>
        </ModalHeader>
        <ModalBody bigPadding overflow={'scroll'}>
          {errors.length > 0 && <div className="auth-errors">
            <ul>
              {errors.map(item => <li><img src={icons.iconAttention} alt=""/> {item} </li>)}
            </ul>
          </div>
          }
          <ModalRow>
            <Input placeholder="First name"
                   changed={(e) => { this.fieldUpdated(e.target.value, 'firstName') }}
                   className={firstName.error ? 'error' : ''}
            />
            <Input placeholder="Last name"
                   changed={(e) => { this.fieldUpdated(e.target.value, 'lastName') }}
                   className={lastName.error ? 'error' : ''}
            />
            <Input placeholder="Email"
                   changed={(e) => { this.fieldUpdated(e.target.value, 'email') }}
                   className={email.error ? 'error' : ''}
            />
            <Input placeholder="Password"
                   type={'password'}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'password') }}
                   className={password.error ? 'error' : ''}
            />
            <Input placeholder="Confirm password"
                   type={'password'}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'confirm') }}
                   className={confirm.error ? 'error' : ''}
            />
          </ModalRow>
          <ModalRow button>
            <button className="big-primary" onClick={this.saveHandler}>SAVE</button>
          </ModalRow>
        </ModalBody>
      </Modal>
    );
  };
}

NewMemberModal.propTypes = {
  show: PropTypes.bool,
  hideModal: PropTypes.func,
  fieldUpdated: PropTypes.func,
  saved: PropTypes.func,
  title: PropTypes.string
};

NewMemberModal.defaultProps = {
  show: true,
  hideModal: () => {},
  saved: () => {},
};

export default NewMemberModal;
