import React, {Component} from 'react';
import Modal from '../../../UI/Modal/Modal';

import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Input from '../../../UI/Input';
import Textarea from '../../../UI/Textarea';
import DatePicker from "../../../UI/Datepicker";
import Cell from "../../../UI/Cell";
import validate from "../../../../helpers/validator";
import icons from "../../../../helpers/iconsLoader";

class UpdateSubjectModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: {
        value: '',
        error: false
      },
      description: {
        value: '',
        error: false
      },
      startDate: {
        value: '',
        error: false
      },
      endDate: {
        value: '',
        error: false
      },
      errors: []
    }
  }

  fieldUpdate = (value, name) => {
    const subject = {...this.state};
    subject[name].value = value;
    this.setState(subject);
  };

  saveHandler = () => {
    const {name, description, startDate, endDate} = this.state;
    const errors = [];

    name.error = validate('Subject name', name.value, {isNotEmpty: true});
    description.error = validate('Description', description.value, {isNotEmpty: true});
    startDate.error = validate('Start Date', startDate.value, {isNotEmptyDate : true});
    endDate.error = validate('End Date', endDate.value, {isNotEmptyDate : true, notLessDate: startDate.value});

    if (name.error) {errors.push(name.error)}
    if (description.error) {errors.push(description.error)}
    if (startDate.error) {errors.push(startDate.error)}
    if (endDate.error) {errors.push(endDate.error)}


    if (!errors.length) {
      this.props.saved({
        ...this.state,
        name: name.value,
        description: description.value,
        startDate: startDate.value.format('l'),
        endDate: endDate.value.format('l'),
      })
    }

    this.setState({
      name,
      description,
      startDate,
      endDate,
      errors
    })
  };

  render() {
    const {show, hideModal} = this.props;
    const {errors, name, description} = this.state;
    return (
      <Modal show={show} hideModal={hideModal}>
        <ModalHeader>
          <h3 className="modal-title">New Subject</h3>
        </ModalHeader>
        <ModalBody bigPadding overflow={false}>
          {errors.length > 0 && <div className="auth-errors">
            <ul>
              {errors.map(item => <li key={item}><img src={icons.iconAttention} alt=""/> {item} </li>)}
            </ul>
          </div>
          }
          <ModalRow>
            <Input placeholder="Subject"
                   defaultValue={name.value}
                   changed={(e) => {this.fieldUpdate(e.target.value, 'name')}}
                   className={name.error ? 'error' : ''}
            />

            <Textarea placeholder="Description"
                      text={description.value}
                      changed={(e) => {this.fieldUpdate(e.target.value, 'description') }}
                      error={description.error}
            />
          </ModalRow>
          <ModalRow flex>
            <Cell column={7}>
              <DatePicker placeholder={'Start date'}
                          changed={(value) => {this.fieldUpdate(value, 'startDate')}}
                          selected={this.state.startDate.value}
              />
            </Cell>
            <Cell column={2}/>
            <Cell column={7}>
              <DatePicker placeholder={'End date'}
                          changed={(value) => {this.fieldUpdate(value, 'endDate')}}
                          selected={this.state.endDate.value}
              />
            </Cell>
          </ModalRow>
          <ModalRow button>
            <button className="big-primary" onClick={this.saveHandler}>SAVE</button>
          </ModalRow>
        </ModalBody>
      </Modal>
    );
  }
}

UpdateSubjectModal.propTypes = {
  show: PropTypes.bool,
  hideModal: PropTypes.func,
  fieldUpdated: PropTypes.func,
  saved: PropTypes.func
};

export default UpdateSubjectModal;
