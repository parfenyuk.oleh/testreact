import React from 'react';
import Card from '../../../UI/Card/Card';
import CardFooter from '../../../UI/Card/CardFooter';
import icons from '../../../../helpers/iconsLoader';
import CardBody from "../../../UI/Card/CardBody";
import RoundAvatar from "../../../UI/RoundAvatar";
import ROLES from '../../../../constants/roles';

import {withInfiniteScroll} from '../../../../hoc/withInfiniteScroll';
import Button from "../../../UI/Elements/Button";
import EmptyState from "../../../UI/EmptyState";

const SubjectCards = ({list, showMemberInfo, deleteMember, showTeacherModal, showStudentModal, userRole}) => {

  const buttons = (
    <div className="button-block" style={{display: 'flex', justifyContent:'space-between', marginBottom: '12px'}}>
      <div />
      <div>
        {userRole === ROLES.ADMIN &&<Button
          type="button"
          size="md"
          className=""
          style={{marginRight: '20px'}}
          onClick={showTeacherModal}
        >
          ADD NEW TEACHER
        </Button> }
        <Button
          type="button"
          size="md"
          className=""
          style={{}}
          onClick={showStudentModal}
        >
          ADD NEW STUDENT
        </Button>
      </div>
    </div>
  );


  if(list.length === 0) {
    return (<div className="admin-cards">
               {buttons}
                <EmptyState/>
            </div>
      )
  }
  return (
    <div className="admin-cards">
      {buttons}
      {list.map(user =>
        <Card key={user.id}>
          <CardBody align={'center'} justify={'space-between'} clicked={() => {showMemberInfo(user.id)}}>
            <RoundAvatar text={`${user.firstName[0]} ${user.lastName[0]}`}
                         size="big"
                         avatar={user.avatar}
            />
            {user.role === ROLES.TEACHER && <div className="admin-cards-teacher">Teacher</div>}
            <div>
              <p className="admin-cards--name">{user.firstName} {user.lastName}</p>
              <p className="admin-cards--email">{user.email}</p>
            </div>
          </CardBody>
          <CardFooter justify="center">
            <img src={icons.iconBin} alt="" onClick={() => {deleteMember(user.id)}}/>
          </CardFooter>
      </Card>
      )}

    </div>
  );
};

SubjectCards.defaultProps = {
  users: []
};

export default withInfiniteScroll(SubjectCards);
