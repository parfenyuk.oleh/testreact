import React from 'react';

import icons from '../../../../helpers/iconsLoader';
import TableHeader from "../../../UI/Table/TableHeader";
import Cell from "../../../UI/Cell";
import TableRow from "../../../UI/Table/TableRow";
import AvatarBlock from "../../../UI/Elements/AvatarBlock";
import Status from "../../../UI/Status";
import {withInfiniteScroll} from '../../../../hoc/withInfiniteScroll';

const TeacherTable = ({list, changeMemberHandler, deleteMemberHandler}) => {
  return (
    <div className='admin-table'>
      <TableHeader>
        <Cell column={2}>ID</Cell>
        <Cell column={4}>Name</Cell>
        <Cell column={5}>Email</Cell>
      </TableHeader>
      {
          list.map(item => (<TableRow key={item.id}>
          <Cell column={2}>#{item.id}</Cell>
          <Cell column={4}>
            <AvatarBlock textAvatar={`${item.firstName[0]}${item.lastName[0]}`}
                         customInfo={<span>{`${item.firstName} ${item.lastName}`}</span>}
                         imgSrc={item.avatar}
            />
          </Cell>
          <Cell column={5}>
            {item.email}
          </Cell>
          <Cell column={3}>
            {/*<Status type="inactive"/>*/}
          </Cell>
          <Cell column={2}>
            <img src={icons.iconEdit} alt="edit"
                 style={{marginRight: `20px`, cursor: 'pointer'}}
                 onClick={() => {changeMemberHandler(item)}}
            />
            <img src={icons.iconBin}
                 style={{cursor: 'pointer'}}
                 alt="delete"
                 onClick={() => {deleteMemberHandler(item)}}
            />
          </Cell>
        </TableRow>))
      }
    </div>
  );
};


export default withInfiniteScroll(TeacherTable);
