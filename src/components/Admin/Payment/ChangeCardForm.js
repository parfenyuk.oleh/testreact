import React, {Component} from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import Button from '../../UI/Elements/Button';
import {userAuthToken} from "../../../helpers";
import axios from '../../../axios-instance';
import Swal from 'sweetalert2';
import connect from "react-redux/es/connect/connect";
import {compose} from "recompose";
import {
    hideLoader,
    showLoader
} from "../../../store/actions";

class CheckoutForm extends Component {
    constructor(props) {
        super(props);
        this.submit = this.submit.bind(this);
    }

    async submit(ev) {
        let {token} = await this.props.stripe.createToken({name: 'Name'});

        if(token) {
            this.props.showLoader();
            let response = await axios.put(`/Payment/CreditCard`,
                {creditCardToken: token.id},
                { headers: { Authorization: userAuthToken.token() }});

            if (response.data) {
                this.props.hideLoader();

                if(response.data.ok) {
                    Swal({
                        title: 'Done',
                        text: 'Card successfully updated',
                        type: 'success',
                        timer: 3000
                    });
                } else {
                    Swal({
                        title: 'Ooops...',
                        text: response.data.message,
                        type: 'error',
                        timer: 3000
                    });
                }
            }
        }
    }


    render() {
        return (
            <div className='checkout'>
                <CardElement/>
                <Button
                    type='button'
                    size='xl'
                    onClick={this.submit}
                    style={{width: 145}}
                >
                    Change card
                </Button>
                {
                    this.props.withCancel && (<Button
                        type="button"
                        size="sm"
                        context="secondary"
                        onClick={this.props.changeCard}
                        style={{position: 'absolute', right: -165}}
                    >
                        Cancel
                    </Button>)
                }
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
});

export default compose(
    injectStripe,
    connect(null, mapDispatchToProps)
)(CheckoutForm);