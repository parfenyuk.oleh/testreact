import React from 'react';
import Grid from '@material-ui/core/Grid/Grid';
import Button from '../../UI/Elements/Button';
import {Elements, StripeProvider} from 'react-stripe-elements';
import Icon from '../../UI/Elements/Icon';
import ChangeCardForm from '../Payment/ChangeCardForm';

export const ChangeCardTab = ({changeCard, changeCardMode, last4}) => {
  return <div className='list'>
    <StripeProvider apiKey='pk_test_954goH66HjXeZOC6Wlss914s'>
      <Grid item className='stripe-form big'>
        {
          changeCardMode ?
            <Elements>
              <ChangeCardForm withCancel changeCard={changeCard}/>
            </Elements>
            :
            <div className='checkout'>
              <div className='StripeElement layout' style={{width: '100%', marginRight: 20}}>
                <div className='__PrivateStripeElement'
                     style={{
                       margin: '0px !important',
                       padding: '0px !important',
                       border: 'none !important',
                       display: 'block !important',
                       background: 'transparent !important',
                       position: 'relative !important',
                       opacity: '1 !important',
                     }}>
                  <div className='hide-wrapper'>
                    <Icon name='credit-card'/>
                    <span>
                                **** - **** - **** - {last4}
                            </span>
                    <span>
                                ****
                            </span>
                    <span style={{marginRight: 0}}>
                                ***
                            </span>
                  </div>
                </div>
              </div>
              <Button onClick={changeCard}
                      style={{width: 145}}
              >
                Change card
              </Button>
            </div>
        }
        <p className='terms'>
          There are times when a payment has not been successfully
          processed when entered, this may be because the transaction
          was interrupted for some reason, perhaps your internet browser
          timed out before it had completed.
          <br/><br/>
          The first thing to try would be to log out of the Online Account Management site,
          sign in and try again. If this still doesn't work, either try again at a later stage
          or call us on 1234 1234 1234. Calls may be recorded. Calls charged at basic rate.
          The number provided may be included as part of any inclusive call minutes provided by
          your phone operator.
          <br/>
        </p>
      </Grid>
    </StripeProvider>
  </div>
};