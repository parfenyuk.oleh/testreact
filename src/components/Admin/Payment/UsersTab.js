import React from 'react';
import '../../../style/admin/UsersTab/index.sass';
import Icon from '../../../components/UI/Elements/Icon';
import Select from '../../../components/UI/Select';
import {PLANS} from '../../../constants/plans';
import AvatarBlock from '../../../components/UI/Elements/AvatarBlock';
import TableHeader from '../../UI/Table/Table';
import Cell from "../../UI/Cell";
import TableRow from "../../UI/Table/TableRow";

class UsersTab extends React.Component  {
    state = {
        users: [
            {
                'id': 1,
                'firstName': 'Angu',
                'lastName': 'Tamba',
                'role': 'Student',
                'active': true
            },
            {
                'id': 2,
                'firstName': 'Anton',
                'lastName': 'Brownstein',
                'role': 'Teacher',
                'active': false
            },
            {
                'id': 3,
                'firstName': 'Angu',
                'lastName': 'Tamba',
                'role': 'Student',
                'active': true
            },
            {
                'id': 4,
                'firstName': 'Angu',
                'lastName': 'Tamba',
                'role': 'Teacher',
                'active': true
            },
            {
                'id': 5,
                'firstName': 'Angu',
                'lastName': 'Tamba',
                'role': 'Student',
                'active': false
            },
            {
                'id': 6,
                'firstName': 'Angu',
                'lastName': 'Tamba',
                'role': 'Teacher',
                'active': true
            },
            {
                'id': 7,
                'firstName': 'Angu',
                'lastName': 'Tamba',
                'role': 'Student',
                'active': true
            },
            {
                'id': 8,
                'firstName': 'Angu',
                'lastName': 'Tamba',
                'role': 'Admin',
                'active': false
            },
            {
                'id': 9,
                'firstName': 'Angu',
                'lastName': 'Tamba',
                'role': 'Student',
                'active': true
            },
        ],
        selectedUsers: [
            {   
                id: 1,
                name: 'All'
            },
            {
                id: 2,
                name: 'Students'
            },
            {
                id: 3,
                name: 'Teachers'
            },
            {
                id: 4,
                name: 'Admins'
            }
        ],
        activePlan: '',
    };

    switchPlanHandler = activePlan => this.setState({activePlan});

    render() {
        const {activePlan} = this.state;

        return (
            <div className="users-tab">
                <div className="pricing-plans">
                    <h2>Pricing plans</h2>
                    <div>
                        <div 
                            className={`plan-item default ${activePlan === PLANS.medium || activePlan === PLANS.large ? 'not-active' : ''}`}
                            onClick={this.switchPlanHandler.bind(this, PLANS.small)}
                        >
                            <div className="plan-name">
                                <input
                                    type="radio" 
                                    name="plan" 
                                    value="small" 
                                    id="small"
                                /> 
                                <label htmlFor="small"> Small</label>
                                <span><b>1 - 15 000</b> Users, <b>$3.00</b> User fees p/m, <b>$3,500.00</b> Montly Management Fee</span>
                            </div>
                            <span className="price">$5000</span>
                        </div>
                        <div 
                            className={`plan-item ${activePlan === PLANS.medium ? 'active' : ''}`}
                            onClick={this.switchPlanHandler.bind(this, PLANS.medium)}
                        >
                            <div className="plan-name">
                                <input 
                                    type="radio" 
                                    name="plan" 
                                    value="medium" 
                                    id="medium"
                                /> 
                                <label htmlFor="medium"> Medium</label>
                                <span><b>15 001 - 30 000</b> Users, <b>$3.00</b> User fees p/m, <b>$3,500.00</b> Montly Management Fee</span>
                            </div>
                            <span className="price">$15 000</span>
                        </div>
                        <div 
                            className={`plan-item ${activePlan === PLANS.large ? 'active' : ''}`}
                            onClick={this.switchPlanHandler.bind(this, PLANS.large)}
                        >
                            <div className="plan-name">
                                <input 
                                    type="radio" 
                                    name="plan" 
                                    value="large" 
                                    id="large"
                                /> 
                                <label htmlFor="large"> Large</label>
                                <span><b>30 000+</b> Users, <b>$3.00</b> User fees p/m, <b>$3,500.00</b> Montly Management Fee</span>
                            </div>
                            <span className="price">$25 000</span>
                        </div>
                    </div>
    
                    <button className={`btn change-plan ${activePlan === PLANS.medium || activePlan === PLANS.large ? 'active' : ''}`}>change plan</button>
                </div>
                <div className="users-table">
                    <h2>Users 13 999 <span>/15 000</span></h2>
                    <div>
                        <div className="users-top">
                            <div className="search">
                                <Icon name={'search'} />
                                <input type="search" placeholder="Search" />
                            </div>
                            <Select 
                                placeholder={'Select user'} 
                                options={this.state.selectedUsers}
                            />
                        </div>

                        <div className='payment-table'>
                            <TableHeader className="head">
                                <Cell column={6}>Name</Cell>
                                <Cell column={3}>Role</Cell>
                                <Cell column={3}>Status</Cell>
                                <Cell column={3}>&nbsp;</Cell>
                            </TableHeader>
                            {
                                this.state.users.map((item) => (
                                    <TableRow key={item.id}>
                                        <Cell 
                                            className="name"
                                            column={6}
                                        >
                                            <AvatarBlock
                                                size='sm'
                                                imgSrc=''
                                                textAvatar={`${item.firstName[0]}${item.lastName[0]}`}
                                            >
                                            </AvatarBlock>
                                            {`${item.firstName} ${item.lastName}`}
                                        </Cell>
                                        <Cell 
                                            className="role"
                                            column={3}
                                        >
                                            {item.role}
                                        </Cell>
                                        <Cell 
                                            className={item.active ? 'active' : 'deactivated'}
                                            column={3}
                                        >
                                            {item.active ? 'Active' : 'Deactivated'}
                                        </Cell>
                                        <Cell 
                                            className="btn-col"
                                            column={4}
                                        >
                                            <button className={item.active ? 'btn-active' : 'btn-deactivated'}>
                                                {item.active ? 'Deactivate' : 'Activate'}
                                            </button>
                                        </Cell>
                                    </TableRow> 
                                ))
                            }
                        </div>
                    </div>
                </div>
            </div>
        ); 
    }
};

export default UsersTab;