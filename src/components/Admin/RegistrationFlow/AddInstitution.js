import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import Input from '../../UI/Input';
import ImageLoader from '../../UI/imageLoader';
import {Elements} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import Grid from '@material-ui/core/Grid/Grid';
import Button from '../../UI/Elements/Button';
import icons from '../../../helpers/iconsLoader';

const AddInstitution = ({
                            colorClickHandler,
                            inputChangeHandler,
                            handleInputChange,
                            saveHandler,
                            colors,
                            name,
                            abbreviation,
                            avatar,
                            color,
                            errors
}) => (
    <Fragment>
        <Grid item className='add-institution'>
            <p className='header'>
                Dashboard Style:
            </p>
            {
                errors.length > 0 &&
                    <div className='auth-errors'>
                        <ul>
                            {errors.map((item, key) => <li key={key}> <img src={icons.iconAttention} alt=''/> {item} </li>)}
                        </ul>
                    </div>
            }
            <div className='content'>
                <Input placeholder='Institution name' 
                       defaultValue={name}
                       changed={handleInputChange} 
                       name='name'
                />
                <Input placeholder='Abbreviation'
                       defaultValue={abbreviation}
                       changed={handleInputChange}
                       name='abbreviation'
                />
                <p className='modal-label'>Select dashboard style</p>
                {colors.map(item => (<div className={`colorPickerColor ${color === item ? 'active' : ''}`}
                                          style={{backgroundColor: item}}
                                          key={item}
                                          onClick={() => {
                                              colorClickHandler(item)
                                          }}/>))}
                <Grid container justify='center' className='loader-photo'>
                    <p className='modal-label'>Upload logo:</p>
                    <ImageLoader placeholder={{
                        image: avatar
                    }}
                                 trigger={<button className='button-default small'>UPLOAD NEW</button>}
                                 changed={(image) => {
                                     inputChangeHandler(image, 'avatar')
                                 }}
                    />
                </Grid>
            </div>
            <Grid container justify='center'>
                <Button
                    type='button'
                    size='xl'
                    onClick={saveHandler}
                    style={{width: 365}}
                >
                    SAVE AND CONTINUE
                </Button>
            </Grid>
        </Grid>
    </Fragment>
);

AddInstitution.defaultProps = {
    colors: ['#6EA6E8', '#74C283', '#F9D949', '#F59540', '#F66C6C', '#F08BB9', '#696CD4', '#6D6D6D', '#F5F5F5'],
};

AddInstitution.propTypes = {
    typeInstitutionArray: PropTypes.array,
    institutionActive: PropTypes.number,
    changeActiveInstitution: PropTypes.func,
    goNext: PropTypes.func,
};

export default AddInstitution;