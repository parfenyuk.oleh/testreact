import React, {Component} from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm from './CheckoutForm';
import Grid from '@material-ui/core/Grid/Grid';

class AddPayment extends Component {
    render() {
        return (
            <StripeProvider apiKey='pk_test_954goH66HjXeZOC6Wlss914s'>
                <Grid item className='stripe-form'>
                    <p className='header'>
                        Card data:
                    </p>
                    <Elements>
                        <CheckoutForm goNext={this.props.goNext} setCardTokenId={this.props.setCardTokenId}/>
                    </Elements>
                    <span className='back' onClick={this.props.goNext}>
                        Skip this step
                    </span>
                    <p className='terms'>
                        Processing payments with Stripe has two parts:
                        <br/>
                        <br/>
                        1. Securely collecting payment details from your customer<br/>
                        2. Using the collected payment method in a charge request<br/>
                        <br/>
                        <br/>
                        You can make use of Stripe Elements, our pre-built UI components, to create a payment
                        form that securely collects your customer’s card information without you needing
                        to handle sensitive card data.
                    </p>
                </Grid>
            </StripeProvider>
        );
    }
}

export default AddPayment;