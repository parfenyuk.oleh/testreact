import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid/Grid';
import Paper from '@material-ui/core/Paper/Paper';
import Icon from '../../UI/Elements/Icon';
import Button from '../../UI/Elements/Button';

const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: 1115,
        margin: '0 auto'
    },
    paper: {
        height: 200,
        width: 200,
        textAlign: 'center',
        color: '#B3C0CE',
        fontSize: 20,
        cursor: 'pointer',
        boxShadow: '0 1px 2px 0 rgba(0,37,59,0.09)',
    },
    paperActive: {
        background: '#0093FF',
        color: '#fff',
    },
    control: {
        padding: theme.spacing.unit * 2,
    },
    progressbar: {
        marginTop: 105,
        marginBottom: 70
    },
    buttonBlock: {
        paddingTop: 50,
        paddingBottom: 20,
        display: 'flex',
        justifyContent: 'flex-end'
    }
});

const ChooseInstitution = ({ typeInstitutionArray, institutionActive, changeActiveInstitution, goNext, classes }) => (
    <Fragment>
        <Grid item xs={12}>
            <Grid container justify='center' spacing={Number(24)}>
                {typeInstitutionArray.map(item => (
                    <Grid key={item.id} item>
                        <div onClick={() => {changeActiveInstitution(item.id)}}>
                            <Paper className={item.id === institutionActive ?
                                `${classes.paper} ${classes.paperActive}` :
                                `${classes.paper}`}
                            >
                                <div style={{paddingTop: 15}}>
                                    <Icon name={item.imgName}
                                          color={item.id === institutionActive ? '#FFFFFF' : '#B3C0CE'}/>
                                </div>
                                <div>
                                    {item.name}
                                </div>
                            </Paper>
                        </div>
                    </Grid>
                ))}
            </Grid>
        </Grid>
        <Grid item xs={12}>
            <div className={classes.buttonBlock}>
                <Button
                    type='button'
                    size='lg'
                    onClick={goNext}
                >
                    Next
                </Button>
            </div>
        </Grid>
    </Fragment>
);

ChooseInstitution.propTypes = {
    typeInstitutionArray: PropTypes.array,
    institutionActive: PropTypes.number,
    changeActiveInstitution: PropTypes.func,
    goNext: PropTypes.func,
};

export default withStyles(styles)(ChooseInstitution);