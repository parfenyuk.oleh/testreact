import React from 'react';
import Button from '../../UI/Elements/Button';

const data = [
  {
    small: {
      users: '1-15000',
      fee: '5000',
      per: '3',
      monthly: '3500'
    },
    medium: {
      users: '15001 - 30000',
      fee: '15000',
      per: '3',
      monthly: '3500'
    },
    large: {
      users: '30000+',
      fee: '25000',
      per: '3',
      monthly: '3500'
    },
  },
  {
    small: {
      users: '1-5000',
      fee: '2500',
      per: '3',
      monthly: '3500'
    },
    medium: {
      users: '5001 - 20000',
      fee: '5000',
      per: '3',
      monthly: '3500'
    },
    large: {
      users: '25000+',
      fee: '8000',
      per: '3',
      monthly: '3500'
    },
  },
  {
    small: {
      users: '1-500',
      fee: '1500',
      per: '3',
      monthly: '3500'
    },
    medium: {
      users: '501 - 1000',
      fee: '2500',
      per: '3',
      monthly: '3500'
    },
    large: {
      users: '1000+',
      fee: '5000',
      per: '3',
      monthly: '3500'
    },
  },
  {
    small: {
      users: '1-50',
      fee: '0',
      per: '3',
      monthly: '0'
    },
    medium: {
      users: '51 - 100',
      fee: '0',
      per: '3',
      monthly: '0'
    },
    large: {
      users: '101-200',
      fee: '0',
      per: '3',
      monthly: '0'
    },
  },
  {
    small: {
      users: '1-2500',
      fee: '10000',
      per: '3',
      monthly: '3500'
    },
    medium: {
      users: '2501 - 5000',
      fee: '20000',
      per: '3',
      monthly: '3500'
    },
    large: {
      users: '5000+',
      fee: '30000',
      per: '3',
      monthly: '3500'
    },
  },
];


const PaymentRules = ({typeInstitutionArray, institutionType, changeActiveInstitution, goNext, goPrev, setType}) => {


  function setInst(type) {
    setType(type);
    goNext();
  }

  institutionType--;



  return (
    <div className="payments-types">
      <div className="payments-type">
        <h2 className="payments-type-name">Small</h2>
        <p className="payments-type-price">${data[institutionType].small.fee}</p>
        <ul>
          <li className="payments-type-detail"><span>${data[institutionType].small.users}</span> Users</li>
          <li className="payments-type-detail"><span>${data[institutionType].small.per}</span> User fees p/m</li>
          <li className="payments-type-detail"><span>${data[institutionType].small.monthly}</span> Monthly Management Fee</li>
        </ul>
        <Button
          type='button'
          size='xl'
          onClick={() => {setInst(1)}}
        >
          START 14-DAY FREE TRIAL*
        </Button>
      </div>
      <div className="payments-type">
        <h2 className="payments-type-name">Medium</h2>
        <p className="payments-type-price">${data[institutionType].medium.fee}</p>
        <ul>
          <li className="payments-type-detail"><span>{data[institutionType].medium.users}</span> Users</li>
          <li className="payments-type-detail"><span>${data[institutionType].medium.per}</span> User fees p/m</li>
          <li className="payments-type-detail"><span>${data[institutionType].medium.monthly}</span> Monthly Management Fee</li>
        </ul>
        <Button
          type='button'
          size='xl'
          onClick={() => {setInst(2)}}
        >
          START 14-DAY FREE TRIAL*
        </Button>
      </div>
      <div className="payments-type">
        <h2 className="payments-type-name">Large</h2>
        <p className="payments-type-price">${data[institutionType].large.fee}</p>
        <ul>
          <li className="payments-type-detail"><span>${data[institutionType].large.users}</span> Users</li>
          <li className="payments-type-detail"><span>${data[institutionType].large.per}</span> User fees p/m</li>
          <li className="payments-type-detail"><span>${data[institutionType].large.monthly}</span> Monthly Management Fee</li>
        </ul>
        <Button
          type='button'
          size='xl'
          onClick={() => {setInst(3)}}
        >
          START 14-DAY FREE TRIAL*
        </Button>
      </div>
      <p className='terms'>
        * You won't be charged during your free trial, and you can cancel anytime.
        We'll remind you when your free trial is about to end, so you can cancel
        your account if you'd rather not become a Reelae member. If you choose to continue enjoying
        Reelae after your trial ends, you’ll be automatically charged one monthly payment.
      </p>
    </div>
  );
};

PaymentRules.propTypes = {};

export default PaymentRules;


{/*<Grid container justify='center'>*/}
{/*<p className='header'>*/}
{/*Payment rules per month:*/}
{/*</p>*/}
{/*</Grid>*/}
{/*<Grid container justify='center'>*/}
{/*<ul>*/}
{/*<li><span>3.00 AUD</span> per student</li>*/}
{/*<li><span>5.00 AUD</span> per teacher</li>*/}
{/*<li><span>8.00 AUD</span> per admin</li>*/}
{/*</ul>*/}
{/*</Grid>*/}
{/*<Grid container justify='center'>*/}
{/*<Button*/}
{/*type='button'*/}
{/*size='xl'*/}
{/*onClick={goNext}*/}
{/*style={{width: 365}}*/}
{/*>*/}
{/*START 14-DAY FREE TRIAL **/}
{/*</Button>*/}
{/*</Grid>*/}
{/*<Grid container justify='center'>*/}
{/*<span className='back' onClick={goPrev}>*/}
{/*Change Institution*/}
{/*</span>*/}
{/*</Grid>*/}
{/*<Grid container justify='center'>*/}
{/*<p className='terms'>*/}
{/** You won't be charged during your free trial, and you can cancel anytime.*/}
{/*We'll remind you when your free trial is about to end, so you can cancel*/}
{/*your account if you'd rather not become a Reelae member. If you choose to continue enjoying*/}
{/*Reelae after your trial ends, you’ll be automatically charged one monthly payment.*/}
{/*</p>*/}
{/*</Grid>*/}