import React from 'react';
import PropTypes from 'prop-types';

const Cursor = ({top, left, visibility}) => {
  return (
    <div style={{
      position: 'absolute',
      height: '20px',
      width: '1px',
      backgroundColor: '#000',
      top: top + 'px',
      left: left + 'px',
      visibility,
    }}> </div>
  );
};

Cursor.propTypes = {
  top: PropTypes.number,
  left: PropTypes.number,
  visibility: PropTypes.string,
};

Cursor.defaultProps = {
  top: 0,
  left: 0,
  visibility: 'hidden',
};

export default Cursor;