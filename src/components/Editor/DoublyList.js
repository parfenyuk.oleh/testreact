import Node from './Node';

class DoublyList {
  constructor() {
    this._length = 0;
    this.head = null;
    this.tail = null;
  }

  add = value => {
    let node = new Node(value);
    if (this._length) {
      this.tail.next = node;
      node.previous = this.tail;
    } else {
      this.head = node;
    }
    this.tail = node;
    this._length++;

    return this;
  };

  shift = value => {
    if (this.head) {
      const nodeToInsert = new Node(value);
      nodeToInsert.next = this.head;
      this.head.previous = nodeToInsert;
      this.head = nodeToInsert;

      this._length++;
    } else {
      this.add(value);
    }

  };

  insert = (value, prevElementIndex) => {
    if (prevElementIndex === this._length) {
      this.add(value)
    } else {
      const prevNode = this.searchNodeAt(prevElementIndex);
      const nextNode = prevNode.next;

      const nodeToInsert = new Node(value);
      nodeToInsert.previous = prevNode;
      nodeToInsert.next = nextNode;

      prevNode.next = nodeToInsert;
      if (nextNode) nextNode.previous = nodeToInsert;

      this._length++;
    }

  };

  foreach = callback => {
    let currentNode = this.head;
    while (currentNode !== null) {
      callback(currentNode.data);
      currentNode = currentNode.next;
    }
  };

  map = callback => {
    const output = [];
    let currentNode = this.head;
    while (currentNode !== null) {
      output.push(callback(currentNode.data));
      currentNode = currentNode.next;
    }
    return output;
  };

  /**
   * Merge this Doubly list with other oDoubly list
   * @param otherDoublyList
   * @return this
   */
  merge = otherDoublyList => {
    // if current list has any item
    if (this.tail) {
      this.tail.next = otherDoublyList.head;
      otherDoublyList.head.previous = this.tail;
      this._length += otherDoublyList._length;
    }
    // if current list is empty
    else {
      this.head = otherDoublyList.head;
      this._length = otherDoublyList._length;
    }
    this.tail = otherDoublyList.tail;
    return this;
  };

  searchNodeAt = position => {
    let currentNode = this.head,
      length = this._length,
      count = 1,
      message = {failure: 'Failure: non-existent node in this list.'};
    // 1st use-case: an invalid position
    if (length === 0 || position < 1 || position > length) {
      throw new Error(message.failure);
    }
    // 2nd use-case: a valid position
    while (count < position) {
      currentNode = currentNode.next;
      count++;
    }
    return currentNode;
  };

  /**
   * Cut from position until tail all nodes and return new {DoublyList} with them
   * Tail in this list will be changed to Node with position @param position
   * @param position
   * @return {DoublyList}
   */
  cut = position => {

    const startNode = this.searchNodeAt(position + 1);
    const cutList = new DoublyList();


    // if we cut all List
    if (position === 0) {
      cutList.head = this.head;
      cutList.tail = this.tail;
      cutList._length = this._length;
      this.head = this.tail = null;
    }
    // if we cut part of list
    else {
      const newTail = startNode.previous;
      startNode.previous = null;
      cutList.head = startNode;
      cutList.tail = this.tail;
      cutList.recount();

      newTail.next = null;
      this.tail = newTail;
    }

    this.recount();

    return cutList;
  };

  /**
   * Recount length of list
   */
  recount = () => {
    if (!this.head) {
      this._length = 0
    } else {
      let count = 0;
      let iterationNode = this.head;
      while (iterationNode != null) {
        count++;
        iterationNode = iterationNode.next;
      }
      this._length = count;
    }
  };

  toArray = callback => {
    const output = [];
    if (this.head) {
      let iterationNode = this.head;
      while (iterationNode !== null) {
        output.push(callback(iterationNode.data));
        iterationNode = iterationNode.next;
      }
    }
    return output;
  };

  remove = position => {
    let currentNode = this.head,
      length = this._length,
      count = 1,
      message = {failure: 'Failure: non-existent node in this list.'},
      beforeNodeToDelete = null,
      nodeToDelete = null,
      deletedNode = null,
      afterNodeToDelete;
    // 1st use-case: an invalid position
    if (length === 0 || position < 1 || position > length) {
      throw new Error(message.failure);
    }
    // 2nd use-case: the first node is removed
    if (position === 1) {
      this.head = currentNode.next;
      // 2nd use-case: there is a second node
      if (this.head) {
        this.head.previous = null;
        // 2nd use-case: there is no second node
      } else {
        this.tail = null;
      }
      // 3rd use-case: the last node is removed
    } else if (position === this._length) {
      this.tail = this.tail.previous;
      this.tail.next = null;
      // 4th use-case: a middle node is removed
    } else {
      while (count < position) {
        currentNode = currentNode.next;
        count++;
      }
      beforeNodeToDelete = currentNode.previous;
      nodeToDelete = currentNode;
      afterNodeToDelete = currentNode.next;
      beforeNodeToDelete.next = afterNodeToDelete;
      afterNodeToDelete.previous = beforeNodeToDelete;
      deletedNode = nodeToDelete;
      nodeToDelete = null;
    }
    this._length--;
    return message.success;
  }

}


export default DoublyList;