import React, {Component} from 'react';
import Cursor from './Cursor';


const ACTIONS = {
  KEY_PRESS: 'KEY_PRESS',
  REMOVE_SYMBOL: 'REMOVE_SYMBOL',
  NEW_LINE: 'NEW_LINE',
  REMOVE_LINE: 'REMOVE_LINE',
};

const KEY_CODES = {
  BACKSPACE: 8,
  DELETE: 46,
  LEFT_KEY: 37,
  RIGHT_KEY: 39,
  UP_KEY: 38,
  DOWN_KEY: 40,
  ENTER: 13,
  A: 65,
};


// TODO tags to constants, 0.78 to const, split component to functions
class HtmlEditor extends Component {

  constructor(props) {
    super(props);
    this.state = {
      editorCoordinates: {
        x: 0,
        y: 0,
      },
      showControls: true,
      controlsCoordinates: {
        x: 0,
        y: 0,
      },
      controlsSizes: {
        width: 0,
        height: 0,
      },
      mouseStartSelect: 0,
      lastAction: '',
      showLink: false,
      resizeableImage: null,
      content: '',
      cursor: {
        top: 0,
        left: 0,
        visibility: 'hidden',
        line: 0,
        column: 0,
      },
      currentTarget: null,
      lastChangeAction: null,
      cursorOnStart: false,
      onHighNode: false,
      outsideAction: {
        finished: true,
        id: 0,
        type: null,
        data: null,
      },
    };
    this.inputFile = React.createRef();
    this.editor = React.createRef();
    this.area = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    let {outsideAction} = state;
    if (props.outsideAction && props.outsideAction.id !== outsideAction.id) {
      outsideAction = props.outsideAction;
    }

    return {
      content: props.content,
      outsideAction,
    }
  }

  componentDidMount() {
    const editor = this.editor.current;
    const controls = editor.querySelector('.editor-controls');
    const inModal = this.inModal();

    this.setState({
      editorCoordinates: {
        x: editor.offsetLeft + inModal.x,
        y: editor.offsetTop + inModal.y,
      },
      showControls: false,
      controlsSizes: {
        width: controls.offsetWidth,
        height: controls.offsetHeight,
      },
    })
  }


  inModal = () => {
    let element = this.editor.current.parentNode;

    while (true) {
      if (element.classList.contains('modal')) {
        return {
          x: element.offsetLeft - element.offsetWidth / 2,
          y: element.offsetTop - element.offsetHeight / 2,
        };
      }
      if (element.tagName === 'BODY') {
        return {x: 0, y: 0};
      }
      element = element.parentNode;
    }

  };

  makeBold = () => {document.execCommand('bold', false, null)};
  makeItalic = () => {document.execCommand('italic', false, null)};
  makeUnderline = () => {document.execCommand('underline', false, null)};
  makeLineThrough = () => {document.execCommand('strikeThrough', false, null)};
  addBlockQuote = () => {document.execCommand('formatBlock', false, '<blockquote>')};
  makeLink = () => {document.execCommand('createLink', false, prompt('Insert your link'))};
  makeImage = () => {
    document.execCommand('insertImage', false, 'https://www.cbronline.com/wp-content/uploads/2016/06/what-is-URL-770x503.jpg');

    /*
     *Need to add ajax where we send image and that response url
     */

  };

  makeComment = () => {
    document.execCommand('backColor', false, 'rgba(0,0,0,0.2)');
    const selectedNode = window.getSelection().focusNode.parentNode;

    selectedNode.className = 'comment';
    selectedNode.id = 'id';
  };

  mouseDownHandler = e => {
    const tagName = e.nativeEvent.target.tagName;

    if (tagName !== 'IMG' && tagName !== 'BUTTON' && tagName !== 'INPUT') {
      this.setState({mouseStartSelect: e.nativeEvent.clientX, showControls: false, lastAction: 'mouseDown'});
    }
    if (tagName === 'IMG') {
      e.nativeEvent.preventDefault();
      this.setState({resizeableImage: e.nativeEvent.target, showControls: false});
      this.initialiseResize();
    }
  };

  inputHandler = (e) => {
    if (this.state.showControls) {this.setState({showControls: false, lastAction: 'input'});}
    else {this.setState({lastAction: 'input'});}
    this.props.onChange(e);
    return false;

  };

  selectionChange = event => {
    return false;
    // CODE BELOW MAYBE WILL BE USEFUL IN THE FUTURE
    // if (this.state.lastAction === 'input') {return;}
    // if (this.state.mouseStartSelect !== event.nativeEvent.clientX) {
    //   try {// Bag fix
    //     this.controlsStateUpdate(event);
    //   } catch (e) {}
    // }
  };

  controlsStateUpdate = ({nativeEvent: {clientX, clientY, target}}) => {
    const {mouseStartSelect, editorCoordinates, controlsSizes} = this.state;
    const coordinateX = (mouseStartSelect + clientX) / 2 - editorCoordinates.x - controlsSizes.width * 0.78;
    const editor = this.editor.current.parentNode;
    const lineHeight = Number(window.getComputedStyle(target).lineHeight.replace('px', ''));
    const mousePosition = ~~((clientY - editorCoordinates.y - target.offsetTop + editor.scrollTop) / lineHeight) * lineHeight;

    this.setState({
      showControls: true,
      controlsCoordinates: {
        x: coordinateX,
        y: target.offsetTop + mousePosition - controlsSizes.height - 10,
      },
    });
  };

  initialiseResize() {
    const editor = this.editor.current;

    editor.style.height = `${editor.offsetHeight}px`;
    window.addEventListener('mousemove', this.startResizing, false);
    window.addEventListener('mouseup', this.stopResizing, false);
  }

  startResizing = e => {
    const {resizeableImage, editorCoordinates} = this.state;
    const editor = this.editor.current;

    resizeableImage.style.width = `${e.clientX - resizeableImage.offsetLeft - editorCoordinates.x}px`;
    resizeableImage.style.height = `${e.clientY - (resizeableImage.offsetTop - editor.parentNode.scrollTop) - editorCoordinates.y}px`;
  };

  stopResizing = () => {
    this.setState({resizeableImage: null, showControls: false});
    window.removeEventListener('mousemove', this.startResizing, false);
    window.removeEventListener('mouseup', this.stopResizing, false);
    this.editor.current.style.height = 'auto';
  };

  mouseMoveHandler = (e) => {
    window.getSelection().removeAllRanges();
    return false;
  };

  doubleClickHandler = e => {
    window.getSelection().removeAllRanges();
    return false;
  };

  /**
   * Find symbol order in paragraph
   * @returns {number}
   */
  getSymbolOrder = (item = null) => {
    let {currentTarget} = this.state;
    if (item) {currentTarget = item}
    let symbolOrder = 0;
    while (currentTarget !== null) {
      symbolOrder++;
      currentTarget = currentTarget.previousSibling;
    }
    return symbolOrder;
  };

  getLineOrder = (item = {}) => {
    let currentLine = item.parentNode || this.state.currentTarget.parentNode;
    let lineOrder = 0;
    while (currentLine !== null) {
      lineOrder++;
      currentLine = currentLine.previousSibling;
    }
    return lineOrder;
  };

  isCurrentNodeParagraph = () => {
    return this.state.currentTarget.tagName === 'P';
  };

  unChangeAbleText = e => {
    e.preventDefault();
    if (e.charCode === KEY_CODES.ENTER) { return; }

    const {currentTarget} = this.state;
    if (currentTarget) {
      this.setState({lastChangeAction: ACTIONS.KEY_PRESS});

      if (currentTarget.tagName === 'P') {
        this.props.onChange(e, 0, this.getSymbolOrder());
      }
      else {
        if (this.state.cursorOnStart) {
          this.props.onChange(e, 0, this.getLineOrder());
        } else {
          this.props.onChange(e, this.getSymbolOrder(), this.getLineOrder());
        }
      }
    }
    return false;
  };

  clickHandler = (e) => {
    e.target.focus();
    let {target} = e;
    if (target.tagName === 'DIV' && target.classList.contains('workspace-area')) {
      target = target.firstChild;
    }
    let left = target.offsetLeft;
    let cursorOnStart = true;
    let line = this.getSymbolOrder(target);
    let column = 0;
    if (target.tagName !== 'P') {
      left += target.offsetWidth;
      cursorOnStart = false;
      line = this.getLineOrder(target);
      column = this.getSymbolOrder(target);
    }
    // if click was on edit area but not on some of nodes put cursor on first paragraph
    this.setState({
      currentTarget: target,
      cursor: {
        top: target.offsetTop,
        left,
        visibility: 'visible',
        line,
        column,
      },
      cursorOnStart,
    })
  };

  blurHandler = () => {
    this.setState({
      cursor: {
        top: 0,
        left: 0,
        visibility: 'hidden',
        line: 0,
        column: 0,
      },
      cursorOnStart: false,
      currentTarget: null,
    })
  };

  getCursorByNode = (span) => {
    return {
      top: span.offsetTop,
      left: span.offsetLeft + span.offsetWidth,
      visibility: 'visible',
      line: this.getLineOrder(span),
      column: this.getSymbolOrder(span),
    }
  };

  putCursorOnStart = (span) => {
    if (span) {
      return {
        top: span.offsetTop,
        left: span.offsetLeft,
        visibility: 'visible',
        line: this.getSymbolOrder(span),
        column: 0,
      }
    }
    else {
      const currentLine = this.state.currentTarget.parentNode;
      return {
        top: currentLine.offsetTop,
        left: currentLine.offsetLeft,
        visibility: 'visible',
        line: this.getSymbolOrder(currentLine),
        column: 0,
      }
    }
  };

  keyDownHandler = (e) => {

    let {currentTarget, cursor, cursorOnStart} = this.state;

    if (currentTarget) {
      // handle delete and remove buttons
      // prevent delete if cursor on start fo line
      if ((e.keyCode === KEY_CODES.BACKSPACE || e.keyCode === KEY_CODES.DELETE)) {
        if (!cursorOnStart) {
          this.props.onBackspace(this.getSymbolOrder(), this.getLineOrder());
          if (currentTarget.previousSibling) {
            currentTarget = currentTarget.previousSibling;
          } else {
            // set cursor on start position
            cursorOnStart = true;
            currentTarget = currentTarget.parentNode;

          }
          this.setState({
            lastChangeAction: ACTIONS.REMOVE_SYMBOL,
            currentTarget,
            cursorOnStart,
          });
        } else {
          // removing paragraph when cursor on paragraph
          if (this.isCurrentNodeParagraph() && currentTarget.previousSibling) {
            //when paragraph with content
            if (currentTarget.childElementCount) {
              this.props.onRemoveLine(this.getSymbolOrder(), true);
              currentTarget = currentTarget.previousSibling;
              // put cursor on last child of previous line if it exist
              if (currentTarget.lastChild) {
                currentTarget = currentTarget.lastChild;
                cursor = this.getCursorByNode(currentTarget);
                cursorOnStart = false;
              }
              // put cursor on start of paragraph if previous line is empty
              else {
                cursor = this.putCursorOnStart(currentTarget);
                cursorOnStart = true;
              }
            }
            // when paragraph is empty
            else {
              this.props.onRemoveLine(this.getSymbolOrder());
              currentTarget = currentTarget.previousSibling;

              if (currentTarget.lastChild) {
                currentTarget = currentTarget.lastChild;
                cursor = this.getCursorByNode(currentTarget);
                cursorOnStart = false;
              } else {
                cursor = this.putCursorOnStart(currentTarget);
                cursorOnStart = true;
              }
            }
          }
          // removing paragraph with content when cursor on span
          if (!this.isCurrentNodeParagraph() && currentTarget.parentNode.previousSibling) {
            this.props.onRemoveLine(this.getLineOrder(), true);
            currentTarget = currentTarget.parentNode.previousSibling;

            if (currentTarget.lastChild) {
              currentTarget = currentTarget.lastChild;
              cursor = this.getCursorByNode(currentTarget);
              cursorOnStart = false;
            } else {
              cursor = this.putCursorOnStart(currentTarget);
              cursorOnStart = true;
            }
          }


          this.setState({currentTarget, cursor, cursorOnStart});

        }
      }

      // handle right arrow click and move cursor to the right
      if (e.keyCode === KEY_CODES.RIGHT_KEY) {
        if (this.isCurrentNodeParagraph()) {
          if (currentTarget.firstChild) {
            currentTarget = currentTarget.firstChild;
            cursorOnStart = false;
            cursor = this.getCursorByNode(currentTarget);
          }
        } else {
          if (cursorOnStart && currentTarget) {
            cursor = this.getCursorByNode(currentTarget);
            cursorOnStart = false;
          }
          else if (currentTarget.nextSibling) {
            currentTarget = currentTarget.nextSibling;
            cursor = this.getCursorByNode(currentTarget);
          }
        }

        this.setState({
          currentTarget,
          cursor,
          cursorOnStart,
        });
      }

      // handle left arrow click and move cursor to the right
      if (e.keyCode === KEY_CODES.LEFT_KEY) {
        if (this.isCurrentNodeParagraph()) {
          // TODO or not TODO improve jump on upper paragraph if we on <p> if it needs
        } else {
          if (currentTarget.previousSibling) {
            currentTarget = currentTarget.previousSibling;
            cursor = this.getCursorByNode(currentTarget);
          } else if (!cursorOnStart) {
            cursor = this.putCursorOnStart(currentTarget);
            cursorOnStart = true;
          }
        }

        this.setState({
          currentTarget,
          cursor,
          cursorOnStart,
        });
      }

      // handle down arrow click and move cursor to the down
      if (e.keyCode === KEY_CODES.DOWN_KEY) {
        let currentLine, symbolOrder;

        if (this.isCurrentNodeParagraph()) {
          currentLine = currentTarget;
          symbolOrder = 0;
        } else {
          currentLine = currentTarget.parentNode;
          symbolOrder = this.getSymbolOrder();
        }

        const nextLine = currentLine.nextSibling;


        if (nextLine && nextLine.tagName === 'P') {

          if (nextLine.childElementCount !== 0 && symbolOrder !== 0) {
            // check if in next line exist symbol with order number as order number
            // of symbol from which we jump off
            if (nextLine.childElementCount >= symbolOrder) {
              currentTarget = nextLine.children[symbolOrder - 1];
              cursor = this.getCursorByNode(currentTarget);
            } else {
              currentTarget = nextLine.children[nextLine.childElementCount - 1];
              cursor = this.getCursorByNode(currentTarget);
            }
          } else {
            currentTarget = nextLine;
            cursor = this.putCursorOnStart(currentTarget);
          }
        }
        this.setState({cursor, currentTarget, cursorOnStart});
      }

      // handle up arrow click and move cursor to the up
      if (e.keyCode === KEY_CODES.UP_KEY) {
        let currentLine, symbolOrder;

        if (this.isCurrentNodeParagraph()) {
          currentLine = currentTarget;
          symbolOrder = 0;
        } else {
          currentLine = currentTarget.parentNode;
          symbolOrder = this.getSymbolOrder();
        }

        const prevLine = currentLine.previousSibling;
        if (prevLine) {
          // check is prevLine empty or not
          if (prevLine.childElementCount !== 0 && symbolOrder !== 0) {
            // check if in prev line exist symbol with order number as order number
            // of symbol from which we jump off
            if (prevLine.childElementCount >= symbolOrder) {
              currentTarget = prevLine.children[symbolOrder - 1];
              cursor = this.getCursorByNode(currentTarget);
            } else {
              currentTarget = prevLine.children[prevLine.childElementCount - 1];
              cursor = this.getCursorByNode(currentTarget);
            }
          } else {
            currentTarget = prevLine;
            cursor = this.putCursorOnStart(currentTarget);
            cursorOnStart = true
          }
        }
        this.setState({cursor, currentTarget, cursorOnStart});
      }

      // handle enter key to crate new Line
      if (e.keyCode === KEY_CODES.ENTER) {
        if (this.isCurrentNodeParagraph()) {
          if (currentTarget.childElementCount) {
            this.props.onNewLine(this.getSymbolOrder(), true, 0);
          } else {
            this.props.onNewLine(this.getSymbolOrder());
          }

        } else if (cursorOnStart) {
          this.props.onNewLine(this.getLineOrder(), true, 0);
          currentTarget = currentTarget.parentNode;
        } else {
          let withCut = false;
          if (currentTarget.nextSibling) withCut = true;
          this.props.onNewLine(this.getLineOrder(), withCut, this.getSymbolOrder());
          currentTarget = currentTarget.parentNode;
        }
        this.setState({lastChangeAction: ACTIONS.NEW_LINE, currentTarget});
      }
    }
    return false;
  };

  handleOutsideActions = () => {
    let {outsideAction, cursor, currentTarget, cursorOnStart} = this.state;

    if (!outsideAction.finished) {

      if (outsideAction.type === ACTIONS.NEW_LINE) {
        const {prevLineIndex, withCut, symbolIndex} = outsideAction.data;

        // if paragraph wrap was on some previous nodes, we jump on next line on current symbol
        // or if that symbol not exist we`ll jump on start of the next paragraph
        if (prevLineIndex < cursor.line) {
          currentTarget = this.area.current.childNodes[cursor.line].childNodes[cursor.column - 1];
          if (currentTarget) {
            cursor = this.getCursorByNode(currentTarget);
          } else {
            currentTarget = this.area.current.childNodes[cursor.line];
            cursor = this.putCursorOnStart(currentTarget);
            cursorOnStart = true;
          }

        }
        else if (prevLineIndex === cursor.line) {
          if (withCut && symbolIndex <= cursor.column) {
            currentTarget = this.area.current.childNodes[cursor.line].childNodes[cursor.column - symbolIndex - 1];
            cursor = this.getCursorByNode(currentTarget);
          }
          if (!withCut) {
            currentTarget = this.area.current.childNodes[cursor.line];
            cursor = this.putCursorOnStart(currentTarget);
          }
        }


      }

      if (outsideAction.type === ACTIONS.KEY_PRESS) {
        const {line, column} = outsideAction.data;
        if (cursor.line === line && cursor.column > column) {
          currentTarget = this.area.current.childNodes[cursor.line - 1].childNodes[cursor.column];
          cursor = this.getCursorByNode(currentTarget);
        }
      }

      if (outsideAction.type === ACTIONS.REMOVE_SYMBOL) {
        const {line, column} = outsideAction.data;
        if (cursor.line === line && cursor.column > column) {
          currentTarget = this.area.current.childNodes[cursor.line - 1].childNodes[cursor.column - 2];
          cursor = this.getCursorByNode(currentTarget);
        }
        // if current target was deleted, hide cursor
        if (cursor.line === line && cursor.column === column) {
          cursor = {
            top: 0,
            left: 0,
            visibility: 'hidden',
            line: 0,
            column: 0,
          };
          currentTarget = null;
          cursorOnStart = false;
        }
      }

      if (outsideAction.type === ACTIONS.REMOVE_LINE) {
        const {line, incomeNodeLength} = outsideAction.data;
        if (line < cursor.line) {
          currentTarget = this.area.current.childNodes[cursor.line - 2].childNodes[cursor.column - 1];
          if (currentTarget) {
            cursor = this.getCursorByNode(currentTarget);
          } else {
            currentTarget = this.area.current.childNodes[cursor.line - 2];
            cursor = this.putCursorOnStart(currentTarget);
            cursorOnStart = true;
          }
        }
        if (line === cursor.line) {
          currentTarget = this.area.current.childNodes[cursor.line - 2].childNodes[cursor.column + incomeNodeLength - 1];
          if (currentTarget) {
            cursor = this.getCursorByNode(currentTarget);
          }
          else {
            currentTarget = this.area.current.childNodes[cursor.line - 2];
            cursor = this.putCursorOnStart(currentTarget);
            cursorOnStart = true;
          }
        }
      }


      outsideAction.finished = true;


      this.setState({
        outsideAction,
        cursor,
        currentTarget,
        cursorOnStart,
      })

    }

  };


  componentDidUpdate(prevProps, prevState) {
    let {currentTarget, lastChangeAction, cursorOnStart, cursor} = this.state;


    this.handleOutsideActions();
    // move cursor to next symbol after adding
    if (lastChangeAction === ACTIONS.KEY_PRESS) {
      if (this.isCurrentNodeParagraph()) {
        // if needs for check added symbol on last iteration or will be added in next
        if (currentTarget.firstChild) {
          currentTarget = currentTarget.firstChild;
          this.setState({
            lastChangeAction: null,
            cursor: this.getCursorByNode(currentTarget),
            currentTarget,
            cursorOnStart: false,
          })
        }
      } else if (cursorOnStart) {
        if (currentTarget.nextSibling) {
          this.setState({
            lastChangeAction: null,
            cursor: this.getCursorByNode(currentTarget.nextSibling),
            cursorOnStart: false,
          })
        }
      }
      else {
        if (currentTarget.nextSibling) {
          this.setState({
            lastChangeAction: null,
            cursor: this.getCursorByNode(currentTarget.nextSibling),
            currentTarget: currentTarget.nextSibling,
          })
        }
      }

    }

    // move cursor to previous symbol on Delete
    if (lastChangeAction === ACTIONS.REMOVE_SYMBOL) {
      let cursor = cursorOnStart ? this.putCursorOnStart() : this.getCursorByNode(currentTarget);
      if (currentTarget.tagName === 'P') {
        cursor = this.putCursorOnStart(currentTarget);
      }
      this.setState({
        lastChangeAction: null,
        cursor,
        cursorOnStart,
      })
    }

    if (lastChangeAction === ACTIONS.NEW_LINE) {
      if (currentTarget.tagName !== 'DIV') {
        if (currentTarget.nextSibling) {
          currentTarget = currentTarget.nextSibling;
        }
        cursor = this.putCursorOnStart(currentTarget);
        this.setState({
          currentTarget,
          cursor,
          lastChangeAction: null,
          cursorOnStart: true,
        })
      }
    }
    
    if(!cursorOnStart && cursor.visibility !== 'hidden'){
      if(!this.area.current.childNodes[cursor.line - 1]) {
        this.setState({
          cursor: {
            top: 0,
            left: 0,
            visibility: 'hidden',
            line: 0,
            column: 0,
          }
        })
      } else if(!this.area.current.childNodes[cursor.line - 1].childNodes[cursor.column - 1]) {
        this.setState({
          cursor: {
            top: 0,
            left: 0,
            visibility: 'hidden',
            line: 0,
            column: 0,
          }
        })
      }

    }
  }

  render() {
    return (
      <div className="editor" ref={this.editor}>
        <div className={`editor-controls ${this.state.showControls ? 'show' : ''}`}
             style={{
               top: `${this.state.controlsCoordinates.y}px`,
               left: `${this.state.controlsCoordinates.x}px`,
               display: 'none',
             }}
        >
          <button onClick={this.makeBold}>
            B
          </button>
          <button onClick={this.makeItalic}>
            I
          </button>
          <button onClick={this.makeUnderline}>
            U
          </button>
          <button onClick={this.makeLineThrough}>
            C
          </button>
          <button onClick={this.addBlockQuote}>
            Q
          </button>
          <button onClick={this.makeLink}>
            L
          </button>
          <button className="editor-file" onClick={this.makeImage}>
            P
            {/* <input type="file" onChange={this.makeImage} ref={this.inputFile}/>*/}
          </button>
          <button onClick={this.makeComment}>
            C
          </button>
        </div>
        {/*<br/>*/}
        <div
          onSelect={this.selectionChange}
          onMouseDown={this.mouseDownHandler}
          onInput={this.inputHandler}
          onDoubleClick={this.controlsStateUpdate}
          onKeyPress={this.unChangeAbleText}
          onClick={this.props.disabled ? null : this.clickHandler}
          style={{position: 'relative', lineHeight: '20px'}}
          tabIndex={0}
          onKeyDown={this.keyDownHandler}
          className={'workspace-area'}
          onBlur={this.blurHandler}
          onMouseMove={this.mouseMoveHandler}
          onDoubleClickCapture={this.doubleClickHandler}
          ref={this.area}
        >
          {this.state.content}
          <Cursor {...this.state.cursor}/>
        </div>
      </div>
    );
  }
}

export default HtmlEditor;
