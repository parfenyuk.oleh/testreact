import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import * as signalR from '@aspnet/signalr';
import HtmlEditor from './Editor';
import DoublyList from './DoublyList';
import Node from './Node';

const ACTIONS = {
  KEY_PRESS: 'KEY_PRESS',
  REMOVE_SYMBOL: 'REMOVE_SYMBOL',
  NEW_LINE: 'NEW_LINE',
  REMOVE_LINE: 'REMOVE_LINE',
};

const SPACE_SYMBOL = ' ';

class EditorController extends Component {
  constructor(props) {
    super(props);

    this.state = ({
      editorValue: null,
      outsideAction: null,
      outsideActionId: 1,
    })
  }

  componentDidMount() {
    const URL = process.env.API_URL.substring(0, process.env.API_URL.length - 2);
    let connection = new signalR.HubConnectionBuilder()
      .withUrl(`${URL}WorkSpaceHub`,
        {
          accessTokenFactory: () => this.props.token,
          skipNegotiation: true,
          transport: signalR.HttpTransportType.WebSockets,
        },
      )
      .build();


    connection.start()
      .then(() => {

        this.setState({socket: connection});
        connection.invoke('ConnectToWorkspace', this.props.assignmentId, this.props.groupId)
          .then(response => {});


        connection.on('onConnectToWorkspace', message => {

          if (!message) {
            message = [
              {content: [], type: 'p'},
            ]
          }

          if (!this.state.editorValue) {
            this.setHubData(message);
          }

        });


        // send data for new connection if this user is first
        connection.on('onGetHubContent', () => {
          connection.invoke('GetHubContent', this.serializeEditorValue()).catch();
        });

        // send data to synchronize all user if this user is first
        connection.on('onGetRefreshContent', (msg) => {
          connection.invoke('RefreshHubContentSend', this.serializeEditorValue()).catch();
        });
        // set synchronized data for user
        connection.on('onGetHubContentSend', data => {
          this.setHubData(data);
        });

        connection.on('onNewSymbol', this.setNewSymbol);
        connection.on('onCreateNewLine', this.setNewLine);
        connection.on('onRemoveSymbol', this.removeSymbol);
        connection.on('onRemoveLine', this.removeLine);

      });
  }

  setHubData = (data) => {
    const state = new DoublyList();

    data.forEach(item => {
      const newItem = new Node({content: new DoublyList(), type: 'p'});
      item.content.forEach(item => { newItem.data.content.add(item)});
      state.add(newItem);
    });
    this.wordsCounter(state);
    this.setState({editorValue: state});
  };

  transformContentToHtml = () => {
    const {editorValue} = this.state;
    return editorValue.map(item => {
      switch (item.data.type) {
        case 'p' : {
          return <p>
            {item.data.content.map(item => <span className={`user-${item.ownerId}`}
            >{item.content}</span>)}
          </p>
        }
        default :
          return ''
      }
    });
  };

  onChange = (e, symbolNumber, lineNumber) => {
    const {socket} = this.state;
    let key = e.nativeEvent.key;
    const newSymbol = {
      value: {
        ownerId: this.props.userId,
        content: key,
        styles: [],
      },
      symbolNumber,
      lineNumber,
    };
    socket.invoke('NewSymbol', newSymbol);
    this.setNewSymbol(newSymbol, true)

  };

  setNewSymbol = (message, isFromCurrent = false) => {
    let {editorValue, outsideAction, outsideActionId, socket} = this.state;
    try {
      const currentLine = editorValue.searchNodeAt(message.lineNumber).data.data.content;
      if (message.symbolNumber === 0) {
        currentLine.shift(message.value);
      } else {
        currentLine.insert(message.value, message.symbolNumber);
      }
      if (isFromCurrent) {
        outsideAction = null;
      } else {
        outsideActionId++;
        outsideAction = {
          type: ACTIONS.KEY_PRESS,
          data: {
            line: message.lineNumber,
            column: message.symbolNumber,
          },
          id: outsideActionId,
        }
      }
      this.setState({editorValue, outsideAction, outsideActionId});
      this.wordsCounter(editorValue);
    } catch (e) {
      socket.invoke('GetRefreshHubContent').catch(err => {});
    }


  };

  createNewLineHandler = (prevLineIndex, withCut, symbolIndex) => {
    const {socket} = this.state;
    const newLine = {prevLineIndex, withCut, symbolIndex};
    socket.invoke('CreateNewLine', newLine);
    this.setNewLine(newLine, true);

  };

  setNewLine = (newLine, isFromCurrent = false) => {

    let {prevLineIndex, withCut, symbolIndex, socket} = newLine;
    let {editorValue, outsideAction, outsideActionId} = this.state;
    const newParagraph = new Node({content: new DoublyList(), type: 'p'});
    let error = false;
    try {
      if (withCut) {
        const currentLineContent = editorValue.searchNodeAt(prevLineIndex).data.data.content;
        newParagraph.data.content = currentLineContent.cut(symbolIndex);
      }
      editorValue.insert(newParagraph, prevLineIndex);
    } catch (e) {
      error = true;
      socket.invoke('GetRefreshHubContent').catch(err => {});
    }

    if (!error) {
      if (isFromCurrent) {
        outsideAction = null;
      } else {
        outsideActionId++;
        outsideAction = {
          type: ACTIONS.NEW_LINE,
          data: {prevLineIndex, withCut, symbolIndex},
          id: outsideActionId,
        }
      }

      this.setState({
        editorValue,
        outsideAction,
        outsideActionId,
      });
      this.wordsCounter(editorValue);
    }


  };

  /**
   * Remove symbol from line
   * @param symbolNumber
   * @param lineNumber
   */
  onRemove = (symbolNumber, lineNumber) => {
    this.state.socket.invoke('RemoveSymbol', {symbolNumber, lineNumber});
    this.removeSymbol({symbolNumber, lineNumber}, true);
  };

  removeSymbol = ({symbolNumber, lineNumber}, isFromCurrent = false) => {
    let {editorValue, outsideAction, outsideActionId, socket} = this.state;
    let error = false;

    try {
      const currentLine = editorValue.searchNodeAt(lineNumber).data.data.content;
      currentLine.remove(symbolNumber);
    } catch (e) {
      socket.invoke('GetRefreshHubContent').catch(err => {});
      error = true;
    }

    if (!error) {
      if (isFromCurrent) {
        outsideAction = null;
      } else {
        outsideActionId++;
        outsideAction = {
          type: ACTIONS.REMOVE_SYMBOL,
          data: {
            line: lineNumber,
            column: symbolNumber,
          },
          id: outsideActionId,
        }
      }

      this.setState({editorValue, outsideAction, outsideActionId});
      this.wordsCounter(editorValue);
    }
  };

  removeLineHandler = (position, withMerge) => {
    this.state.socket.invoke('RemoveLine', {position, withMerge});
    this.removeLine({position, withMerge}, true);
  };

  removeLine = ({position, withMerge}, isFromCurrent = false) => {
    let {editorValue, outsideAction, outsideActionId, socket} = this.state;
    let incomeNodeLength = 0;
    let error = false;

    try {
      if (withMerge) {
        const nodeToMerge = editorValue.searchNodeAt(position).data.data.content;
        const nodeToIncome = editorValue.searchNodeAt(position - 1).data.data.content;
        incomeNodeLength = nodeToIncome._length;
        nodeToIncome.merge(nodeToMerge);
      }
      editorValue.remove(position);
    } catch (e) {
      socket.invoke('GetRefreshHubContent').catch(err => {});
      error = true;
    }
    if (!error) {
      if (isFromCurrent) {
        outsideAction = null;
      } else {
        outsideActionId++;
        outsideAction = {
          type: ACTIONS.REMOVE_LINE,
          data: {
            line: position,
            withMerge,
            incomeNodeLength,
          },
          id: outsideActionId,
        }
      }

      this.setState({editorValue, outsideAction, outsideActionId});
      this.wordsCounter(editorValue);
    }
  };

  serializeEditorValue = () => {
    if (this.state.editorValue) {
      return this.state.editorValue.toArray(item => {
        const newItem = {...item.data};
        newItem.content = newItem.content.toArray(item => item);
        return newItem;
      });
    }
    return null;
  };

  componentWillUnmount() {
    this.state.socket.invoke('SaveHubContent', this.serializeEditorValue())
      .then(() => { this.state.socket.stop(); });
  }

  wordsCounter = (editorValue) => {
    let amount = {
      total: 0,
      users: {},
    };
    let lastSymbol = SPACE_SYMBOL;
    if (!editorValue) { editorValue = this.state.editorValue }
    editorValue.foreach(item => {
      lastSymbol = SPACE_SYMBOL;
      item.data.content.foreach(symbol => {
        if (symbol === undefined) {
          lastSymbol = SPACE_SYMBOL;
          return;
        }
        if (lastSymbol === SPACE_SYMBOL && symbol.content !== SPACE_SYMBOL) {
          amount.total++;
          if (amount.users[symbol.ownerId]) {
            amount.users[symbol.ownerId]++;
          } else {
            amount.users[symbol.ownerId] = 1;
          }
        }
        lastSymbol = symbol.content;
      })
    });

    this.props.onWordsCountChange(amount);
    return amount;
  };

  render() {

    if (this.state.editorValue) {
      let style = this.props.students.reduce((acc, item) => {
        return acc + `.user-${item.id}{border-bottom: 1px solid ${item.color}!important}`;
      }, '');
      return (
        <Fragment>
          <style>{this.props.colorCoding ? style : ''}</style>
          <HtmlEditor content={this.transformContentToHtml()}
                      onChange={this.onChange}
                      onNewLine={this.createNewLineHandler}
                      onBackspace={this.onRemove}
                      onRemoveLine={this.removeLineHandler}
                      outsideAction={this.state.outsideAction}
                      disabled={this.props.disabled}
          />
        </Fragment>
      );
    } else {
      return (<div className="loading">LOADING...</div>)
    }
  }
}

EditorController.defaultProps = {
  onWordsCountChange: () => {},
};

EditorController.propTypes = {
  assignmentId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  colorCoding: PropTypes.bool,
  onWordsCountChange: PropTypes.func,
  disabled: PropTypes.bool,
};

export default EditorController;