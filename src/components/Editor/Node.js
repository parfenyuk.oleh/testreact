class Node {
  constructor(props){
    this.data = props;
    this.previous = null;
    this.next = null;
  }
}

export default Node;