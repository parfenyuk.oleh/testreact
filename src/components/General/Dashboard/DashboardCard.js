import React from 'react';
import PropTypes from 'prop-types';

const DashboardCard = ({icon, number, title, vertical}) => {
  return (
    <div className={`dashboard-card ${vertical ? 'vertical' : ''}`}>
      <span className="dashboard-card-icon">
        {icon}
      </span>
      <p className="dashboard-card-number">{number}</p>
      <p className="dashboard-card-title">{title}</p>
    </div>
  );
};

DashboardCard.propTypes = {
  icon: PropTypes.element,
  number: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  title: PropTypes.string,
  vertical: PropTypes.bool,
};

export default DashboardCard;