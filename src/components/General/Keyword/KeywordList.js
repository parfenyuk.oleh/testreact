import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import Select from "../../UI/Select";
import Icon from "../../UI/Elements/Icon";
import Preloader from "../../UI/Preloader";
import EmptyState from "../../UI/EmptyState";

export const KeywordList = ({list, deleteKeywordItemHandler, isLoading}) => {
    return <Fragment>
        {
            list.length !== 0 ? (list.map((item) => {
                return <li key={item.id}>{item.name} <Icon name="cross" onClick={() => deleteKeywordItemHandler(item.id)}/></li>
            })) : (
                <div className='emptyState-wrapper'>
                    {
                        isLoading ? <Preloader/> : <EmptyState/>
                    }
                </div>)
        }
    </Fragment>;
};

