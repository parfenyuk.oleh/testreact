import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Popover from 'antd/lib/popover';
import 'antd/lib/popover/style/index.css';
import EventItemPopover from './EventItemPopover';
import moment from 'moment';
import AvatarBlock from '../../UI/Elements/AvatarBlock';

class EventItem extends Component {
    constructor(props) {
        super(props);

        const {left, top, width} = props;

        this.state = {
            left: left,
            top: top,
            width: width,
        };
        this.startResizer = null;
        this.endResizer = null;
    }

    static propTypes = {
        schedulerData: PropTypes.object.isRequired,
        eventItem: PropTypes.object.isRequired,
        isStart: PropTypes.bool.isRequired,
        isEnd: PropTypes.bool.isRequired,
        left: PropTypes.number.isRequired,
        width: PropTypes.number.isRequired,
        top: PropTypes.number.isRequired,
        isInPopover: PropTypes.bool.isRequired,
        leftIndex: PropTypes.number.isRequired,
        rightIndex: PropTypes.number.isRequired,
        isDragging: PropTypes.bool.isRequired,
        connectDragSource: PropTypes.func.isRequired,
        connectDragPreview: PropTypes.func.isRequired,
        updateEventStart: PropTypes.func,
        updateEventEnd: PropTypes.func,
        moveEvent: PropTypes.func,
        subtitleGetter: PropTypes.func,
        eventItemClick: PropTypes.func,
        viewEventClick: PropTypes.func,
        viewEventText: PropTypes.node,
        viewEvent2Click: PropTypes.func,
        viewEvent2Text: PropTypes.node,
        conflictOccurred: PropTypes.func,
        eventItemTemplateResolver: PropTypes.func,
    }

    componentWillReceiveProps(np) {
        const {left, top, width} = np;
        this.setState({
            left: left,
            top: top,
            width: width,
        });
    }

    componentDidMount() {
        this.setState({
            spanWidth: this.refs.span.getBoundingClientRect().width
        })
    }

    render() {
        const {
            eventItem, isStart, isEnd,
            isInPopover, eventItemClick,
            schedulerData, isDragging,
            connectDragSource, connectDragPreview,
            eventItemTemplateResolver, viewType
        } = this.props;
        const {config, localeMoment} = schedulerData;
        let {left, width, top, spanWidth} = this.state;
        let roundCls = isStart ? (isEnd ? 'round-all' : 'round-head') : (isEnd ? 'round-tail' : 'round-none');
        let bgColor = config.defaultEventBgColor;
        const avatarDifferenceWidth = 80;
        const weekViewType = 1;

        if (!!eventItem.bgColor)
            bgColor = eventItem.bgColor;

        if (width - avatarDifferenceWidth < spanWidth) {
            spanWidth = width - avatarDifferenceWidth;
        }

        let content = (
            <EventItemPopover
                {...this.props}
                eventItem={eventItem}
                title={eventItem.title}
                startTime={eventItem.startDate}
                endTime={eventItem.endDate}
                statusColor={bgColor}/>
        );


        let eventItemTemplate = (
            <div className={roundCls + ' event-item'} key={eventItem.id}
                 style={{height: config.eventItemHeight, backgroundColor: bgColor}}>
                <div
                    style={{marginLeft: '20px', height: config.eventItemHeight, display: 'flex', alignItems: 'center'}}>
                    <AvatarBlock textAvatar={`${eventItem.firstName[0]}${eventItem.lastName[0]}`}
                                 imgSrc={eventItem.avatar}
                    />
                    <div>
                        <span style={{
                            fontSize: 15,
                            width: spanWidth,
                            display: 'inline-block',
                            overflow: 'hidden',
                            textOverflow: 'ellipsis',
                            lineHeight: '20px'

                        }} ref='span'>
                            {eventItem.title}
                            </span>
                        <span style={{fontSize: 11, display: 'block'}}>{moment(eventItem.startDate).format('MM/DD/YY')}
                            {
                                viewType === weekViewType ? <span>-</span> : <br/>
                            }

                            {moment(eventItem.endDate).format('MM/DD/YY')}</span>
                    </div>

                </div>
            </div>
        );
        if (eventItemTemplateResolver)
            eventItemTemplate = eventItemTemplateResolver(schedulerData, eventItem, bgColor, isStart, isEnd, 'event-item', config.eventItemHeight, undefined);

        let a = <a className='timeline-event' style={{left, width, top}} onClick={() => {
            if (!!eventItemClick) eventItemClick(schedulerData, eventItem);
        }}>
            {eventItemTemplate}
        </a>;

        return (
            isDragging ? null : (schedulerData._isResizing() || config.eventItemPopoverEnabled == false || eventItem.showPopover == false ?
                    <div>
                        {
                            connectDragPreview(
                                connectDragSource(a)
                            )
                        }
                    </div> :
                    <Popover placement='bottom' content={content} trigger='hover'>
                        {
                            connectDragPreview(
                                connectDragSource(a)
                            )
                        }
                    </Popover>
            )
        );
    }
}

export default EventItem