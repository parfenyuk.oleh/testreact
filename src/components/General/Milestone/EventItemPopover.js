import React, {Component} from 'react'
import PropTypes from 'prop-types';
import Col from 'antd/lib/col'
import Row from 'antd/lib/row'
import 'antd/lib/grid/style/index.css'

class EventItemPopover extends Component {
    constructor(props) {
        super(props);
    }

    static propTypes = {
        schedulerData: PropTypes.object.isRequired,
        eventItem: PropTypes.object.isRequired,
        title: PropTypes.string.isRequired,
        startTime: PropTypes.string.isRequired,
        endTime: PropTypes.string.isRequired,
        statusColor: PropTypes.string.isRequired,
        subtitleGetter: PropTypes.func,
        viewEventClick: PropTypes.func,
        viewEventText:PropTypes.node,
        viewEvent2Click: PropTypes.func,
        viewEvent2Text: PropTypes.node,
    }

    render(){
        const {schedulerData, eventItem, title, startTime, endTime, statusColor,subtitleGetter, viewEventClick, viewEventText, viewEvent2Click, viewEvent2Text} = this.props;
        const {localeMoment, config} = schedulerData;
        let start = localeMoment(startTime), end = localeMoment(endTime);

        let subtitleRow = <div />;
        if(subtitleGetter !== undefined){
            let subtitle = subtitleGetter(schedulerData, eventItem);
            if(subtitle !== undefined){
                console.log(schedulerData, eventItem);
                subtitleRow = (
                    <Row type="flex" align="middle">
                        <Col span={2}>
                            <div />
                        </Col>
                        <Col span={22} className="overflow-text">
                            <span className="header2-text" title={subtitle}>{subtitle}</span>
                        </Col>
                    </Row>
                );
            }
        }

        let opsRow = <div />;
        if(viewEventText !== undefined && viewEventClick !== undefined){
            let col = (
                <div className="icon-block">
                    <span className="header2-text" onClick={() => {viewEventClick(schedulerData, eventItem);}}>{viewEventText}</span>
                </div>
            );
            if(viewEvent2Text !== undefined && viewEvent2Click !== undefined ) {
                col = (
                    <div className="icon-block">
                        <span onClick={() => {viewEventClick(schedulerData, eventItem);}}>{viewEventText}</span><span className="header2-text" style={{color: '#108EE9', cursor: 'pointer', marginLeft: '16px'}} onClick={() => {viewEvent2Click(schedulerData, eventItem);}}>{viewEvent2Text}</span>
                    </div>
                )
            };
            opsRow = (
                <Row type="flex" align="middle">
                    {col}
                </Row>
            );
        }
        else if(viewEvent2Text !== undefined && viewEvent2Click !== undefined && (eventItem.clickable2 == undefined || eventItem.clickable2)) {
            let col = (
                <div className="icon-block">
                    <span onClick={() => {viewEvent2Click(schedulerData, eventItem);}}>{viewEvent2Text}</span>
                </div>
            );
            opsRow = (
                <Row type="flex" align="middle">
                    {col}
                </Row>
            );
        }

        return (
            <div style={{width: '210px'}} className="tooltip">
                <Row type="flex" align="middle">
                    <Col span={22}>
                        <span className="header2-text" title={title}>{title}</span>
                    </Col>
                </Row>
                {subtitleRow}
                <Row type="flex" align="middle">
                    <Col span={22}>
                        <span className="help-text">
                            {start.format('l')}
                        </span>
                        <span className="header2-text">-</span>
                        <span className="help-text">
                            {end.format('l')}
                        </span>
                    </Col>
                </Row>
                <div className="icon">
                    {opsRow}
                </div>
            </div>
        );
    }
}

export default EventItemPopover
