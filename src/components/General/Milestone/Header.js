import React from 'react';
import PropTypes from 'prop-types';
import Select from "../../UI/Select";
import Button from "../../UI/Elements/Button";

export const Header = ({toggleMilestoneModal}) => {
  return (
    <div className="admin-main-header" style={{width: 'auto'}}>
      <h1 className="page-title">Milestones</h1>
        <Button
            type='button'
            size='md'
            onClick={toggleMilestoneModal}
        >
            + add milestone
        </Button>
    </div>
  );
};

