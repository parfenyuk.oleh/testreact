import React from 'react';
import Modal from '../../UI/Modal/Modal';
import PropTypes from 'prop-types';
import ModalHeader from '../../UI/Modal/ModalHeader';
import ModalBody from '../../UI/Modal/ModalBody';
import ModalRow from '../../UI/Modal/ModalRow';
import Input from '../../UI/Input';
import icons from '../../../helpers/iconsLoader';
import Button from '../../UI/Elements/Button';
import Cell from '../../UI/Cell';
import DatePicker from '../../UI/Datepicker';
import moment from 'moment';
import Textarea from '../../UI/Textarea';
import SelectSearch from '../../UI/SelectSearch';

export const MilestoneModal = ({
                                 show,
                                 saved,
                                 hideModal,
                                 handleChange,
                                 title,
                                 activeItem,
                                 description,
                                 errors,
                                 startDate,
                                 endDate,
                                 fieldUpdated,
                                 currentResource,
                                 isLoading,
                                 changeSelect,
                                 members,
                               }) => {
  const descriptionText = 'description';
  const startDateText = 'startDate';
  const endDateText = 'endDate';

  if (startDate.length && endDate.length) {
    startDate = moment(startDate);
    endDate = moment(endDate);
  }
  return (
    <Modal show={show} hideModal={hideModal}>
      <ModalHeader>
        <h3 className='modal-title'>{title ? 'Edit' : 'Add'} milestone</h3>
      </ModalHeader>
      {
        errors.length > 0 && (
          <div className='auth-errors' style={{marginBottom: '-50px'}}>
            <ul>
              {errors.map((item, key) => <li key={key}><img src={icons.iconAttention} alt=''/> {item}
              </li>)}
            </ul>
          </div>
        )
      }
      <ModalBody bigPadding overflow={true}>
        <div>
          <div>
            <ModalRow>
              <Input placeholder='Title'
                     name='title'
                     defaultValue={title}
                     changed={handleChange}/>
            </ModalRow>
            <div style={{marginTop: '0'}}>
              <ModalRow>
                            <Textarea placeholder='Description'
                                      changed={(e) => {
                                        fieldUpdated(e.target.value, descriptionText)
                                      }}
                                      defaultValue={description}
                                      name='description'/>
              </ModalRow>
            </div>
            <div style={{marginTop: '0'}}>
              <ModalRow flex>
                <Cell column={7}>
                  <DatePicker placeholder={'Start date'}
                              changed={(value) => {
                                handleChange(value.format('l'), startDateText)
                              }}
                              selected={startDate}
                  />
                </Cell>
                <Cell column={2}/>
                <Cell column={7}>
                  <DatePicker placeholder={'End date'}
                              changed={(value) => {
                                handleChange(value.format('l'), endDateText)
                              }}
                              selected={endDate}
                  />
                </Cell>
              </ModalRow>
            </div>
            <ModalRow flex>
              {
                members.length !== 0 && <SelectSearch
                  name="country"
                  mode="input"
                  options={members}
                  value={activeItem}
                  placeholder="Assign to"
                  changeSelect={changeSelect}/>
              }
            </ModalRow>
          </div>
        </div>
        <ModalRow button>
          <Button
            onClick={saved}
            type='button'
            size='xl'
            disabled={isLoading}
          >
            Save
          </Button>
        </ModalRow>
      </ModalBody>
    </Modal>
  );
};

MilestoneModal.propTypes = {
  show: PropTypes.bool,
  hideModal: PropTypes.func,
  handleChange: PropTypes.func,
  title: PropTypes.string,
  activeItem: PropTypes.number,
  description: PropTypes.string,
  errors: PropTypes.array,
  saved: PropTypes.func,
  fieldUpdated: PropTypes.func,
  currentResource: PropTypes.number,
  isLoading: PropTypes.bool,
  changeSelect: PropTypes.func,
  members: PropTypes.array,
};