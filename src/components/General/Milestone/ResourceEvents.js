import React, {Component} from 'react'
import {PropTypes} from 'prop-types'
import Summary from './Summary'
import SelectedArea from './SelectedArea'
import {CellUnits, DATETIME_FORMAT, SummaryPos} from '../../../containers/General/Milestone/index'
import {getPos} from './Util'
import {DnDTypes} from './DnDTypes'

class ResourceEvents extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isSelecting: false,
            left: 0,
            width: 0,
        }
    }

    static propTypes = {
        resourceEvents: PropTypes.object.isRequired,
        schedulerData: PropTypes.object.isRequired,
        dndSource: PropTypes.object.isRequired,
        onSetAddMoreState: PropTypes.func,
        updateEventStart: PropTypes.func,
        updateEventEnd: PropTypes.func,
        moveEvent: PropTypes.func,
        conflictOccurred: PropTypes.func,
        subtitleGetter: PropTypes.func,
        eventItemClick: PropTypes.func,
        viewEventClick: PropTypes.func,
        viewEventText:PropTypes.node,
        viewEvent2Click: PropTypes.func,
        viewEvent2Text: PropTypes.node,
        newEvent: PropTypes.func,
        eventItemTemplateResolver: PropTypes.func,
    }

    componentDidMount() {
        const {schedulerData} = this.props;
        const {config} = schedulerData;
    }


    render() {
        const {resourceEvents, schedulerData, connectDropTarget, dndSource} = this.props;
        const {cellUnit, startDate, endDate, config, localeMoment} = schedulerData;
        const {isSelecting, left, width} = this.state;
        let cellWidth = schedulerData.getContentCellWidth();
        let cellMaxEvents = schedulerData.getCellMaxEvents();
        let rowWidth = schedulerData.getContentTableWidth();
        let DnDEventItem = dndSource.getDragSource();

        let selectedArea = isSelecting ? <SelectedArea {...this.props} left={left} width={width} /> : <div />;

        let eventList = [];

        resourceEvents.headerItems.forEach((headerItem, index) => {

            if (headerItem.count || headerItem.summary) {

                let isTop = config.summaryPos === SummaryPos.TopRight || config.summaryPos === SummaryPos.Top || config.summaryPos === SummaryPos.TopLeft;
                let marginTop = resourceEvents.hasSummary && isTop ? 1 + config.eventItemLineHeight : 1;
                let renderEventsMaxIndex = headerItem.addMore === 0 ? cellMaxEvents : headerItem.addMoreIndex;

                headerItem.events.forEach((evt, idx) => {
                    if(idx < renderEventsMaxIndex && evt !== undefined && evt.render) {
                        let durationStart = localeMoment(startDate);
                        let durationEnd = localeMoment(endDate).add(1, 'days');
                        if(cellUnit === CellUnits.Hour){
                            durationStart = localeMoment(startDate).add(config.dayStartFrom, 'hours');
                            durationEnd = localeMoment(endDate).add(config.dayStopTo + 1, 'hours');
                        }
                        let eventStart = localeMoment(evt.eventItem.startDate);
                        let eventEnd = localeMoment(evt.eventItem.endDate);
                        let isStart = eventStart >= durationStart;
                        let isEnd = eventEnd <= durationEnd;
                        let left = index*cellWidth + (index > 0 ? 2 : 3);
                        let width = (evt.span * cellWidth - 3);
                        let top = marginTop + idx*config.eventItemLineHeight;
                        let eventItem = <DnDEventItem
                                                   {...this.props}
                                                   key={evt.eventItem.id}
                                                   eventItem={evt.eventItem}
                                                   isStart={isStart}
                                                   isEnd={isEnd}
                                                   isInPopover={false}
                                                   left={left}
                                                   width={width}
                                                   top={top}
                                                   leftIndex={index}
                                                   rightIndex={index + evt.span}
                                                   />;
                        eventList.push(eventItem);
                    }
                });

                if(headerItem.addMore > 0) {
                    let left = index*cellWidth + (index > 0 ? 2 : 3);
                    let width = cellWidth - (index > 0 ? 5 : 6);
                    let top = marginTop + headerItem.addMoreIndex*config.eventItemLineHeight;
                }

                if(headerItem.summary !== undefined) {
                    let top = isTop ? 1 : resourceEvents.rowHeight - config.eventItemLineHeight + 1;
                    let left = index*cellWidth + (index > 0 ? 2 : 3);
                    let width = cellWidth - (index > 0 ? 5 : 6);
                    let key = `${resourceEvents.slotId}_${headerItem.time}`;
                    let summary = <Summary key={key} schedulerData={schedulerData} summary={headerItem.summary} left={left} width={width} top={top} />;
                    eventList.push(summary);
                }
            }
        });

        return (
            <tr>
                <td style={{width: rowWidth}}>
                    {
                        connectDropTarget(
                            <div ref={this.eventContainerRef} className="event-container" style={{height: resourceEvents.rowHeight}}>
                                {selectedArea}
                                {eventList}
                            </div>
                        )
                    }
                </td>
            </tr>
        );
    }


    eventContainerRef = (element) => {
        this.eventContainer = element;
    }
}

export default ResourceEvents
