import React, {Component} from 'react'
import PropTypes from 'prop-types';
import AvatarBlock from "../../UI/Elements/AvatarBlock";

class ResourceView extends Component {

    constructor(props) {
        super(props);
    }

    static propTypes = {
        schedulerData: PropTypes.object.isRequired,
        contentScrollbarHeight: PropTypes.number.isRequired,
        slotClickedFunc: PropTypes.func,
        slotItemTemplateResolver: PropTypes.func
    }

    render() {
        const {schedulerData, slotClickedFunc, slotItemTemplateResolver} = this.props;
        const {renderData} = schedulerData;

        let width = schedulerData.getResourceTableWidth() - 2;


        let resourceList = renderData.map((item) => {
        let height = item.rowHeight - 10;
                let studentInfo = <div className="user-info">
                    <AvatarBlock textAvatar={`${item.slotFirstName[0]}${item.slotLastName[0]}`}
                                 customInfo={<span>{`${item.slotFirstName} ${item.slotLastName}`}</span>}
                                 imgSrc={item.slotAvatar}
                    />
            </div>;

            let slotItem = (
                <div title={`${item.slotFirstName} ${item.slotLastName}`}
                     className="header2-text"
                     style={{textAlign: "left"}} >
                    {studentInfo}
                </div>
            );

            if(!!slotItemTemplateResolver) {
                let temp = slotItemTemplateResolver(schedulerData, item, slotClickedFunc, width, "overflow-text header2-text");
                if(!!temp)
                    slotItem = temp;
            }

            return (
                <tr key={item.slotId}>
                    <td data-resource-id={item.slotId} style={{height: height, paddingTop: '10px', paddingLeft: '15px'}}>
                         {slotItem}
                    </td>
                </tr>
            );
        });

        return (
            <div style={{backgroundColor: '#fff'}}>
                <table className="resource-table">
                    <tbody>
                        {resourceList}
                    </tbody>
                </table>
            </div>
        )
    }
}

export default ResourceView