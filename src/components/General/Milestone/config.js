import ViewTypes from './ViewTypes';
import SummaryPos from './SummaryPos';

export default {
    schedulerWidth: 1600,
    schedulerMaxHeight: 0,
    tableHeaderHeight: 40,

    agendaResourceTableWidth: 200,
    agendaMaxEventWidth: 100,

    dayResourceTableWidth: 235,
    weekResourceTableWidth: 235,
    monthResourceTableWidth: 235,
    yearResourceTableWidth: 235,
    customResourceTableWidth: 235,

    dayCellWidth: 30,
    weekCellWidth: 195,
    monthCellWidth: 140,
    yearCellWidth: 80,
    customCellWidth: 80,

    dayMaxEvents: 99,
    weekMaxEvents: 99,
    monthMaxEvents: 140,
    yearMaxEvents: 99,
    customMaxEvents: 99,

    eventItemHeight: 64,
    eventItemLineHeight: 66,
    nonAgendaSlotMinHeight: 0,
    dayStartFrom: 0,
    dayStopTo: 23,
    defaultEventBgColor: '#F7981C',
    selectedAreaColor: '#F7981C',
    nonWorkingTimeHeadColor: '#999999',
    nonWorkingTimeHeadBgColor: '#fff0f6',
    nonWorkingTimeBodyBgColor: '#fff0f6',
    summaryColor: '#666',
    summaryPos: SummaryPos.TopRight,

    startResizable: true,
    endResizable: true,
    movable: true,
    creatable: true,
    crossResourceMove: true,
    checkConflict: false,
    scrollToSpecialMomentEnabled: true,
    eventItemPopoverEnabled: true,
    calendarPopoverEnabled: true,
    recurringEventsEnabled: true,
    headerEnabled: true,
    displayWeekend: true,
    relativeMove: true,

    resourceName: 'Student Name',
    taskName: 'Task Name',
    agendaViewHeader: 'Agenda',
    addMorePopoverHeaderFormat: 'MMM D, YYYY dddd',
    eventItemPopoverDateFormat: 'MMM D',
    nonAgendaDayCellHeaderFormat: 'ha',
    nonAgendaOtherCellHeaderFormat: 'ddd D',

    minuteStep: 30,

    views: [
        {viewName: 'Timeline Week', viewType: ViewTypes.Week, showAgenda: false, isEventPerspective: false},
        {viewName: 'Timeline Month', viewType: ViewTypes.Month, showAgenda: false, isEventPerspective: false},
    ],
}
