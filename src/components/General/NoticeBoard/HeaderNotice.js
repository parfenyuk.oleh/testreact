import React from 'react';
import PropTypes from 'prop-types';
import Select from '../../UI/Select';
import Button from '../../UI/Elements/Button';
import Header from '../../UI/Layout/Header';
import {userType} from '../../../constants';

const HeaderNotice = ({
                          groups,
                          toggleNewNoticeItemModal,
                          handleInputChange,
                          checkedItems,
                          disabled,
                          role,
                      }) => {

    return (
        <Header
            headerText='Notice board'
            className=''
            style={{}}
        >
            <div className='buttons-group'>
                <Select placeholder={'Filter by group'}
                        handleInputChange={handleInputChange}
                        multiselect
                        disabled={disabled}
                        checkedItems={checkedItems}
                        options={groups}/>
                {
                    (userType.admin === role || userType.teacher === role) && (
                        <Button
                            onClick={toggleNewNoticeItemModal}
                            type='button'
                            size='md'
                        >
                            CREATE NEW
                        </Button>
                    )
                }
            </div>
        </Header>
    );
};

HeaderNotice.displayName = 'Header';

HeaderNotice.defaultProps = {
    onClick: null,
    style: {}
};

HeaderNotice.propTypes = {
    headerText: PropTypes.string,
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node,
};

export default HeaderNotice;