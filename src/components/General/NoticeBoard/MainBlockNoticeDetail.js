import {Link} from "react-router-dom";
import * as routeConstants from "../../../constants/routes";
import React, {Fragment} from "react";
import Icon from "../../UI/Elements/Icon";
import Table from "../../Admin/Forum/Table";
import Reply from "../../UI/Layout/Reply";
import CommentBlock from "../../Admin/Forum/CommentBlock";
import Cell from "../../UI/Cell";

export const MainBlockNoticeDetail = ({ notice,
                                          auth,
                                          toggleEditFormModal,
                                          deleteForumItem,
                                          saveComment,
                                          message,
                                          handleChange,
                                          loadingReplies,
                                          removeComment,
                                          date
                                      }) =>
    <Fragment>
            <div className="detail-forum__header" style={{display: 'flex', alignItems: 'center'}}>
                <Link to={`${routeConstants.NOTICE_BOARD}`}>
                    <Icon name="left-arrow"/>
                </Link>
                <p style={{marginLeft: '20px'}}>{notice.item.name}</p>
            </div>
            <div className="detail-forum__main">
                <Table user={auth.user}
                       forumDetail={notice.item}
                       forum={false}
                       role={auth.user.role}
                       date={date}
                       editMode={() => toggleEditFormModal(notice.item.id, 'forum')}
                       deleteForumItem={deleteForumItem}/>
                <Reply
                    imgSrc={auth.user.avatar}
                    text={`${auth.user.firstName[0]}${auth.user.lastName[0]}`}
                    placeholderText="Type here to reply…"
                    className=""
                    message={message}
                    handleChange={handleChange}
                    onSubmit={saveComment}
                    style={{}}
                />
                <CommentBlock user={notice}
                              comment={notice.repliesList}
                              auth={auth}
                              loadingReplies={loadingReplies}
                              editMode={toggleEditFormModal}
                              removeComment={removeComment}
                />
            </div>
    </Fragment>;