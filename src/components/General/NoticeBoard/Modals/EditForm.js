import React, {Fragment} from 'react';
import Modal from '../../../UI/Modal/Modal';
import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Textarea from '../../../UI/Textarea';
import Button from "../../../UI/Elements/Button";
import SelectMaterial from "../../../UI/Elements/SelectMaterial";
import Input from "../../../UI/Input";
import icons from "../../../../helpers/iconsLoader";

const EditForm = ({show, saved, hideModal, handleChange, text, type, fieldUpdated, subject, subjectList, newForumItem, errors}) => {

        return (
            <Modal show={show} hideModal={hideModal}>
                <ModalHeader>
                    {
                        type === 'comment' ?
                            <h3 className="modal-title">Edit comment</h3> :
                            <h3 className="modal-title">Edit Notice</h3>
                    }
                </ModalHeader>
                {
                    errors.length > 0 && (
                        <div className="auth-errors" style={{marginBottom: '-50px'}}>
                            <ul>
                                {errors.map((item, key) => <li key={key}> <img src={icons.iconAttention} alt=""/> {item} </li>)}
                            </ul>
                        </div>
                    )
                }
                <ModalBody bigPadding overflow={false}>
                        <ModalRow>
                            {
                                type === 'forum' && (
                                    <Fragment>
                                        <Input placeholder="Title" defaultValue={newForumItem.name} changed={(e) => { fieldUpdated(e.target.value, 'name') }}/>
                                    </Fragment>
                                )
                            }
                            <Textarea placeholder={type === 'forum' ? 'Description' : 'Message'} changed={handleChange} text={text} name="messageEdit"/>
                            {
                                type === 'forum' && (
                                    <Fragment>
                                        <div className="select-wrapper">
                                            <SelectMaterial handleChange={handleChange}
                                                            activeItem={subject}
                                                            placeholder="Choose group"
                                                            itemList={subjectList}/>
                                        </div>
                                        </Fragment>
                                )
                            }
                        </ModalRow>
                    <ModalRow button>
                        <Button
                            onClick={saved}
                            type="button"
                            size="xl"
                            className=""
                        >
                            Save
                        </Button>
                    </ModalRow>
                </ModalBody>
            </Modal>
        );
};

EditForm.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    handleChange: PropTypes.func,
    saved: PropTypes.func
};

export default EditForm;
