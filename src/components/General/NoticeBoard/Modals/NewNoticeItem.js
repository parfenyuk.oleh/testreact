import React from 'react';
import Modal from '../../../UI/Modal/Modal';

import PropTypes from 'prop-types';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import ModalRow from '../../../UI/Modal/ModalRow';
import Input from '../../../UI/Input';
import Textarea from '../../../UI/Textarea';
import Button from "../../../UI/Elements/Button";
import SelectMaterial from "../../../UI/Elements/SelectMaterial";
import icons from "../../../../helpers/iconsLoader";

const NewNoticeItem = ({show, saved, hideModal, handleChange, group, groupList, errors}) => {
        return (
            <Modal show={show} hideModal={hideModal}>
                <ModalHeader>
                    <h3 className="modal-title">New Notice</h3>
                </ModalHeader>
                {
                    errors.length > 0 && (
                        <div className="auth-errors" style={{marginBottom: '-50px'}}>
                            <ul>
                                {errors.map((item, key) => <li key={key}> <img src={icons.iconAttention} alt=""/> {item} </li>)}
                            </ul>
                        </div>
                    )
                }
                <ModalBody bigPadding overflow={true}>
                    <div>
                        <ModalRow>
                            <Input placeholder="Title" name="title" changed={handleChange}/>
                            <Textarea placeholder="Description" name="description" changed={handleChange}/>
                            <div className="select-wrapper">
                                <SelectMaterial placeholder="Choose group" handleChange={handleChange} activeItem={group} itemList={groupList}/>
                            </div>
                        </ModalRow>
                    </div>
                    <ModalRow button>
                        <Button
                            onClick={saved}
                            type="button"
                            size="xl"
                            className=""
                        >
                            Save
                        </Button>
                    </ModalRow>
                </ModalBody>
            </Modal>
        );
    };

NewNoticeItem.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};

export default NewNoticeItem;
