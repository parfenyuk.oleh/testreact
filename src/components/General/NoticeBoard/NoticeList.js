import moment from "moment";
import EmptyState from "../../UI/EmptyState";
import Preloader from "../../UI/Preloader";
import Card from "../../UI/Layout/Card";
import React from "react";

export const NoticeList = ({list, cardClick, role, userId, isLoading}) => {
    return <div className="list">
        {
            list.length !== 0 ? (list.map((item) => {
                const date = moment(item.inactiveDate).format('DD-MM-YYYY');

                return <Card
                    userId={userId}
                    forum={false}
                    role={role}
                    key={item.id}
                    info={item}
                    createdAt={date}
                    cardClick={cardClick}
                >
                    {item.description}
                </Card>
            })) : (
                <div className='emptyState-wrapper'>
                    {
                        isLoading ? <Preloader/> : <EmptyState/>
                    }
                </div>)
        }
    </div>
};