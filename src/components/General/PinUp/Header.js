import React from 'react';
import PropTypes from 'prop-types';
import Select from "../../UI/Select";

export const Header = () => {
  return (
    <div className="admin-main-header">
      <h1 className="page-title">Pin-Up board</h1>
    </div>
  );
};

