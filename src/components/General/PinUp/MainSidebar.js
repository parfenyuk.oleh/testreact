import React from 'react';
import PropTypes from 'prop-types';
import {userType} from "../../../constants";

const ADMIN_ITEM_BODY_SELECTOR = 'admin-main-sidebar-body';

export const MainSidebar = ({
                                pinup,
                                changeActiveGroup,
                                activeGroup,
                                auth,
                     }) => {
    return (
        <div className="admin-main-sidebar groups">
            <div className={activeGroup === 1 ? "admin-main-sidebar-item active" : "admin-main-sidebar-item" }
                 onClick={() => {changeActiveGroup(1)}}>
                <div className={`${ADMIN_ITEM_BODY_SELECTOR}`}>
                    <span className="admin-main-sidebar-name">PUBLIC</span>
                    <span className="admin-main-sidebar-count">{pinup.pinUpsPublicCount}</span>
                </div>
            </div>
            {
                auth.user.role !== userType.teacher && (
                    <div className={activeGroup === 3 ? "admin-main-sidebar-item active" : "admin-main-sidebar-item" }
                                                             onClick={() => {changeActiveGroup(3)}}>
                    <div className={`${ADMIN_ITEM_BODY_SELECTOR}`}>
                        <span className="admin-main-sidebar-name">PRIVATE</span>
                        <span className="admin-main-sidebar-count">{pinup.pinUpsPrivateCount}</span>
                    </div>
                </div>)
            }
            <div className={activeGroup === 2 ? "admin-main-sidebar-item active" : "admin-main-sidebar-item" }
                 onClick={() => {changeActiveGroup(2)}}>
                <div className={`${ADMIN_ITEM_BODY_SELECTOR}`} >
                    <span className="admin-main-sidebar-name">GROUPS</span>
                    <span className="admin-main-sidebar-count">{pinup.pinUpsGroupCount}</span>
                </div>
            </div>
        </div>
    );
};

MainSidebar.propTypes = {
    showNewSubjectModal: PropTypes.func,
    showNewStudentModal: PropTypes.func,
    showNewTeacherModal: PropTypes.func
};

MainSidebar.defaultProps = {
    subject: []
};
