import React, {Fragment} from 'react';
import Modal from '../../UI/Modal/Modal';
import PropTypes from 'prop-types';
import ModalHeader from '../../UI/Modal/ModalHeader';
import ModalBody from '../../UI/Modal/ModalBody';
import ModalRow from '../../UI/Modal/ModalRow';
import Input from '../../UI/Input';
import Textarea from '../../UI/Textarea';
import icons from "../../../helpers/iconsLoader";
import SelectMaterial from "../../UI/Elements/SelectMaterial";
import Button from "../../UI/Elements/Button";

export const NewPinupItemModal = ({
                                      show,
                                      saved,
                                      hideModal,
                                      handleChange,
                                      name,
                                      activeItem,
                                      description,
                                      privacyTypeList,
                                      errors,
                                      groupList,
                                      groupId
}) => {
    return (
        <Modal show={show} hideModal={hideModal}>
            <ModalHeader>
                <h3 className="modal-title">New Pinup</h3>
            </ModalHeader>
            {
                errors.length > 0 && (
                    <div className="auth-errors" style={{marginBottom: '-50px'}}>
                        <ul>
                            {errors.map((item, key) => <li key={key}><img src={icons.iconAttention} alt=""/> {item}
                            </li>)}
                        </ul>
                    </div>
                )
            }
            <ModalBody bigPadding overflow={true}>
                <div>
                    <ModalRow>
                        <Input placeholder="Pin-up name" defaultValue={name} changed={handleChange} name="name"/>
                        <SelectMaterial
                            placeholder={false}
                            handleChange={handleChange}
                            activeItem={activeItem}
                            itemList={privacyTypeList}
                        />
                        {
                            activeItem === 2 && (
                                <SelectMaterial
                                    placeholder={false}
                                    handleChange={handleChange}
                                    activeItem={groupId}
                                    itemList={groupList}
                                    name='groupId'
                                />
                            )
                        }
                        <Textarea placeholder="Description" defaultValue={description} changed={handleChange}
                                  name="description"/>
                    </ModalRow>
                </div>
                <ModalRow button>
                    <Button
                        onClick={saved}
                        type="button"
                        size="xl"
                        className=""
                    >
                        Save
                    </Button>
                </ModalRow>
            </ModalBody>
        </Modal>
    );
};

NewPinupItemModal.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};
