import React from 'react';
import Button from "../../UI/Elements/Button";
import ROLES from '../../../constants/roles';
import {compose} from "recompose";
import {withInfiniteScroll} from "../../../hoc/withInfiniteScroll";
import {PinupList} from "./PinupList";
import {userType} from "../../../constants";

export const PinupCards = ({pinup, showNewPinupItem, isLoading, auth, onPaginated, cardClick}) => {
    return (
        <div className="admin-cards group-main-block">
            <div className="button-block" style={{display: 'flex', justifyContent:'space-between', marginBottom: '12px'}}>
                <div />
                <div>
                    {
                        userType.teacher !== auth.user.role && (
                            <Button
                            type="button"
                            size="md"
                            onClick={showNewPinupItem}
                        >
                            CREATE NEW
                        </Button>)
                    }
                </div>
            </div>
            <AdvancedList
                role={auth.user.role}
                userId={auth.user.id}
                list={pinup}
                lengthArray={pinup.length}
                isLoading={isLoading}
                onPaginated={onPaginated}
                cardClick={cardClick}
            />
        </div>
    );
};

const AdvancedList = compose(
    withInfiniteScroll,
)(PinupList);