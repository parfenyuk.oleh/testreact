import moment from "moment";
import Card from "../../UI/Layout/Card";
import React from "react";
import Preloader from "../../UI/Preloader";
import EmptyState from "../../UI/EmptyState";

export const PinupList = ({list, cardClick, role, userId, isLoading}) => {
    return <div className="list">
        {
            list.length ? (list.map((item) => {
                const date = moment(item.inactiveDate).format('DD-MM-YYYY');
                return <Card
                    userId={userId}
                    forum={false}
                    role={role}
                    key={item.id}
                    info={item}
                    createdAt={date}
                    cardClick={cardClick}
                    editMode
                    isPinup
                >
                    {item.description}
                </Card>
            })) : (
                <div className='emptyState-wrapper'>
                    {
                        isLoading ? <Preloader/> : <EmptyState/>
                    }
                </div>)
        }
    </div>
};