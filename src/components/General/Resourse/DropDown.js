import OutsideEventListener from '../../Layout/AppHeader/OutsideEventListener';
import Icon from '../../../components/UI/Elements/Icon';
import React, {Fragment} from 'react';

export const DropDown = ({showDropdown, toggleDropdown, item, removeResource, updateResource, downloadFile}) => {
  return (
    <Fragment>
      {
        showDropdown && (
          <OutsideEventListener showDropDown={toggleDropdown}>
            <div className='dropdwown'>
              <ul>
                <li
                  onClick={() => {downloadFile(item)}}>
                  <Icon name='download'/>
                  Download
                </li>
                {removeResource && <li onClick={() => {updateResource(item.id)}}>
                  <Icon name='edit-dark'/>
                  Edit
                </li>
                }
                {removeResource && <li onClick={() => {removeResource(item.id)}}>
                  <Icon name='delete-dark'/>
                  Delete
                </li>
                }
              </ul>
            </div>
          </OutsideEventListener>
        )
      }
    </Fragment>
  );
};