
import React from 'react';
import PropTypes from 'prop-types';
import Select from '../../UI/Select';
import Button from '../../UI/Elements/Button';
import Header from '../../UI/Layout/Header';
import {userType} from '../../../constants';

export const HeaderResources = ({
                                    toggleResourcesGroupModal,
                                    handleInputChange,
                                    checkedItems,
                                    resourceTypes,
                                    sortTypes,
                                    changeSortingHandler
                                }) => {

    return (
        <div className='resources-header__wrapper'>
            <Header
                headerText='Resources'
            >
                <div className='buttons-group'>
                    <Select placeholder={'All resources'}
                            handleInputChange={handleInputChange}
                            multiselect
                            checkedItems={checkedItems}
                            options={resourceTypes}/>

                    <Select options={sortTypes} clicked={changeSortingHandler} defaultName="Sort by date"/>
                  {toggleResourcesGroupModal && <Button
                                                      type='button'
                                                      size='lg'
                                                      onClick={toggleResourcesGroupModal}
                                                    >
                                                      add new
                                                    </Button>
                  }
                </div>
            </Header>
        </div>
    );
};

HeaderResources.defaultProps = {
    onClick: null,
    style: {}
};

HeaderResources.propTypes = {
    headerText: PropTypes.string,
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node,
};