import React from 'react';
import Icon from '../../../components/UI/Elements/Icon';

export const ImgComponent = ({img, extension}) => {
    switch (extension) {
        case 'xls':
        case 'xlsx':
            return img = <div className='name'>
                <Icon name='xls'/>.{extension}
            </div>;
        case 'doc':
        case 'docx':
        case 'pdf':
            return img = <div className='name'>
                <Icon name='xls'/>.{extension}
            </div>;
        case 'mp3':
        case 'mov':
        case 'mp4':
        case 'avi':
            return img = <div className='name'>
                <Icon name='zip'/>.{extension}
            </div>;
        case 'rar':
        case 'zip':
            return img = <div className='name'>
                <Icon name='zip'/>.{extension}
            </div>;
        default:
            return img = <img src={img} alt=''/>;
    }
};