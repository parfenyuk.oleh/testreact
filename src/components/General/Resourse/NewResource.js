import React, {Fragment} from 'react';
import Modal from '../../UI/Modal/Modal';
import PropTypes from 'prop-types';
import ModalHeader from '../../UI/Modal/ModalHeader';
import ModalBody from '../../UI/Modal/ModalBody';
import ModalRow from '../../UI/Modal/ModalRow';
import Input from '../../UI/Input';
import icons from '../../../helpers/iconsLoader';
import Button from '../../UI/Elements/Button';
import Cell from '../../UI/Cell';
import DatePicker from '../../UI/Datepicker';
import moment from 'moment';
import SelectMaterial from '../../UI/Elements/SelectMaterial';
import Textarea from '../../UI/Textarea';
import DocumentLoader from '../../UI/DocumentLoader';

export const NewResource = ({
                                show,
                                saved,
                                hideModal,
                                handleChange,
                                heading,
                                activeItem,
                                description,
                                groupList,
                                errors,
                                startDate,
                                endDate,
                                avatar,
                                fieldUpdated,
                                document,
                                currentResource,
                                isLoading
                            }) => {
    const descriptionText = 'description';
    const documentText = 'document';
    const startDateResource = 'startDateResource';
    const endDateResource = 'endDateResource';
    
    if (startDate.length && endDate.length) {
        startDate = moment(startDate);
        endDate = moment(endDate);
    }

    return (
        <Modal show={show} hideModal={hideModal}>
            <ModalHeader>
                {
                    currentResource ? <h3 className='modal-title'>Edit resource</h3>
                        :
                        <h3 className='modal-title'>Add resource</h3>
                }
            </ModalHeader>
            {
                errors.length > 0 && (
                    <div className='auth-errors' style={{marginBottom: '-50px'}}>
                        <ul>
                            {errors.map((item, key) => <li key={key}><img src={icons.iconAttention} alt=''/> {item}
                            </li>)}
                        </ul>
                    </div>
                )
            }
            <ModalBody bigPadding overflow={true}>
                <div>
                    <ModalRow>
                        <SelectMaterial
                            handleChange={(e) => {
                              e.target.name = 'activeId';
                              handleChange(e)}
                            }
                            placeholder={!groupList.length ? "None" : null}
                            inputLabel="Choose resources group"
                            activeItem={activeItem}
                            itemList={groupList}

                        />
                    </ModalRow>
                    <div>
                        <ModalRow flex>
                            <Cell column={7}>
                                <DatePicker placeholder={'Start date'}
                                            changed={(value) => {
                                                handleChange(value.format('l'), startDateResource)
                                            }}
                                            selected={startDate}
                                />
                            </Cell>
                            <Cell column={2}/>
                            <Cell column={7}>
                                <DatePicker placeholder={'End date'}
                                            changed={(value) => {
                                                handleChange(value.format('l'), endDateResource)
                                            }}
                                            selected={endDate}
                                />
                            </Cell>
                        </ModalRow>
                    </div>
                    <ModalRow>
                        <div style={{marginTop: '-15px'}}>
                            <Input placeholder='Resource Heading' defaultValue={heading} changed={handleChange}
                                                       name='heading'/>
                            <Textarea placeholder='Description'
                                      changed={(e) => { fieldUpdated(e.target.value, descriptionText) }}
                                      defaultValue={description}
                                      name='description'/>

                            <DocumentLoader
                                         trigger={<Button
                                             type='button'
                                             size='sm'
                                             context='secondary'
                                         >
                                             upload new
                                         </Button>}
                                         document={document}
                                         changed={(e) => { fieldUpdated(e, documentText) }}
                            />
                        </div>
                    </ModalRow>
                </div>
                <ModalRow button>
                    <Button
                        onClick={saved}
                        type='button'
                        size='xl'
                        disabled={isLoading}
                    >
                        Save
                    </Button>
                </ModalRow>
            </ModalBody>
        </Modal>
    );
};

NewResource.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};