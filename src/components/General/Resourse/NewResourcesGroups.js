import React, {Fragment} from 'react';
import Modal from '../../UI/Modal/Modal';
import PropTypes from 'prop-types';
import ModalHeader from '../../UI/Modal/ModalHeader';
import ModalBody from '../../UI/Modal/ModalBody';
import ModalRow from '../../UI/Modal/ModalRow';
import Input from '../../UI/Input';
import icons from "../../../helpers/iconsLoader";
import Button from "../../UI/Elements/Button";
import Cell from "../../UI/Cell";
import DatePicker from "../../UI/Datepicker";
import moment from "moment";

export const NewResourcesGroups = ({
                                       show,
                                       saved,
                                       hideModal,
                                       handleChange,
                                       name,
                                       activeItem,
                                       description,
                                       privacyTypeList,
                                       errors,
                                       startDate,
                                       endDate,
                                  }) => {
    if (startDate.length > 0 && endDate.length > 0) {
        startDate = moment(startDate);
        endDate = moment(endDate);
    }

    return (
        <Modal show={show} hideModal={hideModal}>
            <ModalHeader>
                {
                    startDate  ? <h3 className="modal-title">Edit Resources Groups</h3>
                        :
                        <h3 className="modal-title">New Resources Groups</h3>
                }
            </ModalHeader>
            {
                errors.length > 0 && (
                    <div className="auth-errors" style={{marginBottom: '-50px'}}>
                        <ul>
                            {errors.map((item, key) => <li key={key}><img src={icons.iconAttention} alt=""/> {item}
                            </li>)}
                        </ul>
                    </div>
                )
            }
            <ModalBody bigPadding overflow={true}>
                <div>
                    <ModalRow flex>
                        <Cell column={7}>
                            <DatePicker placeholder={'Start date'}
                                        changed={(value) => { handleChange(value.format('l'), 'startDate') }}
                                        selected={startDate}
                            />
                        </Cell>
                        <Cell column={2}/>
                        <Cell column={7}>
                            <DatePicker placeholder={'End date'}
                                        changed={(value) => { handleChange(value.format('l'), 'endDate') }}
                                        selected={endDate}
                            />
                        </Cell>
                    </ModalRow>
                    <ModalRow>
                        <div style={{marginTop: '-15px'}}>
                            <Input placeholder="Name" defaultValue={name} changed={handleChange} name="name"/>
                        </div>
                    </ModalRow>
                </div>
                <ModalRow button>
                    <Button
                        onClick={saved}
                        type="button"
                        size="xl"
                        className=""
                    >
                        Save
                    </Button>
                </ModalRow>
            </ModalBody>
        </Modal>
    );
};

NewResourcesGroups.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};