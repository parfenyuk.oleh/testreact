import React from 'react';
import moment from 'moment';
import {ImgComponent} from './ImgComponent';

export const ResourceDetail = ({
                                   activeResource,
                                   backgroundColor
                               }) => {
    let extension, startDate, endDate;

    if (activeResource && activeResource.url) {
        extension = activeResource.url.split('.');
        extension = extension.slice(-1)[0].toLowerCase();

        startDate = moment(activeResource.startDate).format('DD.MM.YYYY');
        endDate = moment(activeResource.endDate).format('DD.MM.YYYY');
    }

    if(activeResource) {
        return (
            <div className='resource-detail'>
                <div className='block'>
                    <div className='img-block' style={{backgroundColor: backgroundColor}}>
                        <ImgComponent img={activeResource.url} extension={extension}/>
                    </div>
                    <table>
                        <tbody>
                        <tr>
                            <td>Name:</td>
                            <td>{activeResource.resourceHeading}.{extension}</td>
                        </tr>
                        <tr>
                            <td>Start date:</td>
                            <td>{startDate}</td>
                        </tr>
                        <tr>
                            <td>End date:</td>
                            <td>{endDate}</td>
                        </tr>
                        <tr />
                        <tr>
                            <td>Description:</td>
                            <td>{activeResource.description}</td>
                        </tr>
                        <tr />
                        <tr />
                        <tr>
                            <td>Size:</td>
                            <td>{activeResource.fileSize} MB</td>
                        </tr>
                        <tr>
                            <td>Owner:</td>
                            <td>{activeResource.ownerName}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        );
    } else {
        return (
            <div className='resource-detail'>
                <div className='block'>

                </div>
            </div>
        );
    }
};
