import React, {Fragment} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Button from '../../UI/Elements/Button';
import {ResourseItem} from './index';
import {withInfiniteScroll} from '../../../hoc/withInfiniteScroll';

const ResourceListWrapper = ({
                                      list,
                                      removeResource,
                                      updateResource,
                                      changeActiveResources
                                  }) => {
    return (
            <ul className='resources-list'>
                {
                    list.map((item) => {
                        let extension;

                        if (item.url) {
                            extension = item.url.split('.');
                            extension = extension.slice(-1)[0].toLowerCase();
                        }

                        const date = moment(item.startDate).format('DD.MM.YYYY');

                        return <ResourseItem key={item.id}
                                             item={item}
                                             date={date}
                                             removeResource={removeResource}
                                             updateResource={updateResource}
                                             extension={extension}
                                             changeActiveResources={changeActiveResources}
                        />
                    })
                }
            </ul>
    );
};

export const ResourceList = withInfiniteScroll(ResourceListWrapper);

ResourceList.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};