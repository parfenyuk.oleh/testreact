import React, {Fragment} from 'react';
import moment from 'moment';
import PropTypes from 'prop-types';
import Button from '../../UI/Elements/Button';
import {ResourseItem, ResourceList} from './index';
import {compose} from 'recompose';
import {withInfiniteScroll} from '../../../hoc/withInfiniteScroll';
import Preloader from '../../UI/Preloader';
import EmptyState from '../../UI/EmptyState';

export const ResourceMainBlock = ({
                                      resources,
                                      showResourcesModal,
                                      removeResource,
                                      updateResource,
                                      changeActiveResources,
                                      skip,
                                      onPaginated,
                                      isLoading
                             }) => {
    return (
        <div className='resources-info'>
          {removeResource && <div style={{marginBottom: '25px'}}>
              <Button
                type='button'
                size='lg'
                onClick={showResourcesModal}
              >
                add file
              </Button>
            </div>
          }
            {
                resources.length ? (<ResourceList
                    list={resources}
                    lengthArray={resources.length}
                    isLoading={isLoading}
                    skip={skip}
                    onPaginated={onPaginated}
                    removeResource={removeResource}
                    updateResource={updateResource}
                    changeActiveResources={changeActiveResources}
                />) : (
                    <div className='emptyState-wrapper'>
                        {
                            isLoading ? <Preloader/> : <EmptyState/>
                        }
                    </div>
                )
            }

        </div>
    );
};

const AdvancedList = compose(
    withInfiniteScroll,
)(ResourceList);

ResourceMainBlock.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};