import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Icon from '../../UI/Elements/Icon';

export const ResourcesSidebar = ({
                                     resources,
                                     activeId,
                                     removeResourceGroup,
                                     editResourceGroup,
                                     setActiveElement,
                                 }) => {
    return (
        <div className='resources-sidebar'>
            <ul>
                {
                    resources.map((item, key) => {
                        const startDate = moment(item.startDate).format('DD MMM YYYY');
                        const endDate = moment(item.endDate).format('DD MMM YYYY');

                        return <li className={item.id === activeId ? 'active' : ''}
                                   onClick={() => setActiveElement(item.id)}
                                   key={key}>
                            {item.name}
                            <span className='date'>
                            {startDate} - {endDate}
                            </span>
                          {removeResourceGroup && <div className='icons'>
                              <Icon name='edit-dark' onClick={() => {
                                editResourceGroup(item.id)
                              }}/>
                              <Icon name='delete-dark' onClick={() => {
                                removeResourceGroup(item.id)
                              }}/>
                            </div>
                          }
                        </li>
                    })
                }
            </ul>
        </div>
    );
};

ResourcesSidebar.propTypes = {
    show: PropTypes.bool,
    hideModal: PropTypes.func,
    fieldUpdated: PropTypes.func,
    saved: PropTypes.func
};