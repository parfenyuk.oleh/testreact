import React, {Component, Fragment} from 'react';
import {ImgComponent, DropDown} from './index';
import Icon from '../../UI/Elements/Icon';
import {getRandomOnlineColor} from '../../../helpers/randomColor';
import fileDownload from 'js-file-download';
import axios from '../../../axios-instance';
import {userAuthToken} from '../../../helpers';

export class ResourseItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      backgroundColor: getRandomOnlineColor(),
    };
  }

  toggleDropdown = () => this.setState({open: !this.state.open});

  downloadFile = item => {
    let extension;

    if (item.url) {
      extension = item.url.split('.');
      extension = extension.slice(-1)[0].toLowerCase();
    }

    axios({
      url: item.url,
      method: 'GET',
      crossdomain: true,
      responseType: 'blob', // important
      headers: {
        Authorization: userAuthToken.token(),
        'Access-Control-Allow-Origin': '*',
      },
    }).then(response => {
      fileDownload(response.data, `${item.resourceHeading}.${extension}`);
    })


  };

  render() {
    const {open, backgroundColor} = this.state;
    const {item, date, extension, removeResource, updateResource, changeActiveResources} = this.props;

    return <li onClick={() => {changeActiveResources(item.id)}}>
      <div className='img-block' style={{backgroundColor: backgroundColor}}>
        <ImgComponent img={item.url} extension={extension}/>
      </div>
      <div className='caption'>
        <div className='info'>
                                <span className='name'>
                                {item.resourceHeading}.{extension}
                                </span>
          <span className='date'>
                                Created {date}
                                </span>
        </div>
        {
          !open ? <div className='wrapper'>
            <button className='more-button' onClick={this.toggleDropdown}>
              <Icon name='more-full'/>
            </button>
          </div> : <button className='more-button' onClick={this.toggleDropdown}>
            <Icon name='more-full'/>
          </button>
        }
        <DropDown item={item}
                  showDropdown={open}
                  downloadFile={this.downloadFile}
                  updateResource={updateResource}
                  removeResource={removeResource}
                  toggleDropdown={this.toggleDropdown}/>
      </div>
    </li>
  }
}