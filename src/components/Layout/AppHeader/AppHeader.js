import React from 'react';
import {NavLink, withRouter} from 'react-router-dom';

import logo from '../../../images/logo.svg';

import {REGISTRATION, LOGIN} from '../../../constants/routes';

const AppHeader = () => {
    return (
        <header className='head'>
            <div className='container'>
                <div className='head-logo'>
                    <img src={logo} alt=''/>
                </div>
                <nav className='head-nav'>
                    <NavLink to='/s' className='head-link'>Home</NavLink>
                    <NavLink to='/s' className='head-link'>About us</NavLink>
                    <NavLink to='/s' className='head-link'>Pricing</NavLink>
                    <NavLink to={LOGIN} className='head-link'>Login</NavLink>
                    <NavLink to={REGISTRATION} className='head-link'>Sign up</NavLink>
                </nav>
            </div>
        </header>
    );
};

export default withRouter(AppHeader);
