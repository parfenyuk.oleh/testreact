import React from 'react';
import {Link} from 'react-router-dom';
import teacherLogo from '../../../images/logoteacher.png';
import icons from '../../../helpers/iconsLoader';
import OutsideEventListener from './OutsideEventListener'
import Icon from '../../UI/Elements/Icon'
import AvatarBlock from '../../UI/Elements/AvatarBlock';
import ROLES from '../../../constants/roles';

const AppHeader = ({university, showUniversityModal, showDropDown, dropDownVisibility, logout, toggleChats, user, toggleSidebar}) => {
  if(user.role === ROLES.TEACHER || user.role === ROLES.STUDENT){
    showUniversityModal = () => {};
  }
  return (
  <header className='header' style={{backgroundColor : university.color || '#0079C4'}}>
    <div className='header-logo'>
        <span onClick={toggleSidebar} className="burger">
            <Icon name='menu-burger'/>
        </span>
      <img src={teacherLogo} alt='logo'/>
    </div>
    <div className='header-body' >
        <h1 className='header-title' onClick={showUniversityModal}>
            <div className={`header-title__add ${university.avatar ? 'avatar' : ''}`}>
                <img src={university.avatar ? university.avatar : icons.iconPlus} alt=''/>
            </div>
            <span className='header-title__text'>{university.name ? university.name : 'MY UNIVERSITY'}</span>
        </h1>
      <div className='header-control'>
        {/* <div className='header-search'>*/}
        {/*  <img src={icons.iconSearch} alt='search'/>*/}
        {/*  <input type='text' placeholder='Search' style={{display: 'none'}}/>*/}
        {/*</div>*/}
        <div className='header-controls'>
          {/*<div className='header-controls__messages active' onClick={toggleChats}>*/}
          {/*  <img src={icons.iconMessage} alt='messages'/>*/}
          {/*</div>*/}
          {/*<div className='header-controls__globe'>*/}
          {/*  <img src={icons.iconGlobe} alt='globe'/>*/}
          {/*</div>*/}
          <div className='header-controls__profile' >
              {
                  !dropDownVisibility ?
                      <div className='wrapper-img'>
                        <AvatarBlock textAvatar={`${user.firstName[0]}${user.lastName[0]}`}
                                     onClick={() => showDropDown()}
                                     imgSrc={user.avatar}
                        />
                      </div>
                      : <AvatarBlock textAvatar={`${user.firstName[0]}${user.lastName[0]}`}
                                     onClick={() => showDropDown()}
                                     imgSrc={user.avatar}
                    />
              }
              {
                  dropDownVisibility && (
                      <OutsideEventListener showDropDown={showDropDown} >
                          <div className='profileDropDown'>
                              <Link to={'/profile'}>
                                  <Icon name='group-member'/>
                                  Profile
                              </Link>
                              <Link to={'/payment'}>
                                  <Icon name='credit-card'/>
                                  Payment
                              </Link>
                              <button onClick={logout}>
                                  <Icon name='logout'/>
                                  Log out
                              </button>
                          </div>
                      </OutsideEventListener>
                  )
              }
          </div>
        </div>
      </div>
    </div>
  </header>
)};


export default AppHeader;
