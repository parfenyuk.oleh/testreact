import React, {Fragment} from 'react';
import {Route, Switch} from "react-router-dom";
import * as routeConstants from "../../../constants/routes";
import RegistrationFlow from "../../../containers/Admin/RegistrationFlow/RegistrationFlow";
import AppHeader from "../../../components/Layout/AppHeader/AppHeader";

export const AuthorizedUserGrid = () => {
    return (
        <Fragment>
            <AppHeader/>
            <Switch>
                <Route exact path={routeConstants.REGISTRATION_FLOW} component={RegistrationFlow}/>
            </Switch>
        </Fragment>
    )};
