import {Fragment} from 'react';
import {Route, Switch} from 'react-router-dom';
import React from 'react';
import AppHeaderAuth from '../AppHeader/AppHeaderAuth';
import SidebarNavigation from '../SidebarNavigation/SidebarNavigation';
import {UserRouter} from '../UserRout';
import Profile from '../../../containers/General/Profile';
import SidebarChats from '../SidebarChats/SidebarChats';
import * as routes from '../../../constants/routes';

export const RegistrationFlowGrid = ({
                                         toggleChats,
                                         showUniversityModal,
                                         showDropDown,
                                         dropDownVisibility,
                                         logout,
                                         auth,
                                         university,
                                         show,
                                         user,
                                         toggleSidebar,
                                         fullSidebar
}) => {
    return (
        <Fragment>
            <AppHeaderAuth toggleChats={toggleChats}
                           showUniversityModal={showUniversityModal}
                           showDropDown={showDropDown}
                           dropDownVisibility={dropDownVisibility}
                           logout={logout}
                           user={auth.user}
                           university={university}
                           toggleSidebar={toggleSidebar}
            />
            <div className='app-body'>
                <SidebarNavigation user={user} fullSidebar={fullSidebar}/>
                <main>
                    <UserRouter user={user}/>
                    <Route exact path={routes.PROFILE} component={Profile}/>
                </main>
                <SidebarChats show={show}/>
            </div>
        </Fragment>
    )};
