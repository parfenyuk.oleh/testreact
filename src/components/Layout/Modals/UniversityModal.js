import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import * as actions from '../../../store/actions';

import Modal from '../../UI/Modal/Modal';
import Input from '../../UI/Input';
import ImageLoader from '../../UI/imageLoader';
import imageLoadApi from '../../../helpers/imageLoadApi';
import Swal from 'sweetalert2';


class UniversityModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
      avatar: this.props.avatar,
      color: this.props.color,
      key: this.props.initialName,
      type: this.props.type,
    };
  }

  colorClickHandler = color => { this.setState({color: color}); };

  inputChangeHandler = (value, field) => {
    const state = {...this.state};
    state[field] = value;
    this.setState(state);
  };

  saveHandler = () => {

    const {token, institutionId} = this.props.auth.user;
    if (typeof this.state.avatar !== 'string' && this.state.avatar) {
      imageLoadApi(this.state.avatar, this.props.auth.user.token)
        .then(response => {
          if (response.data.ok) {
            this.props.updateInstitution(token, institutionId, {
              ...this.state,
              avatar: response.data.result,
            });
          }
          else throw new Error('Cant upload file')

        })
        .catch(e => {
          Swal({
            title: 'Error!',
            text: e,
            type: 'error',
            timer: 10000000,
          });
        })
    }
    else this.props.updateInstitution(token, institutionId, {...this.state});
    this.props.hideModal('university');
  };


  render() {
    const {colors, show, hideModal} = this.props;
    const {name, avatar, key} = this.state;
    return (
      <Modal show={show} hideModal={() => {hideModal('university')}}>
        <div className="modal-header">
          <h3 className="modal-title">Dashboard Style</h3>
        </div>
        <div className="modal-body">
          <div className="modal-row">
            <Input placeholder="Institution name"
                   value={name}
                   changed={(e) => { this.inputChangeHandler(e.target.value, 'name')}}
            />
            <Input placeholder="Institution initial name"
                   value={key}
                   changed={(e) => { this.inputChangeHandler(e.target.value, 'key')}}
            />
          </div>
          <div className="modal-row">
            <p className="modal-label">Select dashboard style</p>
            {colors.map(color => (<div className={`colorPickerColor ${this.state.color === color ? 'active' : ''}`}
                                       style={{backgroundColor: color}}
                                       key={color}
                                       onClick={() => {this.colorClickHandler(color)}}
            />))}
          </div>
          <div className="modal-row">
            <p className="modal-label">Select institution logo</p>
            <ImageLoader placeholder={{
              text: 'A',
              image: avatar,
            }}
                         trigger={<button className="button-default small">UPLOAD NEW</button>}
                         changed={(image) => {this.inputChangeHandler(image, 'avatar')}}
            />
          </div>
          <div className="modal-row button">
            <button className="big-primary" onClick={this.saveHandler}>SAVE AND CONTINUE</button>
          </div>
        </div>
      </Modal>
    );
  }
}

UniversityModal.propTypes = {
  hideModal: PropTypes.func,
  colors: PropTypes.array,
  name: PropTypes.string,
  avatar: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object,
  ]),
  color: PropTypes.string,
  show: PropTypes.bool,
  initialName: PropTypes.string,
  save: PropTypes.func.isRequired,
};

UniversityModal.defaultProps = {
  colors: ['#6EA6E8', '#74C283', '#F9D949', '#F59540', '#F66C6C', '#F08BB9', '#696CD4', '#6D6D6D', '#F5F5F5'],
  name: '',
  avatar: '',
  color: '',
  initialName: '',
};

const mapStateToProps = ({auth}) => ({auth});

const mapDispatchToProps = dispatch => ({
  hideModal: modal => { dispatch(actions.hideModal(modal)) },
  updateInstitution: (token, id, data) => {dispatch(actions.updateInstitution(token, id, data))},
});

export default connect(mapStateToProps, mapDispatchToProps)(UniversityModal);