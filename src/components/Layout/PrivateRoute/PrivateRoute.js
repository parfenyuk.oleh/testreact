import React, {Fragment} from 'react';
import { Route, Redirect } from 'react-router-dom';
import Layout from "../../../containers/General/Layout/Layout";

export const PrivateRoute = ({ component: Component, ...rest }) => {
   return <Route {...rest} render={props => {
      return localStorage.getItem('user')
            ? <div>
               <Component {...props} />
            </div>
            : <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
   }}/>
};

