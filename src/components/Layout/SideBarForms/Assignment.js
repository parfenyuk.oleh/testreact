import React, {Component} from 'react';
import moment from 'moment';
import Icon from '../../UI/Elements/Icon'
import Input from '../../UI/Input';
import Textarea from '../../UI/Textarea';
import Cell from '../../UI/Cell';
import DatePicker from '../../UI/Datepicker';
import ModalRow from '../../UI/Modal/ModalRow';
import ModalCheckBox from '../../UI/Elements/ModalCheckbox';
import AddLink from '../../UI/Elements/AddLink';
import Upload from '../../../containers/General/Upload/Upload'

import {connect} from 'react-redux';
import withSure from '../../../hoc/withSure';
import validate from '../../../helpers/validator';
import {createAssignment, getAllAssessments, getAssignmentInfo, updateAssignment, clearCurrentAssessment} from '../../../store/actions/assessments.actions';
import {hideModal, closeWithConfirm} from '../../../store/actions/modals.action';
import {MODAL_TYPES} from '../../../constants/modalsTypes';
import documentLoadApi from '../../../helpers/documentLoadApi';

class Assignment extends Component {
    state = {
        name: {
            value: '',
            error: false
        },
        description: {
            value: '',
            error: false
        },
        startDate: {
            value: '',
            error: false
        },
        endDate: {
            value: '',
            error: false
        },
        weight: {
            value: '',
            error: false
        },
        links: [],
        isGroup: false,
        newFiles: [],
        oldFiles: [],
        isExcluded: false,
        isClicked: false,
    };

    inputChangeHandler = (value, type) => {
      this.props.closeWithConfirm();
        this.setState({
            [type] : {
                value: value,
                error: false
            }
        })
    };

    saveHandler = async () => {
        const {token, institutionId} = this.props.auth.user;
        let {name, description, startDate, endDate, weight, isClicked, isExcluded} = this.state;
        const data = await this.collectData();
        const endDateValidationRules = {isNotEmptyDate : true, notLessDate: moment(startDate.value)};
        const errors = [];

        name.error = validate('Title', name.value, {isNotEmpty: true});
        description.error = validate('Description', description.value, {isNotEmpty: true});
        startDate.error = validate('Start Date', startDate.value, {isNotEmptyDate : true});
        endDate.error = validate('End Date', endDate.value, {isNotEmptyDate : true});
        if(!isExcluded) {
            weight.error = validate('Weight', weight.value, {
                isNotEmpty: true,
                weightLimit: true,
                isNotEqualToZero: true
            });
        }
        endDate.error = validate('End Date', moment(endDate.value), endDateValidationRules);


        if (name.error) {errors.push(name.error)}
        if (description.error) {errors.push(description.error)}
        if (startDate.error) {errors.push(startDate.error)}
        if (endDate.error) {errors.push(endDate.error)}
        if (weight.error) {errors.push(weight.error)}

        this.setState({name, description, startDate, endDate, weight});

        if(!errors.length) {

          if(this.props.modals.type === MODAL_TYPES.UPDATE_ASSIGNMENT){
            this.props.updateAssignment(token, institutionId, data, this.props.modals.action.assignmentId)
              .then(({status}) => {
                if(status) {
                  this.props.hideModal();
                  this.getAllAssessments();
                  this.props.clearCurrentAssessment();
                  this.setState({isClicked: false});
                }
              });
          } else {
            this.props.createAssignment(token, institutionId, data)
              .then(({status}) => {
                if(status) {
                  this.props.hideModal();
                  this.getAllAssessments();
                }
              });
          }

        }

    };

  getAllAssessments = () => {
    const {auth, getAllAssessments} = this.props;

    getAllAssessments(auth.user.institutionId, this.props.modals.action.subjectId, auth.user.token);
  };

    collectData = async () => {
        const {name, description, startDate, endDate, links, weight, isGroup, newFiles, oldFiles, isExcluded} = this.state;
          const fileUrls = await Promise.all(newFiles.map(item => {
            return documentLoadApi(item, 'Bearer ' + this.props.auth.user.token);
          }));
          let attachments = [].concat(fileUrls.map(imgData => ({attachmentType: 2, url: imgData.url, fileName: imgData.fileName})))
            .concat(links.map(item => ({attachmentType: 1, url: item})))
            .concat(oldFiles);

          return {
              name: name.value,
              description: description.value,
              subjectId: this.props.modals.action.subjectId,
              startDate: startDate.value,
              endDate: endDate.value,
              assessmentResponsibilityType: isGroup ? 2 : 1,
              weight: weight.value,
              attachments,
              isExcluded,
            };

    };

    changeLink = (data) => {
      this.props.closeWithConfirm();
      this.setState({
          links: data
      })
    };

    componentDidMount(){

      const {auth, getAssignmentInfo, modals} = this.props;
      if(modals.type === MODAL_TYPES.UPDATE_ASSIGNMENT){
        getAssignmentInfo(auth.user.institutionId, modals.action.assignmentId, auth.user.token)
          .then(result => {
            this.setState({
              name: {
                value: result.name,
                error: false
              },
              description: {
                value: result.description,
                error: false
              },
              startDate: {
                value: result.startDate,
                error: false
              },
              endDate: {
                value: result.endDate,
                error: false
              },
              weight: {
                value: result.weight.toString(),
                error: false
              },
              links: result.assessmentAttachments.filter(item => item.attachmentType === 1).map(item => item.url),
              isGroup: result.assessmentResponsibilityType === 2,
              oldFiles: result.assessmentAttachments.filter(item => item.attachmentType === 2),
              isExcluded: result.isExcluded,
            })

          });
      }

    }

  setGroupHandler = () => {
    this.props.closeWithConfirm();
    this.setState({isGroup: !this.state.isGroup})
    };

    removeLinkHandler = linkOrder => {
      this.props.closeWithConfirm();
      // this.setState({links: this.state.links.splice(linkOrder, 1)})
    };

  fileUploadHandler = files => {
    this.props.closeWithConfirm();
    this.setState({newFiles: files});
  }

  removeOldFile = index => {
    this.props.closeWithConfirm();
    const {oldFiles} = this.state;
    oldFiles.splice(index, 1);
    this.setState({oldFiles});
  };

  setExcluded = () => {
    this.props.closeWithConfirm();
    this.setState({
    isExcluded: !this.state.isExcluded,
    weight: {
      value: '0',
      error: false
    },
  })};

    render() {
    const {name, description, weight, startDate, endDate, links, isGroup, oldFiles, isExcluded} = this.state;
    return (
        <div className="sidebar-content">
        <div className="popup-container">
            <div className="form-container">
                <Input placeholder="Title"
                       defaultValue={name.value}
                       changed={(e) => {this.inputChangeHandler(e.target.value, 'name')}}
                       className={name.error ? 'error' : ''}
                       modalError={name.error}
                />
                <Textarea placeholder="Description"
                          text={description.value}
                          changed={(e) => {this.inputChangeHandler(e.target.value, 'description') }}
                          error={description.error}
                          modalError={description.error}
                />
                <ModalRow flex>
                    <Cell column={7}>
                        <DatePicker placeholder={'Start date'}
                                    changed={(value) => {this.inputChangeHandler(value, 'startDate')}}
                                    selected={startDate.value}
                                    error={startDate.error}
                                    modalError={startDate.error}
                        />
                    </Cell>
                    <Cell column={2}/>
                    <Cell column={7}>
                        <DatePicker placeholder={'End date'}
                                    changed={(value) => {this.inputChangeHandler(value, 'endDate')}}
                                    selected={endDate.value}
                                    error={endDate.error}
                                    modalError={endDate.error}
                                    popperPlacement={'bottom-end'}
                        />
                    </Cell>

                </ModalRow>
                <ModalRow flex>
                    <Cell column={7}>
                        <Input placeholder="Weight (%)"
                               value={weight.value}
                               changed={(e) => {this.inputChangeHandler(e.target.value, 'weight') }}
                               className={weight.error ? 'error' : ''}
                               modalError={isExcluded ? null : weight.error}
                               disabled={isExcluded}
                               type={'number'}
                        />
                               <div className={'tool-tip ' + (weight.error ? 'error' : '' )}>
                                   <Icon name='info'/>
                                   <div className="tool-tip-text-assignment">
                                       The weight represents the<br/>
                                       percentage of the final grade.
                                   </div>
                               </div>
                    </Cell>
                    <Cell column={2}/>
                    <Cell column={7} className="exclude-container">
                      <ModalCheckBox value={isExcluded}
                                     onChange={this.setExcluded}
                      /> Exclude
                      <div className={'tool-tip'}>
                        <Icon name='info'/>
                        <div className="tool-tip-text">
                          This assessment will not be taken into account when calculating the final grade.
                        </div>
                      </div>
                    </Cell>
                </ModalRow>
              {this.props.modals.type !== MODAL_TYPES.UPDATE_ASSIGNMENT && <div className="checkbox-item">
                <ModalCheckBox value={isGroup}
                               onChange={this.setGroupHandler}
                /> Group work
              </div>
              }
                <AddLink placeholder="Insert link"
                         onNewLinkAdded = {this.changeLink}
                         defaultLinks={links}
                />
               <Upload links={links}
                       removeLink={this.removeLinkHandler}
                       onFileUpload={this.fileUploadHandler}
                       oldFiles={oldFiles}
                       removeOldFile={this.removeOldFile}
               />
            </div>

        </div>

            <div className = "bottomButton">

                <button
                    disabled={this.state.isClicked}
                    type="button"
                    className="btn-modal"
                    onClick={this.saveHandler}>
                  {this.props.modals.type === MODAL_TYPES.UPDATE_ASSIGNMENT ? 'Update' : 'Create'}
                </button>
            </div>
        </div>
        )
    }
}

const mapStateToProps = ({auth, groups, admin, assignments, modals}) => ({auth, groups, admin, assignments, modals});
const mapDispatchToProps = dispatch => ({
    createAssignment: (token, id, data) => dispatch(createAssignment(token, id, data)),
    updateAssignment: (token, id, data, assignmentId) => dispatch(updateAssignment(token, id, data, assignmentId)),
    hideModal: () => {dispatch(hideModal())},
    clearCurrentAssessment: () => {dispatch(clearCurrentAssessment())},
    getAssignmentInfo: (institutionId, assessmentId, userToken) => dispatch(getAssignmentInfo(institutionId, assessmentId, userToken)),
    getAllAssessments: (institutionId, subjectId, userToken) => dispatch(getAllAssessments(institutionId, subjectId, userToken)),
    closeWithConfirm: () => {dispatch(closeWithConfirm())}
});
export default connect(mapStateToProps, mapDispatchToProps)(withSure(Assignment));
