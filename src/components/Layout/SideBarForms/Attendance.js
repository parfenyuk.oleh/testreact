import React, {Component} from 'react';
import Input from '../../UI/Input';
import Textarea from '../../UI/Textarea';
import Cell from '../../UI/Cell';
import DatePicker from '../../UI/Datepicker';
import ModalRow from '../../UI/Modal/ModalRow';
import {connect} from 'react-redux';
import * as actions from '../../../store/actions/assessments.actions'
import withSure from '../../../hoc/withSure';
import Icon from '../../UI/Elements/Icon';
import ModalCheckBox from '../../UI/Elements/ModalCheckbox';
import validate from '../../../helpers/validator';
import {closeWithConfirm, hideModal} from '../../../store/actions/modals.action';
import {getAllAssessments} from '../../../store/actions/assessments.actions';
import {updateAttendance, clearCurrentAssessment, getAttendanceInfo} from '../../../store/actions/assessments.actions';
import {MODAL_TYPES} from '../../../constants/modalsTypes';
import moment from 'moment';

class Attendance extends Component {
  state = {
    name: {
      value: '',
      error: false,
    },
    description: {
      value: '',
      error: false,
    },
    startDate: {
      value: '',
      error: false,
    },
    endDate: {
      value: '',
      error: false,
    },
    weight: {
      value: '',
      error: false,
    },

    isExcluded: false,
    isClicked: false
  };

  saveHandler = () => {
    const {token, institutionId} = this.props.auth.user;
    let {name, description, startDate, endDate, weight, isExcluded} = this.state;
    const data = this.collectData();
    const endDateValidationRules = {isNotEmptyDate: true};
    const errors = [];
    if (this.props.restore) {
      endDateValidationRules.notLessDateThenNow = true;
    }

    name.error = validate('Title', name.value, {isNotEmpty: true});
    description.error = validate('Description', description.value, {isNotEmpty: true});
    startDate.error = validate('Start Date', startDate.value, {isNotEmptyDate : true});
    endDate.error = validate('End Date', endDate.value, endDateValidationRules);
    if(!isExcluded) {
      weight.error = validate('Weight', weight.value, {isNotEmpty: true, weightLimit: true, isNotEqualToZero: true});
    }
    if (name.error) {errors.push(name.error)}
    if (description.error) {errors.push(description.error)}
    if (startDate.error) {errors.push(startDate.error)}
    if (endDate.error) {errors.push(endDate.error)}
    if (weight.error) {errors.push(weight.error)}

    this.setState({name, description, startDate, endDate, weight});

    if(!errors.length){
      this.setState({isClicked: true});
      if(this.props.modals.type === MODAL_TYPES.UPDATE_ATTENDANCE) {
        this.props.updateAttendance(token, institutionId, data, this.props.modals.action.attendanceId)
          .then(({status}) => {
            if (status) {
              this.props.hideModal();
              this.getAllAssessments();
              this.props.clearCurrentAssessment();
            }
          });
      } else {
        this.props.createAttendance(token, institutionId, data)
          .then(({status}) => {
            console.log(status);
            if (status) {
              this.props.hideModal();
              this.getAllAssessments();
            }
          });
      }
    }
  };

  componentDidMount() {
    const {auth, getAttendanceInfo, modals} = this.props;
    if(modals.type === MODAL_TYPES.UPDATE_ATTENDANCE){
      getAttendanceInfo(auth.user.institutionId, modals.action.attendanceId, auth.user.token)
        .then(result => {
          this.setState({
            name: {
              value: result.name,
              error: false
            },
            description: {
              value: result.description,
              error: false
            },
            startDate: {
              value: result.startDate,
              error: false
            },
            endDate: {
              value: result.endDate,
              error: false
            },
            weight: {
              value: result.weight.toString(),
              error: false
            },
            isExcluded: result.isExcluded,
          })
        })
    }
  }

  collectData = () => {
    const {name, description, startDate, endDate, weight, isExcluded} = this.state;
    const assignmentModel = {
      name: name.value,
      description: description.value,
      startDate: startDate.value,
      endDate: endDate.value,
      weight: weight.value,
      subjectId: this.props.modals.action.subjectId,
      isExcluded,
    };
    return assignmentModel;
  };

  getAllAssessments = () => {
    const {auth, getAllAssessments} = this.props;

    getAllAssessments(auth.user.institutionId, this.props.modals.action.subjectId, auth.user.token);
  };

  inputChangeHandler = (value, type) => {
    this.props.closeWithConfirm();
    this.setState({
      [type]: {
        value: value,
        error: false,
      },
    })
  };

  setExcluded = () => {
    this.props.closeWithConfirm();
    this.setState({
    isExcluded: !this.state.isExcluded,
    weight: {
      value: '0',
      error: false
    },
  })};

  render() {
    const {name, description, weight, startDate, endDate, isExcluded} = this.state;
    return (
      <div className="sidebar-content">
        <div className="popup-container">
          <div className="form-container">
            <Input placeholder="Title"
                   defaultValue={name.value}
                   changed={(e) => {this.inputChangeHandler(e.target.value, 'name')}}
                   className={name.error ? 'error' : ''}
                   modalError={name.error}
            />
            <Textarea placeholder="Description"
                      text={description.value}
                      changed={(e) => {this.inputChangeHandler(e.target.value, 'description') }}
                      error={description.error}
                      modalError={description.error}
            />
            <ModalRow flex>
              <Cell column={7}>
                <DatePicker placeholder={'Start date'}
                            changed={(value) => {this.inputChangeHandler(value, 'startDate')}}
                            selected={startDate.value}
                            error={startDate.error}
                            modalError={startDate.error}
                />
              </Cell>
              <Cell column={2}/>
              <Cell column={7}>
                <DatePicker placeholder={'End date'}
                            changed={(value) => {this.inputChangeHandler(value, 'endDate')}}
                            selected={endDate.value}
                            error={endDate.error}
                            modalError={endDate.error}
                            popperPlacement={'bottom-end'}
                />
              </Cell>

            </ModalRow>
            <ModalRow flex>
              <Cell column={7}>
                <Input placeholder="Weight (%)"
                       value={weight.value}
                       changed={(e) => {this.inputChangeHandler(e.target.value, 'weight') }}
                       className={weight.error ? 'error' : ''}
                       modalError={isExcluded ? null : weight.error}
                       disabled={isExcluded}
                       type={'number'}
                />
                <div className={'tool-tip ' + (weight.error ? 'error' : '')}>
                  <Icon name="info"/>
                  <div className="tool-tip-text">
                    The final grade will be recalculated<br/>
                    <br/>Example<br/>
                    <span>Final grade = weight × your  mark<br/>(50 = 50% × 100)</span>
                  </div>
                </div>
              </Cell>
              <Cell column={2}/>
              <Cell column={7} className="exclude-container">
                <ModalCheckBox value={isExcluded}
                               onChange={this.setExcluded}
                /> Exclude
                <div className={'tool-tip'}>
                  <Icon name='info'/>
                  <div className="tool-tip-text">
                    This assessment will not be taken into account when calculating the final grade.
                  </div>
                </div>
              </Cell>
            </ModalRow>
          </div>
        </div>
        <div className="bottomButton">
          <button
            type="button"
            className="btn-modal"
            onClick={this.saveHandler}>
            {this.props.modals.type === MODAL_TYPES.UPDATE_ATTENDANCE ? 'Update' : 'Create'}
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({auth, groups, admin, modals}) => ({auth, groups, admin, modals});
const mapDispatchToProps = dispatch => ({
  createAttendance: (token, id, data) => dispatch(actions.createAttendance(token, id, data)),
  hideModal: () => {dispatch(hideModal())},
  getAllAssessments: (institutionId, subjectId, userToken) => dispatch(getAllAssessments(institutionId, subjectId, userToken)),
  getAttendanceInfo: (institutionId, workspaceId, userToken) => dispatch(getAttendanceInfo(institutionId, workspaceId, userToken)),
  updateAttendance: (token, id, data, workspaceId) => dispatch(updateAttendance(token, id, data, workspaceId)),
  clearCurrentAssessment: () => {dispatch(clearCurrentAssessment())},
  closeWithConfirm: () => {dispatch(closeWithConfirm())}
});
export default connect(mapStateToProps, mapDispatchToProps)(withSure(Attendance));