import React, {Component} from 'react';
import Input from '../../UI/Input';
import Textarea from '../../UI/Textarea';
import Cell from '../../UI/Cell';
import DatePicker from '../../UI/Datepicker';
import ModalRow from '../../UI/Modal/ModalRow';
import {connect} from 'react-redux';
import * as actions from '../../../store/actions/assessments.actions'
import withSure from '../../../hoc/withSure';
import AddLink from '../../UI/Elements/AddLink';
import Icon from '../../UI/Elements/Icon';
import Close from '../../../images/cross.svg';
import ModalCheckBox from '../../UI/Elements/ModalCheckbox';
import validate from '../../../helpers/validator';
import {closeWithConfirm, hideModal} from '../../../store/actions/modals.action';
import {getAllAssessments} from '../../../store/actions/assessments.actions';
import {MODAL_TYPES} from '../../../constants/modalsTypes';
import {getWorkspaceInfo, updateWorkspace} from '../../../store/actions/assessments.actions';
import moment from 'moment';
import {clearCurrentAssessment} from '../../../store/actions/assessments.actions';

class Workspace extends Component {
  state = {
    name: {
      value: '',
      error: false,
    },
    description: {
      value: '',
      error: false,
    },
    startDate: {
      value: '',
      error: false,
    },
    endDate: {
      value: '',
      error: false,
    },
    weight: {
      value: '',
      error: false,
    },
    wordsLimit: {
      value: null,
      error: false,
    },
    keywords: [],
    isExcluded: false,
    isClicked: false
  };

  saveHandler = () => {
    const {token, institutionId} = this.props.auth.user;
    let {name, description, startDate, endDate, weight, keywords, wordsLimit, isExcluded} = this.state;
    const data = this.collectData();

    this.setState({
      name,
      description,
      startDate,
      endDate,
      weight,
      wordsLimit,
      keywords,
    });
    const endDateValidationRules = {isNotEmptyDate: true};
    const errors = [];
    if (this.props.restore) {
      endDateValidationRules.notLessDateThenNow = true;
    }

    name.error = validate('Title', name.value, {isNotEmpty: true});
    description.error = validate('Description', description.value, {isNotEmpty: true});
    startDate.error = validate('Start Date', startDate.value, {isNotEmptyDate: true});
    endDate.error = validate('End Date', endDate.value, endDateValidationRules);
    if(!isExcluded) {
      weight.error = validate('Weight', weight.value, {isNotEmpty: true, weightLimit: true, isNotEqualToZero: true});
    }
    wordsLimit.error = validate('Word count', wordsLimit.value, {isNotEmptyDate: true, isNotEqualToZero: true, intRange: true});

    if (name.error) {errors.push(name.error)}
    if (description.error) {errors.push(description.error)}
    if (startDate.error) {errors.push(startDate.error)}
    if (endDate.error) {errors.push(endDate.error)}
    if (wordsLimit.error) {errors.push(wordsLimit.error)}
    if (weight.error) {errors.push(weight.error)}

    this.setState({
      name,
      description,
      startDate,
      endDate,
      weight,
      wordsLimit,
      keywords,
    });

    if(!errors.length) {
      this.setState({isClicked: true});
      if (this.props.modals.type === MODAL_TYPES.UPDATE_WORKSPACE) {
        this.props.updateWorkspace(token, institutionId, data, this.props.modals.action.workspaceId)
          .then(({status}) => {
            if (status) {
              this.props.hideModal();
              this.props.clearCurrentAssessment();
              this.getAllAssessments();
            }
          });
      } else {
        this.props.createWorkspace(token, institutionId, data)
          .then(({status}) => {
            if (status) {
              this.props.hideModal();
              this.getAllAssessments();
            }
          });
      }
    }
  };

  collectData = () => {
    const {name, description, startDate, endDate, weight, wordsLimit, keywords, isExcluded} = this.state;
    const assignmentModel = {
      name: name.value,
      description: description.value,
      startDate: startDate.value,
      endDate: endDate.value,
      weight: weight.value,
      subjectId: this.props.modals.action.subjectId,
      wordsLimit: wordsLimit.value,
      keywords: keywords.map(item => {
        return {
          name: item,
        }
      }),
      isExcluded
    };
    return assignmentModel;
  };

  componentDidMount() {
    const {auth, getWorkspaceInfo, modals} = this.props;
    if(modals.type === MODAL_TYPES.UPDATE_WORKSPACE){
      getWorkspaceInfo(auth.user.institutionId, modals.action.workspaceId, auth.user.token)
        .then(result => {
          this.setState({
            name: {
              value: result.name,
              error: false
            },
            description: {
              value: result.description,
              error: false
            },
            startDate: {
              value: result.startDate,
              error: false
            },
            endDate: {
              value: result.endDate,
              error: false
            },
            weight: {
              value: result.weight.toString(),
              error: false
            },
            wordsLimit: {
              value: result.wordsLimit,
              error: false,
            },
            keywords: result.keywords,
            isExcluded: result.isExcluded,
          })
        })
    }
  }

  getAllAssessments = () => {
    const {auth, getAllAssessments} = this.props;

    getAllAssessments(auth.user.institutionId, this.props.modals.action.subjectId, auth.user.token);
  };

  inputChangeHandler = (value, type) => {
    this.props.closeWithConfirm();
    this.setState({
      [type]: {
        value: value,
        error: false,
      },
    })
  };

  changeKeyWord = (data) => {
    this.props.closeWithConfirm();
    this.setState({
      keywords: data,
    });

  };

  closeLink = (keywordsIndex) => {
    this.props.closeWithConfirm();
    const {keywords} = this.state;
    keywords.splice(keywordsIndex, 1);
    this.setState({
      keywords: keywords,
      isDragOver: false,
    })
  };

  setExcluded = () => {
    this.props.closeWithConfirm();
    this.setState({
    isExcluded: !this.state.isExcluded,
    weight: {
      value: '0',
      error: false
    },
  })};

  render() {
    const {errors, name, description, weight, startDate, endDate, wordsLimit, keywords, isExcluded} = this.state;
    return (
      <div className='sidebar-content'>
        <div className="popup-container">
          <div className='form-container'>
            <Input placeholder="Title"
                   defaultValue={name.value}
                   changed={(e) => {this.inputChangeHandler(e.target.value, 'name')}}
                   className={name.error ? 'error' : ''}
                   modalError={name.error}
            />
            <Textarea placeholder="Description"
                      text={description.value}
                      changed={(e) => {this.inputChangeHandler(e.target.value, 'description') }}
                      error={description.error}
                      modalError={description.error}
            />
            <ModalRow flex>
              <Cell column={7}>
                <DatePicker placeholder={'Start date'}
                            changed={(value) => {this.inputChangeHandler(value, 'startDate')}}
                            selected={startDate.value}
                            error={startDate.error}
                            modalError={startDate.error}
                />
              </Cell>
              <Cell column={2}/>
              <Cell column={7}>
                <DatePicker placeholder={'End date'}
                            changed={(value) => {this.inputChangeHandler(value, 'endDate')}}
                            selected={endDate.value}
                            error={endDate.error}
                            modalError={endDate.error}
                            popperPlacement={'bottom-end'}
                />
              </Cell>

            </ModalRow>
            <ModalRow flex>
              <Cell column={7}>
                <Input placeholder="Weight (%)"
                       value={weight.value}
                       changed={(e) => {this.inputChangeHandler(e.target.value, 'weight') }}
                       className={weight.error ? 'error' : ''}
                       modalError={isExcluded ? null : weight.error}
                       disabled={isExcluded}
                       type={'number'}
                />
                <div className={'tool-tip ' + (weight.error ? 'error' : '')}>
                  <Icon name='info'/>
                  <div className="tool-tip-text">
                    The final grade will be recalculated<br/>
                    <br/>Example<br/>
                    <span>Final grade = weight × your  mark<br/>(50 = 50% × 100)</span>
                  </div>
                </div>
              </Cell>
              <Cell column={2}/>
              <Cell column={7} className="exclude-container">
                <ModalCheckBox value={isExcluded}
                               onChange={this.setExcluded}
                /> Exclude
                <div className={'tool-tip'}>
                  <Icon name='info'/>
                  <div className="tool-tip-text">
                    This assessment will not be taken into account when calculating the final grade.
                  </div>
                </div>
              </Cell>
            </ModalRow>
            <div className="count-container">
              <Input placeholder="Word count"
                     defaultValue={wordsLimit.value}
                     changed={(e) => {this.inputChangeHandler(e.target.value, 'wordsLimit')}}
                     className={wordsLimit.error ? 'error' : ''}
                     modalError={wordsLimit.error}
                     type={'number'}
              />
            </div>
            <div className="count-container">
              <AddLink placeholder="Insert keyword"
                       onNewLinkAdded={this.changeKeyWord}
                       defaultLinks={keywords}
              />
            </div>
            <div className="keywords-workspace">
              {keywords.map((keywords, keywordsIndex) => {
                return (
                  <div className="keywords-item"
                       key={keywords}>
                    <div className="img-wrapper">
                      <div className="img-wrapper__keywords">
                        <p>{keywords}</p>
                        <img
                          src={Close}
                          alt="close"
                          className="close"
                          onClick={() => {this.closeLink(keywordsIndex)}}
                        />
                      </div>
                    </div>
                  </div>
                )
              })
              }
            </div>


          </div>
        </div>
        <div className="bottomButton">
          <button
            type="button"
            className="btn-modal"
            onClick={this.saveHandler}>
            {this.props.modals.type === MODAL_TYPES.UPDATE_WORKSPACE ? 'Update' : 'Create'}
          </button>
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({auth, groups, admin, modals}) => ({auth, groups, admin, modals});
const mapDispatchToProps = dispatch => ({
  createWorkspace: (token, id, data) => dispatch(actions.createWorkspace(token, id, data)),
  hideModal: () => {dispatch(hideModal())},
  getAllAssessments: (institutionId, subjectId, userToken) => dispatch(getAllAssessments(institutionId, subjectId, userToken)),
  getWorkspaceInfo: (institutionId, workspaceId, userToken) => dispatch(getWorkspaceInfo(institutionId, workspaceId, userToken)),
  updateWorkspace: (token, id, data, workspaceId) => dispatch(updateWorkspace(token, id, data, workspaceId)),
  clearCurrentAssessment: (token, institutionId, userId, assessmentId) => {dispatch(clearCurrentAssessment(token, institutionId, userId, assessmentId))},
  closeWithConfirm: () => {dispatch(closeWithConfirm())}
});
export default connect(mapStateToProps, mapDispatchToProps)(withSure(Workspace));