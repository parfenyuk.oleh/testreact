import React from 'react';
import {NavLink} from 'react-router-dom'
import * as routeConstants from '../../../constants/routes';
import Icon from '../../UI/Elements/Icon'
import * as routes from '../../../constants/routes';

export const AdminSidebar = ({fullSidebar}) => (
  <aside className={fullSidebar ? 'navigation active' : 'navigation'}>
      <nav className='navigation-list'>
        <NavLink exact to='/' className='navigation-link'>
          <Icon name='home'/>
          <span>Main</span>
        </NavLink>
        <NavLink exact to={routes.DASHBOARD} className='navigation-link'>
          <Icon name='main'/>
          <span>Dashboard</span>
        </NavLink>
        <NavLink to={routeConstants.EVENTS} className='navigation-link'>
          <Icon name='events'/>
          <span>Events</span>
        </NavLink>
        <NavLink to={routeConstants.NOTICE_BOARD} className='navigation-link'>
          <Icon name='notice-board'/>
          <span>Notice Board</span>
        </NavLink>
      </nav>
  </aside>
);
