import React from 'react';
import {NavLink} from 'react-router-dom'
import * as routeConstants from '../../../constants/routes';
import Icon from '../../UI/Elements/Icon';
import WithModal from './WithModal';
import * as actions from '../../../store/actions/modals.action';
import * as routes from '../../../constants/routes';
import {connect} from 'react-redux';

const StudentsSidebar = ({fullSidebar, modals}) => (
    <aside className={'navigation ' + (fullSidebar ? 'active ' : '') + (modals.show ? 'with-modal' : '')}>
        <WithModal/>
        <nav className='navigation-list'>

            <NavLink to={routeConstants.DASHBOARD} className='navigation-link'>
                <Icon name='main'/>
                <span>Dashboard</span>
            </NavLink>
            <NavLink to={routeConstants.ASSESSMENTS} className='navigation-link'>
                <Icon name='assignments'/>
                <span>Assessments</span>
            </NavLink>
            <NavLink to={routeConstants.EVENTS} className='navigation-link'>
                <Icon name='events'/>
                <span>Events</span>
            </NavLink>
            <NavLink to={routes.FORUM} className='navigation-link'>
                <Icon name='forum'/>
                <span>Forum</span>
            </NavLink>
            <NavLink to={routeConstants.NOTICE_BOARD} className='navigation-link'>
                <Icon name='notice-board'/>
                <span>Notice Board</span>
            </NavLink>
            <NavLink to={routeConstants.RESOURCES} className='navigation-link'>
                <Icon name='resources'/>
                <span>Resource</span>
            </NavLink>
        </nav>
    </aside>
);

const mapStateToProps = ({modals}) => ({modals});

const mapDispatchToProps = dispatch => ({
    showModal: (modalType) => {
        dispatch(actions.showModal(modalType))
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(StudentsSidebar);