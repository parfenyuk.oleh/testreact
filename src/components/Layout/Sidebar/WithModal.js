import React from 'react';
import {connect} from 'react-redux';
import icons from '../../../helpers/iconsLoader';
import * as actions from '../../../store/actions/modals.action';
import Select from '../../UI/Select';
import Assignment from '../SideBarForms/Assignment';
import Attendance from '../SideBarForms/Attendance';
import Workspace from '../SideBarForms/Workspace';
import AssignmentsModal from '../../Student/Assignments/AssignmentsModal';
import AssignmentsModalMark from '../../Student/Assignments/AssignmentsModalMark';
import {MODAL_TYPES, MODALS_TITLES} from '../../../constants/modalsTypes';
import {clearCurrentAssessment} from '../../../store/actions/assessments.actions';
import AssignmentsModalWorkspace from '../../Student/Assignments/AssignmentsModalWorkspace';
import AssignmentAttendance from '../../Student/Assignments/AssignmentAttendance';
import AssignmentSubmit from '../../Student/Assignments/AssignmentSubmit';
import WorkspaceMark from '../../Student/Assignments/WorkspaceMark';
import confirm from '../../../images/pic_confirm_1_wrong_way.svg';
import Swal from 'sweetalert2';
import WorkspaceSubmit from '../../Student/Assignments/WorkspaceSubmit';
import Assessments from '../../../containers/Teacher/Assessments/Assessments';
import SetAssementMark from '../../../containers/Teacher/Assessments/SetAssementMark';

const WithModal = ({
   modals,
   hideModal,
   showModal,
                     clearCurrentAssessment
}) => {
    let title = null;

    const titleChangeHandler = (event) =>{
        showModal(
            event.type,
            MODALS_TITLES[event.type],
            modals.action,
        )
    };

    if (typeof modals.titles === 'string') {
        title = <span className="modals-title">{modals.titles}</span>;
    }
    else if (modals.titles !== null){
        title = (
            <div className='modal-select'>
                <Select placeholder='Assignment'
                        options={modals.titles}
                        clicked={titleChangeHandler}
                />
            </div>
            )
        }

    const renderSwitch = () => {
        switch (modals.type) {
            case MODAL_TYPES.CREATE_ASSIGNMENT:
                return <Assignment/>;
            case MODAL_TYPES.UPDATE_ASSIGNMENT:
                return <Assignment/>;
            case MODAL_TYPES.CREATE_ATTENDANCE:
                return <Attendance/>;
            case MODAL_TYPES.UPDATE_ATTENDANCE:
                return <Attendance/>;
            case MODAL_TYPES.CREATE_WORKSPACE:
                return <Workspace/>;
            case MODAL_TYPES.UPDATE_WORKSPACE:
                return <Workspace/>;
            case MODAL_TYPES.SHOW_ASSIGNMENT_STUDENT:
                return <AssignmentsModal/>;
          case MODAL_TYPES.SHOW_ASSIGNMENT_MARK_STUDENT:
            return <AssignmentsModalMark/>;
          case MODAL_TYPES.SHOW_WORKSPACE_STUDENT:
            return <AssignmentsModalWorkspace/>;
          case MODAL_TYPES.SHOW_ATTENDANCE_STUDENT:
            return  <AssignmentAttendance/>;
          case MODAL_TYPES.PATCH_ASSIGNMENT:
            return <AssignmentSubmit/>;
          case MODAL_TYPES.SHOW_WORKSPACE_MARK_STUDENT:
            return  <WorkspaceMark/>;
          case MODAL_TYPES.PATCH_WORKSPACE:
            return  <WorkspaceSubmit/>;
          case  MODAL_TYPES.SET_ASSIGNMENT_MARK:
          case  MODAL_TYPES.SET_ATTENDANCE_MARK:
          case  MODAL_TYPES.SET_WORKSPACE_MARK:
            return <SetAssementMark/>;
            default:
                return null
        }
    };

    function hideModalHandler() {
      if (modals.isChanged){
        Swal.fire({
        title: 'Are you sure?',
        text: "If you close this form, all data will be lost.",
        showCancelButton: true,
        reverseButtons: true,
        showCloseButton: true,
        confirmButtonText: 'YES, CLOSE',
        cancelButtonText: 'NO, CANCEL',
        buttonsStyling: false,
        cancelButtonClass: 'cancel-button',
        confirmButtonClass: 'confirm-button',
        imageUrl: confirm,
        imageHeight: 200,
        imageAlt: 'Confirm',
        height: '500px',
        width: '400px',
        customClass: 'confirm-assessments'
      }).then((result) => {
        if (result.value) {
          hideModal();
          clearCurrentAssessment();
        }
    })} else {
        hideModal();
        clearCurrentAssessment()}
}

    return (
        <div className={modals.show ? 'withModal active' : 'withModal'}>
            <div className='modal-top'>
              <span className="modals-subject">{modals.action.subjectName}</span>
              <div className="modals-subtitle">
                {title}
                <div className="sidebar-close">
                  </div>
                      <img src={icons.iconCross} alt="" onClick={hideModalHandler}/>
                  </div>
            </div>
            {renderSwitch()}
          {modals.show && <div className="withModal-cloak"/>}
        </div>
    )
};

const mapStateToProps = ({modals}) => ({modals});
const mapDispatchToProps = dispatch => ({
    hideModal: (modalType) => {dispatch(actions.hideModal(modalType))},
    showModal: (modalType, titles, action) => {dispatch(actions.showModal(modalType, titles, action))},
  clearCurrentAssessment: () => {dispatch(clearCurrentAssessment())}
});

export default connect (mapStateToProps, mapDispatchToProps)(WithModal);
