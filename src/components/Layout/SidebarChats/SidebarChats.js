import React from 'react';
import SidebarChatsLine from "./SidebarChatsLine";

const SidebarChats = ({show}) => {
  const   users = {
    teachers: [
      {
        id: 1,
        avatar: "https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg",
        firstName: "Jayden",
        lastName: " Boone",
        title: "Advertising Secrets",
        online: false
      },
      {
        id: 2,
        avatar: null,
        firstName: "Jayden",
        lastName: "Boone",
        title: "Advertising Secrets",
        online: true
      },
    ],
    institutions: [
      {
        id: 1,
        avatar: "https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg",
        firstName: "Jayden",
        lastName: " Boone",
        title: "Advertising Secrets",
        online: true
      },
      {
        id: 2,
        avatar: null,
        firstName: "Jayden",
        lastName: "Boone",
        title: "Advertising Secrets",
        online: false
      },
      {
        id: 3,
        avatar: null,
        firstName: "Jayden",
        lastName: "Boone",
        title: "Advertising Secrets",
        online: false
      },
      {
        id: 4,
        avatar: "https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg",
        firstName: "Simon",
        lastName: "Noone",
        title: "Advertising Secrets is powerful thing nowadays",
        online: true
      },
      {
        id: 5,
        avatar: null,
        firstName: "Jayden",
        lastName: "Boone",
        title: "Advertising Secrets",
        online: true
      },
    ],
    students: [
      {
        id: 1,
        avatar: null,
        firstName: "Jayden",
        lastName: "Boone",
        title: "Advertising Secrets",
        online: true
      },
      {
        id: 2,
        avatar: "https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg",
        firstName: "Simon",
        lastName: "Noone",
        title: "Advertising Secrets is powerful thing nowadays",
        online: false
      },
      {
        id: 3,
        avatar: null,
        firstName: "Jayden",
        lastName: "Boone",
        title: "Advertising Secrets",
        online: true
      },
    ]
  };

  function toggleItem (e) {
    e.target.parentNode.classList.toggle('closed');
  }

  const renderUsers = type => users[type].map(item => (<SidebarChatsLine user={item} key={item.id}/>));

  return(
    <aside className={`sidebar-online ${ show ? 'show' : ''}`}>
      <div className="sidebar-online__item closed">
        <div className="sidebar-online__control" onClick={toggleItem}>
          <span>institutions</span>
          <span className="sidebar-online__control-sign"></span>
        </div>
        <ul className='sidebar-online__content'>
          { renderUsers('institutions') }
        </ul>
      </div>
      <div className="sidebar-online__item closed">
        <div className="sidebar-online__control" onClick={toggleItem}>
          <span>teachers</span>
          <span className="sidebar-online__control-sign"></span>
        </div>
        <ul className='sidebar-online__content'>
          { renderUsers('teachers') }
        </ul>
      </div>
      <div className="sidebar-online__item closed">
        <div className="sidebar-online__control" onClick={toggleItem}>
          <span>students</span>
          <span className="sidebar-online__control-sign"></span>
        </div>
        <ul className='sidebar-online__content'>
          { renderUsers('students') }
        </ul>
      </div>
    </aside>
  );
};

export default SidebarChats;