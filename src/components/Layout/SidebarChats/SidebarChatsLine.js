import React from 'react';
import RoundAvatar from "../../UI/RoundAvatar";



function formatTitle (string) {
  if (string.length > 20)
    return string.substr(0, 20) + '..';
  return string;
}

const SidebarChatsLine = ({user: {avatar, firstName, lastName, title, online}}) => (
  <li className='sidebar-online__user'>
    <RoundAvatar avatar={avatar} text={`${firstName[0]} ${lastName[0]}`} online={online}/>
    <div className="sidebar-online__user-info">
      <p className="sidebar-online__user-name">{`${firstName} ${lastName}`}</p>
      <p className="sidebar-online__user-title">{formatTitle(title)}</p>
    </div>
  </li>
);

export default SidebarChatsLine;