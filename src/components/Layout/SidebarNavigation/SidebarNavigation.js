import React from 'react';
import {userType} from '../../../constants';
import { AdminSidebar } from '../Sidebar';
import TeacherSidebar from '../Sidebar/TeacherSidebar';
import StudentsSidebar from '../Sidebar/StudentsSidebar';

const SidebarNavigation = (props) => {
    switch (props.user.role) {
        case userType.student:
            return (
                <StudentsSidebar fullSidebar={props.fullSidebar}/>
            );
        case userType.teacher:
            return (
                <TeacherSidebar fullSidebar={props.fullSidebar}/>
            );
        case userType.admin:
            return (
                <AdminSidebar fullSidebar={props.fullSidebar}/>
            );
        case userType.superAdmin:
            return (
                <AdminSidebar fullSidebar={props.fullSidebar}/>
            );
        default:
            return (
                <div>user</div>
            );
        }
    };

export default SidebarNavigation;
