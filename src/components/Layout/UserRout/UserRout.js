import React, {Fragment} from 'react';
import {userType} from "../../../constants";
import {Route, Switch} from 'react-router-dom';
import AdminRoutes from "../../../routes/AdminRoutes";
import TeacherRoutes from "../../../routes/TeacherRoutes";
import StudentsRoutes from "../../../routes/StudentsRoutes";
import Main from "../../../containers/SuperAdmin/Main";
import ConnectAdminUniversity from "../../../containers/SuperAdmin/ConnectAdminUniversity/ConnectAdminUniversity";

export const UserRouter = (props) => {
    switch (props.user.role) {
        case userType.student:
            return (
                <StudentsRoutes/>
            );
        case userType.teacher:
            return (
                <TeacherRoutes/>
            );
        case userType.admin:
            return (
               <AdminRoutes user={props.user}/>
            );
        case userType.superAdmin:
            return (
                <Switch>
                    <Route exact path="/" component={Main}/>
                    <Route path='/connect' component={ConnectAdminUniversity}/>
                </Switch>
            );
        default:
            return (
                <div>user</div>
            );
    }
};