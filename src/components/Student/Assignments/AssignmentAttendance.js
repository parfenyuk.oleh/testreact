import React from 'react';
import icons from '../../../helpers/iconsLoader';
import Icon from '../../UI/Elements/Icon';
import {getStudentAttendance} from '../../../store/actions/assessments.actions';
import connect from 'react-redux/es/connect/connect';
import * as actions from '../../../store/actions/modals.action';
import Input from '../../UI/Input';
import TextArea from '../../UI/Textarea';
import _ from 'lodash';
import {pick} from 'lodash';
import moment from 'moment';
import iconSubmitted from '../../../images/icons/ic_submitted.svg';
import ReactStart from 'react-stars';

class AssignmentAttendance extends React.Component {
  componentDidMount() {
    const {institutionId, token} = this.props.auth.user;
    const assessmentId = this.props.action;
    this.props.getStudentAttendance(institutionId, assessmentId, token);
  }

  getStatusName = (item) => {
    switch (item) {
      case 1:
        return <p>Absent</p>;
      case 2:
        return <p>Present</p>;
      case 3:
        return <p>Late</p>;
      default:
        return 'Error';
    }
  };

  render() {
    const markStatus = (item) => {
      switch (item) {
        case 1:
          return <img className="status-icon" src={icons.iconNotSubmitted} alt="icon not submitted"/>;
        case 2:
          return <img className="status-icon" src={icons.iconSubmitted} alt="icon done"/>;
        case 3:
          return <img className="status-icon" src={icons.iconSubmittedLate} alt="icon submitted late"/>;
        default:
          return 'Error';
      }
    };

    const {studentAttendance} = this.props.assessments;
    return (
      <div className="student-assessment">
        {studentAttendance.userAssessmentAttendancesStats ?
          <div className="attendance-container">
            <div className="attendance-status">
              <p>Absent</p>
              <div className="attendance-mark">
                {studentAttendance.userAssessmentAttendancesStats.absent}
              </div>
            </div>
            <div className="attendance-status">
              <p>Present</p>
              <div className="attendance-mark">
                {studentAttendance.userAssessmentAttendancesStats.present}
              </div>
            </div>
            <div className="attendance-status">
              <p>Late</p>
              <div className="attendance-mark">
                {studentAttendance.userAssessmentAttendancesStats.late}
              </div>
            </div>
            <div className="attendance-status">
              <p>Average mark</p>
              <div className="attendance-mark">
                {studentAttendance.userAssessmentAttendancesStats.averageMark.toFixed(2)}
              </div>
            </div>
            <div className="attendance-status">
              <p>Count of marks</p>
              <div className="attendance-mark">
                {studentAttendance.userAssessmentAttendancesStats.marksCount}
              </div>
            </div>
            <div className="attendance-status">
              <strong>Final grade</strong>
              <div className="attendance-mark">
                {studentAttendance.finalGrade ? studentAttendance.finalGrade : <div className="line">-</div>}
              </div>
            </div>
            <div className="attendance-ranks">
              {studentAttendance.userAssessmentAttendancesFull.map(attendance => {
                  return (

                    <div className="attendance-block">
                      <div className="attendance-date">
                        {moment(attendance.attendanceDate).format('L')}
                      </div>
                      <div className="attendance-rate">
                        <ReactStart half={false}
                                    size={15}
                                    className="attendance-stars"
                                    value={attendance.mark}
                                    color1={'#B3C0CE'}
                                    color2={'#3F8BF0'}
                        />
                      </div>
                      <div className="attendance-status-student">
                        {markStatus(attendance.attendanceMarkType)}
                        <div className="tool-tip-text">{this.getStatusName(attendance.attendanceMarkType)}</div>
                      </div>
                    </div>)
                }
              )}</div>
          </div> :
          null}
      </div>
    )
  }
}

const mapStateToProps = ({auth, user, assessments, action}) => ({auth, user, assessments, action});

const mapDispatchToProps = dispatch => ({
  showModal: (modalType, titles, action) => {
    dispatch(actions.showModal(modalType, titles, action))
  },
  getStudentAttendance: (institutionId, assessmentId, userToken) => dispatch(getStudentAttendance(institutionId, assessmentId, userToken))
});

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentAttendance);