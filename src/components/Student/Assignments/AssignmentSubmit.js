import React from 'react';
import connect from 'react-redux/es/connect/connect';
import * as actions from '../../../store/actions/modals.action';
import AddLink from '../../UI/Elements/AddLink';
import Upload from '../../../containers/General/Upload/Upload';
import {getStudentAssessments, patchAssignment} from '../../../store/actions/assessments.actions';
import documentLoadApi from '../../../helpers/documentLoadApi';
import {showModal} from '../../../store/actions/modals.action';
import {hideModal} from '../../../store/actions/modals.action';
import {closeWithConfirm} from '../../../store/actions/modals.action';

class AssignmentSubmit extends React.Component {

  state = {
    links: [],
    newFiles: [],
    oldFiles: []
  };

  fileUploadHandler = files => {
    this.props.closeWithConfirm();
    this.setState({newFiles: files});
  };

  changeLink = (data) => {
    this.props.closeWithConfirm();
    this.setState({
      links: data
    })
  };

  removeLinkHandler = linkOrder => {
    this.props.closeWithConfirm();
    this.setState({links: this.state.links.splice(linkOrder, 1)})
  };

  removeOldFile = index => {
    this.props.closeWithConfirm();
    const {oldFiles} = this.state;
    oldFiles.splice(index, 1);
    this.setState({oldFiles});
  };

  saveHandler = async () => {
    const {token, institutionId} = this.props.auth.user;
    const data = await this.collectData();
    const {patchAssignment} = this.props;

    patchAssignment(token, institutionId, data)
      .then(status => {
        if (status) {
          this.props.getStudentAssessments(institutionId,token)
        }
      });
    this.props.hideModal();

  };

  collectData = async () => {
    const {links, newFiles, oldFiles} = this.state;
    const fileUrls = await Promise.all(newFiles.map(item => {
      return documentLoadApi(item, 'Bearer ' + this.props.auth.user.token);
    }));
    const test = this.props.modals.action.assignmentId;
    let attachments = [].concat(fileUrls.map(imgData => ({attachmentType: 2, url: imgData.url})))
      .concat(links.map(item => ({attachmentType: 1, url: item})))
      .concat(oldFiles);
    return {
      attachments,
      assessmentId: test
    };

  };

  render() {
    const { links, oldFiles} = this.state;
    return (
      <div className="student-assessment">
        <AddLink placeholder="Insert link"
                 onNewLinkAdded = {this.changeLink}
                 defaultLinks={links}
        />
        <Upload links={links}
                removeLink={this.removeLinkHandler}
                onFileUpload={this.fileUploadHandler}
                oldFiles={oldFiles}
                removeOldFile={this.removeOldFile}
        />
        <div className = "bottomButton">
          <button
            type="button"
            className="btn-modal"
            onClick={this.saveHandler}>
            Submit
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({auth, user, action, modals}) => ({auth, user, action, modals});

const mapDispatchToProps = dispatch => ({
  showModal: (modalType, titles, action) => {dispatch(actions.showModal(modalType, titles, action))},
  hideModal: () => {dispatch(hideModal())},
  patchAssignment: (token, institutionId, data) => dispatch(patchAssignment(token, institutionId, data)),
  closeWithConfirm: () => {dispatch(closeWithConfirm())},
  getStudentAssessments: (institutionId, userToken) => dispatch(getStudentAssessments(institutionId, userToken)),

});

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentSubmit);