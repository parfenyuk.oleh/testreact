import React from 'react';
import {
  getStudentAssessments, getStudentAssignment, getStudentAssignmentMark, getStudentAttendance, getStudentWorkspaceMark
} from '../../../store/actions/assessments.actions';
import {
  hideModal, showModal
} from '../../../store/actions/modals.action';
import * as actions from '../../../store/actions/modals.action';
import connect from 'react-redux/es/connect/connect';
import icons from '../../../helpers/iconsLoader';
import Icon from '../../UI/Elements/Icon';
import {ASSESMENT_TYPES, MODAL_TYPES, MODALS_TITLES} from '../../../constants/modalsTypes';


class AssignmentsCard extends React.Component {

  componentDidMount() {
    const {institutionId, token} = this.props.auth.user;
    this.props.getStudentAssessments(institutionId, token)
  }

  studentAssignmentHandler = (assessment, subjectName) => {
    const {showModal} = this.props;
    const {institutionId, token} = this.props.auth.user;
    const {studentAssignment} = this.props.assessments;

    switch (assessment.type) {
      case ASSESMENT_TYPES.ASSIGNMENT: {
        this.props.getStudentAssignment(institutionId, assessment.id, token);
        showModal(
          MODAL_TYPES.SHOW_ASSIGNMENT_STUDENT,
          MODALS_TITLES[MODAL_TYPES.SHOW_ASSIGNMENT_STUDENT],
          {
            assignmentId: assessment.id,
            subjectName
          }
        );
      }
        break;
      case ASSESMENT_TYPES.WORKSPACE: {
        this.props.getStudentAssignment(institutionId, assessment.id, token);
        showModal(
          MODAL_TYPES.SHOW_WORKSPACE_STUDENT,
          MODALS_TITLES[MODAL_TYPES.UPDATE_WORKSPACE],
          {
            subjectName
          }
        )
      }
        break;
      case ASSESMENT_TYPES.ATTENDANCE: {
        this.props.getStudentAttendance(institutionId, assessment.id, token);
        showModal(
          MODAL_TYPES.SHOW_ATTENDANCE_STUDENT,
          MODALS_TITLES[MODAL_TYPES.SHOW_ATTENDANCE_STUDENT],
          {
            assignmentId: assessment.id,
            subjectName
          }
        );
      }
        break;
      default:
        return true;
    }
  };

  studentAssignmentMarkHandler = (assessment) => {
    const {showModal} = this.props;
    const {institutionId, token} = this.props.auth.user;

    switch (assessment.type) {
      case ASSESMENT_TYPES.ASSIGNMENT: {
        this.props.getStudentAssignmentMark(institutionId, assessment.id, token);
        showModal(
          MODAL_TYPES.SHOW_ASSIGNMENT_MARK_STUDENT,
          MODALS_TITLES[MODAL_TYPES.SHOW_ASSIGNMENT_MARK_STUDENT],
          {
            assignmentId: assessment.id,
          }
        );
      }
        break;
      case ASSESMENT_TYPES.ATTENDANCE: {
        this.props.getStudentAttendance(institutionId, assessment.id, token);
        showModal(
          MODAL_TYPES.SHOW_ATTENDANCE_STUDENT,
          MODALS_TITLES[MODAL_TYPES.SHOW_ATTENDANCE_STUDENT],
          {
            assignmentId: assessment.id,
          }
        );
      }
        break;
      case ASSESMENT_TYPES.WORKSPACE: {
        this.props.getStudentWorkspaceMark(institutionId, assessment.id, token);
        showModal(
          MODAL_TYPES.SHOW_WORKSPACE_MARK_STUDENT,
          MODALS_TITLES[MODAL_TYPES.SHOW_WORKSPACE_MARK_STUDENT],
          {
            assignmentId: assessment.id,
          }
        );
      }
        break;
      default:
        return true;
    }
  };

  studentAssignmentSubmit = (id) => {
    this.props.showModal(
      MODAL_TYPES.PATCH_ASSIGNMENT,
      MODALS_TITLES[MODAL_TYPES.SHOW_ASSIGNMENT_MARK_STUDENT],
      {
        assignmentId: id,
      }
    );
  };

  studentWorkspaceSubmit = (id) => {
    const {showModal} = this.props;
    const {institutionId, token} = this.props.auth.user;
    this.props.getStudentAssignment(institutionId, id, token);
    showModal(
      MODAL_TYPES.PATCH_WORKSPACE,
      MODALS_TITLES[MODAL_TYPES.SHOW_WORKSPACE_MARK_STUDENT],
      {
        assignmentId: id,
      }
    );
  };

  studentWorkspaceStatus = (assessment) => {
    return (
      assessment.userAssessment.assessmentGroupRole === 2 ?
        <div className="mark-button">
          {assessment.userAssessment.userAssessmentStatus === 2 ?
            <div className="submit"
                 onClick={() => {
                   this.studentWorkspaceSubmit(assessment.id)
                 }
                 }
            >Submit
            </div> :
            assessment.userAssessment.userAssessmentStatus === 3 ?
              <div className="submitted">Submitted</div> :
              <div className="submitted">Submit</div>}
        </div> :
        assessment.userAssessment.userAssessmentStatus === 3 ?
            <div className="mark-button"><div className="submitted">Submitted</div> </div>:
            <div className="mark-button"><div className="submitted">Submit</div></div>
    )
  }

  studentAssignmentStatus = (assessment) => {
    return (assessment.responsibilityType === 1 || assessment.userAssessment.assessmentGroupRole === 2) ?
      <div className="mark-button">
        {assessment.userAssessment.userAssessmentStatus === 2 ?
          <div className="submit"
               onClick={() => {
                 this.studentAssignmentSubmit(assessment.id)
               }
               }
          >Submit
          </div> :

          assessment.userAssessment.userAssessmentStatus === 3 ?
            <div className="submitted">Submitted</div> :
            <div className="submitted">Submit</div>
        }
      </div> :
      null
  }

  studentAssignmentHandlerMark = (assessment) => {
    switch (assessment.type) {
      case 1: {
        return (
          <div>
            {assessment.userAssessment.mark === null ?
              <div>{this.studentAssignmentStatus(assessment)}</div> :
              <div className="mark-item"
                   onClick={() => {
                     this.studentAssignmentMarkHandler(assessment)
                   }
                   }
              >{assessment.userAssessment.mark}</div>}
          </div>
        );
      }
        break;
      case 4: {
        return (
          <div>
            {assessment.userAssessment.mark === null ?
              <div>{this.studentWorkspaceStatus(assessment)}</div> :
              <div className="mark-item"
                   onClick={() => {
                     this.studentAssignmentMarkHandler(assessment)
                   }
                   }
              >{assessment.userAssessment.mark}</div>}
          </div>
        )
      }
        break;
      case 5: {
        return (
          <div>
            {assessment.userAssessment.mark === null ?
              <div className="attendance-status">
                {this.markStatus(assessment.userAssessment.userAssessmentStatus)}
                <div className="mark-item">
                  <div className="line">-</div>
                </div>
              </div> :
              <div className="mark-item"
                   onClick={() => {
                     this.studentAssignmentMarkHandler(assessment)
                   }
                   }
              >{assessment.userAssessment.mark}</div>}
          </div>
        )
      }
        break;
      default:
        return true;
    }
  };

  getStatusName = (item) => {
    switch (item) {
      case 1:
        return <p>Not started</p>;
      case 2:
        return <p>In progress</p>;
      case 3:
        return <p>Submitted</p>;
      case 4:
        return <p>Submitted late</p>;
      case 5:
        return <p>Not submitted</p>;
      case 6:
        return <p>Done</p>;
      default:
        return 'Error';
    }
  };

  markStatus = (item) => {
    switch (item) {
      case 1:
        return <div className="img-container"><img className="status-icon" src={icons.iconAlert} alt="icon alert"/>
          <div className="tool-tip-text">{this.getStatusName(item)}</div>
        </div>;
      case 2:
        return <div className="img-container"><img className="status-icon" src={icons.iconInProgress}
                                                   alt="icon in progress"/>
          <div className="tool-tip-text">{this.getStatusName(item)}</div>
        </div>;
      case 3:
        return <div className="img-container"><img className="status-icon" src={icons.iconSubmitted}
                                                   alt="icon submitted"/>
          <div className="tool-tip-text">{this.getStatusName(item)}</div>
        </div>;
      case 4:
        return <div className="img-container"><img className="status-icon" src={icons.iconSubmittedLate}
                                                   alt="icon submitted late"/>
          <div className="tool-tip-text">{this.getStatusName(item)}</div>
        </div>;
      case 5:
        return <div className="img-container"><img className="status-icon" src={icons.iconNotSubmitted}
                                                   alt="icon not submitted"/>
          <div className="tool-tip-text">{this.getStatusName(item)}</div>
        </div>;
      case 6:
        return <div className="img-container"><img className="status-icon" src={icons.iconDone} alt="icon done"/>
          <div className="tool-tip-text">{this.getStatusName(item)}</div>
        </div>;
      default:
        return 'Error';
    }
  };

  render() {
    const {studentAssessmentsList} = this.props.assessments;

    return (<div className="student-table">
        {studentAssessmentsList.map(subject => {
          return <div className="assignment-card__container">
            <div className="assignment-card__content">
              <div className="assignment-card__subject">{subject.name}</div>
              <div className="inline-container">
                {subject.assessments.map(assessment => {
                  return <div className="content">
                    <div className="assignment-card__item">
                                                <span className="assignment-card__name"
                                                      onClick={() => {
                                                        this.studentAssignmentHandler(assessment, subject.name)}
                                                      }
                                                >{assessment.name}</span>
                      <div className="assignment-card__status">
                        {assessment.userAssessment.mark !== null ? this.markStatus(assessment.userAssessment.userAssessmentStatus) : null}
                        {this.studentAssignmentHandlerMark(assessment)}
                      </div>
                    </div>
                    <div className="assignment-card__weight">{assessment.weight + "%"}</div>
                  </div>
                })}
              </div>
              <div className="bottom-line"/>
              <div className="student-mark">
                <div className="student-mark__item">
                  {subject.mark}
                  <div className="student-tool-tip">
                    <Icon name="info"/>
                    <div className="tool-tip-text">
                      General mark for this subject
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        })}
      </div>
    );
  }
};


const mapStateToProps = ({auth, user, assessments, modals}) => ({auth, user, assessments, modals});

const mapDispatchToProps = dispatch => ({
  showModal: (modalType, titles, action) => {
    dispatch(actions.showModal(modalType, titles, action))
  },
  hideModal: modal => {
    dispatch(hideModal(modal))
  },
  getStudentAssessments: (institutionId, userToken) => dispatch(getStudentAssessments(institutionId, userToken)),
  getStudentAssignment: (institutionId, assessmentId, userToken) => dispatch(getStudentAssignment(institutionId, assessmentId, userToken)),
  getStudentAssignmentMark: (institutionId, assessmentId, userToken) => dispatch(getStudentAssignmentMark(institutionId, assessmentId, userToken)),
  getStudentAttendance: (institutionId, assessmentId, userToken) => dispatch(getStudentAttendance(institutionId, assessmentId, userToken)),
  getStudentWorkspaceMark: (institutionId, assessmentId, userToken) => dispatch(getStudentWorkspaceMark(institutionId, assessmentId, userToken))
});

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentsCard);