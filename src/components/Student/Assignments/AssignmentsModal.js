import React from 'react';
import icons from '../../../helpers/iconsLoader';
import {getStudentAssignment} from '../../../store/actions/assessments.actions';
import connect from 'react-redux/es/connect/connect';
import * as actions from '../../../store/actions/modals.action';
import Input from '../../UI/Input';
import TextArea from '../../UI/Textarea';
import moment from 'moment';
import File from '../../../images/doc_no_bg.svg';
import Link from '../../../images/link.svg';
import AvatarBlock from '../../UI/Elements/AvatarBlock';

class AssignmentsModal extends React.Component {

    componentDidMount() {
      const {institutionId, token} = this.props.auth.user;
      const assessmentId = this.props.action;
      this.props.getStudentAssignment(institutionId, assessmentId, token);
    }

    render() {
      const {studentAssignment} = this.props.assessments;
      const startDate = moment(studentAssignment.startDate).format('L');
      const endDate = moment(studentAssignment.endDate).format('L');
      return (
        <div className="student-assessment">
            <div className="student-info">
                <Input defaultValue={studentAssignment.name}
                       onChange={false}
                       disabled={true}
                />
                <TextArea defaultValue={studentAssignment.description}
                          disabled={true}
                />
            </div>
            <div className="date-container">
                <div className="date-picker" >
                    <img src={icons.iconCalendar} alt='calendar'/>
                    <p>{startDate}</p>
                </div>
                <div className="date-picker" >
                    <img src={icons.iconCalendar} alt='calendar'/>
                    <p>{endDate}</p>
                </div>
            </div>
          <div>
          {studentAssignment.assessmentAttachments ?
            studentAssignment.assessmentAttachments.length?
            <div className="dropzone">
              {studentAssignment.assessmentAttachments ?
                studentAssignment.assessmentAttachments.map(attachments => {
                  return (
                    <div className="img-background">
                      <div className="img-wrapper">
                        {attachments.attachmentType === 1 ?
                          <a target="_blank"
                             href={attachments.url}>
                            <img src={Link}
                                 alt={`${attachments.url}`}
                                 className="uploaded-file"
                                 title={`${attachments.url}`}
                            />
                          </a> :
                          <a href={`${attachments.url}`}
                             target="_blank"
                             download={`${attachments.attachmentHeading}`}>
                            <img src={File}
                                 alt={`${attachments.attachmentHeading}`}
                                 className="uploaded-file"
                                 title={`${attachments.attachmentHeading}`}
                            />
                          </a>
                        }
                      </div>
                      <span>{attachments.attachmentType === 1 ? attachments.url : attachments.attachmentHeading}
                        <div className="tool-tip-text">{attachments.attachmentHeading}</div></span>
                    </div>
                  )
                })
                : null}
            </div>
              : null
            : null}
          </div>

            {studentAssignment.groupNumber ?
            <div className='group-work'>
                <p>Group {studentAssignment.groupNumber}:</p>
                {studentAssignment.groupMembers.map(group =>{
                       return(
                           <div className="student">
                               <div className={group.groupRole === 2 ? 'leader': ''}>
                                 <AvatarBlock imgSrc={group.avatar}
                                              textAvatar={`${group.firstName[0]}${group.lastName[0]}`}
                                 />
                               </div>
                                <p>{group.firstName} {group.lastName}</p>
                           </div>
                       )
                    })}
            </div> :
                    null}
            </div>
      );
    }
}

const mapStateToProps = ({auth, user,assessments, action}) => ({auth, user,assessments, action});

const mapDispatchToProps = dispatch => ({
    showModal: (modalType, titles, action) => {dispatch(actions.showModal(modalType, titles, action))},
    getStudentAssignment: (institutionId, assessmentId, userToken) => dispatch(getStudentAssignment(institutionId, assessmentId, userToken)),
});

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentsModal);