import React from 'react';
import icons from '../../../helpers/iconsLoader';
import Icon from '../../UI/Elements/Icon';
import {getStudentAssignmentMark} from '../../../store/actions/assessments.actions';
import connect from 'react-redux/es/connect/connect';
import * as actions from '../../../store/actions/modals.action';
import Input from '../../UI/Input';
import TextArea from '../../UI/Textarea';

import moment from 'moment';

class AssignmentsModalMark extends React.Component {
  componentDidMount() {
    const {institutionId, token} = this.props.auth.user;
    const assessmentId = this.props.action;
    this.props.getStudentAssignmentMark(institutionId, assessmentId, token);
  }
  render() {
    const {studentAssignment} = this.props.assessments;
    return (
      <div className="student-assessment">
        <div className="student-info">
          <div className="assignment-mark" >
            <p>{studentAssignment.mark}</p>
            <div className='tool-tip '>
              <Icon name='info'/>
              <div className="tool-tip-text">
                The mark affects the final grade
              </div>
            </div>
          </div>
          <TextArea defaultValue={studentAssignment.comment}/>
        </div>
        {studentAssignment.groupNumber &&
          <div className='group-work'>
            <p>Group {studentAssignment.groupNumber}:</p>
            {studentAssignment.groupMembers.map(group =>
                <div className="student">
                  <img src={group.avatar}/>
                  <p>{group.firstName} {group.lastName}</p>
                </div>
            )}
          </div>
        }

      </div>
    );
  }
}

const mapStateToProps = ({auth, user,assessments, action}) => ({auth, user,assessments, action});

const mapDispatchToProps = dispatch => ({
  showModal: (modalType, titles, action) => {dispatch(actions.showModal(modalType, titles, action))},
  getStudentAssignmentMark: (institutionId, assessmentId, userToken) => dispatch(getStudentAssignmentMark(institutionId, assessmentId, userToken))
});

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentsModalMark);