import React from 'react';
import icons from '../../../helpers/iconsLoader';
import Icon from '../../UI/Elements/Icon';
import {getStudentWorkspace} from '../../../store/actions/assessments.actions';
import connect from 'react-redux/es/connect/connect';
import * as actions from '../../../store/actions/modals.action';
import Input from '../../UI/Input';
import TextArea from '../../UI/Textarea';
import {withRouter} from 'react-router';
import {history, userAuthToken} from '../../../helpers';
import * as ROUTES from '../../../constants/routes';
import moment from 'moment';
import AvatarBlock from '../../UI/Elements/AvatarBlock';
import * as routes from '../../../constants/routes';
import {NavLink} from 'react-router-dom';
import {hideModal} from '../../../store/actions/modals.action';

class AssignmentsModalWorkspace extends React.Component {

    componentDidMount() {
        const {institutionId, token} = this.props.auth.user;
        const assessmentId = this.props.action;
        const {getStudentAssignment} = this.props;
        getStudentAssignment(institutionId, assessmentId, token);
    }

    openWorkspace = (assessmentId, groupId, description, isLeader) => {
        history.push({
            pathname: ROUTES.WORKSPACE,
            state: {assessmentId, groupId, description, isLeader},
        });
        this.props.hideModal();
    };

    render() {
        const {studentAssignment} = this.props.assessments;
        const assessmentId = this.props.assessments.studentAssignment.id;
        const groupId = this.props.assessments.studentAssignment.groupId;
        const startDate = moment(studentAssignment.startDate).format('L');
        const endDate = moment(studentAssignment.endDate).format('L');
        return (
            <div className="student-assessment">
                <div className="student-info">
                    <Input defaultValue={studentAssignment.name}
                           disabled={true}
                    />
                    <TextArea defaultValue={studentAssignment.description}
                              disabled={true}/>
                </div>
                <div className="date-container">
                    <div className="date-picker">
                        <img src={icons.iconCalendar} alt='calendar'/>
                        <p>{startDate}</p>
                    </div>
                    <div className="date-picker">
                        <img src={icons.iconCalendar} alt='calendar'/>
                        <p>{endDate}</p>
                    </div>
                </div>
                <div onClick={() => {
                    this.openWorkspace(assessmentId, groupId, studentAssignment.description, studentAssignment.groupRole)
                }} className='workspace-link'>
                    <Icon name='workspaceSubmit'/>
                    <span>Workspace</span>
                </div>
                {studentAssignment.groupNumber ?
                    <div className='group-work'>
                        <p>Group {studentAssignment.groupNumber}:</p>
                        {studentAssignment.groupMembers.map(group => {
                            return (
                                <div className="student">
                                    <div className={group.groupRole === 2 ? 'leader' : ''}>
                                        <AvatarBlock imgSrc={group.avatar}
                                                     textAvatar={`${group.firstName[0]}${group.lastName[0]}`}
                                        />
                                    </div>
                                    <p>{group.firstName} {group.lastName}</p>
                                </div>
                            )
                        })}
                    </div> :
                    null
                }

            </div>
        );
    }
}

const mapStateToProps = ({auth, user, assessments, action}) => ({auth, user, assessments, action});

const mapDispatchToProps = dispatch => ({
    showModal: (modalType, titles, action) => {
        dispatch(actions.showModal(modalType, titles, action))
    },
    hideModal: modal => {
        dispatch(hideModal(modal))
    },
    getStudentAssignment: (institutionId, assessmentId, userToken) => dispatch(getStudentWorkspace(institutionId, assessmentId, userToken))
});

export default connect(mapStateToProps, mapDispatchToProps)(AssignmentsModalWorkspace);