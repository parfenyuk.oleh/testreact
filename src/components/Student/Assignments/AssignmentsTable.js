import React from 'react';
import PropTypes from 'prop-types';
import Table from '../../UI/Table/Table';
import TableHeader from '../../UI/Table/TableHeader';
import Cell from '../../UI/Cell';
import TableBody from './TableBody';

const AssignmentsTable = props => {
  return (
    <Table>
      <TableHeader>
        <Cell>Assignment</Cell>
        <Cell>Subject</Cell>
        <Cell>Open date</Cell>
        <Cell>Due date</Cell>
        <Cell>Leader</Cell>
        <Cell>Performer</Cell>
        <Cell>Status</Cell>
        <Cell> </Cell>
      </TableHeader>
      <TableBody {...props}/>
    </Table>
  );
};

AssignmentsTable.propTypes = {
  list: PropTypes.array,
  userId: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
};

export default AssignmentsTable;