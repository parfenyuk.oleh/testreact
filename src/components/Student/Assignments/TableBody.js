import React, {Fragment} from 'react';

import {withInfiniteScroll} from '../../../hoc/withInfiniteScroll';
import TableRow from '../../UI/Table/TableRow';
import Cell from '../../UI/Cell';
import moment from 'moment';
import AvatarBlock from '../../UI/Elements/AvatarBlock';
import Status from '../../Teacher/Assigments/Status';
import {withRouter} from 'react-router';

import * as ROUTES from '../../../constants/routes';
import {STATUSES} from '../../../constants/assignments';

const TableBody = ({
                     list,
                     userId,
                     history,
                     onSubmit,
                   }) => {
  function openWorkspace(assignmentId) {
    history.push({
      pathname: ROUTES.WORKSPACE,
      state: {assignmentId},
    })
  }

  function isDisables(item) {
    return !(+userId === +item.leaderId && item.status !== STATUSES.ToReview && item.status !== STATUSES.Done);
  }

  return (
    <Fragment>
      {
        list.map(item => (
          <TableRow key={item.id}>
            <Cell className={'assignment-name'}>{item.name}</Cell>
            <Cell>{item.subjectName}</Cell>
            <Cell>{moment(item.startDate).format('l')}</Cell>
            <Cell>{moment(item.endDate).format('l')}</Cell>
            <Cell>
              <AvatarBlock textAvatar={`${item.leaderFirstName[0]}${item.leaderLastName[0]}`}
                           customInfo={<span>{`${item.leaderFirstName} ${item.leaderFirstName}`}</span>}
                           imgSrc={item.leaderAvatar}
              />
            </Cell>
            <Cell>
              <AvatarBlock textAvatar={`#${item.groupId}`}
                           customInfo={<span>{item.groupName}</span>}
              />
            </Cell>
            <Cell>
              <Status status={item.status}/>
            </Cell>
            <Cell className="assignment-buttons" justify={'space-between'}>
              <button className="big-primary review" onClick={() => {openWorkspace(item.id)}}>WORKSPACE</button>
              <button className="button-success"
                      disabled={isDisables(item)}
                      onClick={() => {onSubmit(item.id)}}
              >SUBMIT</button>
            </Cell>
          </TableRow>
        ))
      }
    </Fragment>
  );
};

export default withInfiniteScroll(withRouter(TableBody));