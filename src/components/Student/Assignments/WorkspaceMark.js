import React from 'react';
import icons from '../../../helpers/iconsLoader';
import Icon from '../../UI/Elements/Icon';
import {getStudentWorkspaceMark} from '../../../store/actions/assessments.actions';
import connect from 'react-redux/es/connect/connect';
import * as actions from '../../../store/actions/modals.action';
import Input from '../../UI/Input';
import TextArea from '../../UI/Textarea';
import AvatarBlock from '../../UI/Elements/AvatarBlock';

class WorkspaceMark extends React.Component {

  componentDidMount() {
    const {institutionId, token} = this.props.auth.user;
    const assessmentId = this.props.action;
    this.props.getStudentWorkspaceMark(institutionId, assessmentId, token);
  }

  render() {
    const {studentWorkspaceMark} = this.props.assessments;
    return (
      <div className="student-assessment">
        <div className="student-info">
          <div className="assignment-mark" >
            <p>{studentWorkspaceMark.mark}</p>
            <div className='tool-tip '>
              <Icon name='info'/>
              <div className="tool-tip-text">
                The mark affects the final grade
              </div>
            </div>
          </div>
          <TextArea defaultValue={studentWorkspaceMark.comment}/>
        </div>
        {studentWorkspaceMark.groupNumber ?
          <div className='group-work'>
            <p>Group {studentWorkspaceMark.groupNumber}:</p>
            {studentWorkspaceMark.groupMembers.map(group =>{
              return(
                <div className="student">
                  <div className={group.groupRole === 2? 'leader': ''}>
                    <AvatarBlock imgSrc={group.avatar}
                                 textAvatar={`${group.firstName[0]}${group.lastName[0]}`}
                    />
                  </div>
                  <p>{group.firstName} {group.lastName}</p>
                </div>
              )
            })}
          </div> :
          null
        }
      </div>
    );
  }
}

const mapStateToProps = ({auth, user,assessments, action}) => ({auth, user,assessments, action});

const mapDispatchToProps = dispatch => ({
  showModal: (modalType, titles, action) => {dispatch(actions.showModal(modalType, titles, action))},
  getStudentWorkspaceMark: (institutionId, assessmentId, userToken) => dispatch(getStudentWorkspaceMark(institutionId, assessmentId, userToken))
});

export default connect(mapStateToProps, mapDispatchToProps)(WorkspaceMark);