import React from 'react';
import connect from 'react-redux/es/connect/connect';
import * as actions from '../../../store/actions/modals.action';
import {getStudentAssessments, getStudentWorkspace, patchWorkspace} from '../../../store/actions/assessments.actions';
import {NavLink} from 'react-router-dom';
import * as routes from '../../../constants/routes';
import Icon from '../../UI/Elements/Icon';
import {hideModal} from '../../../store/actions/modals.action';
import {history} from '../../../helpers';
import * as ROUTES from '../../../constants/routes';

class WorkspaceSubmit extends React.Component {

  componentDidMount() {
    const {institutionId, token} = this.props.auth.user;
    const assessmentId = this.props.modals.action.assignmentId;
    this.props.getStudentAssignment(institutionId, assessmentId, token);
  }

  saveHandler = () => {
    const {token, institutionId} = this.props.auth.user;
    const data = this.collectData();
    this.props.patchWorkspace(token, institutionId, data)
      .then(status => {
        if (status) {
          this.props.getStudentAssessments(institutionId,token)
        }
      });
    this.props.hideModal();
  };

  collectData = () => {
    const test = this.props.modals.action.assignmentId;
    return {
      assessmentId: test
    };

  };
  openWorkspace = (assessmentId, groupId, description, isLeader)  => {
    history.push({
      pathname: ROUTES.WORKSPACE,
      state: {assessmentId, groupId, description, isLeader},
    });
    this.props.hideModal();
  };

  render() {
    const {studentAssignment} = this.props.assessments;
    const assessmentId = this.props.assessments.studentAssignment.id;
    const groupId = this.props.assessments.studentAssignment.groupId;

    return (
      <div className="student-assessment">
        <div onClick={() => {this.openWorkspace(assessmentId, groupId, studentAssignment.description, studentAssignment.groupRole)}} className='workspace-link'>
          <Icon name='workspaceSubmit'/>
          <span>Workspace</span>
        </div>
        <div className = "bottomButton">
          <p className="workspace-submit-text">When you press Submit button, teacher will<br/> receive your workspace link</p>
          <button
            type="button"
            className="btn-modal"
            onClick={this.saveHandler}>
            Submit
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({auth, user, action, modals, assessments}) => ({auth, user, action, modals, assessments});

const mapDispatchToProps = dispatch => ({
  showModal: (modalType, titles, action) => {dispatch(actions.showModal(modalType, titles, action))},
  hideModal: () => {dispatch(hideModal())},
  patchWorkspace: (token, institutionId, data) => dispatch(patchWorkspace(token, institutionId, data)),
  getStudentAssignment: (institutionId, assessmentId, userToken) => dispatch(getStudentWorkspace(institutionId, assessmentId, userToken)),
  getStudentAssessments: (institutionId, userToken) => dispatch(getStudentAssessments(institutionId, userToken)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WorkspaceSubmit);