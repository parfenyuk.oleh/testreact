import React, {Fragment} from 'react';
import Header from "./GradeBookTable/Header";
import Body from "./GradeBookTable/Body";

const GradeBookTable = () => (
  <Fragment>
    <Header/>
    <Body/>
  </Fragment>
);

export default GradeBookTable;
