import React from 'react';
import { connect } from 'react-redux';
import Row from "./Row";
import * as actions from "../../../../store/actions/submissions";


const submissions = [
  {
    email:      'greenholt_francesco@kuhlman.io',
    avatar:     'https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg',
    userName:   'Olivia Castillo',
  }
];

const Body = ({showModal}) => submissions.map(item => <Row submission={item} key={item.email} showModal={() => { showModal('review') }}/>);

const mapDispatchToProps = dispatch => ({
  showModal: modal => { dispatch( actions.showModal(modal) ) }
});

export default connect(null,mapDispatchToProps)(Body);