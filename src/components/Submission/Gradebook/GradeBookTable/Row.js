import React from 'react';
import Cell from "../../../UI/Cell";
import RoundAvatar from "../../../UI/RoundAvatar";
import Input from "../../../UI/Input";

const Row = ({submission: { avatar, userName, email }}) => (
  <div className="table-row">
    <Cell column={3.5} className="table-user">{/* style in table.sass */}
      <RoundAvatar
        avatar={avatar}
        text="AB"
      />
      <span className="table-user-name">{userName}</span>
    </Cell>
    <Cell column={3.5} className="table-email">
      {email}
    </Cell>
    <Cell column={2.25} className="table-input">
      <Input value={30} placeholder="Enter" className="percent" changed={()=>{}}/>
    </Cell>
    <Cell column={2.25} className="table-input">
      <Input placeholder="Enter" className="percent" changed={()=>{}}/>
    </Cell>
    <Cell column={2.25} className="table-input">
      <Input  value={40} placeholder="Enter" className="percent" changed={()=>{}}/>
    </Cell>
    <Cell column={2.25} className="table-input">
      <Input  value={"100"} placeholder="Enter" className="percent" changed={()=>{}}/>
    </Cell>
  </div>
);

export default Row;