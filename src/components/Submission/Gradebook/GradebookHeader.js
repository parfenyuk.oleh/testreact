import React from 'react';
import { connect } from 'react-redux';

import * as actions from "../../../store/actions/submissions";
import Select from "../../UI/Select";

const GradeBookHeader = props => (
  <div className="componentHeader">
    <h1 className="page-title">Assignment name / Grade report</h1>
    <div className="componentHeader-controls">
      {/*<button className="big-empty-control-gray" onClick={() => { props.showModal('archive') }}>filter</button>*/}
      <Select placeholder={'Filter by'} options={[
        {name: 'Type 1', value: 1},
        {name: 'Type 2', value: 2},
        {name: 'Type 3', value: 3},
        {name: 'Type 4', value: 4},
      ]}/>
    </div>
  </div>
);

const mapDispatchToProps = dispatch => ({
  showModal: modal => { dispatch(actions.showModal(modal)) }
});

export default connect(null,mapDispatchToProps)(GradeBookHeader);