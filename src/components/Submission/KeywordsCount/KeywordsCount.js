import React from 'react';

const KeywordsCount = ({count, name}) => (<div className="keywordsCount"><span>{`${count} Keywords`}</span>{name}</div>);

export default KeywordsCount;