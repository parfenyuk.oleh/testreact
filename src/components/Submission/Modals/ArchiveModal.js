import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/submissions';

import Modal from "../../UI/Modal/Modal";
import DatePicker from "../../UI/Datepicker";
import Select from "../../UI/Select";
import Input from "../../UI/Input";


const ArchiveModal = props => (
  <Modal show={props.show} hideModal={() => {props.hideModal('archive')}} className="tiny">
    <h3 className="modal-title">Archive Submissions</h3>
    <div className="modal-row first">
      <DatePicker/>
    </div>
    <div className="modal-row">
      <Select label="Subject"
              placeholder="Select Subject"
      />
    </div>
    <div className="modal-row">
      <Select label="Select Student"
              placeholder="Select Student"
      />
    </div>
    <div className="modal-row">
      <Select label="Select Group"
              placeholder="Select Group"
      />
    </div>
    <div className="modal-row">
      <Input label="Assignment Name"
             placeholder="Type to search"
      />
    </div>
    <div className="modal-row">
      <button className="big-primary">
        FILTER
      </button>
    </div>
  </Modal>
);

const mapDispatchToProps = dispatch => ({
  hideModal: modal => { dispatch( actions.hideModal(modal) ) }
});

export default connect(null, mapDispatchToProps)(ArchiveModal);