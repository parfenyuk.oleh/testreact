import React, {Component} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../store/actions/submissions';
import Modal from '../../UI/Modal/Modal';
import Cell from '../../UI/Cell';
import Mark from '../../UI/Mark';
import SubmissionComment from '../../UI/SubmissionComment';

const comments = [
  {
    name: 'Chester Vaughn / Teacher',
    time: '3 sec ego',
    text: 'Today, many people rely on computers to do homework, work, and create or store useful information. Therefore, it is important for the information on the computer to be stored and kept properly. '
  },
  {
    name: 'Piter Vaughn / Teacher',
    time: '3 sec ego',
    text: 'Today, many people rely on computers to do homework, work, and create or store useful information. Therefore, it is important for the information on the computer to be stored and kept properly. '
  },
  {
    name: 'Adam Vaughn / Teacher',
    time: '3 sec ego',
    text: 'Today, many people rely on computers to do homework, work, and create or store useful information. Therefore, it is important for the information on the computer to be stored and kept properly. '
  }
];


class MarkModal extends Component{
  constructor(props){
    super(props);

    this.state = {
      buttons: [
        {name: 'BAD' , status: false},
        {name: 'NOT BAD', status: false},
        {name: 'POOR', status: false},
        {name: 'GOOD', status: false},
        {name: 'AWESOME', status: false}
      ]
    };
  }
  handleMarkClick = name => {
    const buttons = this.state.buttons.map(item => {
      if(item.name === name){
        return {...item, status: true}
      }
      return {...item, status: false}
    });

    this.setState({buttons});
  };
  render(){

    return (
      <Modal show={this.props.show} hideModal={() => {this.props.hideModal('mark')}} className="review big">
        <h3 className="modal-title-big">Gradebook</h3>
        <div className="mark-header">
          <Cell column={3}>
            <p className="mark-text-bold">Posted by:</p>
          </Cell>
          <Cell column={13}>
            <p className="mark-text-bold">Mildred Romero</p>
          </Cell>
          <Cell column={3}>
            <p className="mark-text-bold">Date:</p>
          </Cell>
          <Cell column={13}>
            <p className="mark-text-small">10/08/2018</p>
          </Cell>
        </div>
        <div className="mark-body">
          <Cell column={3}>
            <p className="mark-text-bold">Mark:</p>
          </Cell>
          <Cell column={13} className="mark-body-marks">
            {this.state.buttons.map((item, key) => <Mark text={item.name} key={key}
                                       className={`big gray ${item.status ? 'active' : ''}`}
                                       clicked={() => {this.handleMarkClick(item.name)}}
            />)}
          </Cell>
        </div>
        <div className="mark-comments">
          <Cell column={3}>
            <p className="mark-text-bold">Comments:</p>
          </Cell>
          <Cell column={13} >
            {comments.map((item, key) => <SubmissionComment key={key} name={item.name} text={item.text} time={item.time}/>)}
          </Cell>
        </div>
        <div>
          <button className="big-primary right" onClick={() => {this.props.hideModal('mark')}}>Submit</button>
        </div>
      </Modal>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  hideModal: modal => {dispatch(actions.hideModal(modal))}
});

export default connect(null, mapDispatchToProps)(MarkModal);
