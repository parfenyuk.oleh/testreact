import React from 'react';
import { connect } from 'react-redux';

import * as actions from "../../store/actions/submissions";

const SubmissionHeader = props => (
  <div className="componentHeader">
    <h1 className="page-title">Submissions</h1>
    <div className="componentHeader-controls">
      <button className="big-empty-control-gray" onClick={() => { props.showModal('archive') }}>Archive filter</button>
    </div>
  </div>
);

const mapDispatchToProps = dispatch => ({
  showModal: modal => { dispatch(actions.showModal(modal)) }
});

export default connect(null,mapDispatchToProps)(SubmissionHeader);