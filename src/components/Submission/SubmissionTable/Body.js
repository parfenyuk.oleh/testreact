import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router-dom"
import Row from "./Row";
import * as actions from "../../../store/actions/submissions";


const submissions = [
  {
    name:       "Social Medium Integration",
    status:     'awesome',
    date:       '10/08/2018',
    avatar:     'https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg',
    userName:   'Olivia Castillo',
    attendance: ['AB','CD','EV'],
    fresh:      true
  },
  {
    name:       "Online Shipping Tracker",
    status:     'good',
    date:       '10/12/2018',
    avatar:     'https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg',
    userName:   'Olivia Castillo',
    attendance: ['EV'],
    fresh:      false
  },
  {
    name:       "Worldwide Map Widget",
    status:     'Not Bad',
    date:       '10/08/2018',
    avatar:     'https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg',
    userName:   'Olivia Castillo',
    attendance: ['CD','EV'],
    fresh:      false
  },
  {
    name:       "Statistic Visualization",
    status:     'bad',
    date:       '10/08/2018',
    avatar:     'https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg',
    userName:   'Olivia Castillo',
    attendance: ['AB','CD'],
    fresh:      false
  },
  {
    name:       "Online Operator",
    status:     'poor',
    date:       '10/08/2018',
    avatar:     'https://www.maxpixel.net/static/photo/2x/Look-Person-Men-Man-Ernst-Face-Portrait-Human-852762.jpg',
    userName:   'Olivia Castillo',
    attendance: ['AB','CD','EV','VY'],
    fresh:      true
  }
];

const Body = ({showModal, history}) => submissions.map(item =>
  <Row submission={item}
       key={item.name}
       showModal={() => { showModal('review') }}
       openGradebook={() => { history.push('submissions/gradebook')}}
  />);

const mapDispatchToProps = dispatch => ({
  showModal: modal => { dispatch( actions.showModal(modal) ) }
});

export default connect(null,mapDispatchToProps)(withRouter(Body));