import React from 'react';
import Cell from "../../UI/Cell";
import RoundAvatar from "../../UI/RoundAvatar";
import Status from "../../UI/Status";

const Row = ({submission: { name, status, date, avatar, userName, attendance, fresh }, showModal, openGradebook}) => (
  <div className="table-row">
    <Cell column={4.5} className="submission-name">{/* style in table.sass */}
      {name} {fresh ? <div className="table-new">NEW</div> : null}
    </Cell>
    <Cell column={1.5}>
      {/*<Mark text={status}/>*/}
      <Status type={status}/>
    </Cell>
    <Cell column={0.5}/>
    <Cell column={2} className="table-date">
      {date}
    </Cell>
    <Cell column={2.5} className="table-submitted">
      <RoundAvatar
        avatar={avatar}
        text="AB"
      />
      <span className="table-submitted-name">{userName}</span>
    </Cell>
    <Cell column={3} className="table-attendance">
      { attendance.map(item => <RoundAvatar text={item} key={item}/>)}
    </Cell>
    <Cell column={2} className="table-buttons">
      <button className="button-empty-gray" onClick={openGradebook}>Gradebook</button>
      <button className="button-empty-gray" onClick={showModal}>Review</button>
    </Cell>
  </div>
);

export default Row;