import React, {Fragment} from 'react';
import Body from "./SubmissionTable/Body";
import Cell from "../UI/Cell";
import TableHeader from "../UI/Table/TableHeader";

const SubmissionsTable = () => (
  <Fragment>
    <TableHeader>
      <Cell column={4.5}> Assignment </Cell>
      <Cell column={2}> Status </Cell>
      <Cell column={2}> Due Date </Cell>
      <Cell column={2.5}> Submitted by </Cell>
      <Cell column={3}> Attendance </Cell>
    </TableHeader>
    <Body/>
  </Fragment>
);

export default SubmissionsTable;
