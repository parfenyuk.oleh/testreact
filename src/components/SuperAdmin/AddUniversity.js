import React from 'react';
import Input from "../UI/Input";

const AddUniversity = ({img,fileChanged, nameChanged, submited}) => {
  return (
    <div style={{width: '50%', margin: '20px auto'}}>
      <Input placeholder="University name" changed={nameChanged}/>
      <input type="file" onChange={fileChanged}/>
      <img src={img} alt="" style={{
          maxHeight: '300px',
          maxWidth : '100%'
      }}/>
      <button className="big-primary" onClick={submited}>Add</button>
    </div>
  );
};

export default AddUniversity;
