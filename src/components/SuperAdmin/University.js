import React from 'react';
import Cell from '../UI/Cell';
import Input from '../UI/Input';

const University = ({name, changedName, image, changedImage, deleted, updated, imageToShow}) => {
  return (
    <div style={{width: '70%', margin: '20px auto', display: 'flex'}}>
      <Cell column={3}>
        <Input value={name} changed={changedName}/>
      </Cell>
      <Cell column={1}/>
      <Cell column={9} >
        <img src={imageToShow ? imageToShow : image}
            alt="university"
             style={{
                 maxHeight: '300px',
                 maxWidth : '100%'
             }}
        />
        <input type="file"
               onChange={changedImage}/>
      </Cell>
      <Cell column={1}/>
      <Cell column={2}>
        {/*<button className="big-primary" onClick={deleted}>Delete</button>*/}
        <button className="big-primary" onClick={updated}>Update</button>
      </Cell>
    </div>
  );
};

export default University;
