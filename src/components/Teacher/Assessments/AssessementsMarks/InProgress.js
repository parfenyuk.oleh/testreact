import React, {Component} from 'react';
import icons from '../../../../helpers/iconsLoader';

class InProgress extends Component {
    state = {
        isMarkVisible: false
    };

    showHint = () => {
        this.setState({isMarkVisible: true});
    };

    hideHint = () => {
        this.setState({isMarkVisible: false});
    };

    render() {
        return (
            <div onClick={this.props.openMarkModal}
                 className={`mark-wrapper ${this.props.currentAssessment.id ? this.props.headerId === this.props.currentAssessment.id ? '' : 'hide' : ''}`}>
                <div className="mark-weight" onMouseEnter={this.showHint} onMouseLeave={this.hideHint}>
                    <img className="status-icon" src={icons.iconInProgress} alt="icon in progress" />
                    {this.props.getAssessmentMark}
                    <p className={this.state.isMarkVisible ? "status-text" : "status-text hidden"}>In progress</p>
                </div>
            </div>
        );
    }
}

export default InProgress;
