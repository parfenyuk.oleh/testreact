import React, {Component} from 'react';
import icons from '../../../../helpers/iconsLoader';

class NotSubmitted extends Component {
    state = {
        isMarkVisible: false
    };

    showHint = () => {
        this.setState({isMarkVisible: true});
    };

    hideHint = () => {
        this.setState({isMarkVisible: false});
    };

    render() {
        return (
            <div onClick={this.props.openMarkModal}
                 className={`mark-wrapper ${this.props.headerId}  ${this.props.currentAssessment.id ? this.props.headerId === this.props.currentAssessment.id ? '' : 'hide' : ''}`}>
                <div className="mark-weight" onMouseEnter={this.showHint} onMouseLeave={this.hideHint}>
                    <img className="status-icon" src={icons.iconNotSubmitted} alt="icon not submitted" />
                    {this.props.getAssessmentMark}
                    <p className={this.state.isMarkVisible ? "status-text" : "status-text hidden"}>Not submitted</p>
                </div>
            </div>
        );
    }
}

export default NotSubmitted;
