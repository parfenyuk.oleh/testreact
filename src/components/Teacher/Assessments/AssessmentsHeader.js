import React from 'react';
import Select from '../../UI/Select';
import PageHeader from '../../UI/PageHeader';


const AssessmentsHeader  = ({subjects, selectSubject, currentSubject, createNewAssessment})  => (
  <PageHeader>
    <Select placeholder={'Subject'}
            options={subjects}
            clicked={selectSubject}
            text={currentSubject ? currentSubject.name : 'Subject'}
    />
    <button className={'big-primary standart'}
            disabled={subjects.length === 0}
            onClick={createNewAssessment}
    >
      CREATE NEW
    </button>
  </PageHeader>
);

export default AssessmentsHeader;
