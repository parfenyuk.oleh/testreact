import React from 'react';
import Select from '../../UI/Select';
import PageHeader from '../../UI/PageHeader';

class AssignmentsHeader extends React.Component {

  render() {

    const {groups, checkedGroups, createNewAssignment, handleFilterChange} = this.props;
    return (
      <PageHeader>
        <h1 className="page-title">Assignments name / Grader report</h1>
        <div>
          <Select handleInputChange={handleFilterChange}
                  placeholder={'Filter by group'}
                  multiselect
                  checkedItems={checkedGroups}
                  options={groups}
            />
          <button
              onClick={createNewAssignment}
              className={'big-primary standart'}
            >
              CREATE NEW
            </button>

        </div>
      </PageHeader>
    );
  }
}




export default AssignmentsHeader;
