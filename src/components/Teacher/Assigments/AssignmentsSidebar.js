import React from 'react';

const AssignmentsSidebar = ({ amounts,
                              TABS,
                              currentTab,
                              changeTab
}) => {
  return (
    <div className="admin-main-sidebar">
      <div className="admin-main-sidebar-item">
        <div className={`admin-main-sidebar-body ${currentTab === TABS.active ? 'active' : ''}`}
             onClick={() => {changeTab(TABS.active)}}
        >
          <span className="admin-main-sidebar-name">Active</span>
          <span className="admin-main-sidebar-count">{amounts.amountOfActiveAssignments}</span>
        </div>
      </div>
      <div className="admin-main-sidebar-item">
        <div className={`admin-main-sidebar-body ${currentTab === TABS.archive ? 'active' : ''}`}
             onClick={() => {changeTab(TABS.archive)}}
          >
          <span className="admin-main-sidebar-name">Archive</span>
          <span className="admin-main-sidebar-count green">{amounts.amountOfArchiveAssignments}</span>
        </div>
      </div>
    </div>

  );
};

AssignmentsSidebar.defaultProps = {
  counts : {
    active : 0,
    archive: 0
  }
};
export default AssignmentsSidebar;