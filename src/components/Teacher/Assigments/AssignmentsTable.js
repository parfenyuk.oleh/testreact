import React from 'react';
import TableHeader from '../../UI/Table/TableHeader';
import PropTypes from 'prop-types';
import EmptyState from '../../UI/EmptyState';
import AssignmentsTableBody from './AssignmentsTableBody';
import Table from '../../UI/Table/Table';
import Cell from '../../UI/Cell';

const AssignmentsTable = (props) => {
  if (props.list.length === 0) return <EmptyState/>;
  return (
    <Table className={'assignment-teacher-table'}>
      <TableHeader>
        <Cell>Assignment</Cell>
        <Cell>Subject</Cell>
        <Cell>Open date</Cell>
        <Cell>Due date</Cell>
        <Cell>Leader</Cell>
        <Cell>Group</Cell>
        <Cell>Status</Cell>
        <Cell> </Cell>
      </TableHeader>
      <AssignmentsTableBody {...props}/>
    </Table>
  );
};

AssignmentsTable.propTypes = {
  changeAssignmentHandler: PropTypes.func,
  archiveAssignment: PropTypes.func,
  list: PropTypes.array,
};

AssignmentsTable.defaulrProps = {
  changeAssignment: () => {},
  archiveAssignment: () => {},
  list: [],
  restoreAssignment: () => {},
};

export default AssignmentsTable;