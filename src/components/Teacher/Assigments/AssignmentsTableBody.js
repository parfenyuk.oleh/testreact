import React, {Fragment} from 'react';
import TableRow from '../../UI/Table/TableRow';
import Cell from '../../UI/Cell';
import moment from 'moment';
import AvatarBlock from '../../UI/Elements/AvatarBlock';
import {withInfiniteScroll} from '../../../hoc/withInfiniteScroll';
import Status from './Status';
import {withRouter} from 'react-router';
import {ASSIGNMENTS} from '../../../constants/routes';
import Icon from '../../UI/Elements/Icon';

const AssignmentsTableBody = ({
                                changeAssignment,
                                archiveAssignment,
                                list,
                                restoreAssignment,
                                history,
                                deleteAssignment,

                              }) => {

  function showDetailPage(id) {
    history.push(`${ASSIGNMENTS}/${id}`);
  }

  return (
    <Fragment>
      {
        list.map(item => (
          <TableRow key={item.id}>
            <Cell>{item.name}</Cell>
            <Cell>{item.subjectName}</Cell>
            <Cell>{moment(item.startDate).format('l')}</Cell>
            <Cell>{moment(item.endDate).format('l')}</Cell>
            <Cell>
              <AvatarBlock textAvatar={`${item.leaderFirstName[0]}${item.leaderLastName[0]}`}
                           customInfo={<span>{`${item.leaderFirstName} ${item.leaderFirstName}`}</span>}
                           imgSrc={item.leaderAvatar}
              />
            </Cell>
            <Cell>
              <AvatarBlock textAvatar={`#${item.groupId}`}
                           customInfo={<span>{item.groupName}</span>}
              />
            </Cell>
            <Cell>
              <Status status={item.status}/>
            </Cell>
            <Cell className="assignment-icons">
              <Icon name="openDetails" onClick={() => {showDetailPage(item.id)}}/>
              <Icon name="edit-dark" onClick={() => {changeAssignment(item)}}/>
              {restoreAssignment && <Icon name="restore" onClick={() => {restoreAssignment(item)}}/>}
              {deleteAssignment && <Icon name="delete-dark" onClick={() => {deleteAssignment(item.id)}}/>}
              {archiveAssignment && <Icon name="delete-dark" onClick={() => {archiveAssignment(item.id)}}/>}
            </Cell>
          </TableRow>
        ))
      }
    </Fragment>
  );
};

export default withInfiniteScroll(withRouter(AssignmentsTableBody));