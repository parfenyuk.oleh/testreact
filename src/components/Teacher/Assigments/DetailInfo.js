import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import Icon from '../../UI/Elements/Icon';
import {withRouter} from 'react-router';
import {ASSIGNMENTS} from '../../../constants/routes';

const DetailInfo = ({
                      currentAssignment,
                      editAssignment,
                      archiveAssignment,
                      restoreAssignment,
                      deleteAssignment,
                      showFullDescription,
                      onReadMore,
                      history,
                    }) => {
  const assignmentIsArchive = currentAssignment.archievedAt || moment(currentAssignment.endDate).isBefore(moment());
  const DESCRIPTION_PREVIEW_LENGTH = 300;
  function goToResultPage() {
    history.push(`${ASSIGNMENTS}/${currentAssignment.id}/result`)
  }

  return (
    <div className='assignment-detail-block'>
      <div className='assignment-detail-block-header'>
        <div>
          <p className="assignment-detail-title">{currentAssignment.name}</p>
          <p className="assignment-detail-date">Start date: {moment(currentAssignment.startDate).format('l')}</p>
          <p className="assignment-detail-date">End date: {moment(currentAssignment.endDate).format('l')}</p>
        </div>
        <div className="assignment-icons">
          <Icon name="edit-dark" onClick={editAssignment}/>
          {!assignmentIsArchive && <Icon name="delete-dark" onClick={() => {archiveAssignment(currentAssignment.id)}}/>}
          {assignmentIsArchive && <Icon name="restore" onClick={restoreAssignment}/>}
          {assignmentIsArchive && <Icon name="delete-dark" onClick={() => {deleteAssignment(currentAssignment.id)}}/>}
        </div>
      </div>
      <p className="assignment-detail-subject">{currentAssignment.subjectName}</p>
      <p className="assignment-detail-description">{
        showFullDescription || currentAssignment.description.length < DESCRIPTION_PREVIEW_LENGTH
          ?
          currentAssignment.description
          :
          `${currentAssignment.description
            .substr(0, DESCRIPTION_PREVIEW_LENGTH)} ...`
      }</p>
      <div className="assignment-detail-block-controls">
        {

          <button className="read-more"
                  onClick={onReadMore}
                  style={{
                    opacity: currentAssignment.description.length > DESCRIPTION_PREVIEW_LENGTH ? '1' : '0',
                  }}
          >
            Read {showFullDescription ? 'Less' : 'More'}
          </button>
        }
        <button className="big-primary standart"
                style={{width: '195px', letterSpacing: '-0.9px'}}
                onClick={goToResultPage}
        >
          ASSIGNMENT RESULTS
        </button>
      </div>
    </div>
  );
};

DetailInfo.propTypes = {
  currentAssignment: PropTypes.object,
  editAssignment: PropTypes.func,
  archiveAssignment: PropTypes.func,
  restoreAssignment: PropTypes.func,
  deleteAssignment: PropTypes.func,
  showFullDescription: PropTypes.bool,
  onReadMore: PropTypes.func,
};

export default withRouter(DetailInfo);