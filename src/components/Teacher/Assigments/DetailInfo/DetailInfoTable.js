import React from 'react';
import EmptyState from '../../../UI/EmptyState';
import TableHeader from '../../../UI/Table/TableHeader';
import Cell from '../../../UI/Cell';
import TableRow from '../../../UI/Table/TableRow';
import AvatarBlock from '../../../UI/Elements/AvatarBlock';
import Input from '../../../UI/Input';
import Mark from '../../../UI/Mark';
import * as PropTypes from 'prop-types';
import DetailInfoTableBody from './DetailInfoTableBody';
import Table from '../../../UI/Table/Table';

const DetailInfoTable = (props) => {
  if(props.list.length === 0) return <EmptyState/>;
  return (
    <Table>
      <TableHeader>
        <Cell column={3}>User name</Cell>
        <Cell column={3}>Email</Cell>
        <Cell column={2}>Participation</Cell>
        <Cell column={2}>Exam</Cell>
        <Cell column={2}>Quiz</Cell>
        <Cell column={2}>Essay</Cell>
        <Cell column={2}> </Cell>
        <Cell column={2}>Mark</Cell>
      </TableHeader>
     <DetailInfoTableBody {...props}/>
    </Table>
  );
};

DetailInfoTable.propTypes = {
  list: PropTypes.array,
  onReview: PropTypes.func
};

export default DetailInfoTable;