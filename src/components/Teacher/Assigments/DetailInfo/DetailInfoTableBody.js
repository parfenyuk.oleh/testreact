import React from 'react';
import TableRow from '../../../UI/Table/TableRow';
import Cell from '../../../UI/Cell';
import AvatarBlock from '../../../UI/Elements/AvatarBlock';
import Input from '../../../UI/Input';
import Mark from '../../../UI/Mark';
import {withInfiniteScroll} from '../../../../hoc/withInfiniteScroll';
import {SUBMISSION_FIELDS} from '../../../../constants/assignments';

function filterDigitsFromInput (event){
  event.target.value = event.target.value.replace(/[^\d.]/g, '').substr(0,3);
  if(+event.target.value > 100){
    event.target.value = '100';
  }
  return event;
}

const DetailInfoTableBody = (props) => props.list.map(item => (
  <TableRow key={item.userFirstName}>
    <Cell column={3}>
      <AvatarBlock textAvatar={`${item.userFirstName[0]}${item.userLastName[0]}`}
                   customInfo={<span>{`${item.userFirstName} ${item.userLastName}`}</span>}
                   imgSrc={item.avatar}
      />
    </Cell>
    <Cell column={3}>
      {item.email}
    </Cell>
    <Cell column={2}>
      <Input defaultValue={item.participation} placeholder="Enter" className="percent" changed={(e) => {
        e = filterDigitsFromInput(e);
        props.updateSubmission(SUBMISSION_FIELDS.PARTICIPATION, e.target.value, item.userId)
      }}/>
    </Cell>
    <Cell column={2}>
      <Input defaultValue={item.exam} placeholder="Enter" className="percent" changed={(e) => {
        e = filterDigitsFromInput(e);
        props.updateSubmission(SUBMISSION_FIELDS.EXAM, e.target.value, item.userId)
      }}/>
    </Cell>
    <Cell column={2}>
      <Input defaultValue={item.quiz} placesholder="Enter" className="percent" changed={(e) => {
        e = filterDigitsFromInput(e);
        props.updateSubmission(SUBMISSION_FIELDS.QUIZ, e.target.value, item.userId)
      }}/>
    </Cell>
    <Cell column={2}>
      <Input defaultValue={item.essay} placeholder="Enter" className="percent" changed={(e) => {
        e = filterDigitsFromInput(e);
        props.updateSubmission(SUBMISSION_FIELDS.ESSAY, e.target.value, item.userId)
      }}/>
    </Cell>
    <Cell column={2}>
      <button className="big-primary review" onClick={() => {props.onReview(item)}}>REVIEW</button>
    </Cell>
    <Cell column={2}>
      {item.review && <Mark status={item.review.mark}/>}
    </Cell>
  </TableRow>
));

export default withInfiniteScroll(DetailInfoTableBody);