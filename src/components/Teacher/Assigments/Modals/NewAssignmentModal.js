import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import moment from 'moment';

import Modal from '../../../UI/Modal/Modal';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import icons from '../../../../helpers/iconsLoader';
import Input from '../../../UI/Input';
import Textarea from '../../../UI/Textarea';
import ModalRow from '../../../UI/Modal/ModalRow';
import Select from '../../../UI/Select';
import Cell from '../../../UI/Cell';
import DatePicker from '../../../UI/Datepicker';
import * as actions from '../../../../store/actions';
import {userAuthToken} from '../../../../helpers';
import AvatarBlock from '../../../UI/Elements/AvatarBlock';
import validate from '../../../../helpers/validator';
import SearchSelect from '../../../UI/SearchSelect';
import Icon from '../../../UI/Elements/Icon';
import ROLES from '../../../../constants/roles';

const GROUP_ID_SELECT = 'groupId';
const STEPS = {FIRST : 1, SECOND: 2};
const MIN_SEARCH_LENGTH = 2;

const filterByTeacher = item => item.role !== ROLES.TEACHER;

class NewAssignmentModal extends Component {

  constructor(props) {
    super(props);
    // TODO: find solution for defaultState
    this.state = {
      step: 1,
      name: {
        value: '',
        error: false
      },
      description: {
        value: '',
        error: false
      },
      startDate: {
        value: '',
        error: false
      },
      endDate: {
        value: '',
        error: false
      },
      subjectId : {
        value: '',
        error: false
      },
      groupId : {
        value: '',
        error: false
      },
      wordsLimit : {
        value: '',
        error: false
      },
      errors: [],
      checkedMembers: new Map(),
      checkedMembersError: false,
      checkedMembersButtonText: '',
      leader: {
        value: {},
        error: false
      },
      keyWords : {
        search : '',
        chosen : [],
        error : false
      },
      changeId : -1
    };
  }




  static getDerivedStateFromProps(props, state){
    const {changeModel, restore, show} = props;
    const newState = {...state};
    if(changeModel !== null && changeModel.id !== state.changeId && show){
      props.showLoader();
      props.getMemberActiveGroup(userAuthToken.token(),props.auth.user.institutionId, changeModel.groupId)
        .then(() => {
          props.hideLoader();
        });
      newState.changeId = changeModel.id;
      newState.name.value = changeModel.name;
      newState.description.value = changeModel.description;
      newState.subjectId.value = changeModel.subjectId;
      newState.groupId.value = changeModel.groupId;
      newState.wordsLimit.value = changeModel.wordsLimit;
      newState.endDate.value = moment(changeModel.endDate);
      newState.startDate.value = moment(changeModel.startDate);
      newState.leader.value = {id: changeModel.leaderId};
      changeModel.students.forEach(item => {
        newState.checkedMembers.set(`${item}-${item.id}`, true)
      });
      newState.keyWords.chosen = changeModel.keyWords;

      if(restore){
        newState.endDate = {
          value: '',
          error: true
        };
        newState.errors = ['For restore "End Date" should be more than now'];
      }
      return newState;

    }
    return null;
  }

  /**
   * Update checkedMembersButtonText if component receive new assignment for update
   *
   * Reset state to default if modal was hidden
   * @param prevProps
   * @param prevState
   */
  componentDidUpdate(prevProps, prevState){
    if(this.state.checkedMembers.size && !this.state.checkedMembersButtonText && prevProps.changeModel !== this.props.changeModel){
      this.setCheckedMembersButtonText();
    }

    if(!this.props.show && this.props.show !== prevProps.show){
      this.setState({
        step: 1,
        name: {
          value: '',
          error: false
        },
        description: {
          value: '',
          error: false
        },
        startDate: {
          value: '',
          error: false
        },
        endDate: {
          value: '',
          error: false
        },
        subjectId : {
          value: '',
          error: false
        },
        groupId : {
          value: '',
          error: false
        },
        wordsLimit : {
          value: '',
          error: false
        },
        errors: [],
        checkedMembers: new Map(),
        checkedMembersError: false,
        checkedMembersButtonText: '',
        leader: {
          value: {},
          error: false
        },
        keyWords : {
          search : '',
          chosen : [],
          error : false
        },
        changeId : -1
      });
    }
  }

  /**
   * Changing values in state depends on field
   * @param value
   * @param type
   */
  inputChangeHandler = (value, type) => {
    if(type === GROUP_ID_SELECT){
      this.props.getMemberActiveGroup(userAuthToken.token(), this.props.auth.user.institutionId , value);
      this.setState({
        [type] : {
          value: value,
          error: false,
        },
        leader: {
          value: {},
          error: false
        },
        checkedMembers: new Map(),
        checkedMembersError: false,
        checkedMembersButtonText: '',
      });
    }
    else {
      this.setState({
        [type] : {
          value: value,
          error: false
        }
      });
    }
  };

  /**
   * Fetching keywords list from the server
   * @param value
   */
  keyWordsSearchHandler = (value) => {
    if(value.length > MIN_SEARCH_LENGTH){
      this.props.getKeywordsList(userAuthToken.token(), this.props.auth.user.institutionId, 0, 50, value)
    }
    else {
      this.props.clearKeywordsList();
    }
  };

  /**
   *  Modify input array, add field name with <AvatarBlock> component inside
   *  for correct mapping in select
   *
   * @param {array} users
   * @returns {array} users with additional property name
   */
  prepareMemberOptions = (users) => {
    return users.map(user => {
      user.name = <AvatarBlock textAvatar={`${user.firstName[0]}${user.lastName[0]}`}
                               customInfo={<span>{`${user.firstName} ${user.lastName}`}</span>}
                               imgSrc={user.avatar}
      />;
      return user;
    }).filter(filterByTeacher);
  };

  /**
   * Handle choose members in assignment
   *
   * @param e
   * @param id
   */
  multislectCheckboxChangeHandler = (e, id) => {
    const item = e.target.name;
    const isChecked = e.target.checked;

    this.setState(prevState => ({
      checkedMembers: prevState.checkedMembers.set( item, isChecked),
    }),this.setCheckedMembersButtonText);
  };

  /**
   * Check or uncheck all checkboxes depends on Map.size and Map values
   */
  checkAllHandler = () => {
    const {checkedMembers} = this.state;
    const {groups} = this.props;
    groups.activeGroup = groups.activeGroup.filter(filterByTeacher);
    if(checkedMembers.size < groups.activeGroup.length){
      groups.activeGroup.forEach(item => {
        checkedMembers.set(`${item.name}-${item.id}`, true);
      })
    }else {
      let isAllTrue = true;
      checkedMembers.forEach(value => {
        if(!value) { isAllTrue = false}
      });
      checkedMembers.forEach((value, key) => {
        checkedMembers.set(key, !isAllTrue);
      })
    }
    this.setState({checkedMembers},this.setCheckedMembersButtonText);
  };

  chooseKeywordHandler = keyword => {
    const {keyWords} = this.state;
    keyWords.chosen.push(keyword);
    this.setState({keyWords})
  };

  deleteKeywordHandler = (id) => {
    const {keyWords} = this.state;
    keyWords.chosen = keyWords.chosen.filter(item => item.id !== id);
    this.setState({keyWords})
  };

  /**
   * Filter not chosen item in list for prevent add to similar keywords
   * @param list {array}
   * @returns {array}
   */
  prepareKeyWordList = (list) => {
    const savedIds = this.state.keyWords.chosen.reduce((acc, item) => {
      acc.push(item.id);
      return acc;
    },[]);

    return list.filter(item => savedIds.indexOf(item.id) === -1);
  };

  setCheckedMembersButtonText = () => {
    const {checkedMembers} = this.state;
    const {groups} = this.props;
    let count = 0;
    checkedMembers.forEach(value => {if(value) count++;});
    if(count === groups.activeGroup.length){
      this.setState({checkedMembersButtonText: 'All members in group'});
    } else if(count > 0){
      this.setState({checkedMembersButtonText: `${count} member${count > 1 ? 's' : ''} in group`});
    } else {
      this.setState({checkedMembersButtonText: ''})
    }
  };



  leaderSelectHandler = (lead) => {
    const {checkedMembers} = this.state;

    checkedMembers.set(`${lead}-${lead.id}`, true);

    this.setState({leader: {value: lead, error: false, checkedMembers}}, this.setCheckedMembersButtonText);

  };

  /**
   * Prepare assignment Model from state
   * @returns {{name: string, description: string, subjectId: string, startDate: *, endDate: *, groupId: string, userIds: Array, leaderId: *, keyWordIds: Array, id}}
   */
  collectData = () => {
    const {name, description, startDate, endDate, subjectId, groupId, checkedMembers, leader, keyWords,changeId, wordsLimit} = this.state;
    const assignmentModel = {
      name: name.value,
      description: description.value,
      subjectId : subjectId.value,
      startDate : startDate.value.format('l'),
      endDate : endDate.value.format('l'),
      groupId : groupId.value,
      wordsLimit : wordsLimit.value,
      userIds : [],
      leaderId : leader.value.id,
      keyWordIds : [],
      id :  changeId
    };

    const userIds = [];

    checkedMembers.forEach((value,key) => {
      if(value){
        userIds.push(Number(key.replace('[object Object]-','')));
      }
    });

    assignmentModel.userIds = userIds;

    assignmentModel.keyWordIds = keyWords.chosen.reduce((acc, item) => {
      acc.push(item.id);
      return acc;
    },[]);



    return assignmentModel;
  };

  saveHandler = () => {
    let {name, description, startDate, endDate, subjectId, groupId, checkedMembers, checkedMembersError,
      leader, step, keyWords, wordsLimit} = this.state;
    const errors = [];

    if (step === STEPS.SECOND){
      keyWords.error = validate('Key words',keyWords.chosen.length,{isNotEqualToZero: true});
      if (keyWords.error) {errors.push(keyWords.error)}

      if(!errors.length){
        if(this.props.changeModel){
          this.props.update(this.collectData());
        }else {
          this.props.saved(this.collectData());
        }
      }
    }

    if(step === STEPS.FIRST){

      const endDateValidationRules = {isNotEmptyDate : true, notLessDate: startDate.value};

      if(this.props.restore){
        endDateValidationRules.notLessDateThenNow = true;
      }

      name.error = validate('Title', name.value, {isNotEmpty: true});
      description.error = validate('Description', description.value, {isNotEmpty: true});
      startDate.error = validate('Start Date', startDate.value, {isNotEmptyDate : true});
      endDate.error = validate('End Date', endDate.value, endDateValidationRules);
      subjectId.error = validate('Subject', subjectId.value.toString(), {isNotEmpty: true});
      wordsLimit.error = validate('Words limit', wordsLimit.value.toString(), {isNotEmpty: true});
      groupId.error = validate('Group', groupId.value.toString(), {isNotEmpty: true});
      if(groupId.value){
        checkedMembersError = validate('Group members count', checkedMembers.size, {isNotEqualToZero: true});
        leader.error = validate('Group leader', leader.value,{isNotEmptyObject: true})
      }

      if (name.error) {errors.push(name.error)}
      if (description.error) {errors.push(description.error)}
      if (wordsLimit.error) {errors.push(wordsLimit.error)}
      if (startDate.error) {errors.push(startDate.error)}
      if (endDate.error) {errors.push(endDate.error)}
      if (subjectId.error) {errors.push(subjectId.error)}
      if (groupId.error) {errors.push(groupId.error)}
      if (checkedMembersError) {errors.push(checkedMembersError)}
      if (leader.error) {errors.push(leader.error)}

      if(!errors.length){
        step = STEPS.SECOND;
      }
    }

    this.setState({
      name,
      description,
      startDate,
      endDate,
      subjectId,
      groupId,
      checkedMembersError,
      leader,
      errors, step
    });
  };

  backToStepOne = () => {
    this.setState({step: STEPS.FIRST})
  };

  render() {
    const {show, hideModal, subjects, groups, keywords, changeModel} = this.props;
    const {errors, name, description, checkedMembers, leader, groupId,checkedMembersButtonText, subjectId,
    startDate, endDate, checkedMembersError, step, keyWords, wordsLimit} = this.state;
    const checkAllOption = (<AvatarBlock textAvatar={`#1`}
                                         customInfo={<span>All members in group</span>}
    />);
    if(step === 1) return (
      <Modal show={show} hideModal={hideModal}>
        <ModalHeader>
          <h3 className="modal-title">{changeModel ? 'Edit assignment' : 'New assignment'}</h3>
        </ModalHeader>
        <ModalBody bigPadding overflow={false}>
          {errors.length > 0 && <div className="auth-errors">
            <ul>
              {errors.map(item => <li key={item}><img src={icons.iconAttention} alt=""/> {item} </li>)}
            </ul>
          </div>
          }
          <ModalRow>
            <Input placeholder="Title"
                   defaultValue={name.value}
                   changed={(e) => {this.inputChangeHandler(e.target.value, 'name')}}
                   className={name.error ? 'error' : ''}
            />
            <Textarea placeholder="Description"
                      text={description.value}
                      changed={(e) => {this.inputChangeHandler(e.target.value, 'description') }}
                      error={description.error}
            />
            <Input placeholder="Expected Words Count"
                   defaultValue={wordsLimit.value}
                   changed={(e) => {this.inputChangeHandler(e.target.value, 'wordsLimit')}}
                   className={wordsLimit.error ? 'error' : ''}
                   number
            />
            <Select placeholder={'Choose subject'}
                    options={subjects.filter(item => !item.archievedAt)}
                    classic
                    clicked={(e) => {this.inputChangeHandler(e.id,'subjectId')}}
                    error={subjectId.error}
                    checkedItems={subjectId.value}
            />
          </ModalRow>
          <ModalRow flex>
            <Cell column={7}>
              <DatePicker placeholder={'Start date'}
                          changed={(value) => {this.inputChangeHandler(value, 'startDate')}}
                          selected={startDate.value}
                          error={startDate.error}
              />
            </Cell>
            <Cell column={2}/>
            <Cell column={7}>
              <DatePicker placeholder={'End date'}
                          changed={(value) => {this.inputChangeHandler(value, 'endDate')}}
                          selected={endDate.value}
                          error={endDate.error}
              />
            </Cell>
          </ModalRow>
          <ModalRow>
            <Select placeholder={'Choose group'}
                    options={groups.list}
                    classic
                    clicked={(e) => {this.inputChangeHandler(e.id,'groupId')}}
                    error={groupId.error}
                    checkedItems={groupId.value}
            />
            { groupId.value && <Select placeholder={'Choose group members'}
                    options={this.prepareMemberOptions(groups.activeGroup)}
                    classic
                    multiselect
                    checkedItems={checkedMembers}
                    handleInputChange={this.multislectCheckboxChangeHandler}
                    checkAllOption={checkAllOption}
                    onCheckAll={this.checkAllHandler}
                    text={checkedMembersButtonText}
                    error={checkedMembersError}
            />}
            { groupId.value && <Select placeholder={'Choose leader'}
                    options={this.prepareMemberOptions(groups.activeGroup)}
                    classic
                    radio
                    checkedItems={leader.value}
                    handleInputChange={this.leaderSelectHandler}
                    nameFunc={item => `${item.firstName} ${item.lastName}`}
                    error={leader.error}
            />}
          </ModalRow>
          <ModalRow button>
            <button className="big-primary" onClick={this.saveHandler}>CONTINUE</button>
          </ModalRow>
        </ModalBody>
      </Modal>
    );
    if(step === 2) return (
      <Modal show={show} hideModal={hideModal}>
        <ModalHeader>
          <h3 className="modal-title">New assignment</h3>
        </ModalHeader>
        <ModalBody bigPadding overflow={false}>
          {errors.length > 0 && <div className="auth-errors">
            <ul>
              {errors.map(item => <li key={item}><img src={icons.iconAttention} alt=""/> {item} </li>)}
            </ul>
          </div>
          }
          <ModalRow>
            <SearchSelect placeholder="Choose keywords"
                          onSearch={this.keyWordsSearchHandler}
                          error={keyWords.error ? 'error' : ''}
                          options={this.prepareKeyWordList(keywords.list)}
                          onChoose={this.chooseKeywordHandler}
                          loading={keywords.loading}
            />
          </ModalRow>
          <ModalRow>
            <ul className="assignment-keyword-list">
              {keyWords.chosen.map(item => <li key={item.name}>
                <span>{item.name}</span>
                <Icon name="cross" onClick={()=> {this.deleteKeywordHandler(item.id)}}/>
              </li>)}
            </ul>
          </ModalRow>
          <ModalRow flex >
            <Cell column={7}>
              <button className="button-default standart" onClick={this.backToStepOne}>Back</button>
            </Cell>
            <Cell column={2}/>
            <Cell column={7}>
              <button className="big-primary standart" onClick={this.saveHandler}>Finish</button>
            </Cell>
          </ModalRow>
        </ModalBody>
      </Modal>
    )
  }
}

NewAssignmentModal.propTypes = {
  show: PropTypes.bool,
  hideModal: PropTypes.func,
  subjects: PropTypes.array,
  groups: PropTypes.object
};

const mapStateToProps = ({groups, auth, keywords}) => ({groups, auth, keywords});

const mapDispatchToProps = dispatch => ({
  getMemberActiveGroup : (token, id, groupId) => {return dispatch(actions.getMemberActiveGroup(token, id, groupId))},
  getKeywordsList : (token, institutionId, skip , take, search ) => {
    dispatch(actions.getKeywordsList(token, institutionId, skip , take, search ))},
  clearKeywordsList: () => {dispatch(actions.clearKeywordsList())},
  showLoader : () => {dispatch(actions.showLoader())},
  hideLoader : () => {dispatch(actions.hideLoader())},
});

export default connect(mapStateToProps, mapDispatchToProps)(NewAssignmentModal);