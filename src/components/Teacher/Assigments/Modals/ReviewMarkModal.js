import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Modal from '../../../UI/Modal/Modal';
import ModalHeader from '../../../UI/Modal/ModalHeader';
import ModalBody from '../../../UI/Modal/ModalBody';
import icons from '../../../../helpers/iconsLoader';
import ModalRow from '../../../UI/Modal/ModalRow';
import AvatarBlock from '../../../UI/Elements/AvatarBlock';
import Mark from '../../../UI/Mark';
import Textarea from '../../../UI/Textarea';

class ReviewMarkModal extends Component {
  constructor(props){
    super(props);
    this.state = {
      errors: [],
      mark: null,
      description: ''
    }
  }

  static getDerivedStateFromProps(props,state){
    if(props.submission && !state.mark){
      return props.submission.review
    }
    return null;
  }

  markChangeHandler = (mark) => {
    this.setState({mark});
  };

  descriptionChangeHandler = description => {this.setState({description})};

  onSave = () => {
    const {mark, description} = this.state;
    const errors = [];

    if(mark === null) errors.push('You should choose mark');
    if(description.length === 0) errors.push('Description shouldn`t be empty');

    if(!errors.length){
      this.props.saved({
        id: this.props.submission.userId,
        mark,
        description
      })
    }
    this.setState({errors});
  };

  render() {
    const {show, hideModal, submission} = this.props;
    const {errors, mark} = this.state;

    const marks = [];
    for (let i = 1; i <= 5; i++){
      marks.push(<Mark status={i}
                       clicked={()=>{this.markChangeHandler(i)}}
                       className={`big ${i === mark ? 'active' : ''}`}
                       key={i}
      />)
    }

    return (
      <Modal show={show} hideModal={hideModal}>
        <ModalHeader>
          <h3 className="modal-title">Assignment detail</h3>
        </ModalHeader>
        <ModalBody bigPadding overflow={false}>
          {errors.length > 0 && <div className="auth-errors">
            <ul>
              {errors.map(item => <li key={item}><img src={icons.iconAttention} alt=""/> {item} </li>)}
            </ul>
          </div>
          }
          <ModalRow>
            <AvatarBlock textAvatar={`${submission.userFirstName[0]}${submission.userLastName[0]}`}
                         customInfo={<span>{`${submission.userFirstName} ${submission.userLastName}`}</span>}
                         imgSrc={submission.avatar}
            />
          </ModalRow>
          <ModalRow>
            <Textarea text={this.state.description} changed={(e) => {this.descriptionChangeHandler(e.target.value)}}/>
          </ModalRow>
          <ModalRow>
            <div className="assignment-marks">
              {marks}
            </div>
          </ModalRow>
          <ModalRow>
              <button className="big-primary" onClick={this.onSave}>Approve</button>
          </ModalRow>
        </ModalBody>
      </Modal>
    );
  }
}

ReviewMarkModal.propTypes = {
  show : PropTypes.bool,
  hideModal : PropTypes.func,
  assignment : PropTypes.object,
  saved: PropTypes.func
};

export default ReviewMarkModal;