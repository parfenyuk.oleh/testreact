import React from 'react';
import {STATUSES} from '../../../constants/assignments';

const Status = ({status}) => {
  let text = '';
  switch (status) {
    case STATUSES.Active : {
      text = "Active";
      break;
    }
    case STATUSES.Done : {
      text = "Done";
      break;
    }
    case STATUSES.InProgress : {
      text = "In Progress";
      break;
    }
    case STATUSES.ToReview : {
      text = "To Review";
      break;
    }
    default :
      text = '';
      break;
  }
  return (
    <span className={'status-assignment'}>{text}</span>
  );
};

export default Status;