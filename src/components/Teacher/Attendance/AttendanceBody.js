import React from 'react';
import PropTypes from 'prop-types';
import AttendanceRow from './AttendanceRow';
import AttendanceRowPast from './AttendanceRowPast';


const AttendanceBody = ({attendances, updateAttendanceLocal, past, isLoading, onPaginate}) => {
  const handleScroll = (event) => {
    if(
      event.target.offsetHeight - event.target.scrollTop >= event.target.scrollHeight - 100
      && !isLoading
      && attendances.length % 10 === 0
      && attendances.length !== 0
    ) {
      onPaginate();
    }
  };
  return (
    <div className="attendance-table-body"
         onScroll={handleScroll}
    >
      {!past && attendances.map(attendance => <AttendanceRow item={attendance}
                                                             key={attendance.userId}
                                                             updateAttendanceLocal={updateAttendanceLocal}
      />)}

      {past && attendances.map(attendance => <AttendanceRowPast item={attendance}
                                                                key={attendance.userId}
                                                                updateAttendanceLocal={updateAttendanceLocal}
      />)}

    </div>
  );
};

AttendanceBody.propTypes = {
  attendances: PropTypes.array,
};

export default AttendanceBody;