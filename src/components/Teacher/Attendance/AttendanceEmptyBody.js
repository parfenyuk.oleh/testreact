import React from 'react';
import EmptyState from '../../UI/EmptyState';

const AttendanceEmptyBody = () => (
  <div className="attendance-table-body empty">
    <EmptyState text="Attendance is not marked for this day!"/>
  </div>
);

export default AttendanceEmptyBody;