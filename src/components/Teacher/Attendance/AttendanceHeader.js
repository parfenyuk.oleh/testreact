import React from 'react';
import PropTypes from 'prop-types';

const AttendanceHeader = ({currentDay}) => {
  return (
    <div className="attendance-header">
      <h1 className="page-title">Teacher's Attendance Dashboard</h1>
      <p>Attendance For {currentDay}</p>
    </div>
  );
};

AttendanceHeader.propTypes = {
  currentDay: PropTypes.string,
};

AttendanceHeader.defaultProps = {
  currentDay: 'Monday Dec 10, 2018',
};

export default AttendanceHeader;