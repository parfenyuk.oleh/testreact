import React from 'react';
import Spinner from '../../UI/Spinner';

const AttendanceLoading = () => (
  <div className="attendance-table-body load">
    <Spinner/>
  </div>
);

export default AttendanceLoading;