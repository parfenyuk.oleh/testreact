import React from 'react';
import PropTypes from 'prop-types';
import ReactStart from 'react-stars';
import RoundAvatar from '../../UI/RoundAvatar';
import Select from '../../UI/Select';
import {ATTENDANCE_MARKS, COMMENT_MARK} from '../../../constants/attendance';


const SelectValues = [
  {name: 'On Time', value: COMMENT_MARK.ON_TIME},
  {name: 'Late', value: COMMENT_MARK.LATE},
  {name: 'Very Late', value: COMMENT_MARK.VERY_LATE},
];

const AttendanceRow = ({item, onMarkSelect, updateAttendanceLocal}) => {
  let selectPlaceholder;
  switch (item.commentMarkPresent) {
    case COMMENT_MARK.ON_TIME:
      selectPlaceholder = 'On Time';
      break;
    case COMMENT_MARK.LATE:
      selectPlaceholder = 'Late';
      break;
    case COMMENT_MARK.VERY_LATE:
      selectPlaceholder = 'Very Late';
      break;
    default:
      selectPlaceholder = 'Select';
      break;
  }

  if (item.attendanceMark === ATTENDANCE_MARKS.ABSENT) selectPlaceholder = 'Absent';
  console.log(item);
  return (
    <div className="attendance-row">

      <div className="attendance-row-user">
        <RoundAvatar avatar={item.avatar} text={`${item.userFirstName[0]}${item.userLastName[0]}`}/>
        <span className="attendance-row-user-name">{item.userFirstName} {item.userLastName}</span>
      </div>

      <button className={`attendance-button ${item.attendanceMark === ATTENDANCE_MARKS.PRESENT ? 'present' : ''}`}
              onClick={() => {updateAttendanceLocal(item.userId, 'attendanceMark', ATTENDANCE_MARKS.PRESENT)}}
      >
        Present
      </button>

      <Select placeholder={selectPlaceholder}
              options={SelectValues}
              clicked={({value}) => {
                updateAttendanceLocal(item.userId, 'commentMarkPresent', value)
              }}
              disabled={item.attendanceMark !== ATTENDANCE_MARKS.PRESENT}
      />

      <button className={`attendance-button ${item.attendanceMark === ATTENDANCE_MARKS.ABSENT ? 'absent' : ''}`}
              onClick={() => {updateAttendanceLocal(item.userId, 'attendanceMark', ATTENDANCE_MARKS.ABSENT)}}
      >
        Absent
      </button>
      <div className={`react-start-wrapper${item.attendanceMark === ATTENDANCE_MARKS.ABSENT ? ' block': ''}`}>
        <ReactStart half={false}
                    size={20}
                    className="attendance-stars"
                    onChange={rating => { updateAttendanceLocal(item.userId, 'participationMarkPresent', rating)}}
                    value={item.participationMarkPresent}
        />
      </div>
    </div>
  );
};


AttendanceRow.propTypes = {
  item: PropTypes.object,
  onMarkSelect: PropTypes.func,
  onAttendanceSelect: PropTypes.func,
};

export default AttendanceRow;