import React from 'react';
import PropTypes from 'prop-types';
import ReactStart from 'react-stars';
import RoundAvatar from '../../UI/RoundAvatar';
import Select from '../../UI/Select';
import {ATTENDANCE_MARKS, COMMENT_MARK} from '../../../constants/attendance';


const AttendanceRow = ({item, onMarkSelect, updateAttendanceLocal, past}) => {
  let selectPlaceholder;
  switch (item.commentMarkPresent) {
    case COMMENT_MARK.ON_TIME:
      selectPlaceholder = 'On Time';
      break;
    case COMMENT_MARK.LATE:
      selectPlaceholder = 'Late';
      break;
    case COMMENT_MARK.VERY_LATE:
      selectPlaceholder = 'Very Late';
      break;
    default:
      selectPlaceholder = '';
      break;
  }

  if (item.attendanceMark === ATTENDANCE_MARKS.ABSENT) selectPlaceholder = '';
  return (
    <div className="attendance-row past">

      <div className="attendance-row-user">
        <RoundAvatar avatar={item.avatar} text={`${item.userFirstName[0]}${item.userLastName[0]}`}/>
        <span className="attendance-row-user-name">{item.userFirstName} {item.userLastName}</span>
      </div>

      <button className={`attendance-button ${item.attendanceMark === ATTENDANCE_MARKS.PRESENT ? 'present-empty' :
        item.attendanceMark === ATTENDANCE_MARKS.ABSENT ? 'absent-empty' : 'invisible'}`}
      >
        {item.attendanceMark === ATTENDANCE_MARKS.ABSENT ? 'Absent' : 'Present'}
      </button>

      <span className="mark">{selectPlaceholder}</span>

      <button className={`attendance-button invisible`}
      >
        Absent
      </button>

      <div className="react-start-wrapper block">
        {item.participationMarkPresent !== 0 && <ReactStart half={false}
                                                      size={20}
                                                      className="attendance-stars"
                                                      onChange={rating => { updateAttendanceLocal(item.userId, 'participationMarkPresent', rating)}}
                                                      value={item.participationMarkPresent}
                                                      color1={'#ffffff'}
        />
        }
      </div>
    </div>
  );
};

AttendanceRow.propTypes = {
  item: PropTypes.object,
  onMarkSelect: PropTypes.func,
  onAttendanceSelect: PropTypes.func,
};

export default AttendanceRow;