import React from 'react';
import PropTypes from 'prop-types';
import Select from '../../UI/Select';
import AttendanceEmptyBody from './AttendanceEmptyBody';
import AttendanceBody from './AttendanceBody';
import AttendanceLoading from './AttendanceLoading';
import {ATTENDANCE_MARKS} from '../../../constants/attendance';

const AttendanceTable = ({
                           onSubjectSearch,
                           subjectList,
                           onSubjectChoose,
                           subjectLoading,
                           onMarkAll,
                           searchValue,
                           attendances,
                           isBeforeNow,
                           isLoading,
                           updateAttendanceLocal,
                           onCancel,
                           onSave,
                           onChangeMode,
                           changeMode,
                           onPaginate,
                         }) => {
  let body, footer = null;
  if (isLoading && attendances.length === 0) {
    body = <AttendanceLoading/>
  } else if (attendances.length === 0) {
    body = <AttendanceEmptyBody/>;
  } else if (isBeforeNow && !changeMode) {
    body = <AttendanceBody attendances={attendances}
                           updateAttendanceLocal={updateAttendanceLocal}
                           past={true}
                           isLoading={isLoading}
                           onPaginate={onPaginate}
    />;
  } else {
    body = <AttendanceBody attendances={attendances}
                           updateAttendanceLocal={updateAttendanceLocal}
                           past={false}
                           isLoading={isLoading}
                           onPaginate={onPaginate}
    />;
    footer = <div className="attendance-table-footer">
      <button className="error-button xm-button"
              onClick={onCancel}
      >
        Cancel
      </button>
      <button className="big-primary xm-button"
              onClick={onSave}
      >
        Save
      </button>
    </div>
  }

  function nameFunc(opt) {
    if(opt.name.length > 15)
    {
      return opt.name.substring(0, 14) + '...';
    }

    return opt.name;

  }

  return (
    <div className="attendance-table">
      <div className="attendance-table-header">
        <div className="attendance-table-header-subject">
          <span>Attendance For</span>
          <Select placeholder="Select subject"
                  onSearch={onSubjectSearch}
                  options={subjectList}
                  clicked={onSubjectChoose}
                  withSearch
                  searchValue={searchValue}
                  nameFunc={nameFunc}
          />
        </div>
        <div className="attendance-table-header-all">
          {(!isBeforeNow || changeMode) && <Select placeholder="Mark all Us"
                                                   options={[{name: 'Absent', value: ATTENDANCE_MARKS.ABSENT},
                                                     {name: 'Present', value: ATTENDANCE_MARKS.PRESENT}]}
                                                   clicked={onMarkAll}
          />}
        </div>
      </div>
      {body}
      {footer}
    </div>
  );
};

AttendanceTable.propTypes = {
  onSubjectSearch: PropTypes.func,
  subjectList: PropTypes.array,
  onSubjectChoose: PropTypes.func,
  subjectLoading: PropTypes.bool,
  onMarkAll: PropTypes.func,
  searchValue: PropTypes.string,
  attendances: PropTypes.array,
  isBeforeNow: PropTypes.bool,
  isLoading: PropTypes.bool,
};

AttendanceTable.defaultProps = {
  onSubjectSearch: () => {},
  subjectList: [],
  onSubjectChoose: () => {},
  onMarkAll: () => {},
};

export default AttendanceTable;