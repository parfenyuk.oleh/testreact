import React, {Component} from 'react';
import PropTypes from 'prop-types'
import {connect} from 'react-redux';
import Modal from '../../UI/Modal/Modal';
import ModalHeader from '../../UI/Modal/ModalHeader';
import icons from '../../../helpers/iconsLoader';
import ModalBody from '../../UI/Modal/ModalBody';
import Input from '../../UI/Input';
import ModalRow from '../../UI/Modal/ModalRow';
import {userAuthToken} from '../../../helpers';
import Cell from '../../UI/Cell';
import DatePicker from '../../UI/Datepicker';
import Select from '../../UI/Select';
import * as actions from '../../../store/actions';
import AvatarBlock from '../../UI/Elements/AvatarBlock';
import ROLES from '../../../constants/roles';
import Textarea from '../../UI/Textarea';
import validate from '../../../helpers/validator';
import moment from 'moment';
import TimePicker from 'rc-time-picker';

import 'rc-time-picker/assets/index.css';

const GROUP_ID_SELECT = 'groupId';
const filterByTeacher = item => item.role !== ROLES.TEACHER;


const COLORS = [
  'rgba(0,147,255,1)',
  'rgba(3,235,118,1)',
  'rgba(247,152,28,1)',
  'rgba(242,74,94,1)',
  'rgba(38,193,201,1)',
  'rgba(248,231,28,1)',
  'rgba(68,86,108,1)',
];

class EventsModal extends Component {

  constructor(props) {
    super(props);

    this.state = {
      name: {
        value: '',
        error: false,
      },
      description: {
        value: '',
        error: false,
      },
      location: {
        value: '',
        error: false,
      },
      color: {
        value: '',
        error: false,
      },
      startDate: {
        value: '',
        error: false,
      },
      startTime: {
        value: '',
        error: false,
      },
      endDate: {
        value: '',
        error: false,
      },
      endTime: {
        value: '',
        error: false,
      },
      groupId: {
        value: '',
        error: false,
      },
      checkedMembers: new Map(),
      checkedMembersError: false,
      checkedMembersButtonText: '',
      errors: [],
      changeId: -1,
    }

  }

  static getDerivedStateFromProps(props, state) {
    const {changeModel, show} = props;
    const newState = {...state};
    if (changeModel !== null && changeModel.id !== state.changeId && show) {
      props.showLoader();
      props.getMemberActiveGroup(userAuthToken.token(), props.auth.user.institutionId, changeModel.group.id)
        .then(() => {
          props.hideLoader();
        });
      newState.changeId = changeModel.id;
      newState.name.value = changeModel.title;
      newState.description.value = changeModel.description;
      newState.location.value = changeModel.location;
      newState.color.value = changeModel.color;

      newState.startDate.value = moment(changeModel.startDate);
      newState.endDate.value = moment(changeModel.endDate);
      newState.startTime.value = moment(changeModel.startDate);
      newState.endTime.value = moment(changeModel.endDate);

      changeModel.members.forEach(item => {
        newState.checkedMembers.set(`${item}-${item.id}`, true)
      });

      newState.checkedMembersButtonText = changeModel.members.length + ' members';

      newState.groupId.value = changeModel.group.id;
      return newState;
    }
    return null;
  }

  /**
   * Changing values in state depends on field
   * @param value
   * @param type
   */
  inputChangeHandler = (value, type) => {
    if (type === GROUP_ID_SELECT) {
      this.props.getMemberActiveGroup(userAuthToken.token(), this.props.auth.user.institutionId, value);
      this.setState({
        [type]: {
          value: value,
          error: false,
        },
        checkedMembers: new Map(),
        checkedMembersError: false,
        checkedMembersButtonText: '',
      });
    }
    else {
      this.setState({
        [type]: {
          value: value,
          error: false,
        },
      });
    }
  };
  /**
   * Handle choose members in modal
   *
   * @param e
   * @param id
   */
  multislectCheckboxChangeHandler = (e, id) => {
    const item = e.target.name;
    const isChecked = e.target.checked;

    this.setState(prevState => ({
      checkedMembers: prevState.checkedMembers.set(item, isChecked),
    }), this.setCheckedMembersButtonText);
  };

  /**
   *  Modify input array, add field name with <AvatarBlock> component inside
   *  for correct mapping in select
   *
   * @param {array} users
   * @returns {array} users with additional property name
   */
  prepareMemberOptions = (users) => {
    return users.map(user => {
      user.name = <AvatarBlock textAvatar={`${user.firstName[0]}${user.lastName[0]}`}
                               customInfo={<span>{`${user.firstName} ${user.lastName}`}</span>}
                               imgSrc={user.avatar}
      />;
      return user;
    }).filter(filterByTeacher);
  };

  /**
   * Check or uncheck all checkboxes depends on Map.size and Map values
   */
  checkAllHandler = () => {
    const {checkedMembers} = this.state;
    const {groups} = this.props;
    groups.activeGroup = groups.activeGroup.filter(filterByTeacher);
    if (checkedMembers.size < groups.activeGroup.length) {
      groups.activeGroup.forEach(item => {
        checkedMembers.set(`${item.name}-${item.id}`, true);
      })
    } else {
      let isAllTrue = true;
      checkedMembers.forEach(value => {
        if (!value) { isAllTrue = false}
      });
      checkedMembers.forEach((value, key) => {
        checkedMembers.set(key, !isAllTrue);
      })
    }
    this.setState({checkedMembers}, this.setCheckedMembersButtonText);
  };

  setCheckedMembersButtonText = () => {
    const {checkedMembers} = this.state;
    const {groups} = this.props;
    let count = 0;
    checkedMembers.forEach(value => {if (value) count++;});
    if (count === groups.activeGroup.length) {
      this.setState({checkedMembersButtonText: 'All members in group'});
    } else if (count > 0) {
      this.setState({checkedMembersButtonText: `${count} member${count > 1 ? 's' : ''} in group`});
    } else {
      this.setState({checkedMembersButtonText: ''})
    }
  };

  componentDidMount() {
    this.props.getAllGroups(userAuthToken.token(), this.props.auth.user.institutionId)
  }

  collectData = () => {
    let {
      name, startDate, startTime, endDate, endTime, location, groupId, checkedMembers, description, color,
    } = this.state;

    const eventModel = {
      title: name.value,
      description: description.value,
      location: location.value,
      color: color.value,
      startDate: startDate.value.format('YYYY-MM-DD') + 'T' + startTime.value.format('HH:mm:ss'),
      endDate: endDate.value.format('YYYY-MM-DD') + 'T' + endTime.value.format('HH:mm:ss'),
      groupId: groupId.value,
      memberIds: [],
    };

    const memberIds = [];

    checkedMembers.forEach((value, key) => {
      if (value) {
        memberIds.push(Number(key.replace('[object Object]-', '')));
      }
    });

    eventModel.memberIds = memberIds;

    return eventModel;
  };

  saveHandler = () => {
    let {
      name, startDate, startTime, endDate, endTime, location, groupId, checkedMembers,
      checkedMembersError, description, color,
    } = this.state;
    const errors = [];

    name.error = validate('Title', name.value, {isNotEmpty: true});
    description.error = validate('Description', location.value, {isNotEmpty: true});
    location.error = validate('Location', location.value, {isNotEmpty: true});
    startDate.error = validate('Start Date', startDate.value, {isNotEmptyDate: true});
    endDate.error = validate('End Date', endDate.value, {isNotEmptyDate: true, notLessDate: startDate.value});
    groupId.error = validate('Group', groupId.value.toString(), {isNotEmpty: true});
    color.error = validate('Color', color.value, {isNotEmpty: true});

    if (groupId.value) {
      let checkedMembersCount = 0;
      checkedMembers.forEach(value => { if (value) { checkedMembersCount++}});
      checkedMembersError = validate('Group members count', checkedMembersCount, {isNotEqualToZero: true});
    }

    startTime.error = validate('Start Time', startTime.value, {isNotEmptyDate: true});

    const endTimeRule = {isNotEmptyDate: true};

    if (!endDate.error && !startDate.error && endDate.value.isSame(startDate.value)) {
      endTimeRule.notLessTime = startTime.value;
    }
    endTime.error = validate('End Time', endTime.value, endTimeRule);


    if (name.error) {errors.push(name.error)}
    if (description.error) {errors.push(description.error)}
    if (location.error) {errors.push(location.error)}
    if (groupId.error) {errors.push(groupId.error)}
    if (color.error) {errors.push(color.error)}
    if (checkedMembersError) {errors.push(checkedMembersError)}

    if (startDate.error) {errors.push(startDate.error)}
    if (endDate.error) {errors.push(endDate.error)}

    if (startTime.error) {errors.push(startTime.error)}
    if (endTime.error) {errors.push(endTime.error)}

    if (!errors.length) {
      if(this.props.changeModel){
        this.props.onUpdate(this.collectData());
      }else {
        this.props.onSave(this.collectData());
      }
    }

    this.setState({
      name,
      description,
      location,
      startTime,
      endTime,
      startDate,
      endDate,
      groupId,
      checkedMembersError,
      errors,
    });

  };

  render() {
    const {show, hideModal, changeModel, groups} = this.props;
    const {
      errors, name, startDate, startTime, endDate, endTime, location, groupId, checkedMembers,
      checkedMembersButtonText, checkedMembersError, description, color,
    } = this.state;

    const checkAllOption = (<AvatarBlock textAvatar={`#1`}
                                         customInfo={<span>All members in group</span>}
    />);


    return (
      <Modal show={show} hideModal={hideModal}>
        <ModalHeader>
          <h3 className="modal-title">{changeModel ? 'Edit event' : 'New event'}</h3>
        </ModalHeader>
        <ModalBody bigPadding overflow={false}>
          {errors.length > 0 && <div className="auth-errors">
            <ul>
              {errors.map(item => <li key={item}><img src={icons.iconAttention} alt=""/> {item} </li>)}
            </ul>
          </div>
          }
          <ModalRow>
            <Input placeholder="Title"
                   defaultValue={name.value}
                   changed={(e) => {this.inputChangeHandler(e.target.value, 'name')}}
                   className={name.error ? 'error' : ''}
            />
          </ModalRow>
          <ModalRow flex>
            <Cell column={7}>
              <DatePicker placeholder={'Start date'}
                          changed={(value) => {this.inputChangeHandler(value, 'startDate')}}
                          selected={startDate.value}
                          error={startDate.error}
              />
            </Cell>
            <Cell column={2}/>
            <Cell column={7}>
              <div className={`date-picker time ${startTime.error ? 'error' : ''}`}>
                <img src={icons.iconClock} alt='clock'/>
                <TimePicker
                  showSecond={false}
                  defaultValue={startTime.value}
                  className="xxx"
                  onChange={(value) => {this.inputChangeHandler(value, 'startTime')}}
                  format={'h:mm a'}
                  use12Hours
                  inputReadOnly
                  placeholder={'Start time'}
                />
              </div>
            </Cell>

          </ModalRow>
          <ModalRow flex>
            <Cell column={7}>
              <DatePicker placeholder={'End date'}
                          changed={(value) => {this.inputChangeHandler(value, 'endDate')}}
                          selected={endDate.value}
                          error={endDate.error}
              />
            </Cell>
            <Cell column={2}/>
            <Cell column={7}>
              <div className={`date-picker time ${endTime.error ? 'error' : ''}`}>
                <img src={icons.iconClock} alt='clock'/>
                <TimePicker
                  showSecond={false}
                  defaultValue={endTime.value}
                  className="xxx"
                  onChange={(value) => {this.inputChangeHandler(value, 'endTime')}}
                  format={'h:mm a'}
                  use12Hours
                  inputReadOnly
                  placeholder={'End time'}
                />
              </div>

            </Cell>
          </ModalRow>
          <ModalRow>
            <Input placeholder="Location"
                   defaultValue={location.value}
                   changed={(e) => {this.inputChangeHandler(e.target.value, 'location')}}
                   className={location.error ? 'error' : ''}
            />
          </ModalRow>
          <Select placeholder={'Choose group'}
                  options={groups.list}
                  classic
                  clicked={(e) => {this.inputChangeHandler(e.id, GROUP_ID_SELECT)}}
                  error={groupId.error}
                  checkedItems={groupId.value}
          />
          {groupId.value && <Select placeholder={'Choose group members'}
                                    options={this.prepareMemberOptions(groups.activeGroup)}
                                    classic
                                    multiselect
                                    checkedItems={checkedMembers}
                                    handleInputChange={this.multislectCheckboxChangeHandler}
                                    checkAllOption={checkAllOption}
                                    onCheckAll={this.checkAllHandler}
                                    text={checkedMembersButtonText}
                                    error={checkedMembersError}
          />}
          <Textarea placeholder="Description"
                    text={description.value}
                    changed={(e) => {this.inputChangeHandler(e.target.value, 'description') }}
                    error={description.error}
          />

          <div className="events-colors">
            <div className={`events-colors-title ${color.error ? 'error' : null}`}>Event color:</div>
            {COLORS.map(currColor => <div style={{backgroundColor: currColor}}
                                          className={currColor === color.value ? 'active' : ''}
                                          onClick={() => { this.inputChangeHandler(currColor, 'color') }}
                                          key={currColor}
            />)}
          </div>
          <button className="big-primary events-button" onClick={this.saveHandler}>Save</button>
        </ModalBody>
      </Modal>
    );
  }
}

EventsModal.propTypes = {};

const mapStateToProps = ({groups, auth}) => ({groups, auth});

const mapDispatchToProps = dispatch => ({
  getMemberActiveGroup: (token, id, groupId) => {return dispatch(actions.getMemberActiveGroup(token, id, groupId))},
  getAllGroups: (token, id) => {dispatch(actions.getAllGroups(token, id))},
  showLoader: () => {dispatch(actions.showLoader())},
  hideLoader: () => {dispatch(actions.hideLoader())},
});

export default connect(mapStateToProps, mapDispatchToProps)(EventsModal);