import React from 'react';

const Backdrop = ({show, clicked, className, children}) =>
  show ? <div className={`backdrop ${className}`}
              onClick={(e) => {
                if(e.target.classList && e.target.classList.contains('backdrop'))
                  clicked();
              }}
  > {children}</div> : null;

Backdrop.defaultProps = {
 clicked : () => {}
};

export default Backdrop;