import React from 'react';

const CardBody = ({children, justify, align, clicked}) => {
  return (
    <div className="Card--body"
         style={{justifyContent : justify, alignItems : align}}
         onClick={clicked}
    >
      {children}
    </div>
  );
};

CardBody.defaultProps = {
  justify : 'flex-start',
  align : 'flex-start'
};
export default CardBody;
