import React from 'react';

const CardFooter = ({children, className, justify, action}) => {
  return (
    <div className={`Card--footer ${className}`}
         style={{justifyContent: justify}}
         onClick={action}
    >
      {children}
    </div>
  );
};

CardFooter.defaultProps = {
  justify: 'flex-start',
  className: ''
};

export default CardFooter;
