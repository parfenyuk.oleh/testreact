import React from 'react';
import propTypes from 'prop-types';

const Cell = ({column, className, children, justify, block}) => (
  <div style={!block ? { width: `${column*100/16}%` ,
    display: 'flex',
    justifyContent: justify,
    flexWrap : 'wrap'
  } : {width: `${column*100/16}%` , display: 'block',}} className={className}>
    {children}
  </div>
);

Cell.propTypes = {
  column : propTypes.number,
  className : propTypes.string,
  justify : propTypes.string
};

Cell.defaultProps = {
  justify : 'flex-start',
  column : 16
};

export default Cell;