import React, {Component} from 'react';

class ColorPicker extends Component {
  render() {
    return (
      <div className="colorPicker">
        <input type="color"/>
        <button className="button-default small not-upper">Select a custom color…</button>
      </div>
    );
  }
}

export default ColorPicker;