import React, {Component} from 'react';
import ReactDatepicker from 'react-datepicker';
import icons from '../../helpers/iconsLoader';
import moment from 'moment';
import PropTypes from 'prop-types';

import 'react-datepicker/dist/react-datepicker.css';

class DatePicker extends Component {
  state = {
    selected: null,
    isOpen: false,
  };


  changeHandler = (e) => {
    this.props.changed(e);
    this.setState({selected: e});
  };

  static getDerivedStateFromProps(props, state) {
    if (props.selected && state.selected !== props.selected) {
      return {
        selected: props.selected,
      }
    }
    return null;
  }

  render() {
    const {placeholder, error, showTimeSelect, modalError, popperPlacement, maxDate, highlightDates} = this.props;
    const {selected} = this.state;
    let picker = <ReactDatepicker placeholderText={placeholder}
                                  dropdownMode={`select`}
                                  onChange={this.changeHandler}
                                  showTimeSelect={showTimeSelect}
                                  showTimeSelectOnly={showTimeSelect}
                                  timeIntervals={5}
                                  dateFormat={showTimeSelect ? 'h:mm a' : null}
                                  onKeyDown={(e) => { e.preventDefault(); return false}}
                                  popperPlacement={popperPlacement || 'bottom-start'}
                                  maxDate={maxDate}
                                  highlightDates={highlightDates}
    />;

    if (selected) {
      picker = <ReactDatepicker dropdownMode={`select`}
                                onChange={this.changeHandler}
                                selected={moment(selected)}
                                showTimeSelect={showTimeSelect}
                                showTimeSelectOnly={showTimeSelect}
                                timeIntervals={5}
                                dateFormat={showTimeSelect ? 'h:mm a' : null}
                                onKeyDown={(e) => { e.preventDefault(); return false}}
                                shouldCloseOnSelect
                                popperPlacement={popperPlacement || 'bottom-start'}
                                maxDate={maxDate}
                                highlightDates={highlightDates}
      />
    }
    return (
      <div className={`date-picker ${error ? 'error' : ''}`}>
        <img src={error ? icons.iconCalendarRed : icons.iconCalendar } alt='calendar'/>
        {picker}
        {modalError && <div className="with-error">{modalError}</div>}
      </div>
    );
  }
}

DatePicker.defaultProps = {
  showTimeSelect: false,
};

DatePicker.propTypes = {
  showTimeSelect: PropTypes.bool,
};
export default DatePicker;