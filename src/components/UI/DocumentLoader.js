import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Icon from '../UI/Elements/Icon'

import fileReader from '../../helpers/fileReader';

class DocumentLoader extends Component {

    constructor(props){
        super(props);
        this.state = {
            name: null
        };
        this.fileInput = React.createRef();
    }

    fileChangeHandler = ({target: { files }}) => {
        fileReader(files[0],result => {
            this.setState({name: files[0].name})
        });
        this.props.changed(files[0]);
    };

    showFileWindow = () => {
        this.fileInput.current.click();
    };

    clearFile = ({target: { files }}) => {
        this.fileInput.current.value = '';
        this.setState({name: null});
        this.props.changed(null);
    };

    render() {
        const {document, trigger} = this.props;

        let name = document ? this.state.name : null;

        return (
            <div className="imageLoader" style={{justifyContent: 'center', textAlign: 'center'}}>
                <input type="file" onChange={this.fileChangeHandler} ref={this.fileInput}/>
                <div className="imageLoader-buttons">
                    {trigger &&
                    <div className="imageLoader-trigger"
                         onClick={this.showFileWindow}
                    >
                        {trigger}
                    </div>
                    }
                    {
                        name && (
                            <div className="name-file">
                                <Icon name="doc-small" />
                                <p>{name}</p>
                                <div className="cross">
                                    <Icon name="cross" onClick={this.clearFile}/>
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
        );
    }
}

DocumentLoader.propTypes = {
    changed: PropTypes.func,
    placeholder: PropTypes.object,
    trigger: PropTypes.oneOfType([
        PropTypes.instanceOf(Element),
        PropTypes.element
    ]),
};

DocumentLoader.defaultProps = {
    placeholder: {},
    trigger: null
};

export default DocumentLoader;