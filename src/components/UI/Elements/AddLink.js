import React,{Component} from 'react';
import Input from '../../UI/Input';
import Icon from './Icon';

class AddLink extends Component {
    state = {
        links: [],
        currentValue:''
    };

    addLinks = () => {
       let {links, currentValue} = this.state;
        if (links.indexOf(currentValue) === -1) {
          if(/\S/.test(currentValue)) {
              links.unshift(currentValue);
          }
        }
       currentValue = '';
       this.props.onNewLinkAdded(links);
       this.setState({currentValue, links})
    };

    inputChangeHandler = (value, field) => {
        const state = {...this.state};
        state[field] = value;
        this.setState(state);
    };

    componentDidUpdate(prevProps) {
      if(prevProps.defaultLinks.length !== this.props.defaultLinks.length){
        this.setState({
          links: this.props.defaultLinks,
        })
      }
    }

    render() {
        const {currentValue} = this.state;
        return (
            <div className='addMore'>
                <Input placeholder={this.props.placeholder}
                       defaultValue={currentValue}
                       changed={(e) => { this.inputChangeHandler(e.target.value, 'currentValue') }}
                />
                <div className={currentValue.length === 0? 'addLinkGrey' : 'addLink'}
                     onClick = {this.addLinks}
                >
                    <Icon name='addModal'/>
                </div>
            </div>
        );
    }
}

AddLink.defaultProps = {
  defaultLinks: [],
};

export default AddLink;