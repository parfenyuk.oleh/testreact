import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import RoundAvatar from "../RoundAvatar";

const AvatarBlock = ({
                    size,
                    visibleInfo,
                    forum,
                    onClick,
                    className,
                    style,
                    children,
                    createdAt,
                    activeUntil,
                    imgSrc,
                    cityName,
                    textAvatar,
                    online,
                    customInfo,
                    lider,
                    ...otherProps
                }) => {
    const baseClass = 'avatar-block';

    // TO DO: add this prop for RoundAvatar
    const imgClass = cx(className, {
        [`avatar-block__img--${size}`]: size,
    });

    return (
        <div className={baseClass} style={style} onClick={onClick} {...otherProps}>
            <RoundAvatar avatar={imgSrc} className={imgClass} text={textAvatar} online={online}/>
            <div className="avatar-block__info">
                <span className="name">{children}</span>
                {
                    visibleInfo && (<span className="city">{cityName}</span>)
                }
                {
                    forum ? (<div>
                        <span className="created">Created at: {createdAt}</span>
                        <span className="inactive">Active until: {activeUntil}</span>
                    </div>) : null
                    //   (
                    //     ''
                    //     {/*<div>*/}
                    //         {/*<span className="created">Created at: {createdAt}</span>*/}
                    //     {/*</div>*/}
                    // )
                }
              {customInfo}
            </div>
        </div>
    );
};

AvatarBlock.displayName = 'AvatarBlock';

AvatarBlock.defaultProps = {
    size: 'md',
    forum: false,
    visibleInfo: false,
    onClick: null,
    style: {},
    customInfo : null
};

AvatarBlock.propTypes = {
    size: PropTypes.oneOf(['sm', 'md', 'lg']),
    forum: PropTypes.bool,
    visibleInfo: PropTypes.bool,
    onClick: PropTypes.func,
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node,
    imgSrc: PropTypes.string,
    cityName: PropTypes.string,
    textAvatar: PropTypes.string,
    customInfo: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element
    ])
};

export default AvatarBlock;