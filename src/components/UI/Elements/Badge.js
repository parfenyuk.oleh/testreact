import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Badge = ({
                   type,
                   className,
                   style,
                   children,
                   ...otherProps
               }) => {
    const baseClass = 'badge';

    const rootClass = cx(baseClass, className, {
        [`${baseClass}--${type}`]: type,
    });

    return (
        <i className={rootClass} style={style} {...otherProps}>
            {children}
        </i>
    );
};

Badge.displayName = 'Badge';

Badge.defaultProps = {
    style: {}
};

Badge.propTypes = {
    type: PropTypes.oneOf(['circle']),
    style: PropTypes.object,
    children: PropTypes.node
};

export default Badge;