import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';
import {default as Buttons} from '@material-ui/core/Button';

const Button = ({
                    context,
                    type,
                    size,
                    group,
                    onClick,
                    className,
                    style,
                    children,
                    disabled,
                    ...otherProps
                }) => {
    const baseClass = 'button-wrapper';

    const rootClass = cx(baseClass, className, {
        [`${baseClass}--${context}`]: context,
        [`${baseClass}--${size}`]: size,
        [`${baseClass}-group__button`]: group
    });

    return (
        <div className={rootClass} type={type} onClick={onClick} {...otherProps}>
            <Buttons variant="contained" disabled={disabled} style={style}>
                {children}
            </Buttons>
        </div>
    );
};

Button.displayName = 'Button';

Button.defaultProps = {
    context: 'primary',
    disabled: false,
    group: false,
    onClick: null,
    style: {}
};

Button.propTypes = {
    context: PropTypes.string,
    size: PropTypes.oneOf(['xs', 'sm', 'md', 'lg', 'xl']),
    group: PropTypes.bool,
    onClick: PropTypes.func,
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node
};

export default Button;