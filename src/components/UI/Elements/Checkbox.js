import React from 'react';
import PropTypes from 'prop-types';
import {default as CheckboxMaterial} from '@material-ui/core/Checkbox';
import { withStyles } from "@material-ui/core/styles";
import green from '@material-ui/core/colors/green';

const styles = {
    root: {
        color: green[600],
        '&$checked': {
            color: green[500],
        },
    },
    checked: {},
};

const Checkbox = ({ type = 'checkbox', name, checked = false, onChange, classes, disabled }) => (
    <CheckboxMaterial
        classes={{
            root: classes.root,
            checked: classes.checked,
        }}
        disabled={disabled}
        name={name}
        checked={checked}
        onChange={onChange} />
);

Checkbox.propTypes = {
    type: PropTypes.string,
    name: PropTypes.string,
    checked: PropTypes.bool,
    onChange: PropTypes.func.isRequired,
};

Checkbox.defaultProps = {
    disabled: false
};

export default withStyles(styles)(Checkbox);