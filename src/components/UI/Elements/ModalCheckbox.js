import React,{Component} from 'react';

import Icon from './Icon';

class ModalCheckBox extends Component {

    state = {
        checked: false,
    };

    changeCheckbox = () => {
        this.setState({checked: !this.state.checked}, () => {
          this.props.onChange();
        });
    };

    componentDidUpdate() {
      if(this.state.checked !== this.props.value){
          this.setState({
            checked: this.props.value,
          })
      }
    };

    render() {
        return (
            <div className="ModalCheckBox" onClick={this.changeCheckbox}>
                {this.state.checked && <Icon name='submit'/>}
            </div>
        );
    }
}

ModalCheckBox.propTypes = {};

export default ModalCheckBox;