import React from 'react';
import PropTypes from 'prop-types';

const MultistepsBar = ({
                    elementActive,
                    elementArray,
                    onClick,
                }) => {

    return (
        <div className="container-fluid">
            <br/><br/>
            <ul className="list-unstyled multi-steps">
                {elementArray.map(item => (
                    <li key={item.id}
                        className={item.id === elementActive ? 'is-active' : ''}
                        onClick={() => { onClick(item)}}
                    >{item.name}</li>
                ))}
            </ul>
        </div>
    );
};

MultistepsBar.propTypes = {
    elementActive: PropTypes.number,
    elementArray: PropTypes.array,
};

export default MultistepsBar;