import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select/Select';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import InputLabel from '@material-ui/core/es/InputLabel/InputLabel';

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap'
    },
    margin: {
        margin: '0',
        width: '100%',
        marginBottom: '10px'
    },
    cssLabel: {
        color: '#666',
        fontSize: '14px',
        '&$cssFocused': {
            color: '#666',
        }
    },
    cssFocused: {},
    cssUnderline: {
        '&:after': {
            borderBottomColor: '#666'
        }
    },
    option: {
        color: '#666'
    }
});

class SelectMaterial extends Component {
    render() {
        const {activeItem, handleChange, classes, itemList, placeholder, name, isDisabled, inputLabel} = this.props;
        return (
            <div className={classes.container}>
                <FormControl className={classes.margin} disabled={isDisabled}>
                    {
                        !itemList.length
                            ? <InputLabel style={{
                                top: "20px",
                                fontSize: 20,
                                letterSpacing: 0.29,
                                lineHeight: "23px",
                                fontWeight: 400,
                                color: "#b3c0ce",
                                fontFamily: "Titillium Web"
                            }}>
                                {inputLabel}
                            </InputLabel>
                            : null
                    }
                    <Select
                        value={activeItem}

                        onChange={handleChange}
                        inputProps={{
                            name: name ? name : 'activeItem',
                            id: 'activeItem',
                        }}
                        className={`select-custom`}
                    >
                        {
                            placeholder && (
                                <MenuItem selected disabled value='none'>
                                    {placeholder}
                                </MenuItem>
                            )
                        }
                        {
                            itemList.map((item) => (
                                !item.archievedAt && (
                                    <MenuItem className={classes.option} key={item.id}
                                              value={item.id}>{item.name}</MenuItem>
                                )
                            ))
                        }
                    </Select>
                </FormControl>
            </div>
        );
    }
}

SelectMaterial.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(SelectMaterial);