import React from 'react';
import PropTypes from 'prop-types';
import cx from 'classnames';

const Title = ({
                    context,
                    size,
                    className,
                    style,
                    children,
                    ...otherProps
                }) => {
    const baseClass = 'title';

    const rootClass = cx(baseClass, className, {
        [`${baseClass}--${size}`]: size,
    });

    return (
        <p className={rootClass} style={style} {...otherProps}>
            {children}
        </p>
    );
};

Title.displayName = 'Title';

Title.defaultProps = {
    context: 'primary',
    onClick: null,
    style: {}
};

Title.propTypes = {
    size: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4']),
    style: PropTypes.object,
    children: PropTypes.node
};

export default Title;