import React from 'react';
import noDataImage from '../../images/no_data.svg'

const EmptyState = ({text}) =>
  (
    <div className="emptyState">
      <img src={noDataImage} alt=""/>
      <p>{text ? text : 'No data yet.'}</p>
    </div>
  );

export default EmptyState;