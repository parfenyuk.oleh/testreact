import React, {Component} from 'react';

import icons from '../../../helpers/iconsLoader';


/*
 * Props: content      -> html or text which contains content which we wanna edit
 *        unchangeable -> true/false if true prevent input text in editor
 */

class HtmlEditor extends Component {

  constructor(props) {
    super(props);
    this.state = {
      editorCoordinates: {
        x: 0,
        y: 0,
      },
      showControls: true,
      controlsCoordinates: {
        x: 0,
        y: 0,
      },
      controlsSizes: {
        width: 0,
        height: 0,
      },
      mouseStartSelect: 0,
      lastAction: '',
      showLink: false,
      resizeableImage: null,
      content: [{
        ownerId: 1,
        content: 'f',
        styles: ['bold'],
      }, {
        ownerId: 2,
        content: 'd',
        styles: ['italic'],
      }],
    };
    this.inputFile = React.createRef();
    this.editor = React.createRef();
  }

  componentDidMount() {
    const editor = this.editor.current;
    const controls = editor.querySelector('.editor-controls');
    const inModal = this.inModal();

    this.setState({
      editorCoordinates: {
        x: editor.offsetLeft + inModal.x,
        y: editor.offsetTop + inModal.y,
      },
      showControls: false,
      controlsSizes: {
        width: controls.offsetWidth,
        height: controls.offsetHeight,
      },
    })
  }

  inModal = () => {
    let element = this.editor.current.parentNode;

    while (true) {
      if (element.classList.contains('modal')) {
        return {
          x: element.offsetLeft - element.offsetWidth / 2,
          y: element.offsetTop - element.offsetHeight / 2,
        };
      }
      if (element.tagName === 'BODY') {
        return {x: 0, y: 0};
      }
      element = element.parentNode;
    }

  };

  makeBold = () => {document.execCommand('bold', false, null)};
  makeItalic = () => {document.execCommand('italic', false, null)};
  makeUnderline = () => {document.execCommand('underline', false, null)};
  makeLineThrough = () => {document.execCommand('strikeThrough', false, null)};
  addBlockQuote = () => {document.execCommand('formatBlock', false, '<blockquote>')};
  makeLink = () => {document.execCommand('createLink', false, prompt('Insert your link'))};
  makeImage = () => {
    document.execCommand('insertImage', false, 'https://www.cbronline.com/wp-content/uploads/2016/06/what-is-URL-770x503.jpg');

    /*
     *Need to add ajax where we send image and that response url
     * console.log('image', this.inputFile.current.files[0]);
     */

  };

  makeComment = () => {
    document.execCommand('backColor', false, 'rgba(0,0,0,0.2)');
    const selectedNode = window.getSelection().focusNode.parentNode;

    selectedNode.className = 'comment';
    selectedNode.id = 'id';
  };

  mouseDownHandler = e => {
    const tagName = e.nativeEvent.target.tagName;

    if (tagName !== 'IMG' && tagName !== 'BUTTON' && tagName !== 'INPUT') {
      this.setState({mouseStartSelect: e.nativeEvent.clientX, showControls: false, lastAction: 'mouseDown'});
    }
    if (tagName === 'IMG') {
      e.nativeEvent.preventDefault();
      this.setState({resizeableImage: e.nativeEvent.target, showControls: false});
      this.initialiseResize();
    }
  };

  inputHandler = (e) => {
    if (this.state.showControls) {this.setState({showControls: false, lastAction: 'input'});}
    else {this.setState({lastAction: 'input'});}
    console.log(e.nativeEvent.target);
    return false;

  };

  selectionChange = event => {
    if (this.state.lastAction === 'input') {return;}
    if (this.state.mouseStartSelect !== event.nativeEvent.clientX) {
      try {// Bag fix
        this.controlsStateUpdate(event);
      } catch (e) {}
    }
  };

  controlsStateUpdate = ({nativeEvent: {clientX, clientY, target}}) => {
    const {mouseStartSelect, editorCoordinates, controlsSizes} = this.state;
    const coordinateX = (mouseStartSelect + clientX) / 2 - editorCoordinates.x - controlsSizes.width * 0.78;
    const editor = this.editor.current.parentNode;
    const lineHeight = Number(window.getComputedStyle(target).lineHeight.replace('px', ''));
    const mousePosition = ~~((clientY - editorCoordinates.y - target.offsetTop + editor.scrollTop) / lineHeight) * lineHeight;

    this.setState({
      showControls: true,
      controlsCoordinates: {
        x: coordinateX,
        y: target.offsetTop + mousePosition - controlsSizes.height - 10,
      },
    });
  };

  initialiseResize() {
    const editor = this.editor.current;

    editor.style.height = `${editor.offsetHeight}px`;
    window.addEventListener('mousemove', this.startResizing, false);
    window.addEventListener('mouseup', this.stopResizing, false);
  }

  startResizing = e => {
    const {resizeableImage, editorCoordinates} = this.state;
    const editor = this.editor.current;

    resizeableImage.style.width = `${e.clientX - resizeableImage.offsetLeft - editorCoordinates.x}px`;
    resizeableImage.style.height = `${e.clientY - (resizeableImage.offsetTop - editor.parentNode.scrollTop) - editorCoordinates.y}px`;
  };

  stopResizing = () => {
    this.setState({resizeableImage: null, showControls: false});
    window.removeEventListener('mousemove', this.startResizing, false);
    window.removeEventListener('mouseup', this.stopResizing, false);
    this.editor.current.style.height = 'auto';
  };

  unChangeAbleText = e => {
    console.log('keypress');
    e.preventDefault();
    return false;
  };


  jsonContentToHtml = () => {
    const {content} = this.state;
    return content.map(item => {
      return <span className={item.styles.reduce((acc,item) => `${acc} ${item}`)}>{item.content}</span>
    })
  };

  render() {
    return (
      <div className="editor" ref={this.editor}>
        <div className={`editor-controls ${this.state.showControls ? 'show' : ''}`}
             style={{
               top: `${this.state.controlsCoordinates.y}px`,
               left: `${this.state.controlsCoordinates.x}px`,
             }}
        >
          <button onClick={this.makeBold}>
            <img src={icons.iconBold} alt=""/>
          </button>
          <button onClick={this.makeItalic}>
            <img src={icons.iconItalic} alt=""/>
          </button>
          <button onClick={this.makeUnderline}>
            <img src={icons.iconUnder} alt=""/>
          </button>
          <button onClick={this.makeLineThrough}>
            <img src={icons.iconCutText} alt=""/>
          </button>
          <button onClick={this.addBlockQuote}>
            <img src={icons.iconQuote} alt=""/>
          </button>
          <button onClick={this.makeLink}>
            <img src={icons.iconLink} alt=""/>
          </button>
          <button className="editor-file" onClick={this.makeImage}>
            <img src={icons.iconPic} alt=""/>
            {/* <input type="file" onChange={this.makeImage} ref={this.inputFile}/>*/}
          </button>
          <button onClick={this.makeComment}>
            <img src={icons.iconComment} alt=""/>
          </button>
        </div>
        <br/>
        <div contentEditable
             onSelect={this.selectionChange}
             onMouseDown={this.mouseDownHandler}
             onInput={this.inputHandler}
             onDoubleClick={this.controlsStateUpdate}
             onKeyPress={this.props.unchangeable ? this.unChangeAbleText : null}
        >
          {this.jsonContentToHtml()}
          {/*{this.props.content}*/}
          {/* {content}*/}
        </div>
      </div>
    );
  }
}

export default HtmlEditor;
