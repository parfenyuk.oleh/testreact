import React from 'react';

function filterDigitsFromInput (event){
  event.target.value = event.target.value.replace(/[^\d.]/g, '');
  return event;
}

const Input  = ({label, value, placeholder, clicked, changed, className, disabled, type, name, defaultValue, number, modalError, min, max}) => (
  <div className={`input-field ${className}`} onClick={clicked} >
    {label ? <label>{label}</label> : null}
    <input value={value ? value : defaultValue}
           placeholder={placeholder}
           onChange={(e) => { number ? changed(filterDigitsFromInput(e)) : changed(e)}}
           disabled={disabled}
           type={type || 'text'}
           name={name}
    />
      {modalError && <div className="with-error">{modalError}</div>}

  </div>
);

export default Input;