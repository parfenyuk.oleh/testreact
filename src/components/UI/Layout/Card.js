import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import AvatarBlock from '../Elements/AvatarBlock';
import Badge from '../Elements/Badge';
import Icon from '../Elements/Icon';
import {userType} from '../../../constants';


const Card = ({
                  forum,
                  info,
                  role,
                  createdAt,
                  activeUntil,
                  cardClick,
                  className,
                  style,
                  userId,
                  children,
                  editMode,
                  isPinup,
                  ...otherProps
              }) => {
    const maxLength = 500;
    if(children.length > maxLength && !isPinup) children = children.substring(0,500) + '...';


    const descriptionStyle = {};
    if(isPinup){
      children = children.split('\n');
      descriptionStyle['maxHeight'] = '166px';
      descriptionStyle['overflow'] = 'auto';
    }

    return (
        <div className='card-block' style={style} onClick={(e) => {
            cardClick(e, info.id)
        }}>
            <div className='card-block__main'>
                <div className='top-part'>
                    <AvatarBlock
                        forum={forum}
                        createdAt={createdAt}
                        activeUntil={activeUntil}
                        imgSrc={info.avatar}
                        textAvatar={`${info.firstCreatorName[0]} ${info.lastCreatorName[0]}`}
                    >
                        {info.firstCreatorName} {info.lastCreatorName}
                    </AvatarBlock>
                </div>
                <p className='title'>
                    {info.name}
                </p>
                {
                    forum && (
                        <p className='subject'>
                            {info.subjectName}
                        </p>
                    )
                }
                <p className='text' style={descriptionStyle}>
                    {!isPinup && children}
                  {isPinup && children.map((item, index,) => {
                    return <span style={{display: 'block'}} key={item + index}>{item}</span>
                  }) }
                </p>
                <span className='more'>
                </span>
            </div>
            <div className='card-block__footer'>
                <div className='badge-wrapper'>
                    {
                        info.repliesCount !== undefined && (
                            <Fragment>
                                <Badge type='circle'>
                                    {info.repliesCount}
                                </Badge>
                                <span>
                        Replies
                    </span>
                            </Fragment>
                        )
                    }

                </div>
                <div className='icon-block'>
                    {
                        editMode ? (((info.creatorId === userId && userType.teacher !== role) || userType.admin === role)
                            && (<div className='icon edit'>
                                <Icon name='edit-dark'/>
                                Edit
                            </div>)) : (info.creatorId === userId || userType.admin === role) && (
                            <div className='icon delete'>
                                <Icon name='delete-dark'/>
                                Delete
                            </div>
                        )
                    }
                    {
                        !editMode ? (<div className='icon'>
                            <Icon name='reply'/>
                            Reply
                        </div>) : (info.creatorId === userId || userType.admin === role) && (
                            <div className='icon delete'>
                                <Icon name='delete-dark'/>
                                Delete
                            </div>
                        )
                    }
                </div>
            </div>
        </div>
    );
};

Card.displayName = 'Card';

Card.defaultProps = {
    forum: true,
    onClick: null,
    style: {},
    editMode: false
};

Card.propTypes = {
    forum: PropTypes.bool,
    imgSrc: PropTypes.string,
    firstCreatorName: PropTypes.string,
    lastCreatorName: PropTypes.string,
    topicName: PropTypes.string,
    subjectName: PropTypes.string,
    createdAt: PropTypes.string,
    repliesCount: PropTypes.number,
    onClick: PropTypes.func,
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.string,
};

export default Card;