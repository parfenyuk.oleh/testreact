import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Icon from '../Elements/Icon';
import RoundAvatar from "../RoundAvatar";
import {userType} from "../../../constants";

const CommentItem = ({
                   user,
                   creatorId,
                   imgSrc,
                   firstName,
                   lastName,
                   date,
                   onClick,
                   className,
                   role,
                   style,
                   children,
                   editMode,
                   removeComment,
                   ...otherProps
               }) => {
    let name = `${firstName[0]}${lastName[0]}`;

    return (
        <div className="comment-block">
            <RoundAvatar avatar={imgSrc} text={name}/>
            <div className="comment-item__wrapper">
                <div className="comment-item" style={style} {...otherProps}>
                    <div className="comment-item__info">
                        <p className="name">
                            {firstName} {lastName} <span className="date">{date}</span>
                        </p>
                        <p>
                            {children}
                        </p>
                    </div>
                </div>
                {
                    (user.id === creatorId || userType.admin === user.role) && (
                        <div className="mode">
                            <span onClick={editMode}>Edit</span>
                            <span onClick={removeComment}>Delete</span>
                        </div>
                    )
                }
            </div>

        </div>
    );
};

CommentItem.displayName = 'CommentItem';

CommentItem.defaultProps = {
    onClick: null,
    style: {}
};

CommentItem.propTypes = {
    user: PropTypes.object,
    creatorId: PropTypes.number,
    imgSrc: PropTypes.string,
    firstName: PropTypes.string,
    lastName: PropTypes.string,
    date: PropTypes.string,
    onClick: PropTypes.func,
    editMode: PropTypes.func,
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node,
};

export default CommentItem;