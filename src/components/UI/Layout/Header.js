import React from 'react';
import PropTypes from 'prop-types';
import Title from "../Elements/Title";

const Header = ({
                   headerText,
                   className,
                   style,
                   children,
                   ...otherProps
               }) => {

    return (
        <div className="header-part" style={style} {...otherProps}>
                <Title
                    size="h1"
                    style={{}}
                >
                    {headerText}
                </Title>
            {children}
        </div>
    );
};

Header.displayName = 'Header';

Header.defaultProps = {
    onClick: null,
    style: {}
};

Header.propTypes = {
    headerText: PropTypes.string,
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node,
};

export default Header;