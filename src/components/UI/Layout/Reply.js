import React from 'react';
import PropTypes from 'prop-types';
import Button from '../Elements/Button';
import RoundAvatar from '../RoundAvatar';

const Reply = ({
                 imgSrc,
                 placeholderText,
                 onClick,
                 className,
                 style,
                 children,
                 handleChange,
                 onSubmit,
                 message,
                 text,
                 ...otherProps
               }) => {

  return (
    <div className="reply-block" style={style} {...otherProps}>
      <div className="reply-block__body">
        <RoundAvatar avatar={imgSrc} text={text}/>
        <textarea
          rows="5"
          name="message"
          onChange={handleChange}
          placeholder={placeholderText}
          value={message}/>
      </div>
      <div className="reply-block__button">
        <Button
          type="button"
          size="lg"
          onClick={onSubmit}
          disabled={ !/\S/.test(message)}
        >
          POST COMMENT
        </Button>
      </div>
    </div>
  );
};

Reply.displayName = 'Reply';

Reply.defaultProps = {
  onClick: null,
  style: {},
};

Reply.propTypes = {
  imgSrc: PropTypes.string,
  placeholderText: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.node,
};

export default Reply;