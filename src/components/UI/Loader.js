import React, {Fragment} from 'react';
import Backdrop from "./BackDrop";
import Spinner from "./Spinner";

const Loader = () => (
    <Fragment>
      <Backdrop show={true}/>
      <div className="loader">
        <Spinner/>
      </div>
    </Fragment>
);

export default Loader;
