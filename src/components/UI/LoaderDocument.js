import React, {Fragment} from 'react';
import Backdrop from './BackDrop';
import Icon from '../UI/Elements/Icon';

const LoaderDocument = ({closeRequest}) => (
    <Fragment>
      <Backdrop show={true}/>
      <div className='loader'>
          <div className='sk-circle-bounce'>
              <div className='close-loader' >
                  <Icon name='cross' onClick={closeRequest}/>
              </div>
              <div className='sk-child sk-circle-1' />
              <div className='sk-child sk-circle-2' />
              <div className='sk-child sk-circle-3' />
              <div className='sk-child sk-circle-4' />
              <div className='sk-child sk-circle-5' />
              <div className='sk-child sk-circle-6' />
              <div className='sk-child sk-circle-7' />
              <div className='sk-child sk-circle-8' />
              <div className='sk-child sk-circle-9' />
              <div className='sk-child sk-circle-10' />
              <div className='sk-child sk-circle-11' />
              <div className='sk-child sk-circle-12' />
          </div>
      </div>
    </Fragment>
);

export default LoaderDocument;
