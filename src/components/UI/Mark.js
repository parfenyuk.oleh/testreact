import React from 'react';
import {MARKS} from '../../constants/assignments';

const Mark = ({text, className, clicked, status}) => {
  switch (status) {
    case MARKS.BAD :
      status = 'BAD';
      break;
    case MARKS.NOTBAD :
      status = 'NOT BAD';
      break;
    case MARKS.POOR :
      status = 'POOR';
      break;
    case MARKS.GOOD :
      status = 'GOOD';
      break;
    case MARKS.AWESOME :
      status = 'AWESOME';
      break;
    default :
      status = '';
      break;
  }
  return (
    <div className={`markItem ${className ? className : ''}`} onClick={clicked}>
      {status || text}
    </div>
  );
};

export default Mark;
