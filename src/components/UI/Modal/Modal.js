import React, {Fragment} from 'react';
import Backdrop from "../BackDrop";
import PropTypes from 'prop-types';

const Modal = ({show, className, children, hideModal, noHeader, noCross}) =>
  show ?
    <Fragment>
      <Backdrop show={show}
                clicked={hideModal}
                className={'with-modal'}
                >
        <div className="modal-helper">
          <div className={`modal ${className} ${noHeader ? 'no-header' : ''}`}>
            {!noCross && <div className="modal-close" onClick={hideModal}> </div>}
            {children}
          </div>
        </div>
      </Backdrop>

    </Fragment>
    : null;

Modal.propTypes = {
  show: PropTypes.bool,
  className: PropTypes.string,
  hideModal: PropTypes.func,
  noHeader: PropTypes.bool
} ;

Modal.defaultProps = {
  noHeader : false
};
export default Modal;
