import React from 'react';

const ModalBody = ({children, bigPadding}) => (
  <div className={`modal-body ${bigPadding && 'big-padding'}`}
  >{children}</div>);

export default ModalBody;
