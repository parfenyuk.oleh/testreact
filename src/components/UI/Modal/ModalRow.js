import React from 'react';

const ModalRow = ({children, button, flex, center,  style}) => (
  <div className={`modal-row ${button && 'button'}`}
       style={{
         display: flex ? 'flex' : 'block',
         justifyContent: center ? 'center' : 'flex-start',
         ...style,
       }}
  >{children}</div>);

export default ModalRow;
