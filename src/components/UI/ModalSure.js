import React from 'react';
import Modal from './Modal/Modal';
import ModalBody from './Modal/ModalBody';
import ModalRow from './Modal/ModalRow';
import Cell from './Cell';

const ModalSure = ({show, onAgree, onDisagree, text,agreeText}) =>
  <Modal show={show} hideModal={onDisagree} noHeader className={'modal-sure'}>
    <ModalBody>
      <ModalRow>
        <div className={'modal-sure-text'}>{text}</div>
      </ModalRow>
      <ModalRow flex button>
        <Cell column={7}>
          <button className="button-default" onClick={onDisagree}>Cancel</button>
        </Cell>
        <Cell column={2}/>
        <Cell column={7}>
          <button className="large big-primary" onClick={onAgree}>{agreeText || 'Yes, delete'}</button>
        </Cell>
      </ModalRow>
    </ModalBody>
  </Modal>;

export default ModalSure;
