import React from 'react';
import propTypes from 'prop-types';

const RatingLine = ({rating}) => {
  function checkColor(rating) {
    if (rating < 50){
      return '#F24A5E'
    } else if (rating < 90){
      return '#FFD012'
    } else {
      return '#03EB76'
    }
  }
  return (
    <div className={'rating-line'}>
      <div style={{
        backgroundColor: checkColor(rating),
        width : `${rating}%`
      }}/>
    </div>
  );
};

RatingLine.propTypes = {
  rating: propTypes.number
};

RatingLine.defaultProps = {
  rating: 0
};

export default RatingLine;
