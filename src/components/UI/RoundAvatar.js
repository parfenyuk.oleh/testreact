import React from 'react';
import {getRandomOnlineColor} from '../../helpers/randomColor';

class RoundAvatar extends React.Component {
  constructor(props) {
    super(props);
    let {text} = this.props;
    if(!text){
      text = 'AA';
    }
    text = text.replace(/\s/g,'');
    if(!text[0]){
      text = 'A';
    }
    if(!text[1]){
      text = text + 'A';
    }
    const color_num = (text.charCodeAt(0) + text.charCodeAt(1)) % 3 ;
    this.state = {
      color: getRandomOnlineColor(color_num),
    };
  }

  getSize = (size) => {
    switch (size) {
      case 'big' :
        return 'big';
      case 'normal' :
        return 'normal';
      case null :
        return '';
      default :
        return ''
    }
  };

  componentDidMount() {

  }

  render() {
    const {avatar, text, className, online, size} = this.props;

    return (
      <div className={`round-avatar${online ? 'online' : ''} ${className} ${this.getSize(size)}`}
           style={{
             backgroundColor: avatar ? 'transparent' : this.state.color,
           }}>
        {avatar
          ?
          <img src={avatar} className="round-avatar-image" alt=""/>
          :
          <span>{text}</span>
        }
      </div>
    );
  }
}

RoundAvatar.defaultProps = {
  size: null,
};

export default RoundAvatar;