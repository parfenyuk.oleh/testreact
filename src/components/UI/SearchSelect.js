import React, {Component} from 'react';
import Input from './Input';
import * as PropTypes from 'prop-types';
import OutsideEventListener from '../Layout/AppHeader/OutsideEventListener';

class SearchSelect extends Component {

  state = {
    text: '',
    showOptions : false
  };

  componentDidUpdate (prevProps) {
    if(prevProps.options.toString() !== this.props.options.toString() && prevProps.loading !== this.props.loading){
      this.setState({showOptions: true});
    }
  }

  toggleOptions = () => {
    this.setState({showOptions : !this.state.showOptions})
  };

  chooseHandler = (option) => {
    this.toggleOptions();
    this.props.onChoose(option);
    this.setState({text: '', showOptions: false});
  };

  searchHandler = (value) => {
    this.props.onSearch(value);
    this.setState({text: value})
  };

  render() {
    const {searchText, error, options, placeholder} = this.props;
    return (
      <div className='select select-search'>
        <Input placeholder={placeholder}
               defaultValue={searchText ? searchText : this.state.text}
               changed={(e) => {this.searchHandler(e.target.value)}}
               className={error ? 'error' : ''}
        />
        { this.state.showOptions    && <OutsideEventListener showDropDown={this.toggleOptions} >
            <div className="select-list">
              {options.map((option,key) =>
                  <div className="select-list--item" key={key} onClick={()=> { this.chooseHandler(option)}}>
                    {option.name}
                  </div>)}
            </div>
          </OutsideEventListener>
        }

      </div>
    );
  }
}

SearchSelect.propTypes = {
  searchText : PropTypes.string,
  onChoose : PropTypes.func,
  onSearch : PropTypes.func,
  placeholder: PropTypes.string,
  loading: PropTypes.bool
};

SearchSelect.defaultProps = {
  placeholder: '',
  options: []
};

export default SearchSelect;