import React, {Component} from 'react';
import SelectButton from './SelectButton';
import Checkbox from './Elements/Checkbox';

import PropTypes from 'prop-types';
import Radio from '@material-ui/core/Radio';
import OutsideEventListener from '../Layout/AppHeader/OutsideEventListener';

class Select extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '' || props.defaultName,
      showOptions: false,
    }
  }


  static getDerivedStateFromProps(props, state) {
    if (props.checkedItems && !state.text) {
      let current = props.options.find(item => item.id === props.checkedItems);
      if (props.radio) {
        current = props.options.find(item => item.id === props.checkedItems.id);
        if (current)
          return {
            text: props.nameFunc(current),
          }
      }
      if (current) {
        return {
          text: props.options.find(item => item.id === props.checkedItems).name,
        }
      }
    }
    return null;
  }

  toggleOptions = () => {
    this.setState({showOptions: !this.state.showOptions})
  };

  radioHandler = (item) => {
    this.props.handleInputChange(item);
    this.setState({
      showOptions: false,
      text: this.props.nameFunc(item),
    })
  };

  chooseHandler = option => {
    this.props.clicked(option);
    let text = option.name;
    if (this.props.nameFunc) {
      text = this.props.nameFunc(option);
    }
    this.setState({
      showOptions: false,
      text,
    })
  };

  componentDidUpdate(prevProps){
    if(this.props.options.length !== prevProps.options.length){
      this.setState({text: ''});
    } else if(this.props.options.length && this.props.options[0].name !== prevProps.options[0].name){
      this.setState({text: ''});
    }
  }

  render() {
    const {
      label, placeholder, text, className, options, multiselect,
      handleInputChange, checkedItems, disabled, classic, radio, onCheckAll,
      checkAllOption, error, withSearch, onSearch, searchValue,
    } = this.props;
    const {showOptions} = this.state;

    let list = (
      <div className="select-list">
        {withSearch && <input onChange={onSearch} value={searchValue}/>}
        {
          options.map((option, key) =>
            <div className="select-list--item" key={key} onClick={() => {
              this.chooseHandler(option)
            }}>
              {option.name}
            </div>)
        }
      </div>
    );

    if (multiselect) {
      let checkedAllStatus = false,
        isAllChecked = true;
      checkedItems.forEach((value) => {
        if (!value) isAllChecked = false;
      });
      if (options.length === checkedItems.size && isAllChecked) {
        checkedAllStatus = true;
      }
      list = (
        <div className="select-list multiselect">
          {checkAllOption &&
          <div className="select-list--item">
            <label>
              <Checkbox
                name={`Check All`}
                checked={checkedAllStatus}
                onChange={onCheckAll}
              />
              {checkAllOption}
            </label>
          </div>
          }
          {
            options.map((item, key) =>
              <div className="select-list--item" key={key}>
                <label key={key}>
                  <Checkbox disabled={disabled}
                            name={`${item.name}-${item.id}`}
                            checked={checkedItems.get(`${item.name}-${item.id}`)}
                            onChange={(e) => {
                              handleInputChange(e, item.id)
                            }}
                  />
                  {item.name}
                </label>
              </div>)
          }
        </div>
      )
    }

    if (radio) {
      list = (
        <div className="select-list radio">
          {
            options.map((item, key) =>
              <div className="select-list--item" key={key}>
                <label key={key}>
                  <Radio color="primary"
                         name={item.name + item.id}
                         checked={checkedItems && checkedItems.id === item.id}
                         onChange={() => {
                           this.radioHandler(item)
                         }}
                  />
                  {item.name}
                </label>
              </div>)
          }
        </div>
      )
    }


    return (
      <div className={`select ${className} ${classic ? 'classic' : ''} ${error ? 'error' : ''}`}>
        {
          !showOptions
            ?
            <div className='wrapper'>
              <SelectButton label={label}
                            placeholder={placeholder}
                            text={this.state.text ? this.state.text : text}
                            clicked={disabled ? null : this.toggleOptions}
                            disabled={disabled}
              />
            </div>
            :
            <SelectButton label={label}
                          placeholder={placeholder}
                          text={this.state.text ? this.state.text : text}
                          clicked={this.toggleOptions}
            />
        }
        {
          showOptions &&
          <OutsideEventListener showDropDown={this.toggleOptions}>
            {list}
          </OutsideEventListener>
        }

      </div>
    )
  }

}

Select.propTypes = {
  label: PropTypes.string,
  placeholder: PropTypes.string,
  text: PropTypes.string,
  clicked: PropTypes.func,
  className: PropTypes.string,
  options: PropTypes.array,
  withSearch: PropTypes.bool,
};

Select.defaultProps = {
  clicked: () => {
  },
  disabled: false,
  classic: false,
  options: [],
};

export default Select;
