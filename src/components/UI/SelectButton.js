import React from 'react';
import icons from '../../helpers/iconsLoader'

const SelectButton = ({label, icon, text, placeholder, clicked, disabled}) => (
  <div className="select-field" onClick={clicked}>
    {label ? <label>{label}</label> : null}
    <div className={`select-button ${text ? '' : 'placeholder'}${disabled ? ' disabled' : ''}`}>
      {icon ? <img src={icon} alt="" className="select-icon"/> : null}
      <span>{text ? text : placeholder}</span>
      <img src={disabled? icons.iconDropDownDisabled : icons.iconDropDown} alt="" className="select-arrow"/>
    </div>
  </div>
);

export default SelectButton;