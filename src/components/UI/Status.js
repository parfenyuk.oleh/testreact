import React from 'react';

const Status = ({type}) => {
  switch (type) {
    case 'active':
      return <div className="status active">Active</div>;
    case 'inactive':
      return <div className="status inactive">Inactive</div>;
    default :
      return <div className="status default">{type}</div>

  }
};

export default Status;
