import React from 'react';

const SubmissionComment = ({name, time, text}) => {
  return (
    <div className="submissionComment">
      <div className="submissionComment-header">
        <div className="submissionComment-header-name">{name}</div>
        <div className="submissionComment-header-time">{time}</div>
      </div>
      <div className="submissionComment-text">{text}</div>
    </div>
  );
};

export default SubmissionComment;
