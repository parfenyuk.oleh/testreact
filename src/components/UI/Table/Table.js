import React from 'react';

const Table = ({children, className}) => {
  return (
    <div className={`table ${className}`} >
      <div className="table-wrap">
        {children}
      </div>
    </div>
  );
};

export default Table;