import React from 'react';

const TableHeader = ({children}) => <div className="table-header">{children}</div>;

export default TableHeader;