import TextareaAutosize from 'react-textarea-autosize';
import PropTypes from 'prop-types';
import React from 'react';

const Textarea = ({placeholder, changed, name, text, defaultValue, error, modalError, disabled}) =>
  <div className="ui-textarea">
    <TextareaAutosize placeholder={placeholder}
                      onChange={changed}
                      name={name}
                      value={text ? text : defaultValue}
                      className={error ? 'error' : ''}
                      changed={changed}
                      disabled={disabled}
    />
      {modalError && <div className="with-error">{modalError}</div>}
  </div>;

Textarea.propTypes = {
  placeholder: PropTypes.string,
  changed: PropTypes.func
};

export default Textarea;
