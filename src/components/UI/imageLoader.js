import React, {Component} from 'react';
import PropTypes from 'prop-types';

import fileReader from '../../helpers/fileReader';


const EXTENSIONS = ['jpg', 'jpeg', 'gif', 'png',];
class ImageLoader extends Component {

  constructor(props) {
    super(props);
    this.state = {
      image: null,
    };
    this.fileInput = React.createRef();
  }

  fileChangeHandler = ({target: {files}}) => {
    if(files.length === 0) return;
    const isCorrectExtension = EXTENSIONS.includes(files[0].name.split('.').pop().toLowerCase());
    if(this.props.withValidation && !isCorrectExtension){
      return this.props.onValidation('File extension should be: ' + EXTENSIONS.join(', '))
    }
    fileReader(files[0], result => {
      this.setState({image: result})
    });
    this.props.changed(files[0]);
  };

  showFileWindow = () => {
    this.fileInput.current.click();
  };

  getSizeClass(lg) {
    if (lg) return 'lg';
    return ''
  }

  onRemove = () => {
    this.setState({image: null});
    this.props.changed(null, 'removed');
  };

  render() {
    const {placeholder, trigger, lg, remover} = this.props;

    let image = this.props.image && typeof this.props.image.value === 'string' ? this.props.image.value : this.state.image;

    return (
      <div className='imageLoader'>
        <div className={`imageLoader-preview ${image ? 'with-image' : null} ${this.getSizeClass(lg)}`}
             style={{backgroundImage: `url(${image})`}}
             onClick={this.showFileWindow}
        >
          {!image && placeholder.text || null}
        </div>
        <input type='file' onChange={this.fileChangeHandler} ref={this.fileInput}/>
        <div className='imageLoader-buttons'>
          {trigger &&
          <div className='imageLoader-trigger'
               onClick={this.showFileWindow}
               style={{width: '140px'}}
          >
            {trigger}
          </div>
          }
          {remover &&
          <div className='imageLoader-remover'
               style={{width: '140px'}}
               onClick={this.onRemove}
          >
            {remover}
          </div>
          }
        </div>
      </div>
    );
  }
}

ImageLoader.propTypes = {
  changed: PropTypes.func,
  placeholder: PropTypes.object,
  trigger: PropTypes.oneOfType([
    PropTypes.instanceOf(Element),
    PropTypes.element,
  ]),
  remover: PropTypes.oneOfType([
    PropTypes.instanceOf(Element),
    PropTypes.element,
  ]),
  withValidation: PropTypes.bool,
  onValidation: PropTypes.func,
};

ImageLoader.defaultProps = {
  placeholder: {},
  trigger: null,
};

export default ImageLoader;