export const TABS = {
  active: 2,
  archive: 1,
};

export const STATUSES = {
  Active: 1,
  InProgress: 2,
  ToReview: 3,
  Done: 4,
};

export const MARKS = {
  BAD: 1,
  NOTBAD: 2,
  POOR: 3,
  GOOD: 4,
  AWESOME: 5,
};

export const SUBMISSION_FIELDS = {
  PARTICIPATION: 'participation',
  EXAM: 'exam',
  QUIZ: 'quiz',
  ESSAY: 'essay',
};