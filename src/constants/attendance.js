export const ATTENDANCE_MARKS ={
  ALL: 0,
  ABSENT: 1,
  PRESENT: 2
};

export const COMMENT_MARK = {
  NONE: 0,
  ON_TIME: 1,
  LATE: 2,
  VERY_LATE: 3,
};