const ROLES = {
  ADMIN: 1,
  TEACHER: 2,
  STUDENT: 3,
  SUPERADMIN: 4
};

export default ROLES;