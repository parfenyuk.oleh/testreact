export const userType = {
    admin: 1,
    teacher: 2,
    student: 3,
    superAdmin: 4,
};
