import React, {Component} from 'react';
import {connect} from 'react-redux';

import DashboardCard from '../../../components/General/Dashboard/DashboardCard';
import Icon from '../../../components/UI/Elements/Icon';
import * as actions from '../../../store/actions/admin.action';

class Dashboard extends Component {

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    this.props.getMemberAmount(user.institutionId, user.token);
    this.props.getAllSubjects(user.institutionId, user.token);
  }

  render() {
    return (
      <div className="dashboard">
        <h1 className="page-title dashboard-admin-title">Admin’s dashboard</h1>
        <div className="dashboard-row">
          <div className="dashboard-col">
            <DashboardCard icon={<Icon name={'teacher-dash'}/>}
                           number={this.props.admin.amount.amountOfTeachers}
                           title={'Teachers'}
            />
          </div>
          <div className="dashboard-col">
            <DashboardCard icon={<Icon name={'student-dash'}/>}
                           number={this.props.admin.amount.amountOfStudents}
                           title={'Students'}
            />
          </div>
          <div className="dashboard-col">
            <DashboardCard icon={<Icon name={'subject-dash'}/>}
                           number={this.props.admin.subject.length}
                           title={'Subjects'}
            />
          </div>
          {/*<div className="dashboard-col">*/}
          {/*<DashboardCard icon={<Icon name={'updated-dash'}/>}*/}
          {/*number={22400}*/}
          {/*title={'User updated'}*/}
          {/*vertical*/}
          {/*/>*/}
          {/*</div>*/}
          {/*<div className="dashboard-col">*/}
          {/*<DashboardCard icon={<Icon name={'discontinued-dash'}/>}*/}
          {/*number={1731}*/}
          {/*title={'Users discontinued'}*/}
          {/*vertical*/}
          {/*/>*/}
          {/*</div>*/}
        </div>
        {/*<div className="dashboard-activity">*/}
        {/*<h2 className="dashboard-activity-title">User activity</h2>*/}
        {/*<Input label={<Icon name="search-dash"/>}*/}
        {/*defaultValue={searchValue}*/}
        {/*changed={this.searchHandler}*/}
        {/*placeholder={'Search...'}*/}
        {/*/>*/}
        {/*</div>*/}
        {/*<Table className="dashboard-table">*/}
        {/*<TableHeader>*/}
        {/*<Cell column={6}>Name</Cell>*/}
        {/*<Cell column={6}>Last activity </Cell>*/}
        {/*<Cell column={4}>Totally active hours</Cell>*/}
        {/*</TableHeader>*/}
        {/*<TableRow>*/}
        {/*<Cell column={6}>*/}
        {/*<AvatarBlock textAvatar={`OH`}*/}
        {/*customInfo={<span>{`Omar Homar`}</span>}*/}
        {/*imgSrc={''}*/}
        {/*/>*/}
        {/*</Cell>*/}
        {/*<Cell column={6}>*/}
        {/*25 hours*/}
        {/*</Cell>*/}
        {/*<Cell column={4}>*/}
        {/*1250 hours*/}
        {/*</Cell>*/}
        {/*</TableRow>*/}
        {/*</Table>*/}


      </div>
    );
  }
}

const mapStateToProps = ({admin}) => ({admin});

const mapDispatchToProps = dispatch => ({
  getMemberAmount: (institutionId, token) => {dispatch(actions.getMemberAmount(institutionId, token))},
  getAllSubjects: (id, token) => {dispatch(actions.getAllSubjects(id, token))},
});


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);