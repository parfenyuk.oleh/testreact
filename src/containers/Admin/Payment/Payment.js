import React, {Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import * as superAdminActions from '../../../store/actions/superadmin';
import connect from 'react-redux/es/connect/connect';
import {compose} from 'recompose';
import axios from '../../../axios-instance';
import {ChangeCardTab} from '../../../components/Admin/Payment';
import Header from '../../../components/UI/Layout/Header';
import TableHeader from '../../../components/UI/Table/TableHeader';
import Cell from '../../../components/UI/Cell';
import TableRow from '../../../components/UI/Table/TableRow';
import Button from '../../../components/UI/Elements/Button';
import UsersTab from '../../../components/Admin/Payment/UsersTab';
import moment from 'moment';
import * as actions from '../../../store/actions';

let click_counter = 0;

function TabContainer(props) {
  return (
    <Typography component='div' style={{padding: 8 * 3}}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  tabs: {
    width: 617,
    margin: '0 auto',
  },
  tabsRoot: {
    borderBottom: '1px solid rgba(179, 192, 206, 0.5)',
  },
  tabsIndicator: {
    backgroundColor: '#0079C4',
    height: 1,
  },
  tabRoot: {
    textTransform: 'initial',
    minWidth: 218,
    fontSize: 18,
    color: '#44566C',
    opacity: 0.5,
    '&$tabSelected': {
      color: '#44566C',
      opacity: 1,
    },
  },
  tabSelected: {},
  typography: {
    padding: theme.spacing.unit * 3,
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
});

class RegistrationFlowGrid extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: 1,
      changeCardMode: false,
      last4: '0000',
      payments: [],
      isPayNowAvailable: false
    };

    this.swiper = null
  }

  handleChange = (event, value) => {
    this.setState({value});
  };

  changeCard = () => {this.setState({changeCardMode: !this.state.changeCardMode})};

  componentDidMount = () => {
    const user = JSON.parse(localStorage.getItem('user'));
    axios.get('/Payment/CreditCard', {
      headers: {
        Authorization: 'Bearer ' + user.token,
      },
    }).then(response => {
      this.setState({
        last4: response.data.result.last4,
      })
    });

    axios.get('/Payment/Payments', {
      headers: {
        Authorization: 'Bearer ' + user.token,
      },
    }).then(response => {
      this.setState({...response.data.result});
    })
  };

  payNow = event => {
    const user = JSON.parse(localStorage.getItem('user'));

    if (click_counter === 0) {
      click_counter++;
      this.props.showLoader();
      axios.put('/Payment/PayNow', null, {
        headers: {Authorization: 'Bearer ' + user.token},
      }).then(response => {
        if (response.data.ok) {
          this.props.hideLoader();
          window.location.reload();
        }
      })
    } else {
      event.preventDefault();
      return false;
    }

  };

  render() {
    const {classes} = this.props;
    const {value, changeCardMode} = this.state;

    return (
      <main className='registration-flow'>
        <div className={classes.root}>
          <Header
            headerText='Payment'
            style={{paddingTop: 20, paddingLeft: 50}}
          />
          <div className={classes.tabs}>
            <Tabs
              value={this.state.value}
              onChange={this.handleChange}
              classes={{root: classes.tabsRoot, indicator: classes.tabsIndicator}}
            >
              <Tab
                classes={{root: classes.tabRoot, selected: classes.tabSelected}}
                label='Change card'
              />
              <Tab
                classes={{root: classes.tabRoot, selected: classes.tabSelected}}
                label='Payments'
              />
              <Tab
                classes={{root: classes.tabRoot, selected: classes.tabSelected}}
                label='Users & Plans'
              />
            </Tabs>
            
          </div>
          {
            value === 0 && <TabContainer>
              <ChangeCardTab changeCard={this.changeCard}
                             changeCardMode={changeCardMode}
                             last4={this.state.last4}
              />
            </TabContainer>
          }
          {value === 1 && <TabContainer>
            <div className='payment-table'>
              <TableHeader>
                <Cell column={2}>Date</Cell>
                <Cell column={4}>Payment type</Cell>
                <Cell column={5}>Credit card</Cell>
                <Cell column={2}>Amount</Cell>
                <Cell column={2}/>
              </TableHeader>
              {this.state.payments.map((item, index) => {
                return <TableRow key={index}>
                  <Cell column={2}>{moment(item.date).format('DD/MM/YYYY')}</Cell>
                  <Cell column={4}>{item.paymentType}</Cell>
                  <Cell column={5}>
                    **** - **** - **** - {item.creditCardLast4}
                  </Cell>
                  <Cell column={2}>
                                   <span className={(!index && this.state.isPayNowAvailable) ? 'red' : 'green'}>
                                        {(!index && this.state.isPayNowAvailable) ? '-' + item.amount : item.amount}
                                    </span>
                  </Cell>
                  <Cell column={2}>
                    {(!index && this.state.isPayNowAvailable) && <Button
                      type='button'
                      size='md'
                      style={{height: 30}}
                      onClick={this.payNow}
                    >
                      pay now
                    </Button>
                    }
                  </Cell>
                </TableRow>
              })
              }

            </div>
          </TabContainer>}
          {
            value === 2 && <TabContainer>
              <UsersTab />
            </TabContainer>
          }
        </div>
      </main>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  addUniversity: (value, token, refreshTokenValue) => {
    dispatch(superAdminActions.addUniversity(value, token, refreshTokenValue))
  },
  showLoader: () => {dispatch(actions.showLoader())},
  hideLoader: () => {dispatch(actions.hideLoader())},
});

export default compose(
  withStyles(styles),
  connect(null, mapDispatchToProps),
)(RegistrationFlowGrid);