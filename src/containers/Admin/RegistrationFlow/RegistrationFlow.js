import React, {Fragment} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import MultistepsBar from '../../../components/UI/Elements/MultistepsBar';
import ChooseInstitution from '../../../components/Admin/RegistrationFlow/ChooseInstitution';
import PaymentRules from '../../../components/Admin/RegistrationFlow/PaymentRules';
import AddPayment from '../../../components/Admin/RegistrationFlow/AddPayment';
import AddInstitution from '../../../components/Admin/RegistrationFlow/AddInstitution';

import Swiper from 'react-id-swiper';
import imageLoadApi from '../../../helpers/imageLoadApi';
import Swal from 'sweetalert2';
import validate from '../../../helpers/validator';
import {userAuthToken} from '../../../helpers';
import * as superAdminActions from '../../../store/actions/superadmin';
import * as actions from '../../../store/actions/layout';
import connect from 'react-redux/es/connect/connect';
import {compose} from 'recompose';
import {injectStripe} from 'react-stripe-elements';
import { history } from '../../../helpers/index';

const styles = theme => ({
    root: {
        flexGrow: 1,
        maxWidth: 1115,
        margin: '0 auto'
    },
    progressbar: {
        marginTop: 105,
        marginBottom: 70
    },
});



class RegistrationFlowGrid extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            spacing: '24',
            stepActive: 0,
            institutionActive: 1,
            elementArray: [
                {
                    id: 0,
                    name: 'Choose an Institution'
                },
                {
                    id: 1,
                    name: 'Start Free'
                },
                {
                    id: 2,
                    name: 'Payment Method'
                },
                {
                    id: 3,
                    name: 'Create an Institution'
                },
            ],
            typeInstitutionArray: [
                {
                    id: 1,
                    name: 'University',
                    imgName: 'graduate'
                },
                {
                    id: 2,
                    name: 'Vocational',
                    imgName: 'certificate'
                },
                {
                    id: 3,
                    name: 'High School',
                    imgName: 'book'
                },
                {
                    id: 5,
                    name: 'Corporate Business',
                    imgName: 'corporate'
                },
                {
                    id: 4,
                    name: 'Tutors',
                    imgName: 'tutors'
                },
            ],
            name: '',
            avatar: '',
            color: '',
            abbreviation: '',
            errors: [],
            subscriptionType: null,
        };

        this.swiper = null
    }

    changeActiveInstitution = institutionId => this.setState({ institutionActive: institutionId });

    goNext = () => {
        if (this.swiper) this.swiper.slideNext();
        this.setState({
            stepActive: this.state.stepActive + 1
        });

    };

    goPrev = () => {
        if (this.swiper) this.swiper.slidePrev();

        this.setState({
            stepActive: this.state.stepActive - 1
        });
    };

    setSubscriptionType = (type) => {
        this.setState({subscriptionType: type})
    };

    colorClickHandler = color => { this.setState({color : color}); };

    inputChangeHandler = (value,field) => {
        const state = {...this.state};
        state[field] = value;
        this.setState(state);
    };

    handleInputChange = event => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        if (name === 'name' && this.state.name.length < 3) {
            this.setState({
                abbreviation: value
            });
        }

        this.setState({
            [name]: value
        });
    };

    handleChange = event => this.setState({ [event.target.name]: event.target.value });

    saveHandler = () => {
        const {name, abbreviation, avatar, color, institutionActive, subscriptionType, creditCardToken} = this.state;
        const user = JSON.parse(localStorage.getItem('user'));
        let result = {};
        const errors = [];

        const nameField = validate('Institution name',name,{required: 1});
        const abbreviationField = validate('Abbreviation',abbreviation,{required: 1});

        if (nameField) errors.push(nameField);
        if (abbreviationField) errors.push(abbreviationField);

        this.setState({errors: errors});

        if (errors.length === 0) {
            result['name'] = name;
            result['key'] = abbreviation;
            result['color'] = color;
            result['type'] = institutionActive;
            result['subscriptionType'] = subscriptionType;
            result['creditCardToken'] = creditCardToken;

            if( typeof this.state.avatar !== 'string'){
                imageLoadApi(avatar, user.token)
                    .then(response => {
                        if(response.data.ok){
                            result['avatar'] = response.data.result;

                            this.props.addUniversity(result, user.token, user.refreshToken)
                        }

                        else throw new Error('Cant upload file')
                    })
                    .catch(e => {
                        Swal({
                            title: 'Error!',
                            text: e,
                            type: 'error',
                            timer: 3000
                        });
                    })
            } else {
                this.props.addUniversity(result, user.token, user.refreshToken)
            }
        }
    };

    stepClickHandler = item => {
        console.log(item);
        if(this.state.stepActive > item.id){
          this.setState({
            stepActive: this.state.stepActive - (this.state.stepActive - item.id)
          });
          for(let i = 0; this.state.stepActive - item.id !== i; i++){
            this.swiper.slidePrev();
          }
        }

    };

  setCardTokenId = creditCardToken => {
        this.setState({creditCardToken});
  };

    render() {
        const params = {
            spaceBetween: 30,
            allowTouchMove: false,
            initialSlide: 0
        };

        const { classes } = this.props;
        const {
            spacing,
            stepActive,
            elementArray,
            typeInstitutionArray,
            institutionActive,
            avatar,
            name,
            color,
            abbreviation,
            errors
        } = this.state;

        return (
            <main className='registration-flow'>
                <Grid container className={classes.root} spacing={16}>
                    <Grid item xs={12}>
                        <Grid container className={classes.progressbar} justify='center' spacing={Number(spacing)}>
                            <MultistepsBar elementActive={stepActive}
                                           elementArray={elementArray}
                                           onClick={this.stepClickHandler}
                            />
                        </Grid>
                    </Grid>
                    <Swiper {...params} ref={node => {if(node) this.swiper = node.swiper }}>
                        <div>
                            <ChooseInstitution typeInstitutionArray={typeInstitutionArray}
                                               institutionActive={institutionActive}
                                               changeActiveInstitution={this.changeActiveInstitution}
                                               goNext={this.goNext}
                            />
                        </div>
                        <div>
                            <PaymentRules
                              goNext={this.goNext}
                              goPrev={this.goPrev}
                              setType={this.setSubscriptionType}
                              institutionType={institutionActive}
                            />
                        </div>
                        <div>
                            <AddPayment goNext={this.goNext} setCardTokenId={this.setCardTokenId}/>
                        </div>
                        <div>
                            <AddInstitution colorClickHandler={this.colorClickHandler}
                                            inputChangeHandler={this.inputChangeHandler}
                                            saveHandler={this.saveHandler}
                                            name={name}
                                            avatar={avatar}
                                            color={color}
                                            abbreviation={abbreviation}
                                            handleInputChange={this.handleInputChange}
                                            errors={errors}
                            />
                            <div style={{ height: '60px'}}> </div>
                        </div>
                    </Swiper>
                </Grid>
            </main>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    addUniversity: (value, token, refreshTokenValue) => { return dispatch( superAdminActions.addUniversity(value, token, refreshTokenValue) ) }
});

export default compose(
    withStyles(styles),
    connect(null, mapDispatchToProps)
)(RegistrationFlowGrid);