import React, {Component, Fragment} from 'react';
import {NavLink, withRouter, Redirect} from 'react-router-dom';
import {connect} from 'react-redux';

import backGround from '../../../images/authBackground.png';
import Input from '../../../components/UI/Input';
import validate from '../../../helpers/validator';
import * as actions from '../../../store/actions/auth.action';

import icons from '../../../helpers/iconsLoader';

import {REGISTRATION, LOGIN, FORGOT_PASSWORD, ADMIN} from '../../../constants/routes';
import AppHeader from '../../../components/Layout/AppHeader/AppHeader';

class Auth extends Component {
    constructor(props) {
        super(props);

        this.state = {
            activeTab: this.props.location.pathname.indexOf('login') !== -1 ? 'login' : 'registration',
            loginInputs: {
                email: {
                    value: '',
                    error: false
                },
                password: {
                    value: '',
                    error: false
                }
            },
            registerInputs: {
                email: {
                    value: '',
                    error: false
                },
                password: {
                    value: '',
                    error: false
                },
                confirm: {
                    value: '',
                    error: false
                },
                firstName: {
                    value: '',
                    error: false
                },
                lastName: {
                    value: '',
                    error: false
                }
            },
            errors: []

        }
    }
  changeTabHandler = path => {
          this.setState({activeTab: path.slice(1),errors: []});
          this.props.history.push(path);
      };
  inputChangeHandler = (form, type, event) => {
          const newState = {...this.state};

          newState[form][type].value = event.target.value;
          this.setState(newState);
      };
  submitHandler = () => {
          const {activeTab,loginInputs,registerInputs} = this.state;
          const result = {};
          const errors = [];

          if(activeTab === 'login'){
            const email = validate('email',loginInputs.email.value,{isEmail: true});
            const password = validate('password',loginInputs.password.value,{minLength: 6});

            loginInputs.email.error = Boolean(email);
            loginInputs.password.error = Boolean(password);
            if(email) {errors.push(email);}
            if(password) {errors.push(password);}
            this.setState({loginInputs , errors});
            if(!errors.length){
              this.props.signIn(loginInputs.email.value, loginInputs.password.value);
            }
          }

          if(activeTab === 'registration'){
            const email = validate('email',registerInputs.email.value,{isEmail: true});
            const firstName = validate('First name',registerInputs.firstName.value,{isNotEmpty: true});
            const lastName = validate('Last name',registerInputs.lastName.value,{isNotEmpty: true});
            const password = validate('password',registerInputs.password.value,{minLength: 6});
            const confirm = validate('confirm', registerInputs.confirm.value,{isEqual: registerInputs.password.value});

            registerInputs.email.error = Boolean(email);
            registerInputs.password.error = Boolean(password);
            registerInputs.confirm.error = Boolean(confirm);
            registerInputs.firstName.error = Boolean(firstName);
            registerInputs.lastName.error = Boolean(lastName);
            if(email) {errors.push(email);}
            if(password) {errors.push(password);}
            if(confirm) {errors.push(confirm);}
            if(firstName) {errors.push(firstName);}
            if(lastName) {errors.push(lastName);}
            this.setState({registerInputs , errors});
            if(!errors.length){
                result['firstName'] = registerInputs.firstName.value;
                result['lastName'] = registerInputs.lastName.value;
                result['email'] = registerInputs.email.value;
                result['password'] = registerInputs.password.value;

                this.props.signUp(result);
            }
          }

      };
  componentDidUpdate(prevProps){
        if(this.props.location.pathname.indexOf(this.state.activeTab) === -1) {
          this.setState({
                activeTab: this.props.location.pathname.slice(1)
              })
        }

        const {registerInputs, loginInputs, activeTab} = this.state;

        if(this.props.auth.error !== prevProps.auth.error){
          if(activeTab === 'login'){
            this.setState({errors: [this.props.auth.error], loginInputs});
          }
          if(activeTab === 'registration'){
            registerInputs[this.props.auth.error[0]].error = true;
            this.setState({errors: [this.props.auth.error[1]], registerInputs});
          }
        }
      }
    render() {
        const {errors, loginInputs, registerInputs, activeTab} = this.state;
        const {auth: {user}} = this.props;

        const login =
          <div className='auth-login'>
            <Input
              placeholder='Email'
              className={`authenticate ${loginInputs.email.error ? 'error' : ''}`}
              value={loginInputs.email.value}
              changed={e => {this.inputChangeHandler('loginInputs', 'email', e)}}
            />
            <Input
              placeholder='Password'
              className={`authenticate ${loginInputs.password.error ? 'error' : ''}`}
              type='password'
              value={loginInputs.password.value}
              changed={e => {this.inputChangeHandler('loginInputs', 'password', e)}}
            />
            <NavLink to={FORGOT_PASSWORD} className='auth-forgotLink'>Forgot your password?</NavLink>
            <button className='big-primary auth-submit' onClick={this.submitHandler}>Sign in</button>
          </div>;
const register =
          <div className='auth-register'>
            <Input
              placeholder='Email'
              className={`authenticate ${registerInputs.email.error ? 'error' : ''}`}
              value={registerInputs.email.value}
              changed={e => {this.inputChangeHandler('registerInputs', 'email', e)}}
            />
            <Input
              placeholder='First Name'
              className={`authenticate ${registerInputs.firstName.error ? 'error' : ''}`}
              value={registerInputs.firstName.value}
              changed={e => {this.inputChangeHandler('registerInputs', 'firstName', e)}}
            />
            <Input
               placeholder='Last Name'
               className={`authenticate ${registerInputs.lastName.error ? 'error' : ''}`}
               value={registerInputs.lastName.value}
               changed={e => {this.inputChangeHandler('registerInputs', 'lastName', e)}}
            />
            <Input
              placeholder='Password'
              className={`authenticate ${registerInputs.password.error ? 'error' : ''}`}
              type='password'
              value={registerInputs.password.value}
              changed={e => {this.inputChangeHandler('registerInputs', 'password', e)}}
            />
            <Input
              placeholder='Confirm password'
              className={`authenticate ${registerInputs.confirm.error ? 'error' : ''}`}
              type='password'
              value={registerInputs.confirm.value}
              changed={e => {this.inputChangeHandler('registerInputs', 'confirm', e)}}
            />
            <button className='big-primary auth-submit' onClick={this.submitHandler}>Sign up</button>
            <div className='auth-policy'>
              By signing up you agree to our <NavLink to='/s' >Terms of Service</NavLink> and
              <NavLink to='/s' > Privacy Policy</NavLink>
            </div>
          </div>;
const activeTabEquality = activeTab === 'login';// If login equal true else false

        let redirect = null;

        if(Object.entries(user).length)
        {
          switch (user.role) {
            case 'admin':
              redirect = <Redirect to={ADMIN}/>;
              break;
            default:
              break;
          }
        }

        return(
            <Fragment>
                <AppHeader/>
                <div className='auth' style={{backgroundImage: `url(${backGround})`}}>
                    {redirect}
                    <div className='auth-tabs'>
                        <div className='auth-tabs__header'>
                            <div className={`auth-tabs__header-item ${activeTabEquality ? 'active' : ''}`}
                                 onClick={() => {this.changeTabHandler(LOGIN)}}
                            >Sign In</div>
                            <div className={`auth-tabs__header-item ${!activeTabEquality ? 'active' : ''}`}
                                 onClick={() => {this.changeTabHandler(REGISTRATION)}}
                            >Sign Up</div>
                        </div>
                        <div className='auth-errors'>
                            <ul>
                                {errors.map(item => <li> <img src={icons.iconAttention} alt=''/> {item} </li>)}
                            </ul>
                        </div>
                        {activeTabEquality ? login : register}
                    </div>
                </div>
            </Fragment>
        )
    }

}

const mapStateToProps = ({auth}) => ({auth});

const mapDispatchToProps = dispatch => ({
    signIn: (email, password) => {dispatch(actions.signIn(email,password))},
    signUp: (result) => {dispatch(actions.signUp(result))}
});

export default connect(mapStateToProps,mapDispatchToProps)(withRouter(Auth));
