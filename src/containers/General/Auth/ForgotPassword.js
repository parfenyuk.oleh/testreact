import React, {Component, Fragment} from 'react';
import {withRouter} from 'react-router-dom';
import Swal from 'sweetalert2';

import backGround from "../../../images/authBackground.png";
import icons from "../../../helpers/iconsLoader";
import Input from "../../../components/UI/Input";
import validate from "../../../helpers/validator";
import {REGISTRATION, LOGIN} from "../../../constants/routes";
import AppHeader from "../../../components/Layout/AppHeader/AppHeader";
import axios from '../../../axios-instance';
import {showLoader, hideLoader} from '../../../store/actions/loader';
import {connect} from 'react-redux';
class ForgotPassword extends Component {
  constructor(props){
    super(props);

    this.state = {
      errors : [],
      email  : {
        value: '',
        error: false
      }
    };
  }


  emailChangeHandler = (val) => {
    this.setState({
      email: {
        ...this.state.email,
        value: val
      }
    })
  };

  submitHandler = () => {
    const {email} = this.state;
    const emailValidate = validate('email',this.state.email.value,{isEmail: true});
    email.error = !!emailValidate;
    this.setState({email, errors: !!emailValidate ? [emailValidate] : []});
    if(!email.error){
      this.props.showLoader();
      axios.put('/Users/forgotPassword',{email: email.value})
        .then(response => {
          this.props.hideLoader();
          if(response.data.ok){
            Swal({
              title: "Password have changed!",
              text: "More detail on your mail",
              type: "success",
              timer: 6000
            });
            this.props.history.push(LOGIN);
          }
          else throw new Error(response.data.message);
        }).catch(err =>{
        Swal({
          title: "Error",
          text: err,
          type: "error",
          timer: 3000
        })
      })
    }
  };

  render() {
    const {errors, email} = this.state;
    return (
        <Fragment>
            <AppHeader/>
      <div className="auth" style={{ backgroundImage: `url(${backGround})`}}>
        <div className="auth-forgot">
          <h1 className="auth-forgot-title"> Forgot password? </h1>
          <div className="auth-errors auth-errors-forgot">
            <ul>
              {errors.map(item => (<li> <img src={icons.iconAttention} alt=""/> {item} </li>))}
            </ul>
          </div>
          <Input
            placeholder='Email'
            className={`authenticate ${email.error ? 'error' : ''}`}
            value={email.value}
            changed={(e) => { this.emailChangeHandler(e.target.value) }}
          />
          <div className="auth-forgot-buttons">

            <button className="button-default" onClick={()=> {this.props.history.push(LOGIN)}}>Sign in</button>
            <button className="big-primary" onClick={this.submitHandler}>submit</button>
          </div>
        </div>
      </div>
        </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  showLoader: () => dispatch(showLoader()),
  hideLoader: () => dispatch(hideLoader()),
});

export default connect(null,mapDispatchToProps)(withRouter(ForgotPassword));
