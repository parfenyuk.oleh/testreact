import React, {Component, Fragment} from 'react';
import {history} from '../../../helpers/index';
import {hideLoader, showLoader} from '../../../store/actions/loader';
import {userType} from '../../../constants';
import {
    getForumPostDetail,
    getForumReplies,
    addForumReplies,
    updateForumReplies,
    updateForum,
    deleteForumReplies,
    deleteForumItem,
    getAllSubjects,
    clearForumReplies
} from '../../../store/actions/user.action';
import {withRouter} from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';
import {userAuthToken} from '../../../helpers/index';
import MainContent from '../../../components/Admin/Forum/MainContent';
import moment from 'moment';
import EditForm from '../../../components/Admin/Forum/Modals/EditForm';
import withSure from '../../../hoc/withSure';
import validate from '../../../helpers/validator';
import {compose} from 'recompose';
import {withInfiniteScroll} from '../../../hoc/withInfiniteScroll';
import Preloader from '../../../components/UI/Preloader';

class ForumList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newForumItem: {
                name: '',
                description: '',
                inactiveDate: ''
            },
            message: '',
            messageEdit: '',
            showEditFormModal: false,
            subject: '',
            errors: [],
            skip: 0
        };
    }

    handleChange = event => {
        if(event.target.name === 'activeItem'){
            event.target.name = 'subject';
        }
        this.setState({[event.target.name]: event.target.value});
    };

    toggleEditFormModal = (id, type) => {
        const {user} = this.props;

        if (type === 'comment') {
            const currentEl = user.forumReplies.find(item => item.id === id);

            this.setState({
                replyId: id,
                messageEdit: currentEl.message,
                type: 'comment'
            })
        } else {

            this.setState({
                messageEdit: user.item.description,
                type: 'forum',
                newForumItem: {
                    name: user.item.name,
                    description: user.item.description,
                    inactiveDate: user.item.inactiveDate
                },
                subject: user.item.subjectId
            })
        }

        this.setState({
            showEditFormModal: !this.state.showEditFormModal,
            errors: [],
        })
    };

    saveComment = () => {
        const {message} = this.state;
        let result = {};

        const user = JSON.parse(localStorage.getItem('user'));
        const forumId = this.props.match.params.id;

        result['message'] = message;
        if (message.length !== 0) {
            this.props.addForumReplies(userAuthToken.token(), user.institutionId, forumId, result);
            this.setState({message: ''})
        }
    };

    editFormHandler = () => {
        const user = JSON.parse(localStorage.getItem('user'));
        const forumId = this.props.match.params.id;
        const {messageEdit, replyId, newForumItem, subject, activeItem} = this.state;
        let result = {};
        const errors = [];

        const title = validate('title', newForumItem.name, {required: 1});
        const description = validate('description', messageEdit, {required: 1});

        if (description) errors.push(description);
        if (this.state.type === 'forum') if (title) errors.push(title);

        this.setState({errors: errors});

        if (messageEdit.length !== 0) {

            if (this.state.type === 'comment') {
                result['message'] = messageEdit;

                this.props.updateForumReplies(userAuthToken.token(), user.institutionId, forumId, replyId, result)
                    .then(() => {
                        this.setState({showEditFormModal: !this.state.showEditFormModal});
                    })

            } else {
              console.log(this.state);
                result['name'] = newForumItem.name;
                result['description'] = messageEdit;
                result['inactiveDate'] = newForumItem.inactiveDate;
                result['subjectId'] = subject;

                console.log(forumId, result);
                this.props.updateForum(userAuthToken.token(), user.institutionId, forumId, result)
                    .then(() => {
                        this.props.getForumPostDetail(userAuthToken.token(), user.institutionId, forumId);

                        this.setState({showEditFormModal: !this.state.showEditFormModal});
                    })
            }
        }
    };

    deleteForumItem = () => {
        this.props.showSureModal('Are you sure you want to delete this forum?')
            .then(result => {
                const user = JSON.parse(localStorage.getItem('user'));
                const forumId = this.props.match.params.id;

                this.props.deleteForumItem(userAuthToken.token(), user.institutionId, forumId)
                    .then(() => {
                        history.push('/forum')
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    removeComment = (replyId) => {
        this.props.showSureModal('Are you sure you want to delete this comment?')
            .then(result => {
                const user = JSON.parse(localStorage.getItem('user'));
                const forumId = this.props.match.params.id;

                this.props.deleteForumReplies(userAuthToken.token(), user.institutionId, forumId, replyId);
            })
            .catch(error => {
                console.log(error);
            })
    };

    componentDidMount() {
        const {institutionId} = this.props.auth.user;
        const forumId = this.props.match.params.id;
        this.props.clearForumReplies();
        this.props.getForumPostDetail(userAuthToken.token(), institutionId, forumId)
        this.props.getAllSubjects(userAuthToken.token(), institutionId);
        this.props.getForumReplies(userAuthToken.token(), institutionId, forumId);
    }

    newForumItemFieldsUpdateHandler = (value, name) => {
        let {newForumItem} = this.state;
        newForumItem[name] = value;
        this.setState({newForumItem});
    };

    onPaginated = () => {
        const {institutionId} = this.props.auth.user;
        const forumId = this.props.match.params.id;
        const {skip} = this.state;
        const skipAmount = 10;

        this.props.getForumReplies(userAuthToken.token(), institutionId, forumId, skip + skipAmount)
            .then(() => {
                this.setState({skip: skip + skipAmount});
            });

    };

    render() {
        const {user, auth, admin} = this.props;
        const {message} = this.state;
        let date, inactiveDate;

        if (user.item !== undefined) {
            date = moment(user.item.createdAt).format('DD-MM-YYYY');
            inactiveDate = moment(user.item.inactiveDate).format('DD-MM-YYYY');
        }

        return (
            <Fragment>
                {!user.forumReplies || !user.item ?
                    <div style={{display: 'flex', alignItems: 'center'}}><Preloader/></div> : (
                        <div className='detail-forum'>
                            <EditForm show={this.state.showEditFormModal}
                                      text={this.state.messageEdit}
                                      saved={this.editFormHandler}
                                      hideModal={this.toggleEditFormModal}
                                      handleChange={this.handleChange}
                                      type={this.state.type}
                                      fieldUpdated={this.newForumItemFieldsUpdateHandler}
                                      subjectList={admin.subject}
                                      subject={this.state.subject}
                                      newForumItem={this.state.newForumItem}
                                      errors={this.state.errors}
                            />
                            <AdvancedList
                                auth={auth}
                                user={user}
                                date={date}
                                inactiveDate={inactiveDate}
                                toggleEditFormModal={this.toggleEditFormModal}
                                deleteForumItem={this.deleteForumItem}
                                message={message}
                                handleChange={this.handleChange}
                                saveComment={this.saveComment}
                                removeComment={this.removeComment}
                                list={user.forumReplies}
                                lengthArray={user.forumReplies.length}
                                isLoading={user.loading}
                                onPaginated={this.onPaginated}
                            />
                        </div>
                    )}
            </Fragment>
        );
    }
}

const AdvancedList = compose(
    withInfiniteScroll,
)(MainContent);

const mapStateToProps = ({loader, auth, user, admin}) => ({loader, auth, user, admin});

const mapDispatchToProps = dispatch => ({
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
    getForumPostDetail: (token, id, forumId) => {
        return dispatch(getForumPostDetail(token, id, forumId))
    },
    getForumReplies: (token, id, forumId, skip) => {
        return dispatch(getForumReplies(token, id, forumId, skip))
    },
    addForumReplies: (token, id, forumId, message) => {
        dispatch(addForumReplies(token, id, forumId, message))
    },
    updateForumReplies: (token, id, forumId, replyId, message) => {
        return dispatch(updateForumReplies(token, id, forumId, replyId, message))
    },
    updateForum: (token, id, forumId, message) => {
        return dispatch(updateForum(token, id, forumId, message))
    },
    deleteForumReplies: (token, id, forumId, replyId) => {
        return dispatch(deleteForumReplies(token, id, forumId, replyId))
    },
    deleteForumItem: (token, id, forumId) => {
        return dispatch(deleteForumItem(token, id, forumId))
    },
    getAllSubjects: (token, id) => {
        dispatch(getAllSubjects(token, id))
    },
    clearForumReplies: () => {
        dispatch(clearForumReplies())
    }
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(withSure(ForumList)));