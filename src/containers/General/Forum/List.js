import React, {Component, Fragment} from 'react';
import Button from '../../../components/UI/Elements/Button'
import {hideLoader, showLoader} from "../../../store/actions/loader";
import {Link} from "react-router-dom";
import {getAllSubjects, getForumPosts, addForumPosts, deleteForumItem} from "../../../store/actions/user.action";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import { withInfiniteScroll } from "../../../hoc/withInfiniteScroll";
import {history, userAuthToken} from "../../../helpers/index";
import Loader from "../../../components/UI/Loader";
import { List } from "../../../components/Admin/Forum/index";
import Header from "../../../components/UI/Layout/Header";
import NewForumItem from "../../../components/Admin/Forum/Modals/NewForumItem";
import validate from '../../../helpers/validator';
import { compose } from 'recompose';
import Select from "../../../components/UI/Select";
import {userType} from "../../../constants";
import withSure from "../../../hoc/withSure";
import MemberTable from "../../../components/Admin/GroupMembers/MemberTable";

class ForumList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            newForumItem: {
                name: '',
                description: '',
                inactiveDate: ''
            },
            activeItem: 'none',
            showNewForumItemModal : false,
            errors: [],
            skip: 0,
            take: 10,
            checkedItems: new Map(),
            requestUrl: ''
        };
    }

    newForumItemFieldsUpdateHandler = (value, name) => {
        let {newForumItem} = this.state;
        newForumItem[name] = value;
        this.setState({newForumItem});
    };

    saveSubjectHandler = () => {
        const {newForumItem, activeItem} = this.state;
        let result = {};
        const errors = [];

        const title = validate('title',newForumItem.name,{required: 1});
        const description = validate('description',newForumItem.description,{required: 1});
        const inactiveDate = validate('inactiveDate',newForumItem.inactiveDate,{required: 1});
        const subjectField = validate('subject',`${activeItem}`,{required: 1});

        if (title) errors.push(title);
        if (description) errors.push(description);
        if (inactiveDate) errors.push(inactiveDate);
        if (subjectField) errors.push(subjectField);

        this.setState({errors: errors});

        if (errors.length === 0) {
            result['name'] = newForumItem.name;
            result['description'] = newForumItem.description;
            result['inactiveDate'] = newForumItem.inactiveDate;
            result['subjectId'] = activeItem;

            const user = JSON.parse(localStorage.getItem('user'));
            this.props.addForumPosts(userAuthToken.token(), user.institutionId, result)
                .then(result =>
                {
                    this.props.getForumPosts( userAuthToken.token(), user.institutionId, 0, 10, [], ''  );

                    if(result.data === undefined) {
                        this.setState({
                            showNewForumItemModal: !this.state.showNewForumItemModal,
                            newForumItem: {
                                name: '',
                                description: '',
                                inactiveDate: ''
                            },
                            subject: '',
                            checkedItems: new Map(),
                            requestUrl: ''
                        });
                    }
                })
        }
    };

    toggleNewForumItemModal = () => {
        this.setState({showNewForumItemModal : !this.state.showNewForumItemModal})
    };

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    componentDidMount() {
        const user = JSON.parse(localStorage.getItem('user'));
        this.props.getForumPosts(
            userAuthToken.token(),
            user.institutionId,
            this.state.skip,
            this.state.take,
            [],
            this.state.requestUrl
        );
        this.props.getAllSubjects(userAuthToken.token(), user.institutionId);
    }

    onPaginated = (e) => {
        const user = JSON.parse(localStorage.getItem('user'));

        this.fetchStories(user.institutionId, this.state.take, this.state.skip + 10, this.props.user.forums);
    };

    fetchStories = (id, take, skip, list) => {
            this.props.getForumPosts(userAuthToken.token(), id, skip, take, list, this.state.requestUrl)
                .then(() => {
                    this.setState({
                        take: take,
                        skip: skip
                    });
                });
    };

    deleteForumItem = (forumId) => {
        const {institutionId} = this.props.auth.user;

        this.props.showSureModal('Are you sure you want to delete this forum?')
            .then(result => {
                const user = JSON.parse(localStorage.getItem('user'));

                this.props.deleteForumItem(userAuthToken.token(), user.institutionId, forumId)
                    .then(() => {
                        this.props.getForumPosts(
                            userAuthToken.token(),
                            institutionId,
                            this.state.skip,
                            this.state.take,
                            [],
                            this.state.requestUrl
                        );
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    handleInputChange = (e) => {
        let { requestUrl, skip, take } = this.state;
        const user = JSON.parse(localStorage.getItem('user'));

        const item = e.target.name;
        const isChecked = e.target.checked;
        let subjectId = item.split('-').slice(-1)[0];
        let removeStr;

        if (isChecked) {
            requestUrl.length > 0 ?
                requestUrl = `${requestUrl}&subjectsIds=${subjectId}&`
                :
                requestUrl = `subjectsIds=${subjectId}&`;
        }

        if (!isChecked && requestUrl.indexOf(subjectId) + 1) {
            if (requestUrl.length > 28) {
                removeStr = `subjectsIds=${subjectId}&`;
                console.log(requestUrl, removeStr);
                requestUrl = requestUrl.replace(removeStr, '');
            } else {
                removeStr = `subjectsIds=${subjectId}&`;
                requestUrl = requestUrl.replace(removeStr, '');
            }
        }

        console.log(requestUrl);

        this.props.getForumPosts(userAuthToken.token(), user.institutionId, skip, take, [], requestUrl);

        this.setState(prevState => ({
            checkedItems: prevState.checkedItems.set(item, isChecked),
            requestUrl: requestUrl,
            skip: 0,
            take: 10,
        }));
    };

    cardClick = (e, id) => {

        if(e.target.className === 'icon delete') {
            this.deleteForumItem(id);
        } else {
            history.push(`/forum/${id}`);
        }
    };

    render() {
        const {user, admin, auth} = this.props;

        const subjectList = admin.subject.filter((item) => !item.archievedAt);

        return (
                        <div className="topic-items">
                            <NewForumItem show={this.state.showNewForumItemModal}
                                          fieldUpdated={this.newForumItemFieldsUpdateHandler}
                                          saved={this.saveSubjectHandler}
                                          hideModal={this.toggleNewForumItemModal}
                                          handleChange={this.handleChange}
                                          subject={this.state.activeItem}
                                          subjectList={admin.subject}
                                          errors={this.state.errors}
                            />
                            <Header
                                headerText="Forum"
                                className=""
                                style={{}}
                            >
                                <div className="buttons-group">
                                    <Select placeholder={'Filter by subjects'}
                                            handleInputChange={this.handleInputChange}
                                            multiselect
                                            checkedItems={this.state.checkedItems}
                                            options={subjectList}/>
                                    {
                                        auth.user.role !== userType.student && (<Button
                                            onClick={this.toggleNewForumItemModal}
                                            type="button"
                                            size="md"
                                        >
                                            CREATE NEW
                                        </Button>)
                                    }
                                </div>
                            </Header>
                            <AdvancedList
                                  list={user.forums}
                                  userId={auth.user.id}
                                  lengthArray={user.forumLastArray.length}
                                  isLoading={user.loading}
                                  take={this.state.take}
                                  skip={this.state.skip}
                                  onPaginated={this.onPaginated}
                                  cardClick={this.cardClick}
                                  role={auth.user.role}
                            />
                        </div>
        );
    }
}

const AdvancedList = compose(
    withInfiniteScroll,
)(List);

const mapStateToProps = ({loader, auth, user, admin}) => ({loader, auth, user, admin});

const mapDispatchToProps = dispatch => ({
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
    getForumPosts: (token, id, skip, take, list, requestUrl) => {return dispatch(getForumPosts(token, id, skip, take, list, requestUrl))},
    getAllSubjects: (token, id) => {dispatch(getAllSubjects(token, id))},
    addForumPosts: (token, id, result) => {return dispatch(addForumPosts(token, id, result))},
    deleteForumItem: (token, id, forumId) => {return dispatch(deleteForumItem(token, id, forumId))},
});

export default compose(
    withRouter,
    withSure,
    connect(mapStateToProps,mapDispatchToProps)
)(ForumList);