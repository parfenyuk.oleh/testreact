import React, {Component, Fragment} from 'react';
import Header from '../../../components/UI/Layout/Header';
import Icon from '../../../components/UI/Elements/Icon';
import NewKeyword from '../../../components/Admin/Keyword/NewKeyword';
import {compose} from 'recompose';
import {withInfiniteScroll} from '../../../hoc/withInfiniteScroll';
import * as actions from '../../../store/actions/keywords.action';
import {withRouter} from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';
import withSure from '../../../hoc/withSure';
import {userAuthToken} from '../../../helpers';
import Button from '../../../components/UI/Elements/Button';
import validate from '../../../helpers/validator';
import {KeywordList} from '../../../components/General/Keyword';


class KeywordTeacher extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeId: 0,
            errors: [],
            description: '',
            showNewKeywordModal : false,
        };
    }

    toggleNewForumItemModal = () => this.setState({showNewKeywordModal : !this.state.showNewKeywordModal});

    handleChange = event => this.setState({ [event.target.name]: event.target.value });

    saveNoticeHandler = () => {
        const {institutionId} = this.props.auth.user;
        const {description} = this.state;
        let result = {};
        const errors = [];

        const descriptionValidate = validate('description', description, {required: 1});

        if (descriptionValidate) errors.push(descriptionValidate);

        this.setState({errors: errors});

        if (errors.length === 0) {
            result['name'] = description;

            this.props.addKeywordsItem(userAuthToken.token(), institutionId, result)
                .then(result => {
                    this.props.clearKeywordsList();
                    this.props.getKeywordsList(userAuthToken.token(), institutionId);

                    if (result === undefined) {
                        this.setState({
                            description: '',
                            showNewKeywordModal: false,
                        });
                    }
                })
        }
    };

    deleteKeywordItemHandler = id => {
        const {institutionId} = this.props.auth.user;

        this.props.showSureModal('Are you sure you want to delete this member?')
            .then(result => {
                this.props.deleteKeywordsItem(userAuthToken.token(), institutionId, id)
                    .then(() => {
                        this.props.clearKeywordsList();
                        this.props.getKeywordsList(userAuthToken.token(), institutionId);
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    componentDidMount() {
        const {institutionId} = this.props.auth.user;

        this.props.clearKeywordsList();
        this.props.getKeywordsList(userAuthToken.token(), institutionId, 0, 100);
    }

    render() {
        const {keywords} = this.props;

        return (
            <Fragment>
                <div className='keywords'>
                    <NewKeyword show={this.state.showNewKeywordModal}
                                  saved={this.saveNoticeHandler}
                                  hideModal={this.toggleNewForumItemModal}
                                  handleChange={this.handleChange}
                                  errors={this.state.errors}
                    />
                    <div className='keywords-header__wrapper'>
                        <Header
                            headerText='Keywords master'
                        >
                            <Button
                                type='button'
                                size='md'
                                onClick={this.toggleNewForumItemModal}
                            >
                                CREATE NEW
                            </Button>
                        </Header>
                    </div>
                    <div className='main-part'>
                        <div className='keyword-info'>
                            <ul className='keyword-list'>
                                <AdvancedList
                                    list={keywords.list}
                                    lengthArray={keywords.list.length}
                                    deleteKeywordItemHandler={this.deleteKeywordItemHandler}
                                    isLoading={keywords.loading}
                                    onPaginated={this.onPaginated}
                                />
                            </ul>
                        </div>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const AdvancedList = compose(
    withInfiniteScroll,
)(KeywordList);

const mapStateToProps = ({keywords, auth}) => ({keywords, auth});

const mapDispatchToProps = dispatch => ({
    getKeywordsList : (token, id, skip, take) => {return dispatch(actions.getKeywordsList(token, id, skip, take))},
    addKeywordsItem : (token, id, result) => {return dispatch(actions.addKeywordsItem(token, id, result))},
    deleteKeywordsItem : (token, id, keywordId) => {return dispatch(actions.deleteKeywordsItem(token, id, keywordId))},
    clearKeywordsList : () => {return dispatch(actions.clearKeywordsList())},
});

const connectedKeywordTeacher = withRouter(connect(mapStateToProps,mapDispatchToProps)(withSure(KeywordTeacher)));

export { connectedKeywordTeacher as KeywordTeacher };