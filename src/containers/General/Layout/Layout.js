import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {Route, withRouter, Switch} from 'react-router-dom';
import {logout} from '../../../store/actions/auth.action';
import UniversityModal from '../../../components/Layout/Modals/UniversityModal';
import * as actions from '../../../store/actions/layout';
import * as superAdminActions from '../../../store/actions/superadmin';
import {showLoader} from '../../../store/actions/loader';
import * as routeConstants from '../../../constants/routes';
import {AuthorizedUserGrid, RegistrationFlowGrid} from '../../../components/Layout/Grid';

class Layout extends Component{
    constructor(props){
      super(props);

      this.state = {
        showChats: false,
        dropDownVisibility: false,
        fullSidebar: true,

      };
    }

    toggleChats = () => {
      this.setState({showChats: !this.state.showChats});
    };

    showDropDown = () => {
      this.setState({dropDownVisibility: !this.state.dropDownVisibility});
    };

    showUniversityModal = () => {
      this.props.showModal('university');
    };

    logout = () => {
      this.props.logout();
    };

    changeInstitutionHandler = (institution) => {
      const {auth, user} = this.props;
      institution.id = user.institution.id;
      this.props.updateUniversity(institution, auth.user.token);
    };

    toggleSidebar = () => this.setState({fullSidebar: !this.state.fullSidebar});

    render() {
      const {dropDownVisibility, fullSidebar} = this.state;
      const {auth} = this.props;
      const {institution} = this.props.user;
      const user = JSON.parse(localStorage.getItem('user'));

      return <Fragment>
          {
              this.props.match.path === routeConstants.REGISTRATION_FLOW ?
                  <AuthorizedUserGrid /> :
                  <RegistrationFlowGrid
                      toggleChats={this.toggleChats}
                      showUniversityModal={this.showUniversityModal}
                      showDropDown={this.showDropDown}
                      dropDownVisibility={dropDownVisibility}
                      logout={this.logout}
                      auth={auth}
                      university={institution}
                      user={user}
                      show={this.state.showChats}
                      toggleSidebar={this.toggleSidebar}
                      fullSidebar={fullSidebar}
                  />

          }
          {this.props.layout.modals.university  && <UniversityModal show={this.props.layout.modals.university}
                                                                    name={institution.name}
                                                                    avatar={institution.avatar}
                                                                    color={institution.color}
                                                                    initialName={institution.key}
                                                                    save={this.changeInstitutionHandler}
          />}
      </Fragment>
    }
}

const mapStateToProps = ({auth, layout, user}) => ({auth, layout, user});

const mapDispatchToProps = dispatch => ({
  showModal: modal => {dispatch(actions.showModal(modal))},
  hideModal: modal => {dispatch(actions.hideModal(modal))},
  logout: () => {dispatch(logout())},
  updateUniversity: (item, token) => {dispatch(superAdminActions.updateUniversity(item, token))},
  showLoader: () => {dispatch(showLoader())}
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Layout));
