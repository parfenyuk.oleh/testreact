import React, {Component} from 'react';
import {connect} from 'react-redux';
import MainHeader from '../../../components/Admin/Main/MainHeader';
import MainSidebar from '../../../components/Admin/Main/MainSidebar';
import NewSubjectModal from '../../../components/Admin/Main/Modals/NewSubjectModal';
import ChangeMemberModal from '../../../components/Admin/Main/Modals/ChangeMemberModal';

import * as actions from '../../../store/actions/admin.action';
import NewMemberModal from '../../../components/Admin/Main/Modals/NewMemberModal';
import ROLES from '../../../constants/roles';
import StudentTable from '../../../components/Admin/Main/Tabs/StudentTable';
import withSure from '../../../hoc/withSure';
import TeacherTable from '../../../components/Admin/Main/Tabs/TeacherTable';
import SubjectCards from '../../../components/Admin/Main/Tabs/SubjectCards';
import MemberInfo from '../../../components/Admin/Main/Modals/MemberInfo';
import AddStudentToSubjectModal from '../../../components/Admin/Main/Modals/AddStudentToSubjectModal';
import AddTeacherToSubjectModal from '../../../components/Admin/Main/Modals/AddTeacherToSubjectModal';
import UpdateSubjectModal from '../../../components/Admin/Main/Modals/UpdateSubjectModal';

/**
 * Constant for all tabs of Admin Main page
 *
 * @type {{TEACHERS: number, STUDENTS: number, SUBJECTS: number}}
 */
const TABS = {
  TEACHERS: 1,
  STUDENTS: 2,
  ADMIN: 3,
  SUBJECTS: 4,
};

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showNewSubjectModal: false,
      showNewMemberModal: false,
      showChangeMemberModal: false,
      showChangeSubjectModal: false,
      showMemberInfoModal: false,
      showAddStudentToSubjectModal: false,
      showAddTeacherToSubjectModal: false,
      memberForShow: {},
      memberForChange: {},
      subjectForChange: {},
      newMemberRole: null,
      currentTab: props.auth.user.role === ROLES.ADMIN ? TABS.ADMIN : TABS.STUDENTS,
      chosenSubject: null,
      pagination: {
        teacher: 2,
        student: 2,
        admin: 2,
        subject: 1,
      },
      checkedItemsStudents: new Map(),
      checkedItemsTeacher: new Map(),
      searchField: '',
    };
  }


  componentWillMount() {
    this.props.resetReducer();
    const user = JSON.parse(localStorage.getItem('user'));
    this.props.getAllSubjects(user.institutionId, user.token, this.props.auth.user.role === ROLES.ADMIN ? 1 : 2);
    this.props.getAllMembers(user.institutionId, user.token, ROLES.TEACHER, 0, 20);
    this.props.getAllMembers(user.institutionId, user.token, ROLES.STUDENT, 0, 20);
    this.props.getAllMembers(user.institutionId, user.token, ROLES.ADMIN, 0, 20);
    this.props.getMemberAmount(user.institutionId, user.token);
  };

  saveSubjectHandler = (subject) => {
    const {token, institutionId} = this.props.auth.user;
    this.props.createNewSubject(institutionId, subject, token);
    this.toggleNewSubjectModal();
  };

  toggleNewSubjectModal = () => {
    this.setState({showNewSubjectModal: !this.state.showNewSubjectModal})
  };

  toggleUpdateSubjectModal = () => {
    this.setState({showChangeSubjectModal: !this.state.showChangeSubjectModal})
  };

  /**
   * Set clicked subject to state {subjectForChange} and show modal witch clicked subject
   *
   * @param item
   */
  handleStartChangeSubject = (item) => {
    this.setState({subjectForChange: item, showChangeSubjectModal: true})
  };

  /**
   * Archive subject change value {ArchievedAt} by backend
   *
   * @param id
   */
  archiveSubjectHandler = (id) => {
    this.props.showSureModal('Are you sure you want archive this subject?')
      .then(() => {
        const {token, institutionId} = this.props.auth.user;
        this.props.archiveSubject(institutionId, token, id);
      })
  };

  saveUpdateSubjectHandler = subject => {
    const {token, institutionId} = this.props.auth.user;
    this.props.updateSubject(institutionId, token, subject);
    this.toggleUpdateSubjectModal();
  };

  toggleNewMemberModal = (newMemberRole = null) => {
    this.setState({
      showNewMemberModal: !this.state.showNewMemberModal,
      newMemberRole,
    })
  };

  toggleChangeMemberModal = () => {
    this.setState({showChangeMemberModal: !this.state.showChangeMemberModal})
  };

  toggleMemberInfoModal = () => {
    this.setState({showMemberInfoModal: !this.state.showMemberInfoModal})
  };

  saveNewHandlerMember = data => {
    const {token, institutionId} = this.props.auth.user;
    data.roleType = this.state.newMemberRole;
    this.props.createNewMember(institutionId, data, token);
    this.toggleNewMemberModal();
  };

  showMemberInfoHandler = id => {
    this.setState({
      showMemberInfoModal: true,
      memberForShow: this.props.admin.subjectMixed.find(item => item.id === id),
    })
  };

  deleteMemberFromSubjectHandler = id => {
    this.props.showSureModal('Are you sure you want to delete this member?')
      .then(result => {
        const {token, institutionId} = this.props.auth.user;
        this.props.deleteMemberFromSubject(institutionId, token, this.state.chosenSubject.id, id);
      })
      .catch(error => {
      })
  };

  changeTab = (tab, subject = null) => {

    if(this.state.currentTab === TABS.SUBJECTS && tab !== TABS.SUBJECTS){
     this.reloadMembers();
    }
    this.setState({currentTab: tab, chosenSubject: subject, pagination: {...this.state.pagination, subject: 1}});
    if (subject) {
      this.props.clearSubjectMixedFiled();
      const {token, institutionId} = this.props.auth.user;
      this.props.getAssignedMembersToSubject(institutionId, subject.id, token);
    }
  };

  reloadMembers = () => {
    const user = JSON.parse(localStorage.getItem('user'));
    this.props.clearAdminsTeacherStudents();
    this.props.getAllMembers(user.institutionId, user.token, ROLES.TEACHER, 0, 20);
    this.props.getAllMembers(user.institutionId, user.token, ROLES.STUDENT, 0, 20);
    this.props.getAllMembers(user.institutionId, user.token, ROLES.ADMIN, 0, 20);
    this.setState({
      searchField: '',
    });
  };

  /**
   * Update memberForChange in state and show Change Member Modal
   *
   * @param {object} member Model of user which will be changed
   */
  changeMemberHandler = member => {
    this.setState({
      memberForChange: member,
      showChangeMemberModal: !this.showChangeMemberModal,
    });
  };

  /**
   * Save user updated user model
   *
   * @param {object} member Model of user which will be saved by API
   */
  saveChangedMemberHandler = member => {
    const {token, institutionId} = this.props.auth.user;
    this.props.updateByAdminMember(institutionId, token, member);
    this.setState({showChangeMemberModal: false})
  };

  /**
   * Delete user
   *
   * @param {object} member Model of user which will be deleted by API
   */
  deleteMemberHandler = member => {
    this.props.showSureModal('Are you sure you want to delete this member?')
      .then(() => {
        const {token, institutionId} = this.props.auth.user;
        this.props.deleteMember(institutionId, token, member.id);
      })
      .catch(error => {
      })
  };

  /**
   * pagination handler for users and teachers
   *
   * @param role
   */
  loadMoreMembers = (role) => {
    if (this.state.searchField) return;
    const {token, institutionId} = this.props.auth.user;
    const {student: pgStudent, teacher: pgTeacher, admin: pgAdmin, subject} = this.state.pagination;
    if (role === ROLES.TEACHER) {
      this.props.getAllMembers(institutionId, token, role, pgTeacher * 10);
      this.setState({
        pagination: {
          student: pgStudent,
          teacher: pgTeacher + 1,
          admin: pgAdmin,
          subject,
        },
      })
    }
    if (role === ROLES.STUDENT) {
      this.props.getAllMembers(institutionId, token, role, pgStudent * 10);
      this.setState({
        pagination: {
          student: pgStudent + 1,
          teacher: pgTeacher,
          admin: pgAdmin,
          subject,
        },
      })
    }
    if (role === ROLES.ADMIN) {
      this.props.getAllMembers(institutionId, token, role, pgAdmin * 10);
      this.setState({
        pagination: {
          student: pgStudent,
          teacher: pgTeacher,
          admin: pgAdmin + 1,
          subject,
        },
      })
    }

  };

  loadMoreSubjectMembers = () => {
    const {token, institutionId} = this.props.auth.user;
    const {pagination, chosenSubject: subject} = this.state;
    this.props.getAssignedMembersToSubject(institutionId, subject.id, token, null, pagination.subject * 10);
    pagination.subject++;
    this.setState({pagination});
  };

  toggleAddStudentToSubjectModal = () => {
    if(this.state.showAddStudentToSubjectModal) this.reloadMembers();
    this.setState({
      showAddStudentToSubjectModal: !this.state.showAddStudentToSubjectModal,
    })
  };

  toggleAddTeacherToSubjectModal = () => {
    if(this.state.showAddTeacherToSubjectModal) this.reloadMembers();
    this.setState({
      showAddTeacherToSubjectModal: !this.state.showAddTeacherToSubjectModal,
    })
  };

  /**
   * handle AddStudentToSubjectModal checkbox change
   * @param e
   * @param type
   */
  handleCheckboxChange = (e, type) => {
    const item = e.target.name;
    const isChecked = e.target.checked;

    if (type === 'students') {
      this.setState(prevState => ({
        checkedItemsStudents: prevState.checkedItemsStudents.set(item, isChecked),
      }));
    } else if (type === 'teacher') {
      this.setState(prevState => ({
        checkedItemsTeacher: prevState.checkedItemsTeacher.set(item, isChecked),
      }));
    }
  };

  /**
   * handle AddStudentToSubjectModal Save,
   * collect all user ids and send them via API
   *
   * @param type
   */
  handleAddModalSave = type => {
    const {checkedItemsStudents, checkedItemsTeacher} = this.state;
    const {institutionId, token} = this.props.auth.user;
    let members = [];
    if (type === 'students') {

      checkedItemsStudents.forEach(function (value, key) {
        if (value) {
          members.push(+key)
        }
      });
      this.toggleAddStudentToSubjectModal();
    }
    if (type === 'teacher') {

      checkedItemsTeacher.forEach(function (value, key) {
        if (value) {
          members.push(+key)
        }
      });
      this.toggleAddTeacherToSubjectModal();
    }
    this.props.addMemberToSubject(institutionId, token, this.state.chosenSubject.id, members);

    this.setState({
      checkedItemsStudents: new Map(),
      checkedItemsTeacher: new Map(),
    })

  };

  searchingUsers = (event, role) => {
    const {pagination} = this.state;
    const user = JSON.parse(localStorage.getItem('user'));
    if (event.target.value) {
      this.props.clearAdminsTeacherStudents();
      this.props.getAllMembers(user.institutionId, user.token, role, null, null, event.target.value);
      this.setState({
        pagination: {
          teacher: 2,
          student: 2,
          admin: 2,
          subject: pagination.subject,
        },
        searchField: event.target.value,
      })
    } else {
     this.reloadMembers();
    }

  };


  render() {
    const {currentTab, newMemberRole} = this.state;
    return (
      <div className="admin-main">
        <MainHeader/>
        <MainSidebar showNewSubjectModal={this.toggleNewSubjectModal}
                     showNewStudentModal={() => {
                       this.toggleNewMemberModal(ROLES.STUDENT)
                     }}
                     showNewTeacherModal={() => {
                       this.toggleNewMemberModal(ROLES.TEACHER)
                     }}
                     showNewAdminModal={() => {
                       this.toggleNewMemberModal(ROLES.ADMIN)
                     }}
                     changeTab={this.changeTab}
                     TABS={TABS}
                     counts={{
                       teachers: this.props.admin.amount.amountOfTeachers,
                       students: this.props.admin.amount.amountOfStudents,
                       admins: this.props.admin.amount.amountOfAdmins,
                     }}
                     subjects={this.props.admin.subject}
                     activeTab={this.state.currentTab}
                     chosenSubject={this.state.chosenSubject}
                     archiveSubject={this.archiveSubjectHandler}
                     updateSubject={this.handleStartChangeSubject}
        />

        {this.state.showNewSubjectModal && <NewSubjectModal show={this.state.showNewSubjectModal}
                                                            saved={this.saveSubjectHandler}
                                                            hideModal={this.toggleNewSubjectModal}
        />}
        <NewMemberModal show={this.state.showNewMemberModal}
                        hideModal={this.toggleNewMemberModal}
                        saved={this.saveNewHandlerMember}
                        title={newMemberRole === ROLES.STUDENT ? 'New Student' :
                          newMemberRole === ROLES.TEACHER ? 'New Teacher' : 'New admin'
                        }
        />
        <ChangeMemberModal show={this.state.showChangeMemberModal}
                           hideModal={this.toggleChangeMemberModal}
                           user={this.state.memberForChange}
                           saved={this.saveChangedMemberHandler}
        />
        <MemberInfo show={this.state.showMemberInfoModal}
                    user={this.state.memberForShow}
                    hideModal={this.toggleMemberInfoModal}
        />
        <AddStudentToSubjectModal show={this.state.showAddStudentToSubjectModal}
                                  list={this.props.admin.students}
                                  admin={this.props.admin}
                                  checkedItems={this.state.checkedItemsStudents}
                                  onPaginated={() => {
                                    this.loadMoreMembers(ROLES.STUDENT)
                                  }}
                                  handleInputChange={this.handleCheckboxChange}
                                  saved={this.handleAddModalSave}
                                  hideModal={this.toggleAddStudentToSubjectModal}
                                  onSearch={this.searchingUsers}
        />
        <AddTeacherToSubjectModal show={this.state.showAddTeacherToSubjectModal}
                                  list={this.props.admin.teacher}
                                  admin={this.props.admin}
                                  checkedItems={this.state.checkedItemsTeacher}
                                  onPaginated={() => {
                                    this.loadMoreMembers(ROLES.TEACHER)
                                  }}
                                  handleInputChange={this.handleCheckboxChange}
                                  saved={this.handleAddModalSave}
                                  hideModal={this.toggleAddTeacherToSubjectModal}
                                  onSearch={this.searchingUsers}
        />
        <UpdateSubjectModal show={this.state.showChangeSubjectModal}
                            hideModal={this.toggleUpdateSubjectModal}
                            subject={this.state.subjectForChange}
                            saved={this.saveUpdateSubjectHandler}
        />

        {currentTab === TABS.ADMIN && <TeacherTable list={this.props.admin.admins}
                                                    changeMemberHandler={this.changeMemberHandler}
                                                    deleteMemberHandler={this.deleteMemberHandler}
                                                    onPaginated={() => {
                                                      this.loadMoreMembers(ROLES.ADMIN)
                                                    }}
                                                    isLoading={this.props.admin.loading}
        />}
        {currentTab === TABS.TEACHERS && <TeacherTable list={this.props.admin.teacher}
                                                       changeMemberHandler={this.changeMemberHandler}
                                                       deleteMemberHandler={this.deleteMemberHandler}
                                                       onPaginated={() => {
                                                         this.loadMoreMembers(ROLES.TEACHER)
                                                       }}
                                                       isLoading={this.props.admin.loading}
        />}
        {currentTab === TABS.STUDENTS && <StudentTable list={this.props.admin.students}
                                                       changeMemberHandler={this.changeMemberHandler}
                                                       deleteMemberHandler={this.deleteMemberHandler}
                                                       onPaginated={() => {
                                                         this.loadMoreMembers(ROLES.STUDENT)
                                                       }}
                                                       isLoading={this.props.admin.loading}
        />}
        {currentTab === TABS.SUBJECTS && <SubjectCards list={this.props.admin.subjectMixed}
                                                       showMemberInfo={this.showMemberInfoHandler}
                                                       deleteMember={this.deleteMemberFromSubjectHandler}
                                                       onPaginated={() => {
                                                         this.loadMoreSubjectMembers()
                                                       }}
                                                       isLoading={this.props.admin.loading}
                                                       showStudentModal={this.toggleAddStudentToSubjectModal}
                                                       showTeacherModal={this.toggleAddTeacherToSubjectModal}
                                                       userRole={this.props.auth.user.role}
        />}

      </div>
    );
  }
}

const mapStateToProps = ({auth, admin}) => ({auth, admin});

const mapDispatchToProps = dispatch => ({
  createNewSubject: (institutionId, subject, token) => {dispatch(actions.createNewSubject(institutionId, subject, token))},
  createNewMember: (id, data, token) => {dispatch(actions.createNewMember(id, data, token))},
  getAllSubjects: (id, token, relation) => {dispatch(actions.getAllSubjects(id, token, relation))},
  getAllMembers: (id, token, rolesIds, skip, take, search) => {dispatch(actions.getAllMembers(id, token, rolesIds, skip, take, search))},
  getAssignedMembersToSubject: (institutionId, subjectId, token, rolesIds, skip, take) => {dispatch(actions.getAssignedMembersToSubject(institutionId, subjectId, token, rolesIds, skip, take))},
  clearSubjectMixedFiled: () => {dispatch(actions.clearSubjectMixedFiled())},
  deleteMember: (universityId, token, memberId) => {dispatch(actions.deleteMember(universityId, token, memberId))},
  deleteMemberFromSubject: (universityId, token, subjectId, memberId) => {
    dispatch(actions.deleteMemberFromSubject(universityId, token, subjectId, memberId))
  },
  addMemberToSubject: (institutionId, token, subjectId, userIds) => {
    dispatch(actions.addMemberToSubject(institutionId, token, subjectId, userIds))
  },
  getMemberAmount: (institutionId, token) => {dispatch(actions.getMemberAmount(institutionId, token))},
  updateByAdminMember: (institutionId, token, member) => { dispatch(actions.updateByAdminMember(institutionId, token, member))},
  archiveSubject: (institutionId, token, subjectId) => {dispatch(actions.archiveSubject(institutionId, token, subjectId))},
  updateSubject: (institutionId, token, subject) => {dispatch(actions.updateSubject(institutionId, token, subject))},
  resetReducer: () => {dispatch(actions.resetReducer())},
  clearAdminsTeacherStudents: () => {dispatch(actions.clearAdminsTeacherStudents())},
});

export default connect(mapStateToProps, mapDispatchToProps)(withSure(Main));