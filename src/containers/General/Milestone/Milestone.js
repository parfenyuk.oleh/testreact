import React, {Component} from 'react'
import PropTypes from 'prop-types';
import Scheduler, {SchedulerData, ViewTypes} from './index'
import withDragDropContext from '../../../components/General/Milestone/withDnDContext'
import {withRouter} from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';
import moment from 'moment'
import withSure from '../../../hoc/withSure';
import Icon from '../../../components/UI/Elements/Icon';
import {hideLoader, showLoader} from '../../../store/actions';
import {Header} from '../../../components/General/Milestone';
import {MilestoneModal} from '../../../components/General/Milestone';
import validate from '../../../helpers/validator';
import ROLES from '../../../constants/roles';
import {getAllMembers} from '../../../store/actions/admin.action';
import * as actions from '../../../store/actions/milestones.actions';
import '../../../style/admin/Milestone/SelectSearch.css'
import {userAuthToken} from '../../../helpers/index';
import Preloader from '../../../components/UI/Preloader';

class Milestone extends Component{
    constructor(props){
        super(props);

        let schedulerData = new SchedulerData(moment(), ViewTypes.Week, false, false, {
            startResizable: false,
            endResizable: false,
            movable: false,
            creatable: false,
        });

        schedulerData.localeMoment.locale('en');
        this.state = {
            viewModel: schedulerData,
            title: '',
            startDate: '',
            endDate: '',
            description: '',
            showMilestoneModal: false,
            errors: [],
            activeUser: null,
            emptyState: [{
                avatar: props.auth.user.avatar,
                userId: props.auth.user.id,
                firstName: props.auth.user.firstName,
                lastName: props.auth.user.lastName
            }],
            currentMilestone: null,
        }
    }

    saveMilestone = () => {
        const {startDate, endDate, description, title, activeUser, viewModel, currentMilestone} = this.state;
        const {institutionId} = this.props.auth.user;
        let result = {};
        const errors = [];

        const titleValidate = validate('Title', title, {required: 1});
        const descriptionValidate = validate('Description', description, {required: 1});
        const startDateValidate = validate('Start date', startDate, {isNotEmptyDate : true});
        const endDateValidate = validate('End date', endDate, {isNotEmptyDate : true, notLessDate: startDate});
        const activeUserValidate = validate('Assign', activeUser, {isNotEmptyDate : true});

        if (titleValidate) errors.push(titleValidate);
        if (descriptionValidate) errors.push(descriptionValidate);
        if (startDateValidate) errors.push(startDateValidate);
        if (endDateValidate) errors.push(endDateValidate);
        if (activeUserValidate) errors.push(activeUserValidate);

        this.setState({errors: errors});

        if (!errors.length) {
            result['title'] = title;
            result['description'] = description;
            result['startDate'] = moment(startDate).format('l');
            result['endDate'] = moment(endDate).format('l');
            result['userAssignmentId'] = activeUser;

            if(currentMilestone) {
                this.props.updateMilestone(userAuthToken.token(), institutionId, currentMilestone.id, result)
                    .then(() =>
                    {
                        this.getMilestoneRequest(viewModel, institutionId);

                        this.setState({
                            showMilestoneModal: false,
                            title: '',
                            startDate: '',
                            endDate: '',
                            description: '',
                            activeUser: null
                        });
                    })
            } else {
                this.props.addMilestone(userAuthToken.token(), institutionId, result)
                    .then(() => {
                        this.getMilestoneRequest(viewModel, institutionId);

                        this.setState ({
                            showMilestoneModal: false,
                            title: '',
                            startDate: '',
                            endDate: '',
                            description: '',
                            activeUser: null
                        });
                    })
            }
        }
    };

    componentDidMount() {
        const {token, institutionId} = this.props.auth.user;
        const {viewModel} = this.state;
        const amountUser = 1000;

        this.getMilestoneRequest(viewModel, institutionId);
        this.props.getAllMembers(institutionId, token, [ROLES.STUDENT], 0, amountUser);
    }

    getMilestoneRequest = (viewModel, institutionId) => {
        this.props.getMilestonesList(userAuthToken.token(), institutionId, viewModel.startDate, viewModel.endDate)
            .then((item) => {

                if (item.users.length) {
                    viewModel.setResources(item.users);
                    viewModel.setEvents(item.items);
                } else {
                    viewModel.setResources(this.state.emptyState);
                    viewModel.setEvents([]);
                }

                this.setState ({
                    viewModel: viewModel,
                });
            });
    };

    toggleMilestoneModal = () => this.setState({
        showMilestoneModal: !this.state.showMilestoneModal,
        startDate: '',
        endDate: '',
        description: '',
        title: '',
        currentMilestone: null,
        activeUser: null,
        error: []
    });

    fieldUpdated = (e, field) => this.setState({ [field]: e });

    handleChange = (event, type) => {
        !type ?
            this.setState({ [event.target.name]: event.target.value })
            :
            this.setState({ [type]: moment(event) });
    };

    changeSelect = userId => this.setState({activeUser: userId});

    render() {
        const {admin, milestones} = this.props;
        const {viewModel, endDate, startDate, title, description, errors, showMilestoneModal, activeUser} = this.state;


        const edit = <Icon name='edit-dark'/>;
        const remove = <Icon name='delete-dark'/>;

        return (
            <div>

                <MilestoneModal show={showMilestoneModal}
                             saved={this.saveMilestone}
                             hideModal={this.toggleMilestoneModal}
                             handleChange={this.handleChange}
                             errors={errors}
                             title={title}
                             startDate={startDate}
                             endDate={endDate}
                             activeItem={activeUser}
                             fieldUpdated={this.fieldUpdated}
                             description={description}
                             changeSelect={this.changeSelect}
                             members={admin.members}
                />

                <Header toggleMilestoneModal={this.toggleMilestoneModal}/>

                    {
                        milestones.loading ? <div className='milestone preloader-wrapper'>
                            <Preloader/>
                        </div> : <div className='table-wrapper'>
                            <Scheduler schedulerData={viewModel}
                                       prevClick={this.prevClick}
                                       nextClick={this.nextClick}
                                       onSelectDate={this.onSelectDate}
                                       onViewChange={this.onViewChange}
                                       viewEventClick={this.updateEvent}
                                       viewEventText={edit}
                                       viewEvent2Text={remove}
                                       viewEvent2Click={this.deleteEvent}
                                       milestones={milestones}
                            />
                        </div>
                    }

            </div>
        )
    }

    prevClick = (schedulerData)=> {
        const {institutionId} = this.props.auth.user;
        schedulerData.prev();

        this.props.getMilestonesList(userAuthToken.token(), institutionId, schedulerData.startDate, schedulerData.endDate)
            .then((item) => {

                if (item.users.length) {
                    schedulerData.setResources(item.users);
                    schedulerData.setEvents(item.items);
                } else {
                    schedulerData.setResources(this.state.emptyState);
                    schedulerData.setEvents(item.items);
                }

                this.setState({
                    viewModel: schedulerData,
                });
            });
    };

    nextClick = (schedulerData)=> {
        const {institutionId} = this.props.auth.user;
        schedulerData.next();

        this.props.getMilestonesList(userAuthToken.token(), institutionId, schedulerData.startDate, schedulerData.endDate)
            .then((item) => {

                if (item.users.length) {
                    schedulerData.setResources(item.users);
                    schedulerData.setEvents(item.items);
                } else {
                    schedulerData.setResources(this.state.emptyState);
                }

                this.setState({
                    viewModel: schedulerData,
                });
            });
    };

    onViewChange = (schedulerData, view) => {
        const {institutionId} = this.props.auth.user;
        schedulerData.setViewType(view.viewType, view.showAgenda, view.isEventPerspective);

        this.props.getMilestonesList(userAuthToken.token(), institutionId, schedulerData.startDate, schedulerData.endDate)
            .then((item) => {
                if (item.users.length) {
                    schedulerData.setResources(item.users);
                    schedulerData.setEvents(item.items);
                } else {
                    schedulerData.setResources(this.state.emptyState);
                }

                this.setState({
                    viewModel: schedulerData,
                });
            });
    };

    onSelectDate = (schedulerData, date) => {
        const {institutionId} = this.props.auth.user;
        schedulerData.setDate(date);

        this.props.getMilestonesList(userAuthToken.token(), institutionId, schedulerData.startDate, schedulerData.endDate)
            .then((item) => {
                if (item.users.length) {
                    schedulerData.setResources(item.users);
                    schedulerData.setEvents(item.items);
                } else {
                    schedulerData.setResources(this.state.emptyState);
                }

                this.setState({
                    viewModel: schedulerData,
                });
            });
    };

    updateEvent = (schedulerData, event) => {
        const {milestones} = this.props;

        const currentMilestone = milestones.milestoneList.find(item => item.id === event.id);

        this.setState({
            currentMilestone: currentMilestone,
            showMilestoneModal: !this.state.showMilestoneModal,
            title: currentMilestone.title,
            startDate: moment(currentMilestone.startDate),
            endDate: moment(currentMilestone.endDate),
            description: currentMilestone.description,
            activeUser: currentMilestone.userId
        })
    };

    deleteEvent = (schedulerData, event) => {
            const {institutionId} = this.props.auth.user;

            this.props.showSureModal('Are you sure you want to delete this comment?')
                .then(() => {
                    this.props.deleteMilestone(userAuthToken.token(), institutionId, event.id)
                        .then(() => {
                            this.getMilestoneRequest(schedulerData, institutionId);
                        })
                })
                .catch(error => {
                    console.log(error);
                })
    };
}

const mapStateToProps = ({loader, auth, user, admin, milestones}) => ({loader, auth, user, admin, milestones});

const mapDispatchToProps = dispatch => ({
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
    getAllMembers : (id, token,rolesIds,skip,take, search) =>
    {dispatch(getAllMembers(id, token,rolesIds,skip,take, search))},
    getMilestonesList : (token, id, startDate, endDate) => {
        return dispatch(actions.getMilestonesList(token, id, startDate, endDate))
    },
    addMilestone : (token, id, result) => {
        return dispatch(actions.addMilestone(token, id, result))
    },
    deleteMilestone : (token, id, milestoneId) => {
        return dispatch(actions.deleteMilestone(token, id, milestoneId))
    },
    updateMilestone : (token, id, milestoneId, result) => {
        return dispatch(actions.updateMilestone(token, id, milestoneId, result))
    },
});

const connectedMilestone = withRouter(connect(mapStateToProps,mapDispatchToProps)(withSure(withDragDropContext(Milestone))));

export { connectedMilestone as Milestone };