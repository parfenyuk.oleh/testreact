import React, {Component} from 'react';
import {hideLoader, showLoader} from "../../../store/actions/loader";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import NewNoticeItem from "../../../components/General/NoticeBoard/Modals/NewNoticeItem";
import {history, userAuthToken} from "../../../helpers/index";
import {getAllGroups} from '../../../store/actions/groups.action';
import validate from "../../../helpers/validator";
import * as actions from "../../../store/actions/notice.action";
import withSure from "../../../hoc/withSure";
import HeaderNotice from "../../../components/General/NoticeBoard/HeaderNotice";
import {compose} from "recompose";
import {withInfiniteScroll} from "../../../hoc/withInfiniteScroll";
import {NoticeList} from "../../../components/General/NoticeBoard/NoticeList";

class NoticeBoard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            title: '',
            description: '',
            showNewNoticeItemModal : false,
            errors: [],
            activeItem: 'none',
            checkedItems: new Map(),
            paramsUrl: '',
            skip: 0,
            take: 10,
        };
    }

    componentDidMount() {
        const {institutionId} = this.props.auth.user;
        this.props.clearNoticeList();
        this.props.getNoticeList(userAuthToken.token(), institutionId);
        this.props.getAllGroups(userAuthToken.token(), institutionId);
    }

    toggleNewNoticeItemModal = () => this.setState({showNewNoticeItemModal: !this.state.showNewNoticeItemModal});

    handleChange = event => this.setState({ [event.target.name]: event.target.value });

    saveNoticeHandler = () => {
        const {institutionId} = this.props.auth.user;
        const {title, description, activeItem} = this.state;
        let result = {};
        const errors = [];

        const titleValidate = validate('title',title,{required: 1});
        const descriptionValidate = validate('description',description,{required: 1});
        const groupField = validate('Group',`${activeItem}`,{required: 1});

        if (titleValidate) errors.push(titleValidate);
        if (descriptionValidate) errors.push(descriptionValidate);
        if (groupField) errors.push(groupField);

        this.setState({errors: errors});

        if (errors.length === 0) {
            result['name'] = title;
            result['description'] = description;
            result['groupId'] = activeItem;

            this.props.addNoticeItem(userAuthToken.token(), institutionId, result)
                .then(result =>
                {
                    this.props.getNoticeList(userAuthToken.token(), institutionId);

                    if(result === undefined) {
                        this.setState({
                            title: '',
                            description: '',
                            showNewNoticeItemModal : false,
                            checkedItems: new Map(),
                            activeItem: 'none',
                            skip: 0,
                            paramsUrl: '',
                        });
                    }
                })
        }
    };

    deleteNoticeItem = (noticeId) => {
        const {institutionId} = this.props.auth.user;
    
        this.props.showSureModal('Are you sure you want to delete this comment?')
            .then(() => {
                this.props.deleteNoticeItem(userAuthToken.token(), institutionId, noticeId)
                    .then(() => {
                        this.props.clearNoticeList();
                        this.props.getNoticeList(userAuthToken.token(), institutionId);
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    cardClick = (e, id) => {
        if(e.target.className === 'icon delete') {
            this.deleteNoticeItem(id);
        } else {
            history.push(`/notice-board/${id}`);
        }
    };

    handleInputChange = (e) => {
        const {institutionId} = this.props.auth.user;
        const item = e.target.name;
        const isChecked = e.target.checked;

        this.props.clearNoticeList();

        this.setState(prevState => {
            let params = [];
            prevState.checkedItems.set(item, isChecked).forEach((value, key) => {
                if (value) {
                    params.push(+key.split('-').slice(-1)[0])
                }
            });

            let paramsUrl = `?groupsIds=${params.join('&groupsIds=')}`;
            this.props.getNoticeList(userAuthToken.token(), institutionId, paramsUrl);

            return {
                checkedItems: prevState.checkedItems.set(item, isChecked),
                paramsUrl: paramsUrl,
                skip: 0,
                take: 10,
            }
        });
    };

    onPaginated = (e) => {
        const {institutionId} = this.props.auth.user;

        this.fetchStories(institutionId, this.state.skip + 10);
    };

    fetchStories = (id, skip) => {
        let paramsUrl;

        if (this.state.paramsUrl.length === 0) {
            paramsUrl = `?skip=${skip}&take=10`;
        } else {
            paramsUrl = `${this.state.paramsUrl}&skip=${skip}&take=10`;
        }

        this.props.getNoticeList(userAuthToken.token(), id, paramsUrl)
            .then(() => {
                this.setState({
                    skip: skip
                });
            });
    };

    render() {
        const {notice, groups, auth} = this.props;

        return (
                    <div>
                        <div className="topic-items">
                            <div className="topic-items__top-part">
                                <HeaderNotice handleInputChange={this.handleInputChange}
                                              checkedItems={this.state.checkedItems}
                                              groups={groups.list}
                                              toggleNewNoticeItemModal={this.toggleNewNoticeItemModal}
                                              disabled={notice.loading}
                                              role={auth.user.role}
                                />
                                <div className="buttons-group">
                                    <NewNoticeItem show={this.state.showNewNoticeItemModal}
                                                  saved={this.saveNoticeHandler}
                                                  hideModal={this.toggleNewNoticeItemModal}
                                                  handleChange={this.handleChange}
                                                  group={this.state.activeItem}
                                                  groupList={groups.list}
                                                  errors={this.state.errors}
                                    />
                                </div>
                            </div>
                            <AdvancedList
                                role={auth.user.role}
                                userId={auth.user.id}
                                list={notice.list}
                                lengthArray={notice.list.length}
                                isLoading={notice.loading}
                                take={this.state.take}
                                skip={this.state.skip}
                                onPaginated={this.onPaginated}
                                cardClick={this.cardClick}
                            />
                        </div>
                    </div>
        );
    }
}

const AdvancedList = compose(
    withInfiniteScroll,
)(NoticeList);

const mapStateToProps = ({loader, auth, notice, groups}) => ({loader, auth, notice, groups});

const mapDispatchToProps = dispatch => ({
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
    getAllGroups : (token, id) => {dispatch(getAllGroups(token, id))},
    addNoticeItem: (token, id, notice) => {return dispatch(actions.addNoticeItem(token, id, notice))},
    getNoticeList: (token, id, url, skip, take = 10) => {
        return dispatch(actions.getNoticeList(token, id, url, skip, take))
    },
    deleteNoticeItem: (token, id, notice) => {return dispatch(actions.deleteNoticeItem(token, id, notice))},
    clearNoticeList: () => {dispatch(actions.clearNoticeList())}
});

const connectedNoticeBoard = withRouter(connect(mapStateToProps,mapDispatchToProps)(withSure(NoticeBoard)));

export { connectedNoticeBoard as NoticeBoard };