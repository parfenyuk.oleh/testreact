import React, {Component, Fragment} from 'react';
import { history } from '../../../helpers/index';
import {hideLoader, showLoader} from '../../../store/actions/loader';
import * as actions from '../../../store/actions/notice.action';
import {withRouter} from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';
import {userAuthToken} from '../../../helpers/index';
import Loader from '../../../components/UI/Loader';
import moment from 'moment';
import * as routeConstants from '../../../constants/routes';
import EditForm from '../../../components/General/NoticeBoard/Modals/EditForm';
import withSure from '../../../hoc/withSure';
import validate from '../../../helpers/validator';
import {getAllGroups} from '../../../store/actions/groups.action';
import {compose} from 'recompose';
import {withInfiniteScroll} from '../../../hoc/withInfiniteScroll';
import {MainBlockNoticeDetail} from '../../../components/General/NoticeBoard/MainBlockNoticeDetail';

class NoticeDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newForumItem: {
                name: '',
                description: '',
            },
            message: '',
            messageEdit: '',
            showEditFormModal: false,
            loadingReplies: true,
            activeItem: 'none',
            errors: [],
            skip: 0,
            take: 10,
        };
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    toggleEditFormModal = (id, type) => {
        const {notice} = this.props;

        if(type === 'comment') {
            const currentEl = notice.repliesList.find(x => x.id === id);

            this.setState({
                replyId: id,
                messageEdit: currentEl.message,
                type: 'comment'
            })
        } else {
            this.setState({
                messageEdit: notice.item.description,
                type: 'forum',
                newForumItem: {
                    name: notice.item.name,
                    description: notice.item.description,
                    inactiveDate: notice.item.inactiveDate
                },
                activeItem: notice.item.groupId
            })
        }

        this.setState({
            showEditFormModal : !this.state.showEditFormModal,
            errors: [],
        })
    };

    saveComment = () => {
        const {message} = this.state;
        const {institutionId} = this.props.auth.user;
        let result = {};

        const noticeId = this.props.match.params.id;

        result['message'] = message;

        if(message.length !== 0) {
            this.props.addNoticeReplies(userAuthToken.token(), institutionId, noticeId, result)
                .then(() => {
                    this.props.clearNoticeList();
                    this.props.getNoticeReplies(userAuthToken.token(), institutionId, noticeId);
                });
            this.setState({ message : '' })
        }
    };

    editFormHandler = () => {
        const user = JSON.parse(localStorage.getItem('user'));
        const noticeId = this.props.match.params.id;
        const {messageEdit, replyId, newForumItem, activeItem} = this.state;
        const {institutionId} = this.props.auth.user;
        let result = {};
        const errors = [];

        const title = validate('title',newForumItem.name,{required: 1});
        const description = validate('description',messageEdit,{required: 1});
        const groupField = validate('Group',`${activeItem}`,{required: 1});

        if (description) errors.push(description);
        if(this.state.type === 'forum') if (title) errors.push(title);
        if(this.state.type === 'forum') if (groupField) errors.push(groupField);

        this.setState({errors: errors});

        if(messageEdit.length !== 0) {

            if (this.state.type === 'comment') {
                result['message'] = messageEdit;

                this.props.updateNoticeReplies(userAuthToken.token(), institutionId, noticeId, replyId, result)
                    .then(() => {
                        this.props.clearNoticeList();
                        this.props.getNoticeReplies(userAuthToken.token(), institutionId, noticeId);

                        this.setState({ showEditFormModal : !this.state.showEditFormModal });
                    })
            } else {
                result['name'] = newForumItem.name;
                result['description'] = messageEdit;
                result['groupId'] = activeItem;

                this.props.updateNotice(userAuthToken.token(), institutionId, noticeId, result)
                    .then(() => {
                        this.props.getNoticeDetail(userAuthToken.token(), institutionId, noticeId);

                        this.setState({ showEditFormModal : !this.state.showEditFormModal });
                    })
            }
        }
    };

    deleteForumItem = () => {
        const {institutionId} = this.props.auth.user;

        this.props.showSureModal('Are you sure you want to delete this comment?')
            .then(result => {
                const noticeId = this.props.match.params.id;

                this.props.deleteNoticeItem(userAuthToken.token(), institutionId, noticeId)
                    .then(() => {
                        history.push(`${routeConstants.NOTICE_BOARD}`);
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    removeComment = (replyId) => {
        this.props.showSureModal('Are you sure you want to delete this forum?')
            .then(() => {
                const noticeId = this.props.match.params.id;
                const {institutionId} = this.props.auth.user;

                this.props.deleteNoticeReplies(userAuthToken.token(), institutionId, noticeId, replyId)
                    .then(() => {
                        this.props.clearNoticeList();
                        this.props.getNoticeReplies(userAuthToken.token(), institutionId, noticeId);
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    componentDidMount() {
        const {institutionId} = this.props.auth.user;
        const noticeId = this.props.match.params.id;

        this.props.getNoticeDetail(userAuthToken.token(), institutionId, noticeId)
            .then(() => {
                this.setState({loadingReplies: false});
            });
        this.props.getAllGroups(userAuthToken.token(), institutionId);
        this.props.getNoticeReplies(userAuthToken.token(), institutionId, noticeId);
    }

    newForumItemFieldsUpdateHandler = (value, name) => {
        let {newForumItem} = this.state;
        newForumItem[name] = value;
        this.setState({newForumItem});
    };

    onPaginated = (e) => {
        const {institutionId} = this.props.auth.user;

        this.fetchStories(institutionId, this.state.skip + 10);
    };

    fetchStories = (id, skip) => {
        const noticeId = this.props.match.params.id;

        this.props.getNoticeReplies(userAuthToken.token(), id, noticeId, skip)
            .then(() => {
                this.setState({skip: skip});
            });
    };

    render() {
        const {auth, groups, notice} = this.props;
        const {message, loadingReplies} = this.state;
        let date;

        if (notice.item) {
            date = moment(notice.item.createdAt).format('DD-MM-YYYY');
        }

        return (
            <Fragment>
                {notice.item === null ? <Loader/> : (
                    <div className='detail-forum'>
                        <EditForm show={this.state.showEditFormModal}
                                  text={this.state.messageEdit}
                                  saved={this.editFormHandler}
                                  hideModal={this.toggleEditFormModal}
                                  handleChange={this.handleChange}
                                  type={this.state.type}
                                  fieldUpdated={this.newForumItemFieldsUpdateHandler}
                                  subjectList={groups.list}
                                  subject={this.state.activeItem}
                                  newForumItem={this.state.newForumItem}
                                  errors={this.state.errors}
                        />
                        <AdvancedList
                            list={notice.repliesList}
                            lengthArray={notice.repliesList.length}
                            isLoading={notice.loading}
                            take={this.state.take}
                            skip={this.state.skip}
                            onPaginated={this.onPaginated}
                            notice={notice}
                            auth={auth}
                            toggleEditFormModal={this.toggleEditFormModal}
                            deleteForumItem={this.deleteForumItem}
                            saveComment={this.saveComment}
                            message={message}
                            handleChange={this.handleChange}
                            loadingReplie={loadingReplies}
                            removeComment={this.removeComment}
                            date={date}
                        />
                    </div>
                )}
            </Fragment>
        );
    }
}

const AdvancedList = compose(
    withInfiniteScroll,
)(MainBlockNoticeDetail);

const mapStateToProps = ({loader, auth, user, groups, notice}) => ({loader, auth, user, groups, notice});

const mapDispatchToProps = dispatch => ({
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
    getNoticeDetail: (token, id, noticeId) => {return dispatch(actions.getNoticeDetail(token, id, noticeId))},
    getNoticeReplies: (token, id, noticeId, skip, take) => {return dispatch(actions.getNoticeReplies(token, id, noticeId, skip, take = 10))},
    getAllGroups : (token, id) => {dispatch(getAllGroups(token, id))},
    deleteNoticeItem: (token, id, notice) => {return dispatch(actions.deleteNoticeItem(token, id, notice))},
    addNoticeReplies: (token, id, noticeId, message) => {return dispatch(actions.addNoticeReplies(token, id, noticeId, message))},
    deleteNoticeReplies: (token, id, forumId, replyId) => {return dispatch(actions.deleteNoticeReplies(token, id, forumId, replyId))},
    updateNoticeReplies: (token, id, noticeId, replyId, message) => {return dispatch(
        actions.updateNoticeReplies(token, id, noticeId, replyId, message))
    },
    updateNotice: (token, id, noticeId, message) => {return dispatch(actions.updateNotice(token, id, noticeId, message))},
    clearNoticeList: () => {dispatch(actions.clearNoticeList())}
});

const connectedNoticeDetail = withRouter(connect(mapStateToProps,mapDispatchToProps)(withSure(NoticeDetail)));

export { connectedNoticeDetail as NoticeDetail };