import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Header, MainSidebar, PinupCards, NewPinupItemModal} from '../../../components/General/PinUp';
import * as actions from '../../../store/actions/pinup.action';
import {getAllGroups} from '../../../store/actions/groups.action';
import withSure from '../../../hoc/withSure';
import {withRouter} from 'react-router-dom';
import {userAuthToken} from '../../../helpers/index';
import validate from '../../../helpers/validator';

class PinUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      description: '',
      showNewPinupItemModal: false,
      errors: [],
      search: '',
      skip: 0,
      activeItem: 1,
      groupId: ''
    };

    this.activeItemList = [
      {
        id: 1,
        name: 'public',
      },
      {
        id: 3,
        name: 'private',
      },
      {
        id: 2,
        name: 'groups',
      },
    ];

  }

  componentDidMount() {
    const {institutionId} = this.props.auth.user;
    const {activeItem} = this.state;

    this.props.clearPinupList();
    this.props.getPinupList(userAuthToken.token(), institutionId, activeItem);
    this.props.getAllGroups(userAuthToken.token(), institutionId)
      .then((item => {
        if (item && item.length) {
          const groupId = item[0].id;

          this.setState({
            groupId: groupId,
          });
        }
      }));
  };

  handleChange = event => this.setState({[event.target.name]: event.target.value});

  toggleNewPinupItemModal = () => this.setState({
    showNewPinupItemModal: !this.state.showNewPinupItemModal,
    currentPinup: null,
    name: '',
    description: '',
    errors: [],
  });

  savePinupHandler = () => {
    const {name, description, activeItem, showNewPinupItemModal, currentPinup, groupId} = this.state;
    const {institutionId} = this.props.auth.user;
    let result = {};
    const errors = [];

    const title = validate('title', name, {required: 1});
    const descriptionPinup = validate('description', description, {required: 1});
    if(activeItem === 2) {
      const isGroupId = validate('Group Id', groupId.toString(), {required: 1});
      if (isGroupId) errors.push(isGroupId);
    }

    if (title) errors.push(title);
    if (descriptionPinup) errors.push(descriptionPinup);

    this.setState({errors: errors});

    if (errors.length === 0) {
      result['name'] = name;
      result['description'] = description;
      result['privacyType'] = activeItem;

      activeItem === 2 ? result['groupId'] = groupId : result['groupId'] = null;

      if (currentPinup) {
        this.props.updatePinupItem(userAuthToken.token(), institutionId, currentPinup.id, result)
          .then(() => {
            this.props.clearPinupList();

            this.props.getPinupList(userAuthToken.token(), institutionId, activeItem);
            this.setState({
              showNewPinupItemModal: !showNewPinupItemModal,
              name: '',
              description: '',
              skip: 0,
            });
          })
      } else {
        this.props.addPinupItem(userAuthToken.token(), institutionId, result)
          .then(() => {
            this.props.clearPinupList();

            this.props.getPinupList(userAuthToken.token(), institutionId, activeItem);
            this.setState({
              showNewPinupItemModal: !showNewPinupItemModal,
              name: '',
              description: '',
              skip: 0,
            });
          })
      }
    }
  };

  onPaginated = () => {
    const {institutionId} = this.props.auth.user;
    const {skip} = this.state;

    this.fetchStories(institutionId, skip + 10);
  };

  fetchStories = (institutionId, skip) => {
    const {token} = this.props.auth.user;
    const {pinup} = this.props;

    if (pinup.list.length !== 0) {
      this.props.getPinupList(userAuthToken.token(), institutionId, this.state.activeItem, skip)
        .then(() => {
          this.setState({
            skip: skip,
          });
        });
    }
  };

  changeActiveGroup = (id) => {
    const {institutionId} = this.props.auth.user;

    if (id !== this.state.activeItem) {

      this.props.clearPinupList();
      this.props.getPinupList(userAuthToken.token(), institutionId, id);

      this.setState({
        activeItem: id,
        skip: 0,
      });
    }
  };

  cardClick = (e, id) => {
    if (e.target.className === 'icon delete') {
      this.deletePinupItem(id);
    } else if (e.target.className === 'icon edit') {
      this.updatePinupItem(id);
    }
  };

  deletePinupItem = (pinupId) => {
    const {institutionId} = this.props.auth.user;

    this.props.showSureModal('Are you sure you want to delete this comment?')
      .then(() => {
        this.props.deletePinupItem(userAuthToken.token(), institutionId, pinupId)
          .then(() => {
            this.props.clearPinupList();
            this.props.getPinupList(userAuthToken.token(), institutionId, this.state.activeItem);
          })
      })
      .catch(error => {
        console.log(error);
      })
  };

  updatePinupItem = (pinupId) => {
    const {pinup} = this.props;

    const currentPinup = pinup.list.find(x => x.id === pinupId);

    this.setState({
      currentPinup: currentPinup,
      showNewPinupItemModal: !this.state.showNewPinupItemModal,
      name: currentPinup.name,
      activeItem: currentPinup.privacyType,
      description: currentPinup.description,
      groupId: currentPinup.groupId,
    })
  };

  render() {
    const {auth, pinup, groups} = this.props;
    const {activeItem, showNewPinupItemModal, currentPinup, name, description, groupId} = this.state;

    return (
      <div className="admin-main">
        <Header/>
        <MainSidebar pinup={pinup.pinups}
                     changeActiveGroup={this.changeActiveGroup}
                     activeGroup={activeItem}
                     auth={auth}
        />

        <NewPinupItemModal show={showNewPinupItemModal}
                           saved={this.savePinupHandler}
                           hideModal={this.toggleNewPinupItemModal}
                           handleChange={this.handleChange}
                           name={name}
                           description={description}
                           activeItem={activeItem}
                           privacyTypeList={this.activeItemList}
                           errors={this.state.errors}
                           currentPinup={currentPinup}
                           groupList={groups.list}
                           groupId={groupId}
        />

        <PinupCards pinup={pinup.list}
                    showNewPinupItem={this.toggleNewPinupItemModal}
                    isLoading={pinup.loading}
                    auth={auth}
                    onPaginated={this.onPaginated}
                    cardClick={this.cardClick}
        />
      </div>
    );
  }
}

const mapStateToProps = ({auth, admin, pinup, groups}) => ({auth, admin, pinup, groups});

const mapDispatchToProps = dispatch => ({
  getAllGroups: (token, id) => {return dispatch(getAllGroups(token, id))},
  getPinupList: (token, id, activeItem, skip) => {return dispatch(actions.getPinupList(token, id, activeItem, skip))},
  addPinupItem: (token, id, result) => {return dispatch(actions.addPinupItem(token, id, result))},
  deletePinupItem: (token, id, pinupId) => {return dispatch(actions.deletePinupItem(token, id, pinupId))},
  updatePinupItem: (token, id, pinupId, result) => {return dispatch(actions.updatePinupItem(token, id, pinupId, result))},
  clearPinupList: () => {return dispatch(actions.clearPinupList())},
});

const connectedPinUp = withRouter(connect(mapStateToProps, mapDispatchToProps)(withSure(PinUp)));

export {connectedPinUp as PinUp};