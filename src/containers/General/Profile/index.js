import React, {Component} from 'react';
import ImageLoader from '../../../components/UI/imageLoader';
import Input from '../../../components/UI/Input';
import {connect} from 'react-redux';
import Textarea from '../../../components/UI/Textarea';
import imageLoadApi from '../../../helpers/imageLoadApi';
import * as actions from '../../../store/actions/admin.action';
import {getDataByToken} from '../../../store/actions';
import icons from '../../../helpers/iconsLoader';
import validate from '../../../helpers/validator';

class Profile extends Component {
  state = {
    email: {
      value: '',
      error: false,
    },
    avatar: {
      value: '',
      error: false,
    },
    firstName: {
      value: '',
      error: false,
    },
    lastName: {
      value: '',
      error: false,
    },
    location: {
      value: '',
      error: false,
    },
    website: {
      value: '',
      error: false,
    },
    bio: {
      value: '',
      error: false,
    },
    newPassword: {
      value: '',
      error: false,
    },
    oldPassword: {
      value: '',
      error: false,
    },
    confirm: {
      value: '',
      error: false,
    },
    errors: [],
  };

  fieldUpdated = (value, field) => {
    let state = {...this.state};
    state[field].value = value;
    this.setState({...state});
  };

  saveHandler = () => {
    if (!this.passwordsValidate()) return;

    const {email, firstName, lastName, location, bio, website} = this.state;

    const errors = [];

    email.error = validate('email', email.value, {isEmail: true});
    firstName.error = validate('First name', firstName.value, {isNotEmpty: true});
    lastName.error = validate('Last name', lastName.value, {isNotEmpty: true});
    location.error = validate('Location', location.value, {isNotEmpty: true});
    bio.error = validate('Bio', bio.value, {isNotEmpty: true});
    website.error = validate('Website', website.value, {isNotEmpty: true});

    if (email.error) {errors.push(email.error);}
    if (firstName.error) {errors.push(firstName.error);}
    if (lastName.error) {errors.push(lastName.error);}
    if (location.error) {errors.push(location.error);}
    if (bio.error) {errors.push(bio.error);}
    if (website.error) {errors.push(website.error);}

    if (!errors.length) {
      if (typeof this.state.avatar.value === 'object' && this.state.avatar.value !== null) {
        imageLoadApi(this.state.avatar.value, this.props.user.token)
          .then(result => {
            this.setState({
                avatar: {value: result.data.result},
              },
              this.sendContent);
          })
          .catch(err => {
            console.log(err);
          })
      }
      else {
        this.sendContent();
      }
    }

    this.setState({
      email,
      errors,
      firstName,
      lastName,
    });
  };

  passwordsValidate = () => {
    let {newPassword, oldPassword, confirm} = this.state;
    let errors = [];
    confirm.error = false;
    oldPassword.error = false;
    if (newPassword.value) {
      if (newPassword.value !== confirm.value) {
        confirm.error = true;
        errors.push('Password not confirm');
      }
      if (!this.state.oldPassword.value) {
        oldPassword.error = true;
        errors.push('Old password not correct');
      }
      this.setState({confirm, oldPassword, errors});
      return errors.length === 0;
    }
    return true;
  };

  sendContent = () => {
    const toSend = Object.entries(this.state).reduce((acc, [name, params]) => {
      acc[name] = params.value;
      return acc;
    }, {});
    toSend.id = this.props.user.id;

    this.props.updateUser(toSend, this.props.user.institutionId, this.props.user.token)
      .then(response => {
        if (response.data.ok) {
          this.props.getDataByToken(this.props.user.refreshToken, this.props.user.token);
        } else {
          this.setState({
            errors: [response.data.message],
            oldPassword: {
              value: '',
              error: true,
            },
          })
        }
      })
  };

  componentDidMount() {
    this.setState({
      email: {
        value: this.props.user.email || '',
        error: false,
      },
      firstName: {
        value: this.props.user.firstName || '',
        error: false,
      },
      lastName: {
        value: this.props.user.lastName || '',
        error: false,
      },
      avatar: {
        value: this.props.user.avatar || '',
        error: false,
      },
      location: {
        value: this.props.user.location || '',
        error: false,
      },
      website: {
        value: this.props.user.website || '',
        error: false,
      },
      bio: {
        value: this.props.user.bio || '',
        error: false,
      },
    })
  }

  imageValidationHandler = (err) => {
    this.setState({
      errors: [err],
    })
  };


  render() {
    const {user} = this.props;
    const {firstName, lastName, avatar, email, location, bio, website, newPassword, oldPassword, confirm, errors}
      = this.state;

    return (
      <div>
        <div className='componentHeader'>
          <h1 className='page-title'>Profile</h1>
        </div>

        <div className='profile'>
          <div className="profile-block">
            {errors.length > 0 && <div className="auth-errors">
              <ul>
                {errors.map(item => <li key={item}><img src={icons.iconAttention} alt=""/> {item} </li>)}
              </ul>
            </div>
            }
          </div>
          <div className='profile-block'>
            <div className='profile-title'>Photo</div>
            <ImageLoader lg
                         trigger={<button className='big-primary '>upload new</button>}
                         remover={<button className='button-default small'>delete</button>}
                         placeholder={{text: `${user.firstName[0]}${user.lastName[0]}`}}
                         image={avatar}
                         changed={(value) => { this.fieldUpdated(value, 'avatar') }}
                         withValidation
                         onValidation={this.imageValidationHandler}
            />
          </div>

          <div className='profile-block'>
            <div className='profile-title'>Profile details</div>
            <Input placeholder='First name'
                   value={firstName.value}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'firstName') }}
                   className={firstName.error ? 'error' : ''}
            />
            <Input placeholder='Last name'
                   value={lastName.value}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'lastName') }}
                   className={lastName.error ? 'error' : ''}
            />
            <Input placeholder='Email'
                   value={email.value}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'email') }}
                   className={email.error ? 'error' : ''}
            />
            <Input placeholder='Location'
                   value={location.value}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'location') }}
                   className={location.error ? 'error' : ''}
            />
            <Input placeholder='Website'
                   value={website.value}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'website') }}
                   className={website.error ? 'error' : ''}
            />
            <Textarea placeholder='Bio'
                      text={bio.value}
                      changed={(e) => { this.fieldUpdated(e.target.value, 'bio') }}
                      error={bio.error}
            />
          </div>

          <div className='profile-block'>
            <div className='profile-title'>Change password</div>
            <Input placeholder='Old password' type={'password'}
                   value={oldPassword.value}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'oldPassword') }}
                   className={oldPassword.error ? 'error' : ''}
            />
            <Input placeholder='New password' type={'password'}
                   value={newPassword.value}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'newPassword') }}
                   className={newPassword.error ? 'error' : ''}
            />
            <Input placeholder='Confirm new password' type={'password'}
                   value={confirm.value}
                   changed={(e) => { this.fieldUpdated(e.target.value, 'confirm') }}
                   className={confirm.error ? 'error' : ''}
            />
          </div>
          <div className='profile-block' style={{width: '200px'}}>
            <button className='big-primary large' onClick={this.saveHandler}>Save changes</button>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({auth: {user}}) => ({user});

const mapDispatchToProps = dispatch => ({
  updateUser: (member, universityId, token) => {return dispatch(actions.updateMember(member, universityId, token))},
  getDataByToken: (refreshToken, token) => {dispatch(getDataByToken(refreshToken, token))},
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);