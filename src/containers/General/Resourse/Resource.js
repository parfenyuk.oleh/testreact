import React, {Component, Fragment} from 'react';
import * as actions from '../../../store/actions/resources.action';
import {withRouter} from 'react-router-dom';
import connect from 'react-redux/es/connect/connect';
import withSure from '../../../hoc/withSure';
import {userAuthToken} from '../../../helpers';
import { NewResourcesGroups, ResourceMainBlock, HeaderResources, ResourcesSidebar, NewResource, ResourceDetail } from '../../../components/General/Resourse';
import validate from '../../../helpers/validator';
import moment from 'moment';
import {documentLoadApi, cancelRequest} from '../../../store/actions/documentLoadApi.action';
import {getRandomOnlineColor} from '../../../helpers/randomColor';
import LoaderDocument from '../../../components/UI/LoaderDocument';
import axios from 'axios';
import ROLES from '../../../constants/roles';

class Resource extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showNewResourcesGroupsModal: false,
            showNewResourceModal: false,
            errors: [],
            startDate: '',
            endDate: '',
            startDateResource: '',
            endDateResource: '',
            description: '',
            heading: '',
            name: '',
            activeId: 0,
            skipAmount: 10,
            skip: 10,
            document: null,
            open: false,
            checkedItems: new Map(),
            paramsUrl: '',
            sortingUrl: '',
            backgroundColor: getRandomOnlineColor(),
            finalUrlObj: [
                { take: 10} ,
                { skip: 0 },
                { sort: null },
                { resourcesTypes: [] },
            ],
            cancelToken: [],
            cancelCounter: -1,
        };

        this.resourceTypes = [
            {
                id: 1,
                name: 'Photo'
            },
            {
                id: 2,
                name: 'Doc'
            },
            {
                id: 3,
                name: 'Audio and Video'
            },
            {
                id: 4,
                name: 'Archive'
            }
            ];
        this.sortTypes = [
            {name: 'Sort by date', sortTypes: ''},
            {name: 'Sort by name: A-Z', sortTypes: 1},
            {name: 'Sort by name: Z-A', sortTypes: 2},
        ]
    }

    setActiveElement = id => {
        this.setState({ activeId: id });
        const {institutionId} = this.props.auth.user;
        let url = '';
        this.props.getResources(userAuthToken.token(), institutionId, id, url)
            .then(resource => {
                this.setState({
                    activeResource: resource[0]
                });
            });
    };

    toggleResourcesGroupModal = () => this.setState({
        showNewResourcesGroupsModal: !this.state.showNewResourcesGroupsModal,
        currentResourceGroup: null,
        startDate: '',
        endDate: '',
        name: ''
    });

    toggleResourcesModal = () => this.setState({
        showNewResourceModal: !this.state.showNewResourceModal,
        currentResourceGroup: null,
        startDate: '',
        endDate: '',
        name: '',
        description: '',
        heading: '',
        currentResource: null,
    });

    toggleDropdown = () => this.setState({ open: !this.state.open });

    saveResource = () => {
        const {startDateResource, endDateResource, description , heading, activeId, document} = this.state;
        const {institutionId} = this.props.auth.user;

        let result = {};
        const errors = [];

        const resources = validate('Resources', this.props.resources.groupList.length, {isNotEmptyDate: true});
        const headingValidate = validate('Heading', heading, {required: 1});
        const descriptionValidate = validate('Description', description, {required: 1});
        const endDateValidate = validate('End date', endDateResource, {notLessDate: startDateResource});
        const documentValidate = validate('File', document, {isNotEmptyDate: true});

        if (resources) errors.push(resources);
        if (headingValidate) errors.push(headingValidate);
        if (descriptionValidate) errors.push(descriptionValidate);
        if (endDateValidate) errors.push(endDateValidate);
        if(!this.state.currentResource){
            if (documentValidate) errors.push(documentValidate);
        }


        this.setState({errors: errors});

        if (!errors.length) {
            result['resourceHeading'] = heading;
            result['description'] = description;
            result['startDate'] = moment(startDateResource).format('l');
            result['endDate'] = moment(endDateResource).format('l');

            this.onRequestDocument(institutionId, activeId, result);
        }
    };

    onRequestDocument = (institutionId, activeId, result) => {
        const isCancel = false;

        const CancelToken = axios.CancelToken;
        const source = CancelToken.source();
        let cancelTokenArray = this.state.cancelToken;
        cancelTokenArray.push(source);

        if (this.state.currentResource) {
            result['url'] = this.state.currentResource.url;
            this.props.documentLoadApi(this.state.document, userAuthToken.token(), source)
                .then(resultReq => {
                    if (resultReq !== 'error') {
                        result['url'] = resultReq.url;
                    }
                    this.props.updateResource(
                        userAuthToken.token(),
                        institutionId,
                        this.state.activeId,
                        this.state.resourceId,
                        result
                    )
                        .then(() => {
                            this.props.getResources(userAuthToken.token(), institutionId, this.state.activeId, this.state.paramsUrl)
                                .then(resource => {
                                    this.setState({
                                        activeResource: resource[0]
                                    });
                                });

                            this.setState({
                                showNewResourceModal: false,
                            });
                        });
                })
        } else {
        this.props.documentLoadApi(this.state.document, userAuthToken.token(), source)
          .then(resultReq => {
              if (resultReq !== 'error') {
                  result['resourceType'] = resultReq.resourceType;
                  result['url'] = resultReq.url;


                  this.props.addResource(userAuthToken.token(), institutionId, activeId, result)
                    .then(() => {
                        this.props.getResources(userAuthToken.token(), institutionId, this.state.activeId, this.state.paramsUrl)
                          .then(resource => {
                              this.setState({
                                  activeResource: resource[0]
                              });
                          });
                        this.setState({
                            showNewResourceModal: false,
                            document: null,
                        });
                    });

              }
          });
    }
        this.setState({
            cancelCounter: this.state.cancelCounter + 1,
            cancelToken: cancelTokenArray
        });
    };

    saveResourcesGroups = () => {
        const {institutionId} = this.props.auth.user;
        const {name, startDate, endDate} = this.state;
        let result = {};
        const errors = [];

        const nameValidate = validate('title', name, {required: 1});
        const startDateValidate = validate('Start date', startDate, {isNotEmptyDate : true});
        const endDateValidate = validate('End date', endDate, {isNotEmptyDate : true, notLessDate: startDate});

        if (nameValidate) errors.push(nameValidate);
        if (startDateValidate) errors.push(startDateValidate);
        if (endDateValidate) errors.push(endDateValidate);

        this.setState({errors: errors});

        if (errors.length === 0) {

            result['name'] = name;
            result['startDate'] = moment(startDate).format('l');
            result['endDate'] = moment(endDate).format('l');

            if (!this.state.currentResourceGroup) {
                this.props.addResourcesGroupItem(userAuthToken.token(), institutionId, result)
                    .then(result =>
                    {
                        this.props.getResourcesGroupList(userAuthToken.token(), institutionId);

                        if(!result) {
                            this.setState({
                                name: '',
                                startDate: '',
                                endDate: '',
                                showNewResourcesGroupsModal : false,
                            });
                        }
                    })
            } else {
                this.props.updateResourcesGroupItem(userAuthToken.token(), institutionId, this.state.currentResourceGroup.id, result)
                    .then(result =>
                    {
                        this.props.getResourcesGroupList(userAuthToken.token(), institutionId);

                        if(result === undefined) {
                            this.setState({
                                name: '',
                                startDate: '',
                                endDate: '',
                                showNewResourcesGroupsModal : false,
                            });
                        }
                    })
            }
        }
        this.setState({showNewResourcesGroupsModal: false});
    };

    handleChange = (event, type) => {
        let {startDateResource, endDateResource} = this.state;
        if(!type){
          if(event.target.name === 'activeId'){
            const currResourse = this.props.resources.groupList.find(item => item.id === event.target.value);
            startDateResource = moment(currResourse.startDate);
            endDateResource = moment(currResourse.endDate);
          }

          this.setState({
            [event.target.name]: event.target.value,
            startDateResource,
            endDateResource,
          })
        } else {
          this.setState({ [type]: moment(event) });
        }

    };

    componentDidMount(){
        const {institutionId} = this.props.auth.user;
        let { finalUrlObj } = this.state;

        this.reduceString(finalUrlObj);

        this.props.getResourcesGroupList(userAuthToken.token(), institutionId)
            .then((item) => {
                if (item && item.length) {
                    const url = '';
                    this.props.getResources(userAuthToken.token(), institutionId, item[0].id, url)
                        .then(resource => {
                            this.setState({
                                activeResource: resource[0]
                            });
                        });

                    this.setState({
                        activeId: item[0].id || null,
                        startDateResource: moment(item[0].startDate),
                        endDateResource: moment(item[0].endDate),
                    });
                }
            })
    };

    reduceString = (obj) => {
        let paramsArr = obj.reduce(function(acc, value) {
            const key = Object.keys( value )[0];
            const valueObj = Object.values(value)[0];
            let str;

            if (typeof valueObj === 'number') {
                str = `${key}=${valueObj}`;
                acc.push(str);
            }

            if (Array.isArray(valueObj) && valueObj.length && valueObj[0].length) {
                str = `resourcesTypes=${valueObj[0].join('&resourcesTypes=')}`;
                acc.push(str);
            }

            return acc;
        }, []);

        return `?${paramsArr.join('&')}`;
    };

    onPaginated = () => {
        const { institutionId } = this.props.auth.user;
        const { resources } = this.props;
        const { skip, skipAmount, activeId } = this.state;
        const pagination = true;
        let { finalUrlObj } = this.state;
        let url;

        if(resources.list.length) {

            finalUrlObj = [
                { take: finalUrlObj[0].take} ,
                { skip: skip },
                { sort: finalUrlObj[2].sort },
                { resourcesTypes: finalUrlObj[3].resourcesTypes },
            ];

            url = this.reduceString(finalUrlObj);

            this.props.getResources(userAuthToken.token(), institutionId, activeId, url, pagination)
                .then(resource => {
                    this.setState({
                        skip: skip + skipAmount,
                        finalUrlObj
                    });
                });
        }
    };

    fieldUpdated = (e, field) => {
        this.setState({
            [field]: e
        });
    };

    removeResourceGroup = groupId => {
        const {institutionId} = this.props.auth.user;

        this.props.showSureModal('Are you sure you want to delete this group?')
            .then(() => {
                this.props.deleteResourcesGroupItem(userAuthToken.token(), institutionId, groupId)
                    .then(() => {

                        this.props.getResourcesGroupList(userAuthToken.token(), institutionId)
                            .then((item => {
                                if (item.length) {
                                    this.setState({activeId: item[0].id})
                                }
                            }));
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    removeResource = resourceId => {
        const {institutionId} = this.props.auth.user;

        this.props.showSureModal('Are you sure you want to delete this resource?')
            .then(() => {
                this.props.deleteResource(userAuthToken.token(), institutionId, this.state.activeId, resourceId)
                    .then(() => {
                        this.props.getResources(userAuthToken.token(), institutionId, this.state.activeId, this.state.paramsUrl);
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    editResourceGroup = groupId => {
        const {resources} = this.props;

        const currentResourceGroup = resources.groupList.find(x => x.id === groupId);

        this.setState({
            currentResourceGroup: currentResourceGroup,
            showNewResourcesGroupsModal: !this.state.showNewResourcesGroupsModal,
            name: currentResourceGroup.name,
            startDate: moment(currentResourceGroup.startDate),
            endDate: moment(currentResourceGroup.endDate),
        })
    };

    updateResource = (resourceId) => {
        const {resources} = this.props;

        const currentResource = resources.list.find(x => x.id === resourceId);

        this.setState({
            currentResource: currentResource,
            showNewResourceModal: true,
            name: currentResource.name,
            description: currentResource.description,
            heading: currentResource.resourceHeading,
            startDateResource: moment(currentResource.startDate),
            endDateResource: moment(currentResource.endDate),
            resourceId: resourceId,
        })
    };

    handleInputChange = (e) => {
        const {institutionId} = this.props.auth.user;
        let {finalUrlObj} = this.state;
        const item = e.target.name;
        const isChecked = e.target.checked;
        let url;

        this.setState(prevState => {
            let params = [];
            prevState.checkedItems.set(item, isChecked).forEach((value, key) => {
                if (value) {
                    params.push(+key.split('-').slice(-1)[0])
                }
            });

            finalUrlObj = [
                { take: finalUrlObj[0].take} ,
                { skip: 0 },
                { sort: finalUrlObj[2].sort },
                { resourcesTypes: [params] },
            ];

            url = this.reduceString(finalUrlObj);

            this.props.getResources(userAuthToken.token(), institutionId, this.state.activeId, url)
                .then(()=> {
                    this.setState({
                        skip: 10,
                        finalUrlObj
                    });
                });

            return {
                checkedItems: prevState.checkedItems.set(item, isChecked),
            }
        });
    };

    changeSortingHandler = option => {
        const {institutionId} = this.props.auth.user;
        const {paramsUrl} = this.state;
        let {finalUrlObj} = this.state;
        let url;

        finalUrlObj = [
            { take: finalUrlObj[0].take} ,
            { skip: 0 },
            { sort: option.sortTypes },
            { resourcesTypes: finalUrlObj[3].resourcesTypes },
        ];

        url = this.reduceString(finalUrlObj);

        this.props.getResources(userAuthToken.token(), institutionId, this.state.activeId, url)
            .then(()=> {
                this.setState({
                    skip: 10,
                    finalUrlObj
                });
            });

        this.setState({
            finalUrlObj
        });
    };

    changeActiveResources = groupId => {
        const {resources} = this.props;

        const activeResource = resources.list.find(item => item.id === groupId);

        this.setState({
            activeResource: activeResource,
        })
    };

    closeRequest = () => {
        const {cancelCounter, cancelToken} = this.state;

        console.log(cancelToken[cancelCounter], cancelCounter);

        cancelToken[cancelCounter].cancel('Operation canceled by the user.');
    };

    render() {
        const {resources, auth} = this.props;
        const {activeId, document, open, currentResource, description, activeResource, backgroundColor, skipAmount, cancelToken} = this.state;
        return (
            <Fragment>
                <div className={resources.list.length ? 'resources active' : 'resources'}>
                    <NewResourcesGroups show={this.state.showNewResourcesGroupsModal}
                                        saved={this.saveResourcesGroups}
                                        hideModal={this.toggleResourcesGroupModal}
                                        handleChange={this.handleChange}
                                        errors={this.state.errors}
                                        name={this.state.name}
                                        startDate={this.state.startDate}
                                        endDate={this.state.endDate}
                    />

                    <NewResource show={this.state.showNewResourceModal}
                                 saved={this.saveResource}
                                 hideModal={this.toggleResourcesModal}
                                 handleChange={this.handleChange}
                                 errors={this.state.errors}
                                 heading={this.state.heading}
                                 startDate={this.state.startDateResource}
                                 endDate={this.state.endDateResource}
                                 activeItem={activeId}
                                 groupList={resources.groupList}
                                 fieldUpdated={this.fieldUpdated}
                                 document={document}
                                 currentResource={currentResource}
                                 description={description}
                    />

                    <HeaderResources
                        toggleResourcesGroupModal={
                          auth.user.role === ROLES.STUDENT ? null : this.toggleResourcesGroupModal
                        }
                        resourceTypes={this.resourceTypes}
                        handleInputChange={this.handleInputChange}
                        checkedItems={this.state.checkedItems}
                        sortTypes={this.sortTypes}
                        changeSortingHandler={this.changeSortingHandler}
                    />

                    <div className='main-part'>
                        <ResourcesSidebar
                            resources={resources.groupList}
                            activeId={activeId}
                            editResourceGroup={this.editResourceGroup}
                            removeResourceGroup={
                              auth.user.role === ROLES.STUDENT ? null : this.removeResourceGroup
                            }
                            setActiveElement={this.setActiveElement}
                        />

                        <ResourceMainBlock
                                    resources={resources.list}
                                    showResourcesModal={this.toggleResourcesModal}
                                    showDropdown={open}
                                    toggleDropdown={this.toggleDropdown}
                                    removeResource={
                                      auth.user.role === ROLES.STUDENT ? null : this.removeResource
                                    }
                                    updateResource={this.updateResource}
                                    isLoading={resources.loading}
                                    auth={auth}
                                    skip={skipAmount}
                                    onPaginated={this.onPaginated}
                                    changeActiveResources={this.changeActiveResources}
                        />
                    </div>
                </div>

                {resources.list.length !== 0 && <ResourceDetail activeResource={activeResource} backgroundColor={backgroundColor}/>}
                {this.props.loader.showDocumentLoader ? <LoaderDocument closeRequest={this.closeRequest}/> : null}
            </Fragment>
        );
    }
}

const mapStateToProps = ({auth, admin, resources, loader}) => ({auth, admin, resources, loader});

const mapDispatchToProps = dispatch => ({
    getResourcesGroupList : (token, id, activeItem, skip) => {
        return dispatch(actions.getResourcesGroupList(token, id, activeItem, skip))
    },
    addResourcesGroupItem : (token, id, result) => {return dispatch(actions.addResourcesGroupItem(token, id, result))},
    deleteResourcesGroupItem : (token, id, groupId) => {return dispatch(actions.deleteResourcesGroupItem(token, id, groupId))},
    updateResourcesGroupItem : (token, id, groupId, result) => {
        return dispatch(actions.updateResourcesGroupItem(token, id, groupId, result))
    },
    addResource : (token, id, resourcesGroupId, result) => {
        return dispatch(actions.addResource(token, id, resourcesGroupId, result))
    },
    getResources : (token, id, activeItem, url, pagination) => {
        return dispatch(actions.getResources(token, id, activeItem, url, pagination))
    },
    clearResourcesList : () => {return dispatch(actions.clearResourcesList())},
    updateResource : (token, id, resourcesGroupId, resourceId, result) => {
        return dispatch(actions.updateResource(token, id, resourcesGroupId, resourceId, result))
    },
    deleteResource : (token, id, resourcesGroupId, resourceId) => {
        return dispatch(actions.deleteResource(token, id, resourcesGroupId, resourceId))
    },
    documentLoadApi : (document, token, source) => {
        return dispatch(documentLoadApi(document, token, source))
    },
    cancelRequest: () => dispatch(cancelRequest()),
});


const connectedResource = withRouter(connect(mapStateToProps,mapDispatchToProps)(withSure(Resource)));

export { connectedResource as Resource };