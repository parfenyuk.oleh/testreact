import React, {Component, Fragment} from 'react';
import { history } from '../../../helpers/index';
import {hideLoader, showLoader} from "../../../store/actions/loader";
import {Link} from "react-router-dom";
import {
    getForumPostDetail,
    getForumReplies,
    addForumReplies,
    updateForumReplies,
    updateForum,
    deleteForumReplies,
    deleteForumItem,
    getAllSubjects,
} from "../../../store/actions/user.action";
import {withRouter} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import {userAuthToken} from "../../../helpers/index";
import Loader from "../../../components/UI/Loader";
import Table from "../../../components/Admin/Forum/Table";
import CommentBlock from "../../../components/Admin/Forum/CommentBlock";
import Cell from "../../../components/UI/Cell";
import Icon from "../../../components/UI/Elements/Icon";
import Reply from "../../../components/UI/Layout/Reply";
import moment from "moment";
import * as routeConstants from "../../../constants/routes";
import EditForm from "../../../components/Admin/Forum/Modals/EditForm";
import withSure from "../../../hoc/withSure";
import validate from "../../../helpers/validator";

class Submission extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newForumItem: {
                name: '',
                description: '',
                inactiveDate: ''
            },
            message: '',
            messageEdit: '',
            showEditFormModal: false,
            loadingReplies: true,
            subject: '',
            errors: [],
        };
    }

    handleChange = event => {
        this.setState({ [event.target.name]: event.target.value });
    };

    toggleEditFormModal = (id, type) => {
        const {user} = this.props;

        if(type === 'comment') {
            const currentEl = user.forumReplies.find(x => x.id === id);

            this.setState({
                replyId: id,
                messageEdit: currentEl.message,
                type: 'comment'
            })
        } else {

            this.setState({
                messageEdit: user.item.description,
                type: 'forum',
                newForumItem: {
                    name: user.item.name,
                    description: user.item.description,
                    inactiveDate: user.item.inactiveDate
                },
                subject: user.item.subjectId
            })
        }

        this.setState({
            showEditFormModal : !this.state.showEditFormModal,
            errors: [],
        })
    };

    saveComment = () => {
        const {message} = this.state;
        let result = {};

        const user = JSON.parse(localStorage.getItem('user'));
        const forumId = this.props.match.params.id;

        result['message'] = message;
        if(message.length !== 0) {
            this.props.addForumReplies(userAuthToken.token(), user.institutionId, forumId, result);
            this.setState({ message : '' })
        }
    };

    editFormHandler = () => {
        const user = JSON.parse(localStorage.getItem('user'));
        const forumId = this.props.match.params.id;
        const {messageEdit, replyId, newForumItem, subject} = this.state;
        let result = {};
        const errors = [];

        const title = validate('title',newForumItem.name,{required: 1});
        const description = validate('description',messageEdit,{required: 1});

        if (description) errors.push(description);
        if(this.state.type === 'forum') if (title) errors.push(title);

        this.setState({errors: errors});

        if(messageEdit.length !== 0) {

            if (this.state.type === 'comment') {
                result['message'] = messageEdit;

                this.props.updateForumReplies(userAuthToken.token(), user.institutionId, forumId, replyId, result)
                    .then(() => {
                        this.props.getForumReplies(userAuthToken.token(), user.institutionId, forumId);

                        this.setState({ showEditFormModal : !this.state.showEditFormModal });
                    })
            } else {
                result['name'] = newForumItem.name;
                result['description'] = messageEdit;
                result['inactiveDate'] = newForumItem.inactiveDate;
                result['subjectId'] = subject;

                this.props.updateForum(userAuthToken.token(), user.institutionId, forumId, result)
                    .then(() => {
                        this.props.getForumPostDetail(userAuthToken.token(), user.institutionId, forumId);

                        this.setState({ showEditFormModal : !this.state.showEditFormModal });
                    })
            }
        }
    };

    deleteForumItem = () => {
        this.props.showSureModal('Are you sure you want to delete this forum?')
            .then(result => {
                const user = JSON.parse(localStorage.getItem('user'));
                const forumId = this.props.match.params.id;

                this.props.deleteForumItem(userAuthToken.token(), user.institutionId, forumId)
                    .then(() => {
                        history.push('/forum')
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    removeComment = (replyId) => {
        this.props.showSureModal('Are you sure you want to delete this comment?')
            .then(result => {
                const user = JSON.parse(localStorage.getItem('user'));
                const forumId = this.props.match.params.id;

                this.props.deleteForumReplies(userAuthToken.token(), user.institutionId, forumId, replyId)
                    .then(() => {
                        this.props.getForumReplies(userAuthToken.token(), user.institutionId, forumId);
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    componentDidMount() {
        const user = JSON.parse(localStorage.getItem('user'));
        const assignmentsId = this.props.match.params.id;
        this.props.getAssignmentsDetail(userAuthToken.token(), user.institutionId, assignmentsId)
            .then(() => {
                this.setState({loadingReplies: false});
            });
        this.props.getAllSubjects(userAuthToken.token(), user.institutionId);
        this.props.getForumReplies(userAuthToken.token(), user.institutionId, assignmentsId);
    }

    newForumItemFieldsUpdateHandler = (value, name) => {
        let {newForumItem} = this.state;
        newForumItem[name] = value;
        this.setState({newForumItem});
    };

    render() {
        const {user, auth, admin} = this.props;
        const {message} = this.state;
        let date, inactiveDate;

        return (
            <Fragment>
                {user.item === undefined ? <Loader/> : (
                    <div className="detail-forum">
                        {/*<EditForm show={this.state.showEditFormModal}*/}
                                  {/*text={this.state.messageEdit}*/}
                                  {/*saved={this.editFormHandler}*/}
                                  {/*hideModal={this.toggleEditFormModal}*/}
                                  {/*handleChange={this.handleChange}*/}
                                  {/*type={this.state.type}*/}
                                  {/*fieldUpdated={this.newForumItemFieldsUpdateHandler}*/}
                                  {/*subjectList={admin.subject}*/}
                                  {/*subject={this.state.subject}*/}
                                  {/*newForumItem={this.state.newForumItem}*/}
                                  {/*errors={this.state.errors}*/}
                        {/*/>*/}
                        {/*<Cell column={9}>*/}
                            {/*<div className="detail-forum__header" style={{display: 'flex', alignItems: 'center'}}>*/}
                                {/*<Link to={`${routeConstants.FORUM}`}>*/}
                                    {/*<Icon name="left-arrow"/>*/}
                                {/*</Link>*/}
                                {/*<p style={{marginLeft: '20px'}}>{user.item.name}</p>*/}
                            {/*</div>*/}
                            {/*<div className="detail-forum__main">*/}
                                {/*<Table user={auth.user}*/}
                                       {/*forumDetail={user.item}*/}
                                       {/*role={auth.user.role}*/}
                                       {/*date={date}*/}
                                       {/*forum*/}
                                       {/*inactiveDate={inactiveDate}*/}
                                       {/*editMode={() => this.toggleEditFormModal(user.item.id, 'forum')}*/}
                                       {/*deleteForumItem={this.deleteForumItem}/>*/}
                                {/*<Reply*/}
                                    {/*imgSrc={auth.user.avatar}*/}
                                    {/*placeholderText="Type here to reply…"*/}
                                    {/*className=""*/}
                                    {/*message={message}*/}
                                    {/*handleChange={this.handleChange}*/}
                                    {/*onSubmit={this.saveComment}*/}
                                    {/*style={{}}*/}
                                {/*/>*/}
                                {/*<CommentBlock user={user}*/}
                                              {/*comment={user.forumReplies}*/}
                                              {/*auth={auth}*/}
                                              {/*loadingReplies={this.state.loadingReplies}*/}
                                              {/*editMode={this.toggleEditFormModal}*/}
                                              {/*removeComment={this.removeComment}*/}
                                {/*/>*/}
                            {/*</div>*/}
                        {/*</Cell>*/}
                    </div>
                )}
            </Fragment>
        );
    }
}

const mapStateToProps = ({loader, auth, user, admin}) => ({loader, auth, user, admin});

const mapDispatchToProps = dispatch => ({
    showLoader: () => dispatch(showLoader()),
    hideLoader: () => dispatch(hideLoader()),
    getForumPostDetail: (token, id, forumId) => {return dispatch(getForumPostDetail(token, id, forumId))},
    getForumReplies: (token, id, forumId) => {dispatch(getForumReplies(token, id, forumId))},
    addForumReplies: (token, id, forumId, message) => {dispatch(addForumReplies(token, id, forumId, message))},
    updateForumReplies: (token, id, forumId, replyId, message) => {return dispatch(updateForumReplies(token, id, forumId, replyId, message))},
    updateForum: (token, id, forumId, message) => {return dispatch(updateForum(token, id, forumId, message))},
    deleteForumReplies: (token, id, forumId, replyId) => {return dispatch(deleteForumReplies(token, id, forumId, replyId))},
    deleteForumItem: (token, id, forumId) => {return dispatch(deleteForumItem(token, id, forumId))},
    getAllSubjects: (token, id) => {dispatch(getAllSubjects(token, id))},
});

const connectedSubmission = withRouter(connect(mapStateToProps,mapDispatchToProps)(withSure(Submission)));

export { connectedSubmission as Submission };