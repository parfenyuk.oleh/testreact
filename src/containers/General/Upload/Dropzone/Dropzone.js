import React, { Component } from 'react';
import Icon from '../../../../components/UI/Elements/Icon';

class Dropzone extends Component {
    state = {
        isLoad: false,
        isDragOver: false
    };

    fileInputRef = React.createRef();

    openFileDialog = () => {
        this.fileInputRef.current.click();
    };

    onFilesAdded = evt => {
        const files = evt.target.files;
        if (this.props.onFilesAdded) {
            const array = this.fileListToArray(files);
            this.props.onFilesAdded(array);
            this.setState({
                isLoad: true,
                isDragOver: false
            })
        }
    };

    onDragOver = event => {
        event.preventDefault();
        this.setState({
            isDragOver: true
        });
    };

    onDragLeave = event => {
        event.preventDefault();
        this.setState({
            isDragOver: false
        });
    };

    onDrop = event => {
        event.preventDefault();
        this.setState({
            isDragOver: false
        })
        const files = event.dataTransfer.files;
        const format = files[0].name.slice(-4);
        const array = this.fileListToArray(files);
        this.props.onFilesAdded(array);
        if (files.length > 1) {
            this.props.onlyOneFileErrorHandle();
        }
    };

    fileListToArray = list => {
        const array = [];
        for (let i = 0; i < list.length; i++) {
            array.push(list.item(i))
        }
        return array;
    };

    onFileInputClick = event => {
        event.target.value = null;
    };

    render() {
        return (
            <div className={`Dropzone ${this.state.isDragOver ? 'upload-over' : 'upload-over-false'}`}
                onDrop={this.onDrop}
                 onDragOver={this.onDragOver}
                 onDragLeave={this.onDragLeave}
            >
                <p>Drop file here<br /> or</p>
                <Icon name='emptyState' alt='empty'/>
                <input ref={this.fileInputRef}
                    className="FileInput"
                    type="file" multiple
                    onChange={this.onFilesAdded}
                    onClick={this.onFileInputClick}
                    id="test"
                />

                <button onClick={this.openFileDialog}
                        className="btn-secondary">Upload files</button>
            </div>
        );
    }
}

export default Dropzone;
