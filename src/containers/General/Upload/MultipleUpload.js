import React, {Component} from 'react';
import Button from '../../../components/UI/Elements/Button';
import File from '../../../images/doc_no_bg.svg';
import Icon from '../../../components/UI/Elements/Icon';

class MultipleUpload extends Component {
    constructor(props){
        super(props);
        this.state = {
            files: []
        };
    }

    fileInputRef = React.createRef();

    openFileDialog = () => {
        this.fileInputRef.current.click();
    };

    onFilesAdded = evt => {
        const files = evt.target.files;
        console.log(evt.target.files);
        const array = this.fileListToArray(files);
        this.setState(prevState => ({
            files: prevState.files.concat(array)
        }), ()=>{console.log(this.state)})
        console.log(array);
    };

    fileListToArray = list => {
        const array = [];
        for (let i = 0; i < list.length; i++) {
            array.push(list.item(i))
        }
        return array;
    };

    closeHandler = filesIndex => {
        const files = [...this.state.files];
        files.splice(filesIndex, 1);
        this.setState({
            files: files,
        })
    };

    render() {
        const {files} = this.state;
        return (
            <div className="multiply-upload">
                <input ref={this.fileInputRef}
                       className="FileInput"
                       type="file" multiple
                       onChange={this.onFilesAdded}
                       onClick={this.onFileInputClick}
                       id="test"
                />
                <Button
                    type='button'
                    size='sm'
                    context='secondary'
                    onClick={this.openFileDialog}
                >
                    upload new
                </Button>
                <div>
                    {files &&
                    <div className="forum-attachments">
                        {files.map((files, filesIndex) => {
                            return (
                                <div key={files.name + filesIndex}>
                                    <div className="file-container">
                                            <img src={File}
                                                 alt={`${files.attachmentHeading}`}
                                                 className="uploaded-file"
                                            />
                                            <p>{files.name}</p>
                                        <div className="cross">
                                            <Icon name="cross"
                                                  onClick={() => {this.closeHandler(filesIndex)}}/>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                    }
                </div>
            </div>
        );
    }
}

export default MultipleUpload;