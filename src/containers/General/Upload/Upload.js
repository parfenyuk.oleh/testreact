import React, { Component } from 'react';

import Dropzone from './Dropzone/Dropzone';
import File from '../../../images/doc_no_bg.svg';
import Close from '../../../images/round copy.svg';
import Link from '../../../images/link.svg';
import {withInfo} from "@storybook/addon-info";

class Upload extends Component {
    state = {
        files: [],
        currentValue:'',
        loaded: false,
        isDragOver: false
    };

    onlyOneFileErrorHandle = () => {
        this.setState({
            isDragOver: false,
            loaded: false
        });
    };

    onFilesAdded = (files) => {
        this.setState(prevState => ({
            loaded: false,
            files: prevState.files.concat(files),
            isDragOver: false
        }), () => {
          this.props.onFileUpload(this.state.files);
        });

    };

    onDragOver = event => {
            event.preventDefault();
            this.setState({
                isDragOver: true
            });
    };

    onDragLeave = event => {
        event.preventDefault();
        this.setState({
            isDragOver: false
        });
    };

    closeHandler = filesIndex => {
        const files = [...this.state.files];
        files.splice(filesIndex, 1);
        this.setState({
            files: files,
            isDragOver: false
        })
      this.props.onFileUpload(files);
    };

    closeLink = linkIndex => {
        const {links} = this.props;
        links.splice(linkIndex, 1);
        this.setState({
            links: links,
        })
      this.props.removeLink(linkIndex);
    };

    render() {
        const showFiles = this.props.oldFiles.length || this.state.files.length || this.props.links.length >= 1;
      return (
            <div onDragOver={this.onDragOver}
                onDragLeave={this.onDragLeave}
                className="Upload"
            >
                <div className="Content">
                    <div>
                        <Dropzone
                                onDragOver={this.state.isDragOver}
                                onFilesAdded={this.onFilesAdded}
                                onClose={this.closeHandler}
                                files={this.state.files}
                                links={this.props.links}
                                showFiles={showFiles}
                        />
                    </div>
                    <div  className={showFiles ? 'Row' : "Row-none"}>
                        <div className="files-container">
                            {
                              showFiles ?
                                    (
                                        <div className="Files">
                                                {this.state.files.map((file, filesIndex) => {
                                                return (
                                                    <div key={file.name + filesIndex}>
                                                        <div className="img-wrapper">
                                                            <img src={File}
                                                                 alt="file"
                                                                 className="uploaded-file"
                                                                 title={file.name}
                                                            />
                                                            <img src={Close}
                                                                 alt="close"
                                                                 className="close"
                                                                 onClick={() => {this.closeHandler(filesIndex)}}
                                                            />
                                                        </div>
                                                        <span>{file.name}</span>
                                                    </div>

                                                );
                                            })}
                                          {this.props.oldFiles.map((file, filesIndex) => {
                                            return (
                                              <div key={file.attachmentHeading + filesIndex}>
                                                <div className="img-wrapper">
                                                  <img src={File}
                                                       alt="file"
                                                       className="uploaded-file"
                                                       title={file.attachmentHeading}
                                                  />
                                                  <img src={Close}
                                                       alt="close"
                                                       className="close"
                                                       onClick={() => {this.props.removeOldFile(filesIndex)}}
                                                  />
                                                </div>
                                                <span>{file.attachmentHeading}</span>
                                              </div>

                                            );
                                          })}
                                            {this.props.links.map((link,linkIndex) => {
                                                return(
                                                    <div key={link}>
                                                        <div className="img-wrapper">
                                                            <img src={Link}
                                                                alt="link"
                                                                className="uploaded-file"
                                                                 title={link}
                                                            />
                                                            <img src={Close}
                                                                alt="close"
                                                                className="close"
                                                                onClick={() => {this.closeLink(linkIndex)}}
                                                            />
                                                        </div>
                                                        <span>{link}</span>
                                                    </div>
                                                )
                                            })
                                            }
                                        </div>
                                    ) : null
                            }
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Upload;
