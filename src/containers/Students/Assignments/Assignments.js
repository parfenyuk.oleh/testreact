import React, {Component, Fragment} from 'react';
import * as actions from '../../../store/actions';
import connect from 'react-redux/es/connect/connect';
import withSure from '../../../hoc/withSure';
import {userAuthToken} from '../../../helpers';
import AssignmentsTable from '../../../components/Student/Assignments/AssignmentsTable';
import {STATUSES, TABS} from '../../../constants/assignments';
import noTableData from '../../../images/ic_files.svg'
import AssignmentsCard from '../../../components/Student/Assignments/AssignmentsCard';
import {getStudentAssessments} from '../../../store/actions/assessments.actions';

class Assignments extends Component {
  state = {
    pagination: 2,
  };

  componentDidMount() {

    const {institutionId, token} = this.props.auth.user;
    this.props.clearAssignments();
    this.props.getAllGroups(userAuthToken.token(), institutionId);
    this.props.getAllSubjects(institutionId, token);
    this.props.getAllAssignments(userAuthToken.token(), institutionId, null, null, 0, 20);
    this.props.getStudentAssessments(institutionId, token);
  }

  loadMoreMembers = () => {
    const {institutionId} = this.props.auth.user;
    let {pagination} = this.state;

    this.props.getAllAssignments(userAuthToken.token(), institutionId, null, null, pagination++ * 10, 10);

    this.setState({pagination});
  };

  submitHandler = assignmentId => {
    const {institutionId} = this.props.auth.user;
    this.props.changeAssignmentStatus(userAuthToken.token(),assignmentId, institutionId, STATUSES.ToReview);
  };

  render() {
    
    return (
      <div className="assignment-student">
        { this.props.assessments.studentAssessmentsList.length ?
        <AssignmentsCard/>: <img className="no-table-data" src={noTableData} alt="No data"/>
        }
      </div>
    );
  }
}

const mapStateToProps = ({auth, groups, admin, assignments, assessments}) => ({auth, groups, admin, assignments, assessments});

const mapDispatchToProps = dispatch => ({
  getAllGroups: (token, id) => {dispatch(actions.getAllGroups(token, id))},
  getAllSubjects: (id, token) => {dispatch(actions.getAllSubjects(id, token))},
  getStudentAssessments: (institutionId, userToken) => dispatch(getStudentAssessments(institutionId, userToken)),
  getAllAssignments: (token,
                      institutionId,
                      isArchive,
                      groupIds,
                      skip,
                      take,
                      search) => {
    dispatch(actions.getAllAssignments(token,
      institutionId,
      isArchive,
      groupIds,
      skip,
      take,
      search))
  },
  clearAssignments: () => {dispatch(actions.clearAssignments())},
  changeAssignmentStatus: (token, assignmentId, institutionId, assignmentStatus) => {
    dispatch(actions.changeAssignmentStatus(token, assignmentId, institutionId, assignmentStatus))
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(withSure(Assignments));
