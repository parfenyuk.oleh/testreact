import React, {Component} from 'react';
import Select from '../../../components/UI/Select';
import * as actions from '../../../store/actions';
import connect from 'react-redux/es/connect/connect';
import DashboardCard from '../../../components/General/Dashboard/DashboardCard';
import Icon from '../../../components/UI/Elements/Icon';
import moment from 'moment';
import axios from '../../../axios-instance';

const DATE_TYPES = {
  ALL: 'All',
  WEEK: 'Week',
  MONTH: 'Month',
  YEAR: 'Year',
};

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      searchValue: '',
      currentSubject: null,
      date: DATE_TYPES.ALL,
      subjects: [],
    }
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));

    axios.get(`/Institutions/${user.institutionId}/Dashboard/Student/Subjects`, {
        headers: {Authorization: `Bearer ${user.token}`},
      },
    ).then(response => {
      if (response.data.ok) {
        const subjects = response.data.result;
        this.setState({subjects}, () => {
          if (subjects.length) {
            this.subjectChangeHandler(subjects[0]);
          }
        })

      }
    })
  }

  getDates = type => {
    const now = moment();

    switch (type) {
      case DATE_TYPES.WEEK: {
        return {
          end: now.format(),
          start: now.subtract(7, 'days').format(),
        }
      }
      case DATE_TYPES.MONTH: {
        return {
          end: now.format(),
          start: now.subtract(1, 'months').format(),
        }
      }
      case DATE_TYPES.YEAR: {
        return {
          end: now.format(),
          start: now.subtract(1, 'years').format(),
        }
      }

      default:
        return {
          start: null,
          end: null,
        }
    }

  };

  subjectChangeHandler = subject => {
    const user = JSON.parse(localStorage.getItem('user'));
    const dates = this.getDates(this.state.date);
    this.props.getStudentData(user.institutionId,
      user.token,
      subject.id,
      dates.start,
      dates.end);
    this.setState({
      currentSubject: subject,
    })
  };

  dateChangeHandler = ({name}) => {
    const user = JSON.parse(localStorage.getItem('user'));


    const dates = this.getDates(name);
    this.props.getStudentData(user.institutionId,
      user.token,
      this.state.currentSubject.id,
      dates.start,
      dates.end);
    ;
    this.setState({date: name});
  };

  render() {
    const {currentSubject, subjects} = this.state;
    const {studentData} = this.props.dashboard;
    return (
      <div className="dashboard">
        <div className="dashboard-header">
          <h1 className="page-title ">Student’s dashboard</h1>
          <div className="dashboard-header-selects">
            {currentSubject && <Select placeholder="Select subject"
                                       options={subjects}
                                       clicked={this.subjectChangeHandler}
                                       text={currentSubject.name}
            />
            }
            <Select placeholder="Select date"
                    options={[
                      {name: DATE_TYPES.ALL},
                      {name: DATE_TYPES.WEEK},
                      {name: DATE_TYPES.MONTH},
                      {name: DATE_TYPES.YEAR},
                    ]}
                    clicked={this.dateChangeHandler}
            />
          </div>
        </div>

        <div className="dashboard-row">
          <div className="dashboard-attendance">
            <div className="dashboard-attendance-title">Attendance</div>
            <div className="dashboard-attendance-data">
              <div>
                <p className="dashboard-card-number">{studentData.onTimeCount || 0}</p>
                <p className="dashboard-card-title">On time</p>
              </div>
              <div>
                <p className="dashboard-card-number">{studentData.lateCount || 0}</p>
                <p className="dashboard-card-title">Late</p>
              </div>
              <div>
                <p className="dashboard-card-number">{studentData.veryLateCount || 0}</p>
                <p className="dashboard-card-title">Very late</p>
              </div>
              <div>
                <p className="dashboard-card-number">{studentData.absentCount || 0}</p>
                <p className="dashboard-card-title">Absent</p>
              </div>
            </div>
          </div>
        </div>

        <div className="dashboard-row">
          <div className="dashboard-col">
            <DashboardCard icon={<Icon name={'keyword'}/>}
                           number={studentData.participationRating || 0}
                           title={'Participation rating'}
            />
          </div>
          <div className="dashboard-col">
            <DashboardCard icon={<Icon name={'assignments'}/>}
                           number={studentData.assignmentsMarksCount || 0}
                           title={'Assignments marks'}
            />
          </div>
        </div>

      </div>
    );
  }
}

const mapStateToProps = ({admin, dashboard}) => ({admin, dashboard});

const mapDispatchToProps = dispatch => ({
  getStudentData: (institutionId, token, subjectId, startDate, endDate) => {
    dispatch(actions.getStudentData(institutionId, token, subjectId, startDate, endDate))
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);