import React, {Component} from 'react';
import {Redirect, withRouter} from 'react-router';
import {compose} from 'recompose';
import {connect} from 'react-redux';
import * as ROUTES from '../../../constants/routes';
import {Link} from 'react-router-dom';
import Icon from '../../../components/UI/Elements/Icon';
import * as actions from '../../../store/actions';
import {userAuthToken} from '../../../helpers';
import * as signalR from '@aspnet/signalr';
import EditorController from '../../../components/Editor/EditorController';
import {STATUSES} from '../../../constants/assignments';
import {patchWorkspace} from '../../../store/actions';
import {hideModal} from '../../../store/actions';
import ROLES from '../../../constants/roles';

class WorkSpace extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showFullDescription: false,
      colorCoding: true,
      wordsCount: {
        total: 0,
        users: {},
      },
    }
  }

  componentDidMount() {
    const {auth, location} = this.props;
    this.props.getWorkspaceInfo(auth.user.institutionId, userAuthToken.token(), location.state.assessmentId, location.state.groupId);
  }

  onReadMore = () => {this.setState({showFullDescription: !this.state.showFullDescription})};

  saveHandle = () => {this.props.history.push(ROUTES.ASSIGNMENTS)};

  toggleColorCoding = () => {this.setState({colorCoding: !this.state.colorCoding})};

  handleWordsCountChange = wordsCount => { this.setState({wordsCount})};

  submitHandler = () => {
    const {institutionId} = this.props.auth.user;
    const {assignmentId} = this.props.assignments.workspace;
    this.props.changeAssignmentStatus(userAuthToken.token(), assignmentId, institutionId, STATUSES.ToReview);
    this.saveHandle();
  };

  // isSubmitButtonDisabled = () => {
  //
  //   const {assignments} = this.props;
  //   if(!assignments.allAssignments.length) return true;
  //   const currentAssignmentStatus = assignments.workspace.status;
  //   return !(assignments.workspace.leaderId === this.props.auth.user.id &&
  //     currentAssignmentStatus !== STATUSES.ToReview &&
  //     currentAssignmentStatus !== STATUSES.Done
  //   );
  // };

  isEditorDisabled = () => {

    const {status} = this.props.assignments.workspace;
    return status === STATUSES.ToReview || status === STATUSES.Done || this.props.auth.user.role === ROLES.TEACHER;
  };

  saveHandler = () => {
    const {token, institutionId} = this.props.auth.user;
    const data = this.collectData();
    this.props.patchWorkspace(token, institutionId, data);
    this.props.hideModal();
    return <Redirect to={ROUTES.ASSESSMENTS}/>
  };

  collectData = () => {
    const test = this.props.location.state.assessmentId;
    return {
      assessmentId: test
    };

  };

  render() {
    if (!this.props.location.state)
      return <Redirect to={ROUTES.ASSESSMENTS}/>;
    const isLeader = this.props.location.state.isLeader;
    const {workspace} = this.props.assignments;
    const {showFullDescription, colorCoding, wordsCount} = this.state;
    const MIN_STRING_LENGHT_FOR_SHOW_READ_MORE = 800;
    return (
      <div className="workspace">
        <div>
          <div className="workspace__header">
            {isLeader !== 3 &&
              <Link to={`${ROUTES.ASSESSMENTS}`}>
                <Icon name="left-arrow"/>
                <span>Back</span>
              </Link>
            }
          </div>
          <div className={`workspace-description`}>
            <p>{workspace.assignmentDescription}</p>
          </div>
        </div>
        <div className="workspace-body">
          <header>
            <h3>Total Words goal {wordsCount.total}/{workspace.wordsLimit}</h3>
            <div>
              <button className="big-primary xs"
                      onClick={this.toggleColorCoding}
              >{colorCoding ? 'ON' : 'OFF'}</button>
              <p>Color coding is <span>{colorCoding ? 'ON' : 'OFF'}</span></p>
            </div>
          </header>
          <div className="workspace-body-content">
            <div className="workspace-users">
              <div className="workspace-users-list">
                {
                  workspace.students && workspace.students.map(student => {
                    return <p className={`item`}
                              style={{color: student.color}}
                              key={student.fullName + student.id}
                    >
                      {student.fullName} {wordsCount.users[student.id] ? wordsCount.users[student.id] : 0}/{Math.floor(workspace.wordsLimit / workspace.students.length)}
                    </p>
                  })
                }
              </div>
            </div>
            <EditorController assignmentId={this.props.location.state.assessmentId}
                              token={this.props.auth.user.token}
                              students={workspace.students}
                              userId={this.props.auth.user.id}
                              groupId={this.props.location.state.groupId}
                              colorCoding={colorCoding}
                              onWordsCountChange={this.handleWordsCountChange}
                              disabled={this.isEditorDisabled()}
            />
          </div>
          {/*TODO: Hide button Submit and Save*/}
          {/*{isLeader !== 3 &&*/}
          {/*<footer>*/}

          {/*  <button className="big-primary big-button" onClick={this.saveHandle}>Save</button>*/}
          {/*  {isLeader === 2 &&*/}
          {/*  < button className="button-success big-button"*/}
          {/*           disabled={isLeader !== 2}*/}
          {/*           onClick={this.saveHandler}*/}
          {/*  >Submit*/}
          {/*  </button>*/}
          {/*  }*/}
          {/*</footer>*/}
          {/*}*/}
        </div>
      </div>
    );
  }
}


WorkSpace.defaultProps = {
  assignments: {
    assignmentName: null
  }
};

const mapStateToProps = ({auth, assignments}) => ({auth, assignments});

const mapDispatchToProps = dispatch => ({
  getWorkspaceInfo: (institutionId, token, assessmentId, groupForAssessmentId) => {
    dispatch(actions.getWorkspaceInfo(institutionId, token, assessmentId, groupForAssessmentId))
  },
  changeAssignmentStatus: (token, assignmentId, institutionId, assignmentStatus) => {
    dispatch(actions.changeAssignmentStatus(token, assignmentId, institutionId, assignmentStatus))
  },
  patchWorkspace: (token, institutionId, data) => dispatch(patchWorkspace(token, institutionId, data)),
  hideModal: () => {dispatch(hideModal())},
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(WorkSpace);