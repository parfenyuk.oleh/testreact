import React, {Component, Fragment} from 'react';
import GradeBookHeader from "../../../components/Submission/Gradebook/GradebookHeader";
import GradeBookTable from "../../../components/Submission/Gradebook/GradeBookTable";

class GradeBook extends Component {
  render() {
    return (
      <div>
        <GradeBookHeader/>
        <GradeBookTable/>
      </div>
    );
  }
}

export default GradeBook;
