import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';

import SubmissionHeader from "../../components/Submission/SubmissionHeader";
import SubmissionsTable from "../../components/Submission/SubmissionsTable";
import ArchiveModal from '../../components/Submission/Modals/ArchiveModal';
import * as actions from '../../store/actions/submissions';
import ReviewModal from "../../components/Submission/Modals/ReviewModal";
import MarkModal from "../../components/Submission/Modals/MarkModal";

class Submissions extends Component {

  render() {
      return (
        <div>
          <SubmissionHeader/>
          <SubmissionsTable/>
          <ArchiveModal show={this.props.modals.archive}/>
          <ReviewModal show={this.props.modals.review}/>
          <MarkModal show={this.props.modals.mark}/>
        </div>)
  }

}

const mapStateToProps = state => {
  return {
    modals: state.submissions.modal
  }
};

const mapDispatchToProps = dispatch => {
  return {
    showModal: modal => { dispatch(actions.showModal(modal)) },
    hideModal: modal => { dispatch(actions.hideModal(modal)) }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Submissions);