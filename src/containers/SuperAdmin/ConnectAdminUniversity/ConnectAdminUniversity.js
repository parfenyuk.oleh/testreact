import React, {Component} from 'react';
import {connect} from 'react-redux';

import Input from "../../../components/UI/Input";
import * as superAdminActions from "../../../store/actions/superadmin";

class ConnectAdminUniversity extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: '',
      id: ''
    };
  }


  componentDidMount(){
    this.props.getAllUniversities(this.props.user.token);
  }

  changeHandler = (e, field) => {
    let state = {};
    state[field] = e.target.value;
    this.setState(state);
  };

  submitHandler = () => {
    console.log(this.state);
    const {email, id} = this.state;
    if(email && id){
      console.log('submit');
      this.props.connectUserUniversity(id, email, this.props.user.token);
    }
  };

  render() {
    return (
      <div>
        <Input placeholder={'email'} changed={(e) => {this.changeHandler(e,'email')}} />
        <select onChange={(e) => {this.changeHandler(e,'id')}}>
          <option disabled selected value=''>Choose your University</option>
          {this.props.superadmin.universities.map(item => (<option value={item.id} key={item.id}>{item.name}</option>))}
        </select>
        <button className="big-primary" onClick={this.submitHandler}>Select</button>
      </div>
    );
  }
}

const mapStateToProps = ({auth: {user}, superadmin}) => ({user, superadmin});

const mapDispatchToProps = dispatch => ({
  getAllUniversities    : token => {dispatch(superAdminActions.getAllUniversities(token))},
  connectUserUniversity : (id, email, token) => {dispatch(superAdminActions.connectUserUniversity(id, email, token))}
});

export default connect(mapStateToProps, mapDispatchToProps)(ConnectAdminUniversity);