import React, {Component} from 'react';
import {connect} from 'react-redux';

import * as superAdminActions from '../../store/actions/superadmin';
import AddUniversity from '../../components/SuperAdmin/AddUniversity';
import University from '../../components/SuperAdmin/University';
import fileReader from '../../helpers/fileReader';
import imageLoadApi from "../../helpers/imageLoadApi";
import Swal from "sweetalert2";

const NAME = 'name';
const IMAGE = 'avatar';

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      newUniversity: {
        name: '',
        avatar: '',
        color: '#0079C4',
          key: ''
      }
    };
  }
  componentDidMount(){
    this.props.getAllUniversities(this.props.user.token);
  }
    universityChangeHandler = (value, field ,id) => {

        if (field === IMAGE) {fileReader(value, result => {
            this.props.updateUniversityRedux(id, 'avatar', value);
          this.props.updateUniversityRedux(id, 'imageToShow', result);
        });}
        else {this.props.updateUniversityRedux(id, field, value);}

      };
    newUniversityNameChangeHandler = event => {
        this.setState({
          newUniversity: {
            ...this.state.newUniversity,
              name: event.target.value
          }
        });
      };
    newUniversityFileChangeHandler = event => {
      console.log(event.target.files[0]);
      this.setState({
        newUniversity: {
          ...this.state.newUniversity,
          avatar: event.target.files[0],
        }
      });

        fileReader(event.target.files[0],result => {
          this.setState({
            newUniversity: {
              ...this.state.newUniversity,
              image: result
            }
          })
        })
    };

    newUniversityAddHandler = () => {
        const university = {...this.state.newUniversity};
        university.key = university.name.substr(0,3);
        console.log(university.avatar);
        if( typeof university.avatar !== "string") {
          imageLoadApi(university.avatar, this.props.user.token)
            .then(response => {
              console.log('[RESPONSE LOAD API]',response);
              if (response.data.ok) {
                university.avatar = response.data.result;
                delete university.image;
                this.props.addUniversity(university, this.props.user.token);
              }
              else throw new Error('Cant upload file')

            })
            .catch(e => {
              Swal({
                title: "Error!",
                text: e,
                type: "error",
                timer: 3000
              });
            })
        }
        else this.props.addUniversity(university, this.props.user.token);
      };

    updateUniversityHandler = item => {
      if( typeof item.avatar !== "string") {
        imageLoadApi(item.avatar, this.props.user.token)
          .then(response => {
            console.log(response);
            if (response.data.ok) {
              item.avatar = response.data.result;
              this.props.updateUniversity(item, this.props.user.token);
            }
            else throw new Error('Cant upload file')

          })
          .catch(e => {
            Swal({
              title: "Error!",
              text: e,
              type: "error",
              timer: 3000
            });
          })
      }
      else
        this.props.updateUniversity(item, this.props.user.token);
      };
    render() {
        return (
          <div>
            <AddUniversity nameChanged={this.newUniversityNameChangeHandler}
                           img={this.state.newUniversity.image }
                           fileChanged={this.newUniversityFileChangeHandler}
                           submited={this.newUniversityAddHandler}
            />
            {this.props.superadmin.universities.map(item => <University name={item.name}
                          image={item.avatar}
                          key={item.id}
                          deleted={() => {this.props.deleteUniversity(item.id)}}
                          changedName={e => {this.universityChangeHandler(e.target.value, NAME, item.id)}}
                          changedImage={e => this.universityChangeHandler(e.target.files[0], IMAGE, item.id)}
                          updated={() => {this.updateUniversityHandler(item)}}
                                                                        imageToShow={item.imageToShow}
              />)}
          </div>
        );
      }
}

const mapStateToProps = ({superadmin, auth: {user}}) => ({superadmin, user});

const mapDispatchToProps = dispatch => ({
  getAllUniversities: token => {dispatch(superAdminActions.getAllUniversities(token))},
  deleteUniversity: id => {dispatch(superAdminActions.deleteUniversity(id))},
  updateUniversityRedux: (id, field, value) => {dispatch(superAdminActions.updateUniversityRedux(id, field, value))},
  updateUniversity: (item, token) => {dispatch(superAdminActions.updateUniversity(item, token))},
  addUniversity: (value, token) => {dispatch(superAdminActions.addUniversity(value, token))}
});

export default connect(mapStateToProps,mapDispatchToProps)(Main);
