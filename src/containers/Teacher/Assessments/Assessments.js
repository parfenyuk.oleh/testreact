import React, {Component, Fragment} from 'react';
import connect from 'react-redux/es/connect/connect';
import * as actions from '../../../store/actions';
import * as modalActions from '../../../store/actions/modals.action';
import ReactTable from 'react-table';
import 'react-table/react-table.css';
import moment from 'moment';
import AssessmentsHeader from '../../../components/Teacher/Assessments/AssessmentsHeader';
import {
    addAssessmentGroup,
    changeAssessmentGroup,
    changeGroupLeader,
    createUserAttendance,
    getAllAssessments,
    getAssessmentsGroups,
    getAssignmentInfo,
    getAttendanceDate,
    getUserAttendance,
    setCurrentAssessment,
} from '../../../store/actions/assessments.actions';
import icons from '../../../helpers/iconsLoader';
import noTableData from '../../../images/ic_files.svg'

import NotStarted from '../../../components/Teacher/Assessments/AssessementsMarks/NotStarted';
import InProgress from '../../../components/Teacher/Assessments/AssessementsMarks/InProgress';
import Submitted from '../../../components/Teacher/Assessments/AssessementsMarks/Submitted';
import SubmittedLate from '../../../components/Teacher/Assessments/AssessementsMarks/SubmittedLate';
import NotSubmitted from '../../../components/Teacher/Assessments/AssessementsMarks/NotSubmitted';
import Done from '../../../components/Teacher/Assessments/AssessementsMarks/Done';
import Select from '../../../components/UI/Select';
import {ASSESMENT_TYPES, MODAL_TYPES, MODALS_TITLES} from '../../../constants/modalsTypes';
import AttendanceUser from './AttendanceUser';
import Icon from '../../../components/UI/Elements/Icon';
import Swal from 'sweetalert2';
import confirm from '../../../images/pic_man.svg';
import {getStudentAttendanceStats} from '../../../store/actions';
import DatePicker from '../../../components/UI/Datepicker';


class Assessments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userId: null,
            mark: 0,
            attendanceMarkType: null,
            currentDate: '',
            subjects: [],
            currentSubject: null,
            groups: [],
            rating: null,
            currentAssessment: null,
            selectedGroup: null,
            selectedUserId: null,
            maxDate: '',
            highlightDates: [],
        };
    }

    componentDidMount() {
        this.getAllSubjects();
    }

    rowClickHandler = (event, original) => {
        if (this.props.modals.type === MODAL_TYPES.SET_ATTENDANCE_MARK
            && (event.target.classList.contains("rt-tr")
                || event.target.classList.contains("rt-td")
                || event.target.classList.contains("default-avatar")
                || event.target.classList.contains("name")
                || event.target.classList.contains("pointsCount"))) {
            this.openAttendanceModal(original, this.state.currentAssessment);
        }
    }

    selectDateHandler = event => {
        const {institutionId, token} = this.props.auth.user;
        const {currentAssessment} = this.state;

        this.setState({
            currentDate: moment(event).toISOString(),
        }, () => {
            this.props.getUserAttendance(token, institutionId, currentAssessment.id, this.state.currentDate)
        })
    };

    renderDate = dates => {
        return dates.map(date => {
            date.name = moment(date.attendanceDate).format('L');
            return date;
        });
    };

    getAllSubjects = () => {
        const {auth, getAllSubjects} = this.props;

        getAllSubjects(auth.user.institutionId, auth.user.token)
            .then(result => {
                this.setState({subjects: result}, () => {
                    if (result.length !== 0) {
                        this.setState({currentSubject: result[0]}, () => {
                            this.getAllAssessments();
                        });
                    }
                })
            });
    };

    subjectChangeHandler = subject => {
        this.setState({currentSubject: subject}, () => {
            this.getAllAssessments();
        });
    };

    getAllAssessments = () => {
        const {auth, getAllAssessments} = this.props;
        const {currentSubject} = this.state;

        getAllAssessments(auth.user.institutionId, currentSubject.id, auth.user.token);

    };

    createNewAssessment = () => {
        this.props.showModal(
            MODAL_TYPES.CREATE_ASSIGNMENT,
            MODALS_TITLES[MODAL_TYPES.CREATE_ASSIGNMENT],
            {
                subjectId: this.state.currentSubject.id,
            },
        )
    };

    editAssessment = header => {
        const {currentSubject} = this.state;

        this.setState({currentAssessment: header});

        switch (header.type) {
            case ASSESMENT_TYPES.ASSIGNMENT: {
                this.props.showModal(
                    MODAL_TYPES.UPDATE_ASSIGNMENT,
                    MODALS_TITLES[MODAL_TYPES.UPDATE_ASSIGNMENT],
                    {
                        assignmentId: header.id,
                        subjectId: currentSubject.id,
                    },
                );
            }
                break;
            case ASSESMENT_TYPES.WORKSPACE: {
                this.props.showModal(
                    MODAL_TYPES.UPDATE_WORKSPACE,
                    MODALS_TITLES[MODAL_TYPES.UPDATE_WORKSPACE],
                    {
                        workspaceId: header.id,
                        subjectId: currentSubject.id,
                    },
                );
            }
                break;
            case ASSESMENT_TYPES.ATTENDANCE: {
                this.props.showModal(
                    MODAL_TYPES.UPDATE_ATTENDANCE,
                    MODALS_TITLES[MODAL_TYPES.UPDATE_ATTENDANCE],
                    {
                        attendanceId: header.id,
                        subjectId: currentSubject.id,
                    },
                );
            }
                break;
            default :
                break;
        }
        const {getAssessmentsGroups, auth} = this.props;
        // getAssignmentInfo(auth.user.institutionId, assessmentId, auth.user.token)
        //     .then(result => {
        //         this.setState({currentAssessment: result});
        //           this.props.showModal(
        //             MODAL_TYPES.UPDATE_ASSIGNMENT,
        //             MODALS_TITLES[MODAL_TYPES.UPDATE_ASSIGNMENT],
        //           )
        //     });
        //

        getAssessmentsGroups(auth.user.institutionId, header.id, auth.user.token);
    };

    groupChangeHandler = group => {
        this.setState({selectedGroup: group}, () => {
            this.changeAssessmentGroup();
        });
    };

    userChangeHandler = userId => {
        this.setState({selectedUserId: userId});
    };

    changeAssessmentGroup = () => {
        const {currentAssessment, selectedGroup, selectedUserId} = this.state;
        const {changeAssessmentGroup, auth, getAssessmentsGroups} = this.props;
        const data = {
            assessmentId: currentAssessment.id,
            userId: selectedUserId,
            groupForAssessmentId: selectedGroup.id,
        };

        changeAssessmentGroup(auth.user.institutionId, data, auth.user.token)
            .then(response => {
                if (response.data.ok) {
                    getAssessmentsGroups(auth.user.institutionId, currentAssessment.id, auth.user.token);
                    this.getAllAssessments();
                }
            });
    };

    addAssessmentGroup = () => {
        const {currentAssessment} = this.state;
        const {addAssessmentGroup, auth} = this.props;

        addAssessmentGroup(auth.user.institutionId, currentAssessment.id, auth.user.token)
            .then(groupId => {
                this.groupChangeHandler(groupId);
            });
    };

    prepareGroupsOptions = groups => {
        const addGroupButton = [{
            name: <div className="add-group-wrapper" onClick={this.addAssessmentGroup}>
                <img src={icons.iconAdd} alt="icon plus"/>
                Add group
            </div>,
        }];

        return groups.map(group => {
            group.name = <div className="groups-wrapper">
                <div className="group-item-wrapper" onClick={() => {
                    this.groupChangeHandler(group)
                }}>
                    Group {group.number}
                    <div className="group-item-wrapper__members-count"
                         style={{backgroundColor: group.color}}
                    >
                        {group.membersCount}
                    </div>
                </div>
            </div>;
            return group;
        }).concat(addGroupButton);
    };

    openAttendanceModal = (original, header) => {
        const {institutionId, token} = this.props.auth.user;
        const {currentSubject} = this.state;
        this.props.getAttendanceDate(header.id, institutionId, token);
        this.setState({currentAssessment: header});
        this.props.setCurrentAssessment(header);
        this.props.getStudentAttendanceStats(token, institutionId, original.id, header.id);
        this.props.showModal(
            MODAL_TYPES.SET_ATTENDANCE_MARK,
            MODALS_TITLES[MODAL_TYPES.SET_ATTENDANCE_MARK],
            {
                assessmentId: header.id,
                studentId: original.id,
                subjectId: currentSubject.id,
            },
        );
    };

    openMarkModal = (original, header) => {
        if (!this.props.modals.show) {
            const {institutionId, token} = this.props.auth.user;
            const {currentSubject} = this.state;
            this.setState({
                currentDate: moment().toISOString(),
                maxDate: moment().toISOString(),
            }, () => {
                this.props.getUserAttendance(token, institutionId, header.id, this.state.currentDate)
                    .then(response => {
                        if(response){
                            this.setState({
                                highlightDates: this.props.assessments.attendanceDate.map(a => moment(a.attendanceDate))
                            })
                        }
                    })
            });

            this.setState({currentAssessment: header});
            this.props.setCurrentAssessment(header);

            switch (header.type) {
                case ASSESMENT_TYPES.ASSIGNMENT: {
                    this.props.showModal(
                        MODAL_TYPES.SET_ASSIGNMENT_MARK,
                        MODALS_TITLES[MODAL_TYPES.SET_ASSIGNMENT_MARK],
                        {
                            assessmentId: header.id,
                            studentId: original.id,
                            subjectId: currentSubject.id,
                        },
                    );
                }
                    break;
                case ASSESMENT_TYPES.WORKSPACE: {
                    this.props.showModal(
                        MODAL_TYPES.SET_WORKSPACE_MARK,
                        MODALS_TITLES[MODAL_TYPES.SET_WORKSPACE_MARK],
                        {
                            assessmentId: header.id,
                            studentId: original.id,
                            subjectId: currentSubject.id,
                        },
                    );
                }
                    break;
                case ASSESMENT_TYPES.ATTENDANCE: {
                    const {institutionId, token} = this.props.auth.user;
                    this.props.getAttendanceDate(header.id, institutionId, token);
                    this.props.getStudentAttendanceStats(token, institutionId, original.id, header.id);
                    this.props.showModal(
                        MODAL_TYPES.SET_ATTENDANCE_MARK,
                        MODALS_TITLES[MODAL_TYPES.SET_ATTENDANCE_MARK],
                        {
                            assessmentId: header.id,
                            studentId: original.id,
                            subjectId: currentSubject.id,
                        },
                    );
                }
                    break;
                default :
                    break;
            }
        }
    };

    changeLeader = (assessmentId, userId) => {
        Swal.fire({
            title: 'Are you sure?',
            text: 'The leader will be changed',
            showCancelButton: true,
            reverseButtons: true,
            showCloseButton: true,
            confirmButtonText: 'YES, CLOSE',
            cancelButtonText: 'NO, CANCEL',
            buttonsStyling: false,
            cancelButtonClass: 'cancel-button',
            confirmButtonClass: 'confirm-button',
            imageUrl: confirm,
            imageHeight: 200,
            imageAlt: 'Confirm',
            height: '500px',
            width: '400px',
            customClass: 'confirm-assessments',
        }).then((result) => {
            if (result.value) {
                this.props.changeGroupLeader(this.props.auth.user.institutionId, this.props.auth.user.token, assessmentId, userId)
                    .then(isOk => {
                        if (isOk) this.getAllAssessments();
                    });
            }
        })
    };

    render() {
        const {subjects, currentSubject, currentDate} = this.state;
        const {institutionId, token} = this.props.auth.user;
        const {assessmentsList, assessmentsGroups, currentAssessment, attendanceDate, attendanceUserMark} = this.props.assessments;
        const getAssessmentMark = (original, header) => {
            return <span onClick={() => {
                this.openMarkModal(original, header)
            }}>
                {
                    original['header' + header.id].mark
                        ? <span style={{color: '#448AFF'}}>{original['header' + header.id].mark}</span>
                        : <span style={{color: '#7E8E9E'}}>&ndash;</span>
                }
            </span>
        };

        const tableHeaders = assessmentsList.assessments ? assessmentsList.assessments.map(header => {
            return {

                Header: () => (
                    <div
                        className={`assessments-table__header cell ${currentAssessment.id ? header.id === currentAssessment.id ? '' : 'hide' : ''}`}>
                        <span className="assessments-table__header">{header.name}</span>
                        <img className="assessments-table__header edit"
                             src={icons.iconPencil}
                             alt="icon pencil"
                             onClick={() => {
                                 this.editAssessment(header)
                             }}
                        />
                        {currentAssessment.id ?
                            <div className="assessments-table__header weight">
                                {this.props.modals.type === MODAL_TYPES.SET_ATTENDANCE_MARK ?

                                    <DatePicker placeholder={'Choose date'}
                                                changed={(event) => {
                                                    this.selectDateHandler(event)
                                                }}
                                                selected={currentDate}
                                                maxDate={this.state.maxDate}
                                                highlightDates={this.state.highlightDates}
                                    />
                                    : `${currentAssessment.weight}%`}
                            </div>
                            : <div className="assessments-table__header__weight">
                                <p>{header.weight}%</p>
                                <span className="tooltip">Weight</span>
                            </div>
                        }
                    </div>
                ),
                accessor: 'header' + header.id,
                width: currentAssessment.id ? header.id === currentAssessment.id ? 180 : 0 : 180,

                Cell: ({original}) => {
                    if (this.props.modals.type === MODAL_TYPES.SET_ATTENDANCE_MARK &&
                        header.id === this.props.modals.action.assessmentId &&
                        header.type === ASSESMENT_TYPES.ATTENDANCE
                    ) {
                        let currentMark = attendanceUserMark.filter(item => item.userId === original.id);
                        currentMark = currentMark.length ? currentMark[0] : {
                            userId: original.id,
                            mark: 0,
                            attendanceMarkType: 0
                        };
                        return <AttendanceUser
                            studentId={this.props.modals.action.studentId}
                            assessmentId={header.id}
                            attendanceDate={currentDate}
                            userAttendance={currentMark}
                            institution={this.props.auth.user.institutionId}
                            token={this.props.auth.user.token}
                        />;
                    } else {
                        switch (original['header' + header.id].userAssessmentStatus) {
                            case 1:
                                return <NotStarted getAssessmentMark={getAssessmentMark(original, header)}
                                                   currentAssessment={currentAssessment}
                                                   headerId={header.id}
                                                   openMarkModal={() => {
                                                       this.openMarkModal(original, header)
                                                   }}
                                />;
                            case 2:
                                return <InProgress getAssessmentMark={getAssessmentMark(original, header)}
                                                   currentAssessment={currentAssessment}
                                                   headerId={header.id}
                                                   openMarkModal={() => {
                                                       this.openMarkModal(original, header)
                                                   }}
                                />;
                            case 3:
                                return <Submitted getAssessmentMark={getAssessmentMark(original, header)}
                                                  currentAssessment={currentAssessment}
                                                  headerId={header.id}
                                                  openMarkModal={() => {
                                                      this.openMarkModal(original, header)
                                                  }}
                                />;
                            case 4:
                                return <SubmittedLate getAssessmentMark={getAssessmentMark(original, header)}
                                                      currentAssessment={currentAssessment}
                                                      headerId={header.id}
                                                      openMarkModal={() => {
                                                          this.openMarkModal(original, header)
                                                      }}
                                />;
                            case 5:
                                return <NotSubmitted getAssessmentMark={getAssessmentMark(original, header)}
                                                     currentAssessment={currentAssessment}
                                                     headerId={header.id}
                                                     openMarkModal={() => {
                                                         this.openMarkModal(original, header)
                                                     }}
                                />;
                            case 6:
                                return <Done getAssessmentMark={getAssessmentMark(original, header)}
                                             currentAssessment={currentAssessment}
                                             headerId={header.id}
                                             openMarkModal={() => {
                                                 this.openMarkModal(original, header)
                                             }}
                                />;

                            default:
                                return 'Error';
                        }
                    }
                },
            }
        }) : [];

        const columns = [
            {
                Header: 'Name / Group',
                Cell: row => {
                    const isGroupWork = currentAssessment.id &&
                        ((currentAssessment.assessmentType === ASSESMENT_TYPES.ASSIGNMENT && currentAssessment.assessmentResponsibilityType === 2) ||
                            currentAssessment.assessmentType === ASSESMENT_TYPES.WORKSPACE
                        );

                    return (<div className="user-info">
                        <div className="avatar-block" onClick={() => {
                            if (isGroupWork && !row.original['header' + currentAssessment.id].isLeader) {
                                this.changeLeader(currentAssessment.id, row.original.id);
                            }
                        }
                        }>
                            {
                                row.original.avatar
                                    ? <img src={row.original.avatar} alt="avatar"/>
                                    : <div className="default-avatar"
                                           style={{backgroundColor: this.props.user.institution.color ? this.props.user.institution.color : 'rgb(0, 121, 196)'}}>
                                        {row.original.firstName.slice(0, 1)}
                                        {row.original.lastName.slice(0, 1)}
                                    </div>
                            }
                            {
                                isGroupWork ? row.original['header' + currentAssessment.id].isLeader ?
                                    <Icon name="leader-star-active"/>
                                    : <Icon name="leader-star-inactive"/>
                                    : null
                            }
                        </div>
                        <div>
                            <div className="name">{row.original.firstName} {row.original.lastName}</div>
                            <div className="pointsCount">{row.original.pointsCount}/100</div>
                            {
                                isGroupWork ? <Select placeholder='Select group'
                                                      className="select-group"
                                                      options={this.prepareGroupsOptions(assessmentsGroups)}
                                                      clicked={() => {
                                                          this.userChangeHandler(row.original.id)
                                                      }}
                                                      text={`Group #${row.original.groupId.toString().replace(/,/g, '')}`}
                                    />
                                    : null
                            }
                        </div>
                    </div>)
                },
                width: 180,
            },
            ...tableHeaders,
        ];

        const tableData = assessmentsList.students ? assessmentsList.students.map(student => {
            const studData = {
                id: student.id,
                firstName: student.firstName,
                lastName: student.lastName,
                pointsCount: student.pointsCount,
                groupId: student.userAssessments.map(item => {
                    if (currentAssessment.id ? currentAssessment.id === item.assessmentId : null) {
                        return assessmentsGroups.map(group => {
                            if (group.id === item.groupId) {
                                return group.number;
                            }
                        })
                    }
                }),
            };

            student.userAssessments.forEach(assessement => {
                studData['header' + assessement.assessmentId] = assessement;
            });

            return studData;
        }) : [];

        return (
            <Fragment>
                <AssessmentsHeader subjects={subjects}
                                   currentSubject={currentSubject}
                                   selectSubject={this.subjectChangeHandler}
                                   createNewAssessment={this.createNewAssessment}

                />
                {
                    assessmentsList.assessments ?
                        assessmentsList.assessments.length
                            ? <ReactTable
                                className={`assessments-table ${this.props.modals.type === MODAL_TYPES.SET_ATTENDANCE_MARK ? 'attendance' : ''}`}
                                data={tableData}
                                columns={columns}
                                showPagination={false}
                                minRows={0}
                                sortable={false}
                                resizable={false}
                                getTrProps={(props, rowInfo, instance) => {
                                    return {

                                        onClick: (event) => {
                                            this.rowClickHandler(event, rowInfo.original)
                                        },
                                        className: rowInfo ? rowInfo.original.id == this.props.modals.action.studentId ? 'active' : '' : '',
                                    };
                                }}
                            />
                            : <img className="no-table-data" src={noTableData} alt="No data"/> : null
                }
            </Fragment>
        );
    }
}

const mapStateToProps = ({auth, user, assessments, modals}) => ({auth, user, assessments, modals});

const mapDispatchToProps = dispatch => ({
    showModal: (modalType, titles, action) => {
        dispatch(modalActions.showModal(modalType, titles, action))
    },

    getAllSubjects: (id, token) => dispatch(actions.getAllSubjects(id, token)),
    getAllAssessments: (institutionId, subjectId, userToken) => dispatch(getAllAssessments(institutionId, subjectId, userToken)),
    getAssignmentInfo: (institutionId, assessmentId, userToken) => dispatch(getAssignmentInfo(institutionId, assessmentId, userToken)),
    getAssessmentsGroups: (institutionId, assessmentId, userToken) => dispatch(getAssessmentsGroups(institutionId, assessmentId, userToken)),
    addAssessmentGroup: (institutionId, assessmentId, userToken) => dispatch(addAssessmentGroup(institutionId, assessmentId, userToken)),
    changeAssessmentGroup: (institutionId, data, userToken) => dispatch(changeAssessmentGroup(institutionId, data, userToken)),
    setCurrentAssessment: assessment => {
        dispatch(setCurrentAssessment(assessment))
    },
    getAttendanceDate: (assessmentId, institutionId, userToken) => dispatch(getAttendanceDate(assessmentId, institutionId, userToken)),
    createUserAttendance: (token, id, data) => dispatch(createUserAttendance(token, id, data)),
    getUserAttendance: (userToken, id, assessmentId, attendanceDate) => dispatch(getUserAttendance(userToken, id, assessmentId, attendanceDate)),
    changeGroupLeader: (id, userToken, assessmentId, userId) => dispatch(changeGroupLeader(id, userToken, assessmentId, userId)),
    getStudentAttendanceStats: (token, institutionId, userId, assessmentId) => {
        dispatch(getStudentAttendanceStats(token, institutionId, userId, assessmentId))
    },

});

export default connect(mapStateToProps, mapDispatchToProps)(Assessments);
