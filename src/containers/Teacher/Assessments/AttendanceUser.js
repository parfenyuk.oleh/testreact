import React from 'react';
import ReactStart from 'react-stars';
import {createUserAttendance, getUserAttendance, getStudentAttendanceStats} from '../../../store/actions/assessments.actions';
import connect from 'react-redux/es/connect/connect';
import SetAssementMark from './SetAssementMark';

const AttendanceUser = ({assessmentId, attendanceDate, userAttendance, token, institution, createUserAttendance, getUserAttendance, getStudentAttendanceStats, studentId}) => {
  const change = (key, value) => {
    const data = {...userAttendance};
    data[key] = value;
    if (data.attendanceMarkType === 1) {
      data.mark = 0
    } else if(data.attendanceMarkType === 0) {
      data.attendanceMarkType = 2;
    }

    const outputData = {
        assessmentId: assessmentId,
        attendanceDate: attendanceDate,
        userAttendances:[data]
      };
    createUserAttendance(token, institution, outputData)
      .then(() => {
        getUserAttendance(token, institution, assessmentId, attendanceDate);
        getStudentAttendanceStats(token, institution, studentId, assessmentId)
      })
  };
  return (
    <div className="assessments-table-attendance-data">
      <ReactStart half={false}
        size={15}
        edit={userAttendance.attendanceMarkType !== 1}
        className="attendance-stars"
        value={userAttendance.mark}
        onChange={rating => { change('mark', rating)}}
        color1={'#B3C0CE'}
        color2={'#3F8BF0'}
        mouseOver={false}
      />
      <div className="switcher">
        <span className={userAttendance.attendanceMarkType === 1 ? "current-status-left": "status-left"}
              onClick={() => {change('attendanceMarkType', 1)}}>Absent</span>
        <span className={userAttendance.attendanceMarkType === 2 ? "current-status": "status-middle"}
              onClick={() => {change('attendanceMarkType', 2)}}>Present</span>
        <span className={userAttendance.attendanceMarkType === 3 ? "current-status-right": "status-right"}
              onClick={() => {change('attendanceMarkType', 3)}}>Late</span>
      </div>
    </div>
  );
};

const mapStateToProps = ({auth, user, assessments, modals}) => ({auth, user, assessments, modals});

const mapDispatchToProps = dispatch => ({
  createUserAttendance: (token, id, data) => dispatch(createUserAttendance(token, id, data)),
  getUserAttendance: (userToken, id, assessmentId, attendanceDate) => dispatch(getUserAttendance(userToken, id, assessmentId, attendanceDate)),
  getStudentAttendanceStats: (token, institutionId, userId, assessmentId) => {dispatch(getStudentAttendanceStats(token, institutionId, userId, assessmentId))},

});

export default connect(mapStateToProps, mapDispatchToProps)(AttendanceUser);
