import React, {Component, Fragment} from 'react';
import {
  clearCurrentAssessment,
  getAllAssessments,
  getStudentAttendance,
  getStudentMark,
  setStudentMark,
  getStudentAttendanceStats,
  getAssignmentsAttachments, getStudentWorkspace, getAssessmentsGroups, getWorkspaceInfo, setCurrentAssessment,
} from '../../../store/actions/assessments.actions';

import connect from 'react-redux/es/connect/connect';
import Textarea from '../../../components/UI/Textarea';
import Input from '../../../components/UI/Input';
import Icon from '../../../components/UI/Elements/Icon';
import Cell from '../../../components/UI/Cell';
import {closeWithConfirm, hideModal} from '../../../store/actions/modals.action';
import {MODAL_TYPES} from '../../../constants/modalsTypes';
import NavLink from 'react-router-dom/es/NavLink';
import Link from '../../../images/link.svg';
import File from '../../../images/doc_no_bg.svg';
import {history} from '../../../helpers';
import * as ROUTES from '../../../constants/routes';
import moment from '../../../components/Layout/SideBarForms/Workspace';
import validate from '../../../helpers/validator';

class SetAssementMark extends Component {
  state = {
    comment: {
      value: '',
      error: false,
    },
    mark: {
      value: '',
      error: false,
    },
  };

  componentDidMount() {
    const {auth, modals} = this.props;
    this.props.getAssignmentsAttachments(auth.user.institutionId, modals.action.assessmentId, modals.action.studentId, auth.user.token);
    this.props.getAssessmentsGroups(auth.user.institutionId, modals.action.assessmentId, auth.user.token)
    this.props.getStudentMark(auth.user.token, auth.user.institutionId, modals.action)
      .then(result => {
        this.setState({
          comment: {
            value: result.comment,
            error: false,
          },
          mark: {
            value: result.mark,
            error: false,
          },
        })
      });
    if (modals.type === MODAL_TYPES.SET_ATTENDANCE_MARK) {
      this.props.getStudentAttendanceStats(auth.user.token, auth.user.institutionId,  modals.action.studentId, modals.action.assessmentId);
    }
  }

  inputChangeHandler = (value, type) => {
    this.props.closeWithConfirm();
    this.setState({
      [type]: {
        value: value,
        error: false,
      },
    })
  };

  openWorkspace = (assessmentId, description, isLeader)  => {
    const groupId = this.props.assessments.assessmentsGroups[0].id;
    history.push({
      pathname: ROUTES.WORKSPACE,
      state: {assessmentId, groupId, description, isLeader},
    });
  };

  saveHandler = () => {
    const {auth, getAllAssessments, modals} = this.props;
    let {mark} = this.state;
    const errors = [];

    mark.error = validate('Mark', mark.value, {isNotEmptyDate: true, weightLimit: true, isNotEqualToZero: true});
    if (mark.error) {errors.push(mark.error)}
    this.setState({mark});

    if(!errors.length)
    this.props.setStudentMark(auth.user.token, auth.user.institutionId, {
      assessmentId: modals.action.assessmentId,
      userId: modals.action.studentId,
      comment: this.state.comment.value,
      mark: parseInt(this.state.mark.value),
    })
      .then(() => {
        getAllAssessments(auth.user.institutionId, modals.action.subjectId, auth.user.token);

        this.props.hideModal();
        this.props.clearCurrentAssessment();
      });

  };

  render() {
    const {comment, mark} = this.state;
    const {studentAttendanceStats, assignmentsAttachments} = this.props.assessments;
    const assessmentId = this.props.modals.action.assessmentId;
    let marks = null;
    if (this.props.modals.type === MODAL_TYPES.SET_ATTENDANCE_MARK) {
      marks = <Fragment>
        <div className="attendance-status" style={{marginTop: '60px'}}>
          <p>Absent</p>
          <div className="attendance-mark">
            {studentAttendanceStats.absent}
          </div>
        </div>
        <div className="attendance-status">
          <p>Present</p>
          <div className="attendance-mark">
            {studentAttendanceStats.present}
          </div>
        </div>
        <div className="attendance-status">
          <p>Late</p>
          <div className="attendance-mark">
            {studentAttendanceStats.late}
          </div>
        </div>
        <div className="attendance-status">
          <p>Average mark</p>
          <div className="attendance-mark">
            {studentAttendanceStats.averageMark}
          </div>
        </div>
        <div className="attendance-status">
          <p>Count of marks</p>
          <div className="attendance-mark">
            {studentAttendanceStats.marksCount}
          </div>
        </div>
      </Fragment>
    }
    return (
      <div className='sidebar-content'>
        <div className="popup-container">
          <div className='form-container set-assessment-mark'>
            <Cell column={7}>
              <Input placeholder="Mark"
                     value={mark.value}
                     changed={(e) => {this.inputChangeHandler(e.target.value, 'mark') }}
                     className={mark.error ? 'error' : ''}
                     modalError={mark.error}
                     type={'number'}
              />
              <div className={'tool-tip ' + (mark.error ? 'error' : '' )}>
                <Icon name='info'/>
                <div className="tool-tip-text">
                  The mark can be from 1 to 100
                </div>
              </div>
            </Cell>
            {this.props.modals.type !== MODAL_TYPES.SET_ATTENDANCE_MARK && <Textarea placeholder="Comment"
                                                                                     text={comment.value}
                                                                                     changed={(e) => {this.inputChangeHandler(e.target.value, 'comment') }}
                                                                                     error={comment.error}
                                                                                     modalError={comment.error}
            />}
            {marks}
            {this.props.modals.type === MODAL_TYPES.SET_ASSIGNMENT_MARK && <div className="student-assessment">
              <div className="dropzone">
                {assignmentsAttachments ?
                  assignmentsAttachments.map(attachments =>(
                      <div className="img-background">
                        <div className="img-wrapper">
                          {attachments.attachmentType === 1 ?
                              <a target="_blank"
                                 href={attachments.url}>
                                <img src={Link}
                                     alt={`${attachments.url}`}
                                     className="uploaded-file"
                                     title={`${attachments.url}`}
                                />
                              </a> :
                              <a href={`${attachments.url}`}
                                 target="_blank"
                                 download={`${attachments.attachmentHeading}`}>
                                <img src={File}
                                     alt={`${attachments.attachmentHeading}`}
                                     className="uploaded-file"
                                     title={`${attachments.attachmentHeading}`}
                                />
                              </a>
                          }
                        </div>
                        <span>{attachments.attachmentType === 1 ? attachments.url : attachments.attachmentHeading}
                          <div className="tool-tip-text">{attachments.attachmentHeading}</div></span>
                      </div>
                  )):
                  null
                }
              </div>
            </div>}
            {this.props.modals.type === (MODAL_TYPES.SET_WORKSPACE_MARK) &&
            <div onClick={() => {this.openWorkspace(assessmentId, null, 3)}} className='workspace-link'>
              <Icon name='workspaceSubmit'/>
              <span>Workspace</span>
            </div>}
          </div>

        </div>

        <div className="bottomButton">
          <button
            type="button"
            className="btn-modal"
            onClick={this.saveHandler}>
            Submit
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({auth, modals, assessments}) => ({auth, modals, assessments});

const mapDispatchToProps = dispatch => ({
  getStudentMark: (token, id, params) => dispatch(getStudentMark(token, id, params)),
  setStudentMark: (token, id, params) => dispatch(setStudentMark(token, id, params)),
  getAllAssessments: (institutionId, subjectId, userToken) => dispatch(getAllAssessments(institutionId, subjectId, userToken)),
  hideModal: () => {dispatch(hideModal())},
  clearCurrentAssessment: () => {dispatch(clearCurrentAssessment())},
  setCurrentAssessment: assessment => {dispatch(setCurrentAssessment(assessment))},
  getAssignmentsAttachments: (institutionId, assessmentId, studentId, userToken) => {dispatch(getAssignmentsAttachments(institutionId, assessmentId, studentId, userToken))},
  getStudentAttendanceStats: (token, institutionId, userId, assessmentId) => {dispatch(getStudentAttendanceStats(token, institutionId, userId, assessmentId))},
  getStudentAttendance: (institutionId, assessmentId, userToken) => dispatch(getStudentAttendance(institutionId, assessmentId, userToken)),
  getAssessmentsGroups: (institutionId, assessmentId, userToken) => dispatch(getAssessmentsGroups(institutionId, assessmentId, userToken)),
  getWorkspaceInfo: (institutionId, workspaceId, userToken) => dispatch(getWorkspaceInfo(institutionId, workspaceId, userToken)),
  closeWithConfirm: () => {dispatch(closeWithConfirm())}


});

export default connect(mapStateToProps, mapDispatchToProps)(SetAssementMark);