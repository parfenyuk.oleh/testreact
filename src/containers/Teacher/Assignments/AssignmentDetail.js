import React, {Component, Fragment} from 'react';
import {Redirect, withRouter} from 'react-router';
import {Link} from 'react-router-dom';
import * as routeConstants from '../../../constants/routes';
import Icon from '../../../components/UI/Elements/Icon';
import DetailInfo from '../../../components/Teacher/Assigments/DetailInfo';
import {connect} from 'react-redux';
import {ASSIGNMENTS} from '../../../constants/routes';
import * as actions from '../../../store/actions/index';
import {userAuthToken} from '../../../helpers/index';
import DetailInfoTable from '../../../components/Teacher/Assigments/DetailInfo/DetailInfoTable';
import ReviewMarkModal from '../../../components/Teacher/Assigments/Modals/ReviewMarkModal';
import withSure from '../../../hoc/withSure';
import NewAssignmentModal from '../../../components/Teacher/Assigments/Modals/NewAssignmentModal';
import moment from 'moment';
import {TABS} from '../../../constants/assignments';
import AssignmentsTable from '../../../components/Teacher/Assigments/AssignmentsTable';


class AssignmentDetail extends Component {

  constructor(props) {
    super(props);
    const {activeAssignments, archiveAssignments} = props.assignments;
    const currentAssignment = [...activeAssignments, ...archiveAssignments]
      .find(item => +item.id === +props.match.params.id);
    this.state = {
      currentAssignment: currentAssignment || {},
      reviewItem: null,
      showReviewMarkModal: false,
      wasChanged: false,
      showNewAssignmentModal: false,
      restoreAssignmentMode: false,
      showFullDescription: false,
      pagination: 1,
    }
  }

  /**
   * Update assignment model if it was updated, archived or restored
   * @param props
   * @param state
   * @returns {*}
   */
  static getDerivedStateFromProps(props, state) {
    if (state.currentAssignment.id) {
      const {activeAssignments, archiveAssignments} = props.assignments;
      const currentAssignment = [...activeAssignments, ...archiveAssignments]
        .find(item => +item.id === +props.match.params.id);
      return {currentAssignment, wasChanged: false}
    }
    return null;
  }

  componentDidMount() {
    this.props.clearSubmission();
    if (this.state.currentAssignment.id) {
      this.props.getAllSubmissions(userAuthToken.token(),
        this.props.auth.user.institutionId,
        this.state.currentAssignment.id,
        0,
        10,
      );
    }
  }

  toggleShowFullDescription = () => {this.setState({showFullDescription: !this.state.showFullDescription})};

  toggleReviewMarkModal = () => {
    this.setState(prevProps => { return {showReviewMarkModal: !prevProps.showReviewMarkModal}})
  };

  onReviewStart = item => {
    this.setState({
      showReviewMarkModal: true,
      reviewItem: item,
    })
  };

  onReviewSave = item => {
    this.props.setReview(userAuthToken.token(),
      this.props.auth.user.institutionId,
      this.state.currentAssignment.id,
      item,
    );
    this.setState({
      showReviewMarkModal: false,
      reviewItem: null,
    })
  };

  deleteAssignmentHandler = (id) => {
    this.props.showSureModal('Are you sure you want to delete this assignment?', 'Yes, Delete')
      .then(() => {
        const {institutionId} = this.props.auth.user;
        this.props.deleteAssignment(userAuthToken.token(), institutionId, id);
        this.setState({currentAssignment: {}})
      })
      .catch(error => {
      })
  };

  archiveAssignment = (item) => {
    this.props.showSureModal('Are you sure you want to archive this assignment?', 'Yes, Archive')
      .then(() => {
        const {institutionId} = this.props.auth.user;
        this.props.archiveAssignment(userAuthToken.token(), institutionId, item);
        this.setState({wasChanged: true})
      })
  };

  updateAssignment = (model) => {
    const {institutionId} = this.props.auth.user;
    this.setState({
      showNewAssignmentModal: false,
      restoreAssignmentMode: false,
      wasChanged: true,
    });
    this.props.updateAssignment(userAuthToken.token(), institutionId, model, this.state.restoreAssignmentMode);
  };

  toggleNewAssignmentModal = () => {this.setState({showNewAssignmentModal: !this.state.showNewAssignmentModal})};

  restoreAssignment = () => {
    this.props.showSureModal('Are you sure you want to restore this assignment?', 'Yes, Restore')
      .then(() => {
        const item = this.state.currentAssignment;
        if (moment(item.endDate).isBefore(moment())) {
          this.setState({
            restoreAssignmentMode: true,
            showNewAssignmentModal: true,
          })
        }
        else {
          const {institutionId} = this.props.auth.user;
          this.props.restoreAssignment(userAuthToken.token(), institutionId, item.id)
        }
      })
  };

  loadMoreSubmissions = () => {
    let {currentAssignment, pagination} = this.state;
    this.props.getAllSubmissions(userAuthToken.token(),
      this.props.auth.user.institutionId,
      currentAssignment.id,
      pagination++ * 10,
      10,
    );

    this.setState({pagination});
  };

  updateSubmission = (field, value, userId) => {
    const {institutionId} = this.props.auth.user;
    this.props.updateSubmission(userAuthToken.token(), userId, field, this.state.currentAssignment.id, value, institutionId);
  };

  render() {
    const {
      currentAssignment, showReviewMarkModal, reviewItem, showNewAssignmentModal,
      restoreAssignmentMode, showFullDescription,
    } = this.state;
    if (Object.entries(currentAssignment).length === 0) return <Redirect to={ASSIGNMENTS}/>;
    else return (
      <Fragment>
        {reviewItem && <ReviewMarkModal show={showReviewMarkModal}
                                        hideModal={this.toggleReviewMarkModal}
                                        assignment={currentAssignment}
                                        submission={reviewItem}
                                        saved={this.onReviewSave}
        />}
        <NewAssignmentModal show={showNewAssignmentModal}
                            hideModal={this.toggleNewAssignmentModal}
                            subjects={this.props.admin.subject}
                            changeModel={currentAssignment}
                            update={this.updateAssignment}
                            restore={restoreAssignmentMode}
        />
        <div className="assignment-detail">
          <div className="assignment-detail__header">
            <Link to={`${routeConstants.ASSIGNMENTS}`}>
              <Icon name="left-arrow"/>
              <span>All assignments</span>
            </Link>
            <DetailInfo currentAssignment={currentAssignment}
                        deleteAssignment={this.deleteAssignmentHandler}
                        archiveAssignment={this.archiveAssignment}
                        editAssignment={this.toggleNewAssignmentModal}
                        restoreAssignment={this.restoreAssignment}
                        showFullDescription={showFullDescription}
                        onReadMore={this.toggleShowFullDescription}
            />
          </div>
          <DetailInfoTable list={this.props.assignments.submissions}
                           onReview={this.onReviewStart}
                           onPaginated={() => {
                             this.loadMoreSubmissions()
                           }}
                           isLoading={this.props.assignments.loading}
                           updateSubmission={this.updateSubmission}
          />

        </div>
      </Fragment>
    );
  }
}

AssignmentDetail.propTypes = {};

const mapStateToProps = ({auth, assignments, admin}) => ({auth, assignments, admin});

const mapDispatchToProps = dispatch => ({
  getAllSubmissions: (token, institutionId, assignmentId, skip, take) =>
    dispatch(actions.getAllSubmissions(token, institutionId, assignmentId, skip, take)),
  setReview: (token, institutionId, assignmentId, data) => dispatch(actions.setReview(token, institutionId, assignmentId, data)),
  deleteAssignment: (token, institutionId, assignmentId) => {
    dispatch(actions.deleteAssignment(token, institutionId, assignmentId))
  },
  updateAssignment: (token, id, data, restore) => {dispatch(actions.updateAssignment(token, id, data, restore))},
  getAssignmentsAmount: (token, id) => {dispatch(actions.getAssignmentsAmount(token, id))},
  archiveAssignment: (token, id, data) => {dispatch(actions.archiveAssignment(token, id, data))},
  restoreAssignment: (token, id, data) => {dispatch(actions.restoreAssignment(token, id, data))},
  clearSubmission: () => {dispatch(actions.clearSubmissions())},
  updateSubmission: (token, userId, field, assignmentId, value, institutionId) => {
    dispatch(actions.updateSubmission(token, userId, field, assignmentId, value, institutionId));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(withSure(AssignmentDetail)));