import React, {Component} from 'react';
import PropTypes from 'prop-types';
import * as actions from '../../../store/actions';
import {compose} from 'recompose';
import {withRouter} from 'react-router';
import connect from 'react-redux/es/connect/connect';
import {Link} from 'react-router-dom';
import * as ROUTES from '../../../constants/routes';
import Icon from '../../../components/UI/Elements/Icon';
import {userAuthToken} from '../../../helpers';
import EditorController from '../../../components/Editor/EditorController';

class AssignmentResult extends Component {

  state = {
    wordsCount: {
      total: 0,
      users: {},
    },
  };

  componentDidMount() {
    const {auth, match} = this.props;
    this.props.getWorkspaceInfo(auth.user.institutionId, userAuthToken.token(), match.params.id);
  }

  handleWordsCountChange = wordsCount => { this.setState({wordsCount})};


  render() {
    const {id} = this.props.match.params;
    const {wordsCount} = this.state;
    const {workspace} = this.props.assignments;
    return (
      <div className="workspace">
        <div className="workspace__header">
          <Link to={`${ROUTES.ASSIGNMENTS}/${id}`}>
            <Icon name="left-arrow"/>
            <span>Assignments detail</span>
          </Link>
        </div>
        <div className="workspace-body">
          <header>
            <h3>Total Words goal {wordsCount.total}/{workspace.wordsLimit}</h3>
          </header>
          <div className="workspace-body-content teacher">
            <div className="workspace-users">
              <div className="workspace-users-list">
                {
                  workspace.students && workspace.students.map(student => {
                    return <p className={`item`}
                              style={{color: student.color}}
                              key={student.fullName + student.id}
                    >
                      {student.fullName} {wordsCount.users[student.id] ? wordsCount.users[student.id] : 0}/{Math.floor(workspace.wordsLimit / workspace.students.length)}
                    </p>
                  })
                }
              </div>
            </div>
            <EditorController assignmentId={id}
                              token={this.props.auth.user.token}
                              students={workspace.students}
                              userId={this.props.auth.user.id}
                              colorCoding={true}
                              onWordsCountChange={this.handleWordsCountChange}
                              disabled={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

AssignmentResult.propTypes = {};


const mapStateToProps = ({auth, assignments}) => ({auth, assignments});

const mapDispatchToProps = dispatch => ({
  getWorkspaceInfo: (institutionId, token, assignmentId) => {
    dispatch(actions.getWorkspaceInfo(institutionId, token, assignmentId))
  },
});

export default compose(
  withRouter,
  connect(mapStateToProps, mapDispatchToProps),
)(AssignmentResult)