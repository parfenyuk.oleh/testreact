import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import * as actions from '../../../store/actions'
import {userAuthToken} from '../../../helpers/index';

import AssignmentsHeader from '../../../components/Teacher/Assigments/AssignmentsHeader';
import AssignmentsSidebar from '../../../components/Teacher/Assigments/AssignmentsSidebar';
import AssignmentsTable from '../../../components/Teacher/Assigments/AssignmentsTable';
import NewAssignmentModal from '../../../components/Teacher/Assigments/Modals/NewAssignmentModal';
import withSure from '../../../hoc/withSure';
import {TABS} from '../../../constants/assignments';
import moment from 'moment';

/**
 * Constant for all types of assignment
 *
 * @type {{active: number, archive: number}}
 */

const DEFAULT_PAGINATION = {
  active: 2,
  archive: 2,
};

class Assignments extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentTab: TABS.active,
      checkedGroups: new Map(),
      showNewAssignmentModal: false,
      pagination: {...DEFAULT_PAGINATION},
      filterIds: null,
      changeModel: null,
      restoreAssignmentMode: false,
    };
  }

  componentDidMount() {
    const {institutionId, token} = this.props.auth.user;
    this.props.clearAssignments();
    this.props.getAssignmentsAmount(userAuthToken.token(), institutionId);
    this.props.getAllGroups(userAuthToken.token(), institutionId);
    this.props.getAllSubjects(institutionId, token);
    this.props.getAllAssignments(userAuthToken.token(), institutionId, TABS.active, this.state.filterIds, 0, 20);
    this.props.getAllAssignments(userAuthToken.token(), institutionId, TABS.archive, this.state.filterIds, 0, 20);
  }


  /**
   * toggle visibility of New Assignment Modal
   */
  toggleNewAssignmentModal = () => {
    this.setState({
      showNewAssignmentModal: !this.state.showNewAssignmentModal,
      changeModel: null,
    })
  };

  /**
   * Change tab handler which affect the filter of assignments
   *
   * @param tab (selected tab id)
   */
  changeTabHandler = (tab) => {
    this.setState({
      currentTab: tab,
    })
  };

  setAssignmentWillBeChanged = item => {
    this.setState({
      changeModel: item,
      showNewAssignmentModal: true,
    })
  };

  createNewAssignmentHandler = model => {
    const {institutionId} = this.props.auth.user;

    this.props.createNewAssignment(userAuthToken.token(), institutionId, model)
      .then(res => {
        if (res.status) {
          this.props.clearAssignments();
          this.props.getAllAssignments(userAuthToken.token(), institutionId, TABS.active, null, 0, 20);
          this.props.getAllAssignments(userAuthToken.token(), institutionId, TABS.archive, null, 0, 20);
          this.setState({
            currentTab: TABS.active,
            checkedGroups: new Map(),
            pagination: {...DEFAULT_PAGINATION},
            filterIds: null,
          })
        }
      });
    this.setState({showNewAssignmentModal: false})
  };

  updateAssignment = (model) => {
    const {institutionId} = this.props.auth.user;
    this.props.updateAssignment(userAuthToken.token(), institutionId, model, this.state.restoreAssignmentMode,{
      ...this.state.pagination,
    });
    this.setState({
      showNewAssignmentModal: false,
      changeModel: null,
      restoreAssignmentMode: false,
    });

  };

  deleteAssignmentHandler = (id) => {
    this.props.showSureModal('Are you sure you want to delete this assignment?', 'Yes, Delete')
      .then(() => {
        const {institutionId} = this.props.auth.user;
        this.props.deleteAssignment(userAuthToken.token(), institutionId, id);
      })
      .catch(error => {
      })
  };

  handleFilterChange = (e) => {
    const {institutionId} = this.props.auth.user;
    const item = e.target.name;
    const isChecked = e.target.checked;

    this.setState(prevState => {
      const checkedGroups = prevState.checkedGroups.set(item, isChecked);
      const filterIds = [];
      checkedGroups.forEach((value, key) => {
        if (value) filterIds.push(+key.split('-').slice(-1)[0]);
      });

      this.props.clearAssignments();
      this.props.getAllAssignments(userAuthToken.token(), institutionId, TABS.active, filterIds, 0, 20);
      this.props.getAllAssignments(userAuthToken.token(), institutionId, TABS.archive, filterIds, 0, 20);
      return {
        checkedGroups,
        filterIds,
        pagination: {...DEFAULT_PAGINATION},
      }
    });

  };

  loadMoreMembers = type => {
    const {institutionId} = this.props.auth.user;
    const {pagination, filterIds} = this.state;

    if (type === TABS.active) {
      this.props.getAllAssignments(userAuthToken.token(), institutionId, type, filterIds, pagination.active++ * 10, 10);
    }
    if (type === TABS.archive) {
      this.props.getAllAssignments(userAuthToken.token(), institutionId, type, filterIds, pagination.archive++ * 10, 10);
    }

    this.setState({pagination});
  };

  restoreAssignment = (item) => {
    this.props.showSureModal('Are you sure you want to restore this assignment?', 'Yes, Restore')
      .then(() => {
        if (moment(item.endDate).isBefore(moment())) {
          this.setState({
            changeModel: item,
            restoreAssignmentMode: true,
            showNewAssignmentModal: true,
          })
        }
        else {
          const {institutionId} = this.props.auth.user;
          this.props.restoreAssignment(userAuthToken.token(), institutionId, item.id)
        }
      })
  };

  archiveAssignment = (item) => {
    this.props.showSureModal('Are you sure you want to archive this assignment?', 'Yes, Archive')
      .then(() => {
        const {institutionId} = this.props.auth.user;
        this.props.archiveAssignment(userAuthToken.token(), institutionId, item);
      })
  };

  render() {
    const {currentTab, checkedGroups, showNewAssignmentModal, changeModel, restoreAssignmentMode} = this.state;
    const {groups, admin, assignments} = this.props;

    return (
      <Fragment>
        <AssignmentsHeader groups={groups.list}
                           checkedGroups={checkedGroups}
                           createNewAssignment={this.toggleNewAssignmentModal}
                           handleFilterChange={this.handleFilterChange}
        />
        <NewAssignmentModal show={showNewAssignmentModal}
                            hideModal={this.toggleNewAssignmentModal}
                            subjects={admin.subject}
                            saved={this.createNewAssignmentHandler}
                            changeModel={changeModel}
                            update={this.updateAssignment}
                            restore={restoreAssignmentMode}
        />

        <AssignmentsSidebar TABS={TABS}
                            currentTab={currentTab}
                            changeTab={this.changeTabHandler}
                            amounts={assignments.amounts}
        />

        <div className="assignment-content" style={{maxWidth: 'calc(100% - 230xp)'}}>
          {/*{assignments.loading && <div className="assignment-loader"><Spinner/></div>}*/}
          {currentTab === TABS.active && <AssignmentsTable list={assignments.activeAssignments}
                                                           archiveAssignment={this.archiveAssignment}
                                                           changeAssignment={this.setAssignmentWillBeChanged}
                                                           onPaginated={() => {
                                                             this.loadMoreMembers(TABS.active)
                                                           }}
                                                           isLoading={this.props.assignments.loading}
          />
          }
          {currentTab === TABS.archive && <AssignmentsTable list={assignments.archiveAssignments}
                                                            changeAssignment={this.setAssignmentWillBeChanged}
                                                            onPaginated={() => {
                                                              this.loadMoreMembers(TABS.archive)
                                                            }}
                                                            isLoading={this.props.assignments.loading}
                                                            restoreAssignment={this.restoreAssignment}
                                                            deleteAssignment={this.deleteAssignmentHandler}
          />
          }
        </div>
      </Fragment>
    );
  }
}


const mapStateToProps = ({auth, groups, admin, assignments}) => ({auth, groups, admin, assignments});

const mapDispatchToProps = dispatch => ({
  getAllGroups: (token, id) => {dispatch(actions.getAllGroups(token, id))},
  getAllSubjects: (id, token) => {dispatch(actions.getAllSubjects(id, token))},
  getAllAssignments: (token,
                      institutionId,
                      isArchive,
                      groupIds,
                      skip,
                      take,
                      search) => {
    dispatch(actions.getAllAssignments(token,
      institutionId,
      isArchive,
      groupIds,
      skip,
      take,
      search))
  },
  createNewAssignment: (token, id, data) => {return dispatch(actions.createNewAssignment(token, id, data))},
  clearAssignments: () => {dispatch(actions.clearAssignments())},
  deleteAssignment: (token, institutionId, assignmentId) => {
    dispatch(actions.deleteAssignment(token, institutionId, assignmentId))
  },
  updateAssignment: (token, id, data, restore, pagination) => {
    dispatch(actions.updateAssignment(token, id, data, restore, pagination));
  },
  getAssignmentsAmount: (token, id) => {dispatch(actions.getAssignmentsAmount(token, id))},
  archiveAssignment: (token, id, data) => {dispatch(actions.archiveAssignment(token, id, data))},
  restoreAssignment: (token, id, data) => {dispatch(actions.restoreAssignment(token, id, data))},
});

export default connect(mapStateToProps, mapDispatchToProps)(withSure(Assignments));