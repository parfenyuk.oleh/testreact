import React, {Component, Fragment} from 'react';
import moment from 'moment';
import {connect} from 'react-redux';

import * as actions from '../../../store/actions';
import AttendanceHeader from '../../../components/Teacher/Attendance/AttendanceHeader';
import Calendar from './Calendar/Calendar';
import AttendanceTable from '../../../components/Teacher/Attendance/AttendanceTable';
import {userAuthToken} from '../../../helpers';
import {ATTENDANCE_MARKS} from '../../../constants/attendance';


class Attendance extends Component {
  constructor(props) {
    super(props);

    const currentDate = moment();
    this.state = {
      selectedDate: currentDate,
      chosenSubject: null,
      pagination: 1,
      subjectFilter: '',
      currentDate,
      changeMode: false,
    };

  }

  daySelectHandler = (date) => {
    this.setState({
      selectedDate: date,
      changeMode: false,
    }, this.getAttendances);
    return true;
  };

  todayHandler = () => {
    this.setState({
      selectedDate: this.state.currentDate,
      pagination: 1,
      changeMode: false,
    }, this.getAttendances);
  };

  changeSubjectHandler = subject => {
    this.setState({
      chosenSubject: subject.id,
      pagination: 1,
      changeMode: false,
    }, this.getAttendances);
  };

  getAttendances = () => {
    const {selectedDate, chosenSubject} = this.state;
    const {institutionId} = this.props.auth.user;
    this.props.clearAttendancesList();

    this.setState({changeMode: false, pagination: 1});

    this.props.getAttendancesList(userAuthToken.token(),
      institutionId,
      selectedDate.format('L'),
      chosenSubject,
      ATTENDANCE_MARKS.ALL,
      0,
      10,
    )
  };

  searchSubjectHandler = data => {this.setState({subjectFilter: data.target.value})};
  filterSubject = subject => subject.filter(item => item.name.indexOf(this.state.subjectFilter) !== -1 && !item.archievedAt);

  saveAttendancesHandler = () => {
    const {chosenSubject} = this.state;
    const {auth, attendance, saveAttendances} = this.props;
    if (chosenSubject) {
      saveAttendances(auth.user.institutionId,
        userAuthToken.token(),
        attendance.list,
        chosenSubject,
      )
    }
  };

  markAllHandler = item => {
    this.props.markAll(item.value);
  };

  handlePaginate = () => {
    const {selectedDate, chosenSubject, pagination} = this.state;
    const {institutionId} = this.props.auth.user;
    this.props.getAttendancesList(userAuthToken.token(),
      institutionId,
      selectedDate.format('L'),
      chosenSubject,
      ATTENDANCE_MARKS.ALL,
      pagination * 10,
      (pagination + 1) * 10,
    );
    this.setState({pagination: pagination + 1});
  };

  componentDidMount() {
    const {institutionId, token} = this.props.auth.user;
    this.props.getAllSubjects(institutionId, token);
    this.props.clearAttendancesList();
  }

  handleChangeMode = () => { this.setState({changeMode: true})};

  render() {
    const {selectedDate, subjectFilter} = this.state;
    const {admin, attendance} = this.props;
    return (
      <Fragment>
        <AttendanceHeader currentDay={selectedDate.format('dddd MMM D, YYYY')}/>
        <div className="attendance-body">
          <Calendar onSelect={this.daySelectHandler}
                    date={selectedDate}
                    onToday={this.todayHandler}
          />
          <AttendanceTable subjectList={this.filterSubject(admin.subject)}
                           onSubjectChoose={this.changeSubjectHandler}
                           onSubjectSearch={this.searchSubjectHandler}
                           searchValue={subjectFilter}
                           attendances={attendance.list}
                           isBeforeNow={selectedDate.isBefore(moment(), 'day')}
                           isLoading={attendance.loading}
                           updateAttendanceLocal={this.props.updateAttendanceLocal}
                           onCancel={this.getAttendances}
                           onSave={this.saveAttendancesHandler}
                           onMarkAll={this.markAllHandler}
                           changeMode={this.state.changeMode}
                           onChangeMode={this.handleChangeMode}
                           onPaginate={this.handlePaginate}
          />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = ({auth, admin, attendance}) => ({auth, admin, attendance});

const mapDispatchToProps = dispatch => () => ({
  getAttendancesList: (token, institutionId, dateTime, subjectId, attendanceMark, skip, take) => {
    dispatch(actions.getAttendancesList(token, institutionId, dateTime, subjectId, attendanceMark, skip, take))
  },
  getAllSubjects: (id, token) => {dispatch(actions.getAllSubjects(id, token))},
  clearAttendancesList: () => {dispatch(actions.clearAttendancesList())},
  updateAttendanceLocal: (userId, filed, value) => {dispatch(actions.updateAttendanceLocal(userId, filed, value))},
  saveAttendances: (institutionId, token, attendances, subjectId) => {
    dispatch(actions.saveAttendances(institutionId, token, attendances, subjectId))
  },
  markAll: mark => {dispatch(actions.markAll(mark))},
});

export default connect(mapStateToProps, mapDispatchToProps)(Attendance);