import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

const DayOfWeek = (props) => {
  const { date, format } = props;

  return (
    <div className="DayOfWeek">
      {date.format(format)}
    </div>
  );
};

DayOfWeek.propTypes = {
  date: PropTypes.instanceOf(moment).isRequired,
  format: PropTypes.string,
};

export default DayOfWeek;
