import React from 'react';

const Week = ({ children }) => <div className="calendar-row">{children}</div>;

export default Week;
