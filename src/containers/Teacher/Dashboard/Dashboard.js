import React, {Component} from 'react';

import Table from '../../../components/UI/Table/Table';
import Cell from '../../../components/UI/Cell';
import TableHeader from '../../../components/UI/Table/TableHeader';
import TableRow from '../../../components/UI/Table/TableRow';
import AvatarBlock from '../../../components/UI/Elements/AvatarBlock';
import Select from '../../../components/UI/Select';
import ReactStart from 'react-stars';
import * as actions from '../../../store/actions';
import connect from 'react-redux/es/connect/connect';
import moment from 'moment';

const DATE_TYPES = {
  ALL: 'All',
  WEEK: 'Week',
  MONTH: 'Month',
  YEAR: 'Year',
};

class Dashboard extends Component {

  constructor(props) {
    super(props);

    this.state = {
      searchValue: '',
      currentSubject: null,
      currentAssignment: {},
      date: DATE_TYPES.ALL,
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props.admin.subject.length && state.currentSubject === null) {
      return {
        currentSubject: props.admin.subject[0],
      }
    }
    return null;
  }


  subjectChangeHandler = subject => {
    const user = JSON.parse(localStorage.getItem('user'));
    this.props.getDashboardAssignments(user.institutionId, user.token, [subject.id]).then(assignments => {
      const dates = this.getDates(this.state.date);
      this.props.getTeacherData(user.institutionId,
        user.token,
        subject.id,
        null,
        dates.start,
        dates.end);
    });
    this.setState({
      currentSubject: subject,
    })
  };

  assignmentHandler = assignment => {
    const user = JSON.parse(localStorage.getItem('user'));
    const dates = this.getDates(this.state.date);
    this.props.getTeacherData(user.institutionId,
      user.token,
      this.state.currentSubject.id,
      assignment.id,
      dates.start,
      dates.end);
  };

  getDates = type => {
    const now = moment();

    switch (type) {
      case DATE_TYPES.WEEK: {
        return {
          end: now.format(),
          start: now.subtract(7, 'days').format(),
        }
      }
      case DATE_TYPES.MONTH: {
        return {
          end: now.format(),
          start: now.subtract(1, 'months').format(),
        }
      }
      case DATE_TYPES.YEAR: {
        return {
          end: now.format(),
          start: now.subtract(1, 'years').format(),
        }
      }

      default:
        return {
          start: null,
          end: null,
        }
    }

  };

  dateChangeHandler = ({name}) => {
    const assignmentId = this.state.currentAssignment.id || null;
    const user = JSON.parse(localStorage.getItem('user'));


    const dates = this.getDates(name);
    this.setState({date: name});
    this.props.getTeacherData(user.institutionId,
      user.token,
      this.state.currentSubject.id,
      assignmentId,
      dates.start,
      dates.end,
    );
  };

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));
    this.props.getAllSubjects(user.institutionId, user.token).then(subjects => {
      if (subjects.length) {
        this.subjectChangeHandler(subjects[0]);
      }
    });
  }

  render() {
    const {currentSubject, currentAssignment} = this.state;
    const {teacherData} = this.props.dashboard;

    return (
      <div className="dashboard">
        <div className="dashboard-header">
          <h1 className="page-title ">Teacher’s dashboard</h1>
          <div className="dashboard-header-selects">
            {currentSubject && <Select placeholder="Select subject"
                                       options={this.props.admin.subject}
                                       clicked={this.subjectChangeHandler}
                                       text={currentSubject.name}
            />
            }
            <Select placeholder="Select assignment"
                    options={this.props.dashboard.assignments}
                    clicked={this.assignmentHandler}
                    text={currentAssignment.name}
            />
            <Select placeholder="Select date"
                    options={[
                      {name: DATE_TYPES.ALL},
                      {name: DATE_TYPES.WEEK},
                      {name: DATE_TYPES.MONTH},
                      {name: DATE_TYPES.YEAR},
                    ]}
                    clicked={this.dateChangeHandler}
            />
          </div>
        </div>
        <div className="dashboard-rating">
          <div className="dashboard-rating-header">Rating</div>
          <div className="dashboard-rating-content">
            <div className="dashboard-rating-block">
              <p className="dashboard-card-number">{teacherData.mean || 0}</p>
              <p className="dashboard-card-title">Mean</p>
            </div>
            <div className="dashboard-rating-block">
              <p className="dashboard-card-number">{teacherData.median || 0}</p>
              <p className="dashboard-card-title">Median</p>
            </div>
            <div className="dashboard-rating-block">
              <p className="dashboard-card-number">{teacherData.mode || 0}</p>
              <p className="dashboard-card-title">Mode</p>
            </div>
            <div className="dashboard-rating-block">
              <p className="dashboard-card-number">{teacherData.range || 0}</p>
              <p className="dashboard-card-title">Range</p>
            </div>
          </div>
        </div>
        <Table className="dashboard-table">
          <TableHeader>
            <Cell column={4}>Name</Cell>
            <Cell column={4}>Rating</Cell>
            <Cell column={3}>Late</Cell>
            <Cell column={3}>Absent</Cell>
            <Cell column={2}>Mark</Cell>
          </TableHeader>
          <div className="dashboard-table-body">

            {teacherData.dashboardUserReviews && teacherData.dashboardUserReviews.map(item =>
              <TableRow key={item.late + item.rating}>
                <Cell column={4}>
                  <AvatarBlock textAvatar={`${item.studentFirstName[0]}${item.studentLastName[0]}`}
                               customInfo={<span>{`${item.studentFirstName} ${item.studentLastName}`}</span>}
                               imgSrc={item.avatar}
                  />
                </Cell>
                <Cell column={4}>
                  <ReactStart half={false}
                              size={20}
                              onChange={rating => { }}
                              value={item.rating}
                              edit={false}
                  />
                </Cell>
                <Cell column={3}>
                  {item.late}
                </Cell>
                <Cell column={3}>
                  {item.absent}
                </Cell>
                <Cell column={2}>
                  {item.mark}
                </Cell>
              </TableRow>)
            }

          </div>
        </Table>
      </div>
    )
  }
}

const mapStateToProps = ({admin, dashboard}) => ({admin, dashboard});

const mapDispatchToProps = dispatch => ({
  getAllSubjects: (id, token) => {return dispatch(actions.getAllSubjects(id, token))},
  getDashboardAssignments: (id, token, subjectIds) => {
    return dispatch(actions.getDashboardAssignments(id, token, subjectIds))
  },
  getTeacherData: (institutionId, token, subjectId, assignmentId, startDate, endDate) => {
    dispatch(actions.getTeacherData(institutionId, token, subjectId, assignmentId, startDate, endDate))
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
