import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../../../components/UI/Elements/Icon';
import RoundAvatar from '../../../components/UI/RoundAvatar';

const EventMembers = ({membersAmount, onClose, list, transform}) => {
  return (
    <div className="events-info-members" style={{transform}}>
      <div className="events-info-members-cross">
        <Icon name="cross" onClick={onClose}/>
      </div>
      <div className="events-info-members-title">
        {membersAmount} members
      </div>
      <div className="events-info-members-list">
        {list.map(item => (
          <div key={item.firstName + item.lastName}>
            <RoundAvatar avatar={item.avatar}
                         text={`${item.firstName[0]} ${item.lastName[0]}`}/>
            <span>{`${item.firstName} ${item.lastName}`}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

EventMembers.propTypes = {
  membersAmount: PropTypes.number,
  onClose: PropTypes.func,
  list: PropTypes.array,
};

export default EventMembers;