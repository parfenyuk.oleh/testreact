import React, {Component} from 'react';
import PropTypes from 'prop-types';
import FullCalendar from 'fullcalendar-reactwrapper';
import {connect} from 'react-redux';

import * as actions from '../../../store/actions';
import 'fullcalendar-reactwrapper/dist/css/fullcalendar.min.css';
import EventsModal from '../../../components/Teacher/Events/EventsModal';
import {userAuthToken} from '../../../helpers';
import moment from 'moment';
import EventsInfo from './EventsInfo';
import withSure from '../../../hoc/withSure';
import ROLES from '../../../constants/roles';

const CALENDAR_VIEW = {
  MONTH: 'month',
  WEEK: 'basicWeek',
};

const POPOVER_WIDTH = 430;
const POPOVER_HEIGHT = 350;

class Events extends Component {
  constructor(props) {
    super(props);
    this.state = {
      calendarView: CALENDAR_VIEW.MONTH,
      showModal: false,
      calendarDates: {
        startDate: '',
        endDate: '',
      },
      calendarDefaultDate: moment(),
      eventsInfo: {
        top: 0,
        left: 0,
        show: false,
        data: {},
      },
      editEventData: null,
    }
  }

  toggleModal = () => {
    let {editEventData, showModal} = this.state;
    if (showModal) {
      editEventData = null;
    }
    this.setState({showModal: !showModal, editEventData})
  };

  changeCalendarView = calendarView => {this.setState({calendarView})};

  createNewEvent = model => {
    const {institutionId} = this.props.auth.user;
    this.props.showLoader();
    this.props.createNewEvent(userAuthToken.token(), institutionId, model)
      .then(response => {
        this.toggleModal();
        const {institutionId} = this.props.auth.user;
        const {startDate, endDate} = this.state.calendarDates;
        this.props.getAllEvents(userAuthToken.token(), institutionId, startDate, endDate);
        this.props.hideLoader();
      });

  };

  deleteEvent = () => {
    this.props.showSureModal('Are you sure you want to delete this event?')
      .then(() => {
        this.props.showLoader();
        const {institutionId} = this.props.auth.user;
        this.props.deleteEvent(userAuthToken.token(), institutionId, this.state.eventsInfo.data.id)
          .then(() => {
            const {institutionId} = this.props.auth.user;
            const {startDate, endDate} = this.state.calendarDates;
            this.props.getAllEvents(userAuthToken.token(), institutionId, startDate, endDate);
            this.setState({eventsInfo: {show: false}});
            this.props.hideLoader();
          })
      }).catch(() => {})

  };

  updateEventHandler = model => {
    model.id = this.state.editEventData.id;
    const {institutionId} = this.props.auth.user;
    this.props.showLoader();
    this.props.updateEvent(userAuthToken.token(), institutionId, model)
      .then(response => {
        this.setState({
          showModal: false,
          editEventData: null,
        });
        this.props.hideLoader();
        const {institutionId} = this.props.auth.user;
        const {startDate, endDate} = this.state.calendarDates;
        this.props.getAllEvents(userAuthToken.token(), institutionId, startDate, endDate);
      });

  };

  startEditEvent = () => {
    this.setState({
      editEventData: this.state.eventsInfo.data,
      eventsInfo: {
        show: false,
      },
      showModal: true,
    })
  };

  onCalendarViewRender = (view) => {
    const startDate = view.start.format('YYYY-MM-DD');
    const endDate = view.end.format('YYYY-MM-DD');

    if (this.state.calendarDates.startDate !== startDate && this.state.calendarDates.endDate !== endDate) {
      const {institutionId} = this.props.auth.user;
      this.props.getAllEvents(userAuthToken.token(), institutionId, startDate, endDate);
      this.setState({
        calendarDates: {
          startDate,
          endDate,
        },
        calendarDefaultDate: view.intervalStart,
      });
    }
  };

  prepareEvents = () => {
    const {list} = this.props.events;

    return list.map(item => ({
      id: item.id,
      title: item.title,
      start: item.startDate,
      end: item.endDate,
      backgroundColor: item.color.replace('1)', '0.3)'),
      className: 'eventValue',
      textColor: item.color,
      borderColor: item.color,
    }))
  };

  eventClickHandler = (event, jsEvent) => {
    const documentWidth = document.documentElement.offsetWidth;
    const realtiveParentHeight = document.querySelector('main').offsetHeight;

    let top = jsEvent.target.getBoundingClientRect().y + document.documentElement.scrollTop - 50;// 50 is optimal size for correct position relative to design
    let left = jsEvent.clientX - document.querySelector('aside.navigation').offsetWidth;
    let transform = 'translateY(-0%)';
    let borderRadius = ['0px', '8px', '8px', '8px'];
    if (jsEvent.clientX + POPOVER_WIDTH >= documentWidth) {
      left = left - POPOVER_WIDTH;
      borderRadius[0] = '8px';
      borderRadius[1] = '0px';
    }
    if (top + POPOVER_HEIGHT >= realtiveParentHeight) {
      transform = 'translateY(-100%)';
      borderRadius = borderRadius.reverse();
    }

    const eventData = this.props.events.list.find(item => item.id === event.id);

    this.setState({
      eventsInfo: {
        top,
        left,
        show: true,
        borderColor: event.borderColor,
        borderRadius: borderRadius.join(' '),
        data: eventData,
        transform,
      },
    })
  };

  eventsWrapperCLickHandler = event => {
    let {target} = event.nativeEvent,
      found = false;


    while (!target.className.includes('events-wrapper') && target) {
      target = target.parentNode;
      if (target.className.includes('eventValue')) {
        found = true;
      }
    }

    if (!found && this.state.eventsInfo.show) {
      this.setState({eventsInfo: {show: false}});
    }
  };

  closeEventsInfoHandler = () => {
    this.setState({eventsInfo: {show: false}});
  };

  componentDidMount() {
    const realtiveParentHeight = document.querySelector('main').offsetHeight;
    document.querySelector('main').style.minHeight = realtiveParentHeight + 'px';
    //document.querySelector('.header-controls__messages').addEventListener('click', this.closeEventsInfoHandler);
  }

  componentWillUnmount() {
    document.querySelector('main').style.minHeight = '0px';
    //document.querySelector('.header-controls__messages').removeEventListener('click', this.closeEventsInfoHandler);
  }

  render() {
    const {calendarView, showModal, calendarDefaultDate, eventsInfo, editEventData} = this.state;
    this.prepareEvents();
    return (
      <div className="events">
        <div className="events-header">
          <h1 className="page-title">Calendar</h1>
          <div className="events-mode">
            <button className={calendarView === CALENDAR_VIEW.MONTH ? 'active' : ''}
                    onClick={() => {this.changeCalendarView(CALENDAR_VIEW.MONTH)}}
            >Timeline Month
            </button>
            <button className={calendarView === CALENDAR_VIEW.WEEK ? 'active' : ''}
                    onClick={() => { this.changeCalendarView(CALENDAR_VIEW.WEEK)}}
            >Timeline Week
            </button>
          </div>
          <button
            onClick={this.toggleModal}
            className={'big-primary standart'}
          >
            CREATE NEW
          </button>
        </div>
        <div className="events-wrapper" onClick={this.eventsWrapperCLickHandler}>
          <FullCalendar
            header={{
              left: '',
              center: 'prev, title, next',
              right: 'today',
            }}
            eventLimit={true} // allow "more" link when too many events
            events={this.prepareEvents()}
            defaultView={calendarView}
            defaultDate={calendarDefaultDate}
            viewRender={this.onCalendarViewRender}
            eventClick={this.eventClickHandler}
          />
        </div>
        {showModal && <EventsModal show={showModal}
                                   onSave={this.createNewEvent}
                                   onUpdate={this.updateEventHandler}
                                   hideModal={this.toggleModal}
                                   changeModel={editEventData}
        />
        }
        {eventsInfo.show && <EventsInfo left={eventsInfo.left}
                                        top={eventsInfo.top}
                                        borderColor={eventsInfo.borderColor}
                                        borderRadius={eventsInfo.borderRadius}
                                        eventData={eventsInfo.data}
                                        transform={eventsInfo.transform}
                                        onClose={this.closeEventsInfoHandler}
                                        onDelete={this.deleteEvent}
                                        onEdit={this.startEditEvent}
        />}
      </div>
    );
  }
}

Events.propTypes = {};

const mapStateToProps = ({auth, events}) => ({auth, events});

const mapDispatchToProps = dispatch => ({
  createNewEvent: (token, institutionId, model) => {
    return dispatch(actions.createNewEvent(token, institutionId, model));
  },
  updateEvent: (token, institutionId, model) => {
    return dispatch(actions.updateEvent(token, institutionId, model));
  },
  deleteEvent: (token, institutionId, eventId) => {
    return dispatch(actions.deleteEvent(token, institutionId, eventId));
  },
  getAllEvents: (token, institutionId, startDate, endDate) => { dispatch(actions.getAllEvents(token, institutionId, startDate, endDate))},
  showLoader: () => {dispatch(actions.showLoader())},
  hideLoader: () => {dispatch(actions.hideLoader())},
});

export default connect(mapStateToProps, mapDispatchToProps)(withSure(Events));
