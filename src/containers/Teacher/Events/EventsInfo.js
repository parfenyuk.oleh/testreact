import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../../../components/UI/Elements/Icon';
import moment from 'moment';
import EventMembers from './EventMembers';

const EventsInfo = ({top, left, borderColor, borderRadius, onDelete, onEdit, onClose, eventData, transform}) => {
  const startDate = moment(eventData.startDate),
    endDate = moment(eventData.endDate),
    membersAmount = eventData.members.length;

  function toggleMembers () {
    document.querySelector('.events-info-members-link').classList.toggle('hide');
  }
  return (
    <div className="events-info"
         style={{
           top: top + 'px',
           left: left + 'px',
           borderColor,
           borderRadius,
           transform,
         }}
    >
      <div className="events-info-controls">
        <Icon name="delete-dark" onClick={onDelete}/>
        <Icon name="edit-dark" onClick={onEdit}/>
        <Icon name="cross" onClick={onClose}/>
      </div>
      <h1 className="events-info-title">{eventData.title}</h1>
      <div className="events-info-row">
        <Icon name="clock"/> {startDate.format('h:mm a')} - {endDate.format('h:mm a')}, {eventData.group.name}
        <span className="events-info-members-link hide" onClick={toggleMembers}>
          {membersAmount} members
          <EventMembers membersAmount={membersAmount}
                        list={eventData.members}
                        transform={transform}
          />
        </span>

      </div>
      <div className="events-info-row">
        <Icon name="calendar"/> {startDate.format('DD/MM/YY')} - {endDate.format('DD/MM/YY')}
      </div>
      <div className="events-info-row">
        <Icon name="location"/> {eventData.location}
      </div>
      <div className="events-info-text">
        {eventData.description}
      </div>
    </div>
  );
};

EventsInfo.propTypes = {
  top: PropTypes.number,
  left: PropTypes.number,
  borderColor: PropTypes.string,
  borderRadius: PropTypes.string,
  eventData: PropTypes.object,
  transform: PropTypes.string,
};

EventsInfo.defaultVelue = {
  top: 0,
  left: 0,
  borderColor: '#ffffff',
  borderRadius: '0px',
  eventData: {},
  transform: '',
};

export default EventsInfo;