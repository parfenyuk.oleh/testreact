import React, {Component} from 'react';
import {connect} from 'react-redux';
import MainHeader from '../../../components/Admin/GroupMembers/MainHeader';
import MainSidebar from '../../../components/Admin/GroupMembers/MainSidebar';
import NewGroupModal from '../../../components/Admin/GroupMembers/Modals/NewGroupModal';
import {getAssignedMembersToSubject, clearMembers, searchAssignedMembers} from '../../../store/actions/admin.action';
import * as actions from '../../../store/actions/groups.action';
import NewMemberModal from '../../../components/Admin/GroupMembers/Modals/NewMemberModal';
import NewTeacherModal from '../../../components/Admin/GroupMembers/Modals/NewTeacherModal';
import withSure from '../../../hoc/withSure';
import SubjectCards from "../../../components/Admin/GroupMembers/Tabs/SubjectCards";
import {withRouter} from "react-router-dom";
import {userAuthToken} from "../../../helpers/index";
import {getAllSubjects} from "../../../store/actions/user.action";
import validate from "../../../helpers/validator";
import ROLES from "../../../constants/roles";

class GroupMembers extends Component {
    constructor (props){
        super(props);
        this.state = {
            newGroup: {
                name: '',
                description: '',
            },
            activeItem: 'none',
            showNewGroupModal : false,
            showNewMemberModal : false,
            showNewTeacherModal : false,
            showMemberInfoModal : false,
            errors: [],
            subjectId: '',
            checkedItemsStudents: new Map(),
            checkedItemsTeacher: new Map(),
            search: '',
            take: 10,
            skip: 0,
        };
    }

    componentDidMount(){
        const {institutionId, token} = this.props.auth.user;

        this.props.getAllGroups(userAuthToken.token(), institutionId)
            .then((item => {
                let subjectId, groupId;

                if(item.length !== 0) {
                    subjectId = item[0].subjectId;
                    groupId = item[0].id;
                }

                this.props.clearMembers();
                this.props.getMemberActiveGroup(userAuthToken.token(), institutionId, groupId);
                this.props.getAssignedMembersToSubject(institutionId, subjectId, token, ROLES.STUDENT);
                this.props.getAssignedMembersToSubject(institutionId, subjectId, token, ROLES.TEACHER);

                this.setState({
                    subjectId: subjectId,
                    groupId: groupId
                });
            }));
        this.props.getAllSubjects(userAuthToken.token(), institutionId);
    };

    newForumItemFieldsUpdateHandler = (value, name) => {
        let {newGroup} = this.state;
        newGroup[name] = value;
        this.setState({newGroup});
    };

    removeGroupItem = (groupId) => {
        const {institutionId, token} = this.props.auth.user;

        this.props.showSureModal('Are you sure you want to delete this group?')
            .then(() => {
                this.props.deleteGroupItem(userAuthToken.token(), institutionId, groupId)
                    .then(() => {

                        this.props.getAllGroups(userAuthToken.token(), institutionId)
                            .then((item => {
                                let subjectId;

                                if(item.length !== 0) {
                                    subjectId = item[0].subjectId;
                                }

                                this.props.getMemberActiveGroup(userAuthToken.token(), institutionId, subjectId);
                                this.props.getAssignedMembersToSubject(institutionId, subjectId, token, ROLES.STUDENT);
                                this.props.getAssignedMembersToSubject(institutionId, subjectId, token, ROLES.TEACHER);

                                this.setState({
                                    subjectId: subjectId,
                                });
                            }));
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    handleChange = (event, type) => {
        const {institutionId, token} = this.props.auth.user;
        const {groups} = this.props;
        let searchStr = event.target.value;
        const newStudentText = 'newStudent';
        const newTeacherText = 'newTeacher';
        let skip = 0;
        let take = 10;
        let role;

        if( type === newStudentText) {
            role = ROLES.STUDENT;
        } else if (type === newTeacherText) {
            role = ROLES.TEACHER;
        }

        if( type === newStudentText || type === newTeacherText) {

            if(!groups.loading) {
                this.props.searchAssignedMembers(institutionId, this.state.subjectId, token, role, skip, take, searchStr.toLowerCase());
            }
        }

        this.setState({
            [event.target.name]: event.target.value,
            skip: 0
        });
    };

    saveGroupHandler = () => {
        const {newGroup, activeItem} = this.state;
        const {institutionId} = this.props.auth.user;
        let result = {};
        const errors = [];

        const title = validate('title',newGroup.name,{required: 1});
        const description = validate('description',newGroup.description,{required: 1});
        const subjectField = validate('subject',`${activeItem}`,{required: 1});

        if (title) errors.push(title);
        if (description) errors.push(description);
        if (subjectField) errors.push(subjectField);

        this.setState({errors: errors});

        if (errors.length === 0) {
            result['name'] = newGroup.name;
            result['description'] = newGroup.description;
            result['subjectId'] = activeItem;

            this.props.addGroupItem(userAuthToken.token(), institutionId, result)
                .then(result =>
                {
                    this.props.getAllGroups(userAuthToken.token(), institutionId)

                    if(result.data === undefined) {
                        this.setState({
                            showNewGroupModal : !this.state.showNewGroupModal,
                            newForumItem: {
                                name: '',
                                description: '',
                            },
                            activeItem: '',
                        });
                    }
                })
        }

    };

    toggleNewGroupModal = () => {
        this.setState({
            showNewGroupModal : !this.state.showNewGroupModal,
            skip: 0,
            type: 'group'
        })
    };

    toggleNewMemberModal = () => {
        this.setState({
            showNewMemberModal : !this.state.showNewMemberModal,
            skip: 0,
            type: 'students'
        })
    };

    toggleNewTeacherModal = () => {
        this.setState({
            showNewTeacherModal : !this.state.showNewTeacherModal,
            skip: 0,
            type: 'teachers'
        })
    };

    saveNewHandlerMember = type => {
        const { checkedItemsStudents, checkedItemsTeacher } = this.state;
        const { institutionId } = this.props.auth.user;
        let result = {};
        let members = [];

        if (type === 'students') {
            checkedItemsStudents.forEach(function(value, key) {
                if(value) {
                    members.push(+key)
                }
            });

        } else if (type === 'teachers') {

            checkedItemsTeacher.forEach(function(value, key) {
                if(value) {
                    members.push(+key)
                }
            });
        }

        result['userIds'] = members;

        this.props.addGroupMember(userAuthToken.token(), institutionId, this.state.groupId, result)
            .then(() => {
                this.props.getMemberActiveGroup(userAuthToken.token(), institutionId, this.state.groupId);

                this.setState({
                    checkedItemsTeacher: new Map(),
                    checkedItemsStudents: new Map(),
                    showNewTeacherModal : false,
                    showNewMemberModal : false,
                });
            });
    };

    showMemberInfoHandler = id => {
        this.setState({
            showMemberInfoModal : true,
            memberForShow : this.props.admin.members.find(item => item.id === id)
        })
    };

    deleteGroupMemberHandler = id => {
        const {institutionId} = this.props.auth.user;

        this.props.showSureModal('Are you sure you want to delete this member?')
            .then(result => {

                this.props.deleteGroupMember(userAuthToken.token(), institutionId, this.state.groupId, id)
                    .then(() => {
                        this.props.getMemberActiveGroup(userAuthToken.token(), institutionId, this.state.groupId);
                    })
            })
            .catch(error => {
                console.log(error);
            })
    };

    handleInputChange = (e, type) => {
        const item = e.target.name;
        const isChecked = e.target.checked;

        if(type === 'students') {
            this.setState(prevState => ({
                checkedItemsStudents: prevState.checkedItemsStudents.set(item, isChecked),
            }));
        } else if (type === 'teacher') {
            this.setState(prevState => ({
                checkedItemsTeacher: prevState.checkedItemsTeacher.set(item, isChecked),
            }));
        }
    };

    onPaginated = (type) => {
        const {institutionId} = this.props.auth.user;
        const { skip } = this.state;

        if(type === 'students') {
            let role = ROLES.STUDENT;

            this.fetchStories(institutionId, role, skip + 10);
        } else if (type === 'teacher') {
            let role = ROLES.TEACHER;

            this.fetchStories(institutionId, role, skip + 10);
        }
    };

    fetchStories = (institutionId, role, skip) => {
        const {token} = this.props.auth.user;
        const {admin} = this.props;

        if(admin.subject.length !== 0 ) {
            this.props.getAssignedMembersToSubject(institutionId, this.state.subjectId, token, role, skip)
                .then(() => {
                    this.setState({
                        skip: skip
                    });
                });
        }
    };

    changeActiveGroup = (id, subjectId) => {
        const {institutionId, token} = this.props.auth.user;
        this.props.clearMembers();

        if(id !== this.state.subjectId) {
            this.props.getMemberActiveGroup(userAuthToken.token(), institutionId, id);
            this.props.getAssignedMembersToSubject(institutionId, subjectId, token, ROLES.STUDENT);
            this.props.getAssignedMembersToSubject(institutionId, subjectId, token, ROLES.TEACHER);

            this.setState({
                groupId: id,
                subjectId: subjectId
            });
        }
    };

    filterMembers = members => {
        const userIds = this.props.groups.activeGroup.reduce((acc, item) => {
           acc.push(item.id);
           return acc;
        },[]);
        return members.filter(item => !userIds.includes(item.id));
    };

    render() {
        const {admin, groups} = this.props;
        const {skip, take, groupId} = this.state;

        return (
            <div className="admin-main">
                <MainHeader />
                <MainSidebar showNewGroupModal={this.toggleNewGroupModal}
                             removeGroupItem={this.removeGroupItem}
                             groups={groups.list}
                             changeActiveGroup={this.changeActiveGroup}
                             activeGroup={groupId}
                />

                <NewGroupModal show={this.state.showNewGroupModal}
                              fieldUpdated={this.newForumItemFieldsUpdateHandler}
                              saved={this.saveGroupHandler}
                              hideModal={this.toggleNewGroupModal}
                              handleChange={this.handleChange}
                              subject={this.state.activeItem}
                              subjectList={admin.subject}
                              errors={this.state.errors}
                />

                <NewMemberModal show={this.state.showNewMemberModal}
                                hideModal={this.toggleNewMemberModal}
                                saved={this.saveNewHandlerMember}
                                list={this.filterMembers(admin.subjectStudents)}
                                handleInputChange={this.handleInputChange}
                                checkedItems={this.state.checkedItemsStudents}
                                handleChange={this.handleChange}
                                admin={admin}
                                take={take}
                                skip={skip}
                                onPaginated={this.onPaginated}
                />

                <NewTeacherModal show={this.state.showNewTeacherModal}
                                hideModal={this.toggleNewTeacherModal}
                                saved={this.saveNewHandlerMember}
                                list={this.filterMembers(admin.subjectTeachers)}
                                handleInputChange={this.handleInputChange}
                                checkedItems={this.state.checkedItemsTeacher}
                                handleChange={this.handleChange}
                                 admin={admin}
                                 take={take}
                                 skip={skip}
                                 onPaginated={this.onPaginated}

                />

                <SubjectCards users={groups.activeGroup}
                              showMemberInfo={this.showMemberInfoHandler}
                              showMemberModal={this.toggleNewMemberModal}
                              showTeacherModal={this.toggleNewTeacherModal}
                              deleteMember={this.deleteGroupMemberHandler}
                              isLoading={groups.loading}
                />

            </div>
        );
    }
}

const mapStateToProps = ({auth, admin, groups}) => ({auth, admin, groups});

const mapDispatchToProps = dispatch => ({
    getAllGroups : (token, id) => {return dispatch(actions.getAllGroups(token, id))},
    getMemberActiveGroup : (token, id, subjectId) => {return dispatch(actions.getMemberActiveGroup(token, id, subjectId))},
    getAllSubjects: (token, id) => {dispatch(getAllSubjects(token, id))},
    addGroupItem: (token, id, result) => {return dispatch(actions.addGroupItem(token, id, result))},
    deleteGroupItem: (token, id, subjectId) => {return dispatch(actions.deleteGroupItem(token, id, subjectId))},
    deleteGroupMember: (token, id, subjectId, userId) => {return dispatch(actions.deleteGroupMember(token, id, subjectId, userId))},
    getAssignedMembersToSubject : (institutionId, subjectId, token, rolesIds, skip, take) =>
    {return dispatch(getAssignedMembersToSubject(institutionId, subjectId, token, rolesIds, skip, take))},
    searchAssignedMembers : (institutionId, subjectId, token, rolesIds, skip, take, search) =>
    {return dispatch(searchAssignedMembers(institutionId, subjectId, token, rolesIds, skip, take, search))},
    addGroupMember: (id, token, subjectId, membersIds) => {return dispatch(actions.addGroupMember(id, token, subjectId, membersIds))},
    clearMembers: () => {dispatch(clearMembers())}
});

const connectedGroupMembers = withRouter(connect(mapStateToProps,mapDispatchToProps)(withSure(GroupMembers)));

export { connectedGroupMembers as GroupMembers };