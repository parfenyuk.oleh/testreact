import axios from '../axios-instance';
import Swal from 'sweetalert2';

function documentLoadApi(file, token) {
    const data = new FormData();
    data.append('file', file);
    return axios.post('/Files/UploadResource',data,{
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
            }
         })
        .then(response => {

            if (!response.data.ok) {
                throw response.data.message
            }
            return response.data.result
        })
        .then(result => {
            const obj2 = {fileName: file.name} ;
            return Object.assign(result, obj2)
        })
        .catch(err => {
            Swal({
                title: 'Oops...',
                text: err,
                type: 'error',
                timer: 3000
            });

            return 'error'
        });
}

export default documentLoadApi;