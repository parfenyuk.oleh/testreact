const fileReader = (value, callback) => {
  const reader = new FileReader();

  reader.onload = () => {
    callback(reader.result);
    return reader.result;
  };

  if(value) {
      reader.readAsDataURL(value);
  }
};

export default fileReader;
