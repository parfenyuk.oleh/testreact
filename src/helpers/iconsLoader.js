import iconMain from '../images/icons/ic_main.svg';
import iconSubmissions from '../images/icons/ic_asignments_active.svg';
import iconGroup from '../images/icons/ic_group_memebers.svg';
import iconKeyword from '../images/icons/ic_keyword_master.svg';
import iconAssignment from '../images/icons/ic_assignment.svg';
import iconForum from '../images/icons/ic_forum.svg';
import iconResource from '../images/icons/ic_resource.svg';
import iconNoticeBoard from '../images/icons/ic_notice_board.svg';
import iconCalendar from '../images/icons/ic-calendar.svg';
import iconCalendarRed from '../images/icons/ic-calendar-red.svg';
import iconCutText from '../images/icons/ic_cut_text.svg';
import iconLink from '../images/icons/ic_link.svg';
import iconQuote from '../images/icons/ic_quote.svg';
import iconUnder from '../images/icons/ic_under.svg';
import iconBold from '../images/icons/ic_bold.svg';
import iconItalic from '../images/icons/ic_Italic.svg';
import iconComment from '../images/icons/ic_coment.svg';
import iconPic from '../images/icons/ic_pic.svg';
import iconAttention from '../images/icons/ic_attention.svg';
import iconPlus from '../images/icons/plus.svg';
import iconSearch from '../images/icons/ic_search.svg';
import iconMessage from '../images/icons/ic_message.svg';
import iconGlobe from '../images/icons/ic_globe.svg';
import iconFolder from '../images/icons/folder.svg';
import iconDropDown from '../images/icons/dropdow_black_closed.svg';
import iconDropDownDisabled from '../images/icons/dropdow_disabled_closed.svg';
import iconBin from '../images/icons/ic_bin.svg';
import iconEdit from '../images/icons/Ic_edit.svg'
import iconResotre from '../images/icons/restore.svg';
import iconOpenFull from '../images/icons/open_full.svg';
import iconClock from '../images/icons/ic_clock.svg';
import iconCross from '../images/icons/cross.svg'
import iconAlert from '../images/icons/ic_alert.svg';
import iconInProgress from '../images/icons/ic_inProgress.svg';
import iconSubmitted from '../images/icons/ic_submitted.svg';
import iconSubmittedLate from '../images/icons/ic_submittedLate.svg';
import iconNotSubmitted from '../images/icons/ic_notSubmitted.svg';
import iconDone from '../images/icons/ic_done.svg';
import iconPencil from '../images/icons/ic_pencil.svg';
import iconAdd from '../images/icons/ic_plus.svg';
import iconWorkspace from '../images/icons/ic_workspace.svg';

const icons = {
  iconWorkspace,
  iconCross,
  iconMain,
  iconSubmissions,
  iconGroup,
  iconKeyword,
  iconAssignment,
  iconForum,
  iconResource,
  iconNoticeBoard,
  iconCalendar,
  iconCalendarRed,
  iconCutText,
  iconLink,
  iconQuote,
  iconUnder,
  iconBold,
  iconItalic,
  iconComment,
  iconPic,
  iconAttention,
  iconPlus,
  iconSearch,
  iconMessage,
  iconGlobe,
  iconFolder,
  iconDropDown,
  iconDropDownDisabled,
  iconBin,
  iconEdit,
  iconResotre,
  iconOpenFull,
  iconClock,
  iconAlert,
  iconInProgress,
  iconSubmitted,
  iconSubmittedLate,
  iconNotSubmitted,
  iconDone,
  iconPencil,
  iconAdd
};

export default icons;
