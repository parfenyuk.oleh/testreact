import axios from '../axios-instance';

function imageLoadApi(file, token) {
    const data = new FormData();
    data.append('file', file);
    return axios.post('/Files/Photo',data,{
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: `Bearer ${token}`
        }
    });
}

export default imageLoadApi;