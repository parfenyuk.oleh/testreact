const ONLINE_COLORS = [
'#73D500',
'#C27AEB',
'#03C4C6'
];

export const getRandomInt = max => Math.floor(Math.random() * Math.floor(max));

export const getRandomOnlineColor = (num) => ONLINE_COLORS[num];

