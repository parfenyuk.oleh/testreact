export const userAuthToken = {
    token,
};

function token() {
    let user, authToken;

    if (localStorage.getItem('user')) {
        user = JSON.parse(localStorage.getItem('user'));

        authToken = `Bearer ${user.token}`;
    }

    return authToken;
}
