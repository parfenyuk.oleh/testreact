import moment from "moment";

const rulesDefinition = {

  weightLimit: value => value <= 100? null : ' can\'t be more than 100',

  minLength:  (value,min) => value.length >= min ? null : `min length ${min}`,

  maxLength:  (value,max) => value.length <= max ? null : `max length ${max}`,

  required:  (value,min) => value.length >= min && value !== 'none' ? null : `field is required`,

  isEmail: value => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    const emailCorrect =  re.test(String(value).toLowerCase());
    if(!emailCorrect) return 'incorrect';
    return null;
  },

  isEqual: (value, equal) => value === equal ? null : 'not equal',

  isNotEmpty : value => value.replace(/\s/g,'').length > 0 ? null : 'can\'t be empty',

  isNotEmptyDate : value => value ? null : 'can\'t be empty',

  notLessDate : (bigger, smaller) => smaller.isSameOrBefore(bigger, 'day') ? null : 'must be more than Start Date',

  notLessTime : (bigger, smaller) => smaller.isSameOrBefore(bigger) ? null : 'must be more than Start Time',

  isNotEqualToZero : value => value > 0 ? null : 'must be more than 0',

  isNotEmptyObject : value => Object.entries(value).length > 0 ? null :  'can\'t be empty',

  notLessDateThenNow : value => value.isBefore(moment()) ? 'must be more then now' : null,

  intRange : value => value < 2147483647 ? null : 'must be smaller then 2147483647'


};

const capitalize = string => string[0].toUpperCase() + string.slice(1);

const validate = (field, value, rules) => {
  let errors = null;
  Object.entries(rules).forEach(item => {
    if(errors) return;
    const error = rulesDefinition[item[0]](value,item[1]);
    if(error){
      errors = `${capitalize(field)} ${error}`;
    }

  });
  return errors;
};

export default validate;