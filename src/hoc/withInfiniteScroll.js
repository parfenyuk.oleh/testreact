import React from 'react';

export const withInfiniteScroll = (Component) =>
    class WithInfiniteScroll extends React.Component {
        handleScroll = () => {
            let scrollHeight = this.scroller.scrollHeight - this.scroller.offsetHeight;

            if(
                scrollHeight - this.scroller.scrollTop <= 300
                && !this.props.isLoading
                && this.props.list.length % 10 === 0
                && this.props.lengthArray !== 0
            ) {
                this.props.onPaginated();
            }
        };

        render() {
            return <div onScroll={this.handleScroll}
                        ref={(scroller) => {
                            this.scroller = scroller;
                        }}
                        className="list-wrapper"
            >
                <Component {...this.props}/>
            </div>
        }
    };