import React, {Fragment} from 'react';
import ModalSure from '../components/UI/ModalSure';

/**
 *
 * @param WrappedComponent
 * @return Wrapped component with property showModal which we can use for agree or disagree with something
 */
const withSure = (WrappedComponent) => {
  return class extends React.Component {
    state={
      showModal: false,
      answer: '',
      modal: ''
    };

    /**
     *
     * @param text - will be shown in ModalSure
     * @returns {Promise<any>}
     */
    showModal = (text, agreeText) => {

      return new Promise((resolve, reject) => {

        this.setState({
          showModal: true,
          modal : <ModalSure show={true}
                             onAgree={() => {
                               this.onAgree(resolve)
                             }}
                             onDisagree={() => {
                               this.onDisagree(reject)
                             }}
                             text={text}
                             agreeText={agreeText}
          />
        });

      });
    };

    onAgree = (resolve) => {
      resolve('ok');
      this.setState({showModal: false})
    };

    onDisagree = (reject) =>{
      reject('bad');
      this.setState({showModal: false})
    };

    render () {
      return (
        <Fragment>
          {this.state.showModal ? this.state.modal : ''}
          <WrappedComponent {...this.props} showSureModal={this.showModal}/>
        </Fragment>
      )
    }
  }
};

export default withSure;
