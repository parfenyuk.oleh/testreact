import React from 'react';
import {Redirect, Route, Switch, withRouter} from 'react-router-dom';
import * as routeConstants from '../constants/routes';
import Main from '../containers/General/Main/Main';
import ForumList from '../containers/General/Forum/List';
import ForumDetail from '../containers/General/Forum/Detail';
import {NoticeBoard, NoticeDetail} from '../containers/General/NoticeBoard';
import {PinUp} from '../containers/General/PinUp';
import Payment from '../containers/Admin/Payment/Payment';
import Dashboard from '../containers/Admin/Dashboard/Dashboard';
import moment from 'moment';

const AdminRoutes = (props) => {
  if (moment().isBefore(moment(props.user.institutionExpireAt))) {
    return (
      <Switch>
        <Route exact path="/" component={Main}/>
        <Route exact path={routeConstants.FORUM} component={ForumList}/>
        <Route path={`${routeConstants.FORUM}/:id`} component={ForumDetail}/>
        <Route exact path={routeConstants.NOTICE_BOARD} component={NoticeBoard}/>
        <Route path={`${routeConstants.NOTICE_BOARD}/:id`} component={NoticeDetail}/>
        <Route path={routeConstants.PINUP} component={PinUp}/>
        <Route exact path={routeConstants.PAYMENT} component={Payment}/>
        <Route exact path={routeConstants.DASHBOARD} component={Dashboard}/>
      </Switch>

    );
  } else {
    return (
      <div>
        <Route exact path={routeConstants.PAYMENT} component={Payment}/>
        {props.location.pathname !== routeConstants.PAYMENT &&
           <Route render={() => <Redirect to={routeConstants.PAYMENT}/>}/>
        }
      </div>
    )
  }

};

export default withRouter(AdminRoutes);
