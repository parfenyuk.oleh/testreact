import React from 'react';
import {Route, Switch} from 'react-router-dom';

import * as ROUTES from '../constants/routes';
import {NoticeBoard, NoticeDetail} from '../containers/General/NoticeBoard';
import {PinUp} from '../containers/General/PinUp';
import WorkSpace from '../containers/Students/WorkSpace/WorkSpace';
import * as routeConstants from '../constants/routes';
import ForumList from '../containers/General/Forum/List';
import ForumDetail from '../containers/General/Forum/Detail';
import {Resource} from '../containers/General/Resourse';
import {Milestone} from '../containers/General/Milestone/Milestone';
import Assignments from '../containers/Students/Assignments/Assignments';
import Events from '../containers/Teacher/Events/Events';
import Dashboard from '../containers/Students/Dashboard/Dashboard';
import AssignmentsCard from '../components/Student/Assignments/AssignmentsCard';

const StudentsRoutes = () => {
  return (
    <Switch>
      <Route exact path={ROUTES.FORUM} component={ForumList}/>
      <Route path={`${ROUTES.FORUM}/:id`} component={ForumDetail}/>
      <Route exact path={ROUTES.NOTICE_BOARD} component={NoticeBoard}/>
      <Route path={`${ROUTES.NOTICE_BOARD}/:id`} component={NoticeDetail}/>
      <Route path={ROUTES.PINUP} component={PinUp}/>
      <Route path={ROUTES.ASSIGNMENTS} component={Assignments}/>
      <Route path={ROUTES.WORKSPACE} exact component={WorkSpace}/>
      <Route exact path={routeConstants.RESOURCES} component={Resource}/>
      <Route exact path={routeConstants.MILESTONE} component={Milestone}/>
      <Route path={routeConstants.EVENTS} component={Events} exact/>
      <Route path={routeConstants.DASHBOARD} component={Dashboard} exact/>
      <Route path={routeConstants.ASSESSMENTS} component={AssignmentsCard} exact/>
    </Switch>
  );
};

export default StudentsRoutes;
