import React from 'react';
import {Route, Switch} from 'react-router-dom';
import * as routeConstants from '../constants/routes';
import {MainTeacher} from '../containers/Teacher/Main';
import ForumList from '../containers/General/Forum/List';
import ForumDetail from '../containers/General/Forum/Detail';
import {GroupMembers} from '../containers/Teacher/GroupMembers';
import Assignments from '../containers/Teacher/Assignments/Assignments';
import {KeywordTeacher} from '../containers/General/Keyword';
import {Resource} from '../containers/General/Resourse';
import {NoticeBoard, NoticeDetail} from '../containers/General/NoticeBoard';
import {PinUp} from '../containers/General/PinUp';
import Main from '../containers/General/Main/Main';
import AssignmentDetail from '../containers/Teacher/Assignments/AssignmentDetail';
import AssignmentResult from '../containers/Teacher/Assignments/AssignmentResult';
import Attendance from '../containers/Teacher/Attendance/Attendance';
import Events from '../containers/Teacher/Events/Events';
import Dashboard from '../containers/Teacher/Dashboard/Dashboard';
import Assessments from '../containers/Teacher/Assessments/Assessments';
import WorkSpace from '../containers/Students/WorkSpace/WorkSpace';
import * as ROUTES from '../constants/routes';

const TeacherRoutes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Main}/>
      <Route exact path={routeConstants.GROUP_MEMBER} component={GroupMembers}/>
      <Route exact path={routeConstants.FORUM} component={ForumList}/>
      <Route path={`${routeConstants.FORUM}/:id`} component={ForumDetail}/>
      <Route exact path={routeConstants.NOTICE_BOARD} component={NoticeBoard}/>
      <Route path={`${routeConstants.NOTICE_BOARD}/:id`} component={NoticeDetail}/>
      <Route exact path={routeConstants.ASSIGNMENTS} component={Assignments}/>
      <Route exact path={routeConstants.ASSESSMENTS} component={Assessments}/>
      <Route exact path={routeConstants.KEYWORD} component={KeywordTeacher}/>
      <Route exact path={routeConstants.RESOURCES} component={Resource}/>
      <Route path={routeConstants.ASSIGNMENT_DETAIL} component={AssignmentDetail} exact/>
      <Route path={routeConstants.ASSIGNMENT_RESULT} component={AssignmentResult} exact/>
      <Route path={routeConstants.ATTENDANCE} component={Attendance} exact/>
      <Route path={routeConstants.EVENTS} component={Events} exact/>
      <Route path={routeConstants.DASHBOARD} component={Dashboard} exact/>
      <Route path={ROUTES.WORKSPACE} exact component={WorkSpace}/>
    </Switch>
  );
};

export default TeacherRoutes;
