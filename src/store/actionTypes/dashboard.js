export const GET_DASHBOARD_ASSIGNMENTS_START = 'GET_DASHBOARD_ASSIGNMENTS_START';
export const GET_DASHBOARD_ASSIGNMENTS_SUCCESS = 'GET_DASHBOARD_ASSIGNMENTS_SUCCESS';
export const GET_DASHBOARD_ASSIGNMENTS_FAIL = 'GET_DASHBOARD_ASSIGNMENTS_FAIL';

export const GET_TEACHER_DATA_START = 'GET_TEACHER_DATA_START';
export const GET_TEACHER_DATA_SUCCESS = 'GET_TEACHER_DATA_SUCCESS';
export const GET_TEACHER_DATA_FAIL = 'GET_TEACHER_DATA_FAIL';

export const GET_STUDENT_DATA_START = 'GET_STUDENT_DATA_START';
export const GET_STUDENT_DATA_SUCCESS = 'GET_STUDENT_DATA_SUCCESS';
export const GET_STUDENT_DATA_FAIL = 'GET_STUDENT_DATA_FAIL';
