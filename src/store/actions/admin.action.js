import Swal from 'sweetalert2';

import * as actionTypes from '../actionTypes/index';
import * as loaderActions from '../actions/loader';

import axios from '../../axios-instance';
import ROLES from '../../constants/roles';
import Qs from "qs";

const createNewSubjectStart = () => ({
  type: actionTypes.CREATE_NEW_SUBJECT_START
});

const createNewSubjectSuccess = () => ({
  type: actionTypes.CREATE_NEW_SUBJECT_SUCCESS
});

const createNewSubjectFail = () => ({
  type: actionTypes.CREATE_NEW_SUBJECT_FAIL
});

export const createNewSubject = (instId, subject, token) => dispatch => {
  dispatch(createNewSubjectStart());
  dispatch(loaderActions.showLoader());
  axios.post(`/Institutions/${instId}/Subjects`, subject, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  }).then(response => {
    dispatch(loaderActions.hideLoader());
    if (response.data.ok) {
      dispatch(createNewSubjectSuccess());
      dispatch(getAllSubjects(instId, token));
      Swal({
        title: "Created!",
        text: "New subject was created",
        type: "success",
        timer: 3000
      });
    } else {
      throw new Error(response.data.message);
    }


  }).catch(e => {
    dispatch(loaderActions.hideLoader());
    dispatch(createNewSubjectFail());
    Swal({
      title: "Failed!",
      text: e,
      type: "error",
      timer: 3000
    });
  })
};

const createNewMemberStart = () => ({
  type: actionTypes.CREATE_NEW_MEMBER_START
});

const createNewMemberSuccess = (newMember) => ({
  type: actionTypes.CREATE_NEW_MEMBER_SUCCESS,
  newMember
});

const createNewMemberFail = () => ({
  type: actionTypes.CREATE_NEW_MEMBER_FAIL
});

export const createNewMember = (universityId, data, token) => dispatch => {
  dispatch(createNewMemberStart());
  dispatch(loaderActions.showLoader());
  axios.post(`/Institutions/${universityId}/Members`, data, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  }).then(response => {
    console.log(response);
    if (response.data.ok) {
      dispatch(loaderActions.hideLoader());
      dispatch(getMemberAmount(universityId, token));
      dispatch(createNewMemberSuccess(response.data.result));
      Swal({
        title: "Created!",
        text: "New member was created",
        type: "success",
        timer: 3000
      });
    } else {
      if (response.data.code === 6)
        throw new Error(`User with "${data.email}" email exist!`);
    }

  }).catch(e => {
    Swal({
      title: "Error!",
      text: e,
      type: "error",
      timer: 3000
    });
    dispatch(loaderActions.hideLoader());
    dispatch(createNewMemberFail());
  })
};

const getAllSubjectsStart = () => ({
  type: actionTypes.GET_ALL_SUBJECTS_START
});

const getAllSubjectsSuccess = subjects => ({
  type: actionTypes.GET_ALL_SUBJECTS_SUCCESS,
  subjects
});

const getAllSubjectsError = error => ({
  type: actionTypes.GET_ALL_SUBJECTS_FAIL,
  error
});

export const getAllSubjects = (id, token, relation) => dispatch => {
  dispatch(getAllSubjectsStart());
  return axios.get(`/Institutions/${id}/Subjects`, {
    headers:
      {
        Authorization: `Bearer ${token}`
      },
    params: {relation},
  }).then(result => {
    dispatch(getAllSubjectsSuccess(result.data.result));
    return result.data.result;
  }).catch(e => {
    dispatch(getAllSubjectsError());
  })
};


const getAllMembersStart = () => ({
  type: actionTypes.GET_ALL_MEMBERS_START
});

const getAllMembersSuccess = (students, teacher, admins, search) => ({
  type: actionTypes.GET_ALL_MEMBERS_SUCCESS,
  students, teacher, admins, search,
});

const getAllTypesMembersSuccess = (members) => ({
    type: actionTypes.GET_ALL_TYPES_MEMBERS_SUCCESS,
    members
});

const getAllMembersError = error => ({
  type: actionTypes.GET_ALL_MEMBERS_FAIL,
  error
});

export const getAllMembers = (id, token, rolesIds = null, skip = 0, take = 10, search = '') => dispatch => {

  dispatch(getAllMembersStart());
  if(search) {
    skip = 0;
    take = 100;
  }
  return axios.get(`/Institutions/${id}/Members`, {
    headers: {Authorization: `Bearer ${token}`},
    params: {
      rolesIds,
      skip,
      take,
      search,
    },
    paramsSerializer: params => Qs.stringify(params, {arrayFormat: 'repeat'})
  }).then(response => {
    if (rolesIds === ROLES.STUDENT)
      dispatch(getAllMembersSuccess(response.data.result, [], [], search));
    if (rolesIds === ROLES.TEACHER)
      dispatch(getAllMembersSuccess([], response.data.result, [], search));
    if (rolesIds === ROLES.ADMIN)
      dispatch(getAllMembersSuccess([], [], response.data.result, search));

    dispatch(getAllTypesMembersSuccess(response.data.result));

  }).catch(e => {
    dispatch(getAllMembersError());
  })
};

export const clearAdminsTeacherStudents = () => ({type: actionTypes.CLEAR_ADMINS_TEACHERS_STUDENTS});
const updateMemberStart = () => ({
  type: actionTypes.UPDATE_MEMBER_START
});

const updateMemberSuccess = () => ({
  type: actionTypes.UPDATE_MEMBER_SUCCESS,
});

const updateMemberError = error => ({
  type: actionTypes.UPDATE_MEMBER_FAIL,
  error
});

export const updateMember = (member, idUniversity, token) => dispatch => {
  dispatch(updateMemberStart());
  return axios.put(`/Institutions/${idUniversity}/Members/${member.id}`, member, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  }).then(response => {
    if (response.data.ok) return response;
    throw response;
  }).then(response => {
    dispatch(updateMemberSuccess());
    dispatch(getAllMembers(idUniversity, token));
    Swal({
      title: "Updated!",
      text: "Member  was updated",
      type: "success",
      timer: 3000
    });
    return response;
  }).catch(err => {
    dispatch(updateMemberError(err));
    return err;
  });
};

const updateByAdminMemberStart = () => ({
  type: actionTypes.UPDATE_BY_ADMIN_MEMBER_START
});

const updateByAdminMemberSuccess = (member) => ({
  type: actionTypes.UPDATE_BY_ADMIN_MEMBER_SUCCESS,
  member
});

const updateByAdminMemberError = error => ({
  type: actionTypes.UPDATE_BY_ADMIN_MEMBER_FAIL,
  error
});

export const updateByAdminMember = (idUniversity, token, member) => dispatch => {
  dispatch(updateByAdminMemberStart());
  axios.put(`/Institutions/${idUniversity}/Members/${member.id}/Manage`, member, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  }).then(response => {
    if (response.data.ok) return response.data;
    throw response;
  }).then(() => {
    dispatch(updateByAdminMemberSuccess(member));
    Swal({
      title: "Updated!",
      text: "Member  was updated",
      type: "success",
      timer: 3000
    });
  }).catch(err => {
    dispatch(updateByAdminMemberError(err));
  });
};

const getAssignedMembersToSubjectStart = () => ({
  type: actionTypes.GET_ASSIGNED_MEMBERS_TO_SUBJECT_START
});

const getAssignedMembersToSubjectSuccess = (students, teachers, mixed) => ({
  type: actionTypes.GET_ASSIGNED_MEMBERS_TO_SUBJECT_SUCCESS,
  students,
  teachers,
  mixed
});

const getAssignedMembersToSubjectFail = () => ({
  type: actionTypes.GET_ASSIGNED_MEMBERS_TO_SUBJECT_FAIL
});

export const getAssignedMembersToSubject = (institutionId,
                                            subjectId,
                                            token,
                                            rolesIds = null,
                                            skip = 0,
                                            take = 10,
                                            search) => dispatch => {
  dispatch(getAssignedMembersToSubjectStart());
  return axios.get(`/Institutions/${institutionId}/Subjects/${subjectId}/Members`, {
    headers: {Authorization: `Bearer ${token}`},
    params: {
      rolesIds,
      skip,
      take,
      search
    }
  })
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            if(rolesIds === ROLES.STUDENT)
                dispatch(getAssignedMembersToSubjectSuccess(result,[],[]));
            if(rolesIds === ROLES.TEACHER)
                dispatch(getAssignedMembersToSubjectSuccess([],result,[]));
            if(rolesIds === null){
                dispatch(getAssignedMembersToSubjectSuccess([],[],result));
            }
        })
        .catch(error => {
            dispatch(getAssignedMembersToSubjectFail());
        })
};

const getAssignedStudentSuccess = (students) => ({
    type: actionTypes.GET_ASSIGNED_STUDENTS_SUCCESS,
    students,
});

const getAssignedTeacherSuccess = (teachers) => ({
    type: actionTypes.GET_ASSIGNED_TEACHER_SUCCESS,
    teachers,
});

export const searchAssignedMembers = (institutionId,
                                            subjectId,
                                            token,
                                            rolesIds = null,
                                            skip = 0,
                                            take = 10,
                                            search,) => dispatch =>
{
    dispatch(getAssignedMembersToSubjectStart());
    return axios.get(`/Institutions/${institutionId}/Subjects/${subjectId}/Members`,{
        headers: { Authorization: `Bearer ${token}`},
        params: {
            rolesIds,
            skip,
            take,
            search
        }
    })
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            if(rolesIds === ROLES.STUDENT)
                dispatch(getAssignedStudentSuccess(result));
            if(rolesIds === ROLES.TEACHER)
                dispatch(getAssignedTeacherSuccess(result));
        })
        .catch(error => {
            dispatch(getAssignedMembersToSubjectFail());
        })
};

export const clearSubjectMixedFiled = () => ({type: actionTypes.CLEAR_SUBJECT_MIXED_FIELD});

export const clearMembers = () => ({type: actionTypes.CLEAR_MEMBER_LIST});

const deleteMemberStart = () => ({type: actionTypes.DELETE_MEMBER_START});
const deleteMemberSuccess = memberId => ({
  type: actionTypes.DELETE_MEMBER_SUCCESS,
  memberId
});
const deleteMemberFail = () => ({type: actionTypes.DELETE_MEMBER_FAIL});

export const deleteMember = (universityId, token, memberId) => dispatch => {
  dispatch(deleteMemberStart());
  axios.delete(`/Institutions/${universityId}/Members/${memberId}`, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  })
    .then(response => {
      if (response.data.ok) {
        dispatch(getMemberAmount(universityId, token));
        dispatch(deleteMemberSuccess(memberId));
        Swal({
          title: "Deleted!",
          text: "Member  have been deleted",
          type: "success",
          timer: 3000
        });

      } else {
        throw response;
      }
    }).catch(error => {
    dispatch(deleteMemberFail());
  })
};

const deleteMemberFromSubjectStart = () => ({type: actionTypes.DELETE_MEMBER_FROM_SUBJECT_START});
const deleteMemberFromSubjectSuccess = (memberId) => ({type: actionTypes.DELETE_MEMBER_FROM_SUBJECT_SUCCESS, memberId});
const deleteMemberFromSubjectFail = () => ({type: actionTypes.DELETE_MEMBER_FROM_SUBJECT_FAIL});

export const deleteMemberFromSubject = (universityId, token, subjectId, memberId) => dispatch => {
  dispatch(deleteMemberFromSubjectStart());
  axios.delete(`/Institutions/${universityId}/Subjects/${subjectId}/Members/${memberId}`, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  })
    .then(response => {
      if (response.data.ok) {
        dispatch(deleteMemberFromSubjectSuccess(memberId));
      } else {
        dispatch(deleteMemberFromSubjectFail());
      }
    })
};

const addMemberToSubjectStart = () => ({type: actionTypes.ADD_MEMBER_TO_SUBJECT_START});
const addMemberToSubjectSuccess = (userIds) => ({
  type: actionTypes.ADD_MEMBER_TO_SUBJECT_SUCCESS,
  userIds
});
const addMemberToSubjectFail = () => ({type: actionTypes.ADD_MEMBER_TO_SUBJECT_FAIL});

export const addMemberToSubject = (institutionId, token, subjectId, userIds) => dispatch => {
  dispatch(addMemberToSubjectStart());
  axios.post(`/Institutions/${institutionId}/Subjects/${subjectId}/Members`, {userIds}, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  }).then(response => {
    if (response.data.ok) {
      dispatch(addMemberToSubjectSuccess(userIds));
    } else {
      Swal({
        title: "Error!",
        text: "One of users already exist in subject",
        type: "error",
        timer: 3000
      });
      dispatch(addMemberToSubjectFail());
    }
  })
};

const getMemberAmountStart = () => ({type: actionTypes.GET_MEMBER_AMOUNT_START});
const getMemberAmountSuccess = (amount) => ({
  type: actionTypes.GET_MEMBER_AMOUNT_SUCCESS,
  amount
});
const getMemberAmountFail = () => ({type: actionTypes.GET_MEMBER_AMOUNT_FAIL});

export const getMemberAmount = (institutionId, token) => dispatch => {
  dispatch(getMemberAmountStart());
  axios.get(`/Institutions/${institutionId}/MembersAmount`, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  }).then(response => {
    if (response.data.ok) {
      dispatch(getMemberAmountSuccess(response.data.result));
    } else {
      dispatch(getMemberAmountFail());
    }
  })
};

const archiveSubjectStart = () => ({type: actionTypes.ARCHIVE_SUBJECT_START});
const archiveSubjectSucess = (id) => ({type: actionTypes.ARCHIVE_SUBJECT_SUCCESS, id});
const archiveSubjectFail = () => ({type: actionTypes.ARCHIVE_SUBJECT_FAIL});

export const archiveSubject = (institutionId, token, subjectId) => dispatch => {
  dispatch(archiveSubjectStart());
  axios.patch(`/Institutions/${institutionId}/Subjects/${subjectId}`, null,{
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  }).then(response => {
    if(response.data.ok){
      dispatch(archiveSubjectSucess(subjectId));
    }
    else throw new Error(response.data.message)
  }).catch(err => {
    dispatch(archiveSubjectFail());
    Swal({
      title: "Error!",
      text: "One of users already exist in subject",
      type: "error",
      timer: 3000
    })
  })
};

const updateSubjectStart = ()  => ({type: actionTypes.UPDATE_SUBJECT_START});
const updateSubjectSuccess = subject => ({
  type: actionTypes.UPDATE_SUBJECT_SUCCESS,
  subject
});
const updateSubjectFail = () => ({type: actionTypes.UPDATE_SUBJECT_FAIL});

export const updateSubject = (institutionId, token, subject) => dispatch => {
  dispatch(updateSubjectStart());
  axios.put(`/Institutions/${institutionId}/Subjects/${subject.id}`, subject, {
    headers:
      {
        Authorization: `Bearer ${token}`
      }
  })
    .then(response => {
      if(response.data.ok){
        dispatch(updateSubjectSuccess(subject));
      }
      else throw new Error(response.data.message)
    })
    .catch(err => {
      dispatch(updateSubjectFail());
      Swal({
        title: "Error!",
        text: err,
        type: "error",
        timer: 3000
      })
    })
};

export const resetReducer = () => ({type: actionTypes.RESET_REDUCER});



