import axios from '../../axios-instance';
import * as actionTypes from '../actionTypes/assessmentTypes';
import Swal from 'sweetalert2';
import * as loader from './loader';

const getHeaders = (token) => (
    {
        'Authorization': `Bearer ${token}`,
        'Accept': 'text/plain'
    }
);
const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
const getAssessmentsStart = () => ({type: actionTypes.GET_ASSESSMENTS_START});
const getAssessmentsSuccess = assessments => ({type: actionTypes.GET_ASSESSMENTS_SUCCESS, payload: assessments});
const getAssessmentsFail = error => ({type: actionTypes.GET_ASSESSMENTS_FAIL, error});

export const getAllAssessments = (institutionId, subjectId, userToken) => dispatch => {
    dispatch(getAssessmentsStart());
    axios.get(`/Institutions/${institutionId}/Assessments?subjectId=${subjectId}`, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            return response.data.result;
        })
        .then(result => {
            dispatch(getAssessmentsSuccess(result));
        })
        .catch(error => {
            dispatch(getAssessmentsFail(error));
        })
};

const getAssignmentInfoStart = () => ({type: actionTypes.GET_ASSESSMENT_INFO_START});
const getAssignmentInfoSuccess = assessment => ({type: actionTypes.GET_ASSESSMENT_INFO_SUCCESS, payload: assessment});
const getAssignmentInfoFail = error => ({type: actionTypes.GET_ASSESSMENT_INFO_FAIL, error});

export const getAssignmentInfo = (institutionId, assessmentId, userToken) => dispatch => {
    dispatch(getAssignmentInfoStart());
  dispatch(loader.showLoader());
    return axios.get(`/Institutions/${institutionId}/AssessmentAssignments/${assessmentId}`, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            return response.data.result;
        })
        .then(result => {
            dispatch(getAssignmentInfoSuccess(result));
          dispatch(loader.hideLoader());
            return result;
        })
        .catch(error => {
            dispatch(getAssignmentInfoFail(error));
          dispatch(loader.hideLoader());
        })
};

const getAssessmentsGroupsStart = () => ({type: actionTypes.GET_ASSESSMENTS_GROUPS_START});
const getAssessmentsGroupsSuccess = groups => ({type: actionTypes.GET_ASSESSMENTS_GROUPS_SUCCESS, payload: groups});
const getAssessmentsGroupsFail = error => ({type: actionTypes.GET_ASSESSMENTS_GROUPS_FAIL, error});

export const getAssessmentsGroups = (institutionId, assessmentId, userToken) => dispatch => {
    dispatch(getAssessmentsGroupsStart());
    return axios.get(`/Institutions/${institutionId}/GroupsForAssessment?assessmentId=${assessmentId}`, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            return response.data.result;
        })
        .then(result => {
            dispatch(getAssessmentsGroupsSuccess(result));
            return result;
        })
        .catch(error => {
            dispatch(getAssessmentsGroupsFail(error));
        })
};

const addAssessmentsGroupStart = () => ({type: actionTypes.ADD_ASSESSMENTS_GROUP_START});
const addAssessmentsGroupSuccess = group => ({type: actionTypes.ADD_ASSESSMENTS_GROUP_SUCCESS, payload: group});
const addAssessmentsGroupFail = error => ({type: actionTypes.ADD_ASSESSMENTS_GROUP_FAIL, error});

export const addAssessmentGroup = (institutionId, assessmentId, userToken) => dispatch => {
    dispatch(addAssessmentsGroupStart());
    return axios.post(`/Institutions/${institutionId}/GroupsForAssessment`, {assessmentId}, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            return response.data.result;
        })
        .then(result => {
            dispatch(addAssessmentsGroupSuccess(result));
            return result;
        })
        .catch(error => {
            dispatch(addAssessmentsGroupFail(error));
        })
};

const changeAssessmentsGroupStart = () => ({type: actionTypes.CHANGE_ASSESSMENTS_GROUP_START});
const changeAssessmentsGroupSuccess = () => ({type: actionTypes.CHANGE_ASSESSMENTS_GROUP_SUCCESS});
const changeAssessmentsGroupFail = error => ({type: actionTypes.CHANGE_ASSESSMENTS_GROUP_FAIL, error});

export const changeAssessmentGroup = (institutionId, data, userToken) => dispatch => {
    dispatch(changeAssessmentsGroupStart());
    return axios.put(`Institutions/${institutionId}/GroupsForAssessment/Member`, data, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            return response;
        })
        .catch(error => {
            dispatch(changeAssessmentsGroupFail(error));
        })
};

const createAssignmentStart = () => ({
    type: actionTypes.CREATE_NEW_ASSIGNMENT_START
});
const createAssignmentSuccess = updatedAssignment => ({
    type: actionTypes.CREATE_NEW_ASSIGNMENT_SUCCESS,
    updatedAssignment,
});
const createAssignmentFail = () => ({
    type: actionTypes.CREATE_NEW_ASSIGNMENT_FAIL
});

export const createAssignment = (userToken, id, data) => dispatch => {
    dispatch(createAssignmentStart());
    dispatch(loader.showLoader());
    return axios.post(`/Institutions/${id}/AssessmentAssignments`, data, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            dispatch(createAssignmentSuccess());
          dispatch(loader.hideLoader());
            Toast.fire({
              type: 'success',
              title: 'Assignment has been created'
            })
            return {status: true};
        })
        .catch(err => {
            dispatch(createAssignmentFail());
          dispatch(loader.hideLoader());
            Toast.fire({
                type: 'error',
                title: 'Failed!',
                text: err,
                timer: 3000
            });
            return {status: false};
        })
};

const createAttendanceStart = () => ({
    type: actionTypes.CREATE_NEW_ATTENDANCE_START
});
const createAttendanceSuccess = updatedAttendance => ({
    type: actionTypes.CREATE_NEW_ATTENDANCE_SUCCESS,
    updatedAttendance,
});
const createAttendanceFail = () => ({
    type: actionTypes.CREATE_NEW_ATTENDANCE_FAIL
});

export const createAttendance = (userToken, id, data) => dispatch => {
    dispatch(createAttendanceStart());
    dispatch(loader.showLoader());
    return axios.post(`Institutions/${id}/AssessmentsAttendances`, data, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            dispatch(createAttendanceSuccess());
          dispatch(loader.hideLoader());
            Toast.fire({
              type: 'success',
              title: 'Attendance has been created'
            })
            return {status: true};
        })
        .catch(err => {
            dispatch(createAttendanceFail());
          dispatch(loader.hideLoader());
            Toast.fire({
                type: 'error',
                title: 'Failed!',
                text: err,
                timer: 3000
            });
            return {status: false};
        })
};

const createWorkspaceStart = () => ({
    type: actionTypes.CREATE_NEW_WORKSPACE_START
});
const createWorkspaceSuccess = updatedWorkspace => ({
    type: actionTypes.CREATE_NEW_WORKSPACE_SUCCESS,
    updatedWorkspace,
});
const createWorkspaceFail = () => ({
    type: actionTypes.CREATE_NEW_WORKSPACE_FAIL
});

export const createWorkspace = (userToken, id, data) => dispatch => {
    dispatch(createWorkspaceStart());
    dispatch(loader.showLoader());
    return axios.post(`Institutions/${id}/AssessmentsWorkspaces`, data, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            dispatch(createWorkspaceSuccess());
          dispatch(loader.hideLoader());
            Toast.fire({
              type: 'success',
              title: 'Workspace has been created'
            })
            return {status: true};
        })
        .catch(err => {
            dispatch(createWorkspaceFail());
          dispatch(loader.hideLoader());
            Toast.fire({
                type: 'error',
                title: 'Failed!',
                text: err,
                timer: 3000
            });
            return {status: false};
        })
};

const getStudentAssessmentsStart = () => ({type: actionTypes.GET_STUDENTS_ASSESSMENTS_START});
const getStudentAssessmentsSuccess = studentAssessments => ({type: actionTypes.GET_STUDENTS_ASSESSMENTS_SUCCESS, payload: studentAssessments});
const getStudentAssessmentsFail = error => ({type: actionTypes.GET_STUDENTS_ASSESSMENTS_FAIL, error});

export const getStudentAssessments = (institutionId, userToken) => dispatch => {
    dispatch(getStudentAssessmentsStart());
    dispatch(loader.showLoader());
    axios.get(`/Institutions/${institutionId}/Subjects/Assessments`, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            return response.data.result;
        })
        .then(result => {
            dispatch(getStudentAssessmentsSuccess(result));
          dispatch(loader.hideLoader());
        })
        .catch(error => {
            dispatch(getStudentAssessmentsFail(error));
          dispatch(loader.hideLoader());
        })
};

const getStudentAssignmentStart = () => ({type: actionTypes.GET_STUDENTS_ASSIGNMENT_START});
const getStudentAssignmentSuccess = studentAssignment => ({type: actionTypes.GET_STUDENTS_ASSIGNMENT_SUCCESS, payload: studentAssignment});
const getStudentAssignmentFail = error => ({type: actionTypes.GET_STUDENTS_ASSIGNMENT_FAIL, error});

export const getStudentAssignment = (institutionId, assessmentId, userToken) => dispatch => {
    dispatch(getStudentAssignmentStart());
    dispatch(loader.showLoader());
    return axios.get(`/Institutions/${institutionId}/Assessments/${assessmentId}/StudentInfo`, {headers: getHeaders(userToken)})
        .then(response => {
            if (!response.data.ok) {
                throw new Error(response.data.message)
            }
            return response.data.result;
        })
        .then(result => {
            dispatch(getStudentAssignmentSuccess(result));
          dispatch(loader.hideLoader());
        })
        .catch(error => {
            dispatch(getStudentAssignmentFail(error));
          dispatch(loader.hideLoader());
        })
};

const getStudentAttendanceStart = () => ({type: actionTypes.GET_STUDENTS_ATTENDANCE_START});
const getStudentAttendanceSuccess = studentAttendance => ({type: actionTypes.GET_STUDENTS_ATTENDANCE_SUCCESS, payload: studentAttendance});
const getStudentAttendanceFail = error => ({type: actionTypes.GET_STUDENTS_ATTENDANCE_FAIL, error});

export const getStudentAttendance = (institutionId, assessmentId, userToken) => dispatch => {
  dispatch(getStudentAttendanceStart());
  dispatch(loader.showLoader());
  axios.get(`/Institutions/${institutionId}/AssessmentsAttendances/${assessmentId}/Stats/Self`, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getStudentAttendanceSuccess(result));
      dispatch(loader.hideLoader());
    })
    .catch(error => {
      dispatch(getStudentAttendanceFail(error));
      dispatch(loader.hideLoader());
    })
};

const getStudentWorkspaceStart = () => ({type: actionTypes.GET_STUDENTS_WORKSPACE_START});
const getStudentWorkspaceSuccess = studentWorkspace => ({type: actionTypes.GET_STUDENTS_WORKSPACE_SUCCESS, payload: studentWorkspace});
const getStudentWorkspaceFail = error => ({type: actionTypes.GET_STUDENTS_WORKSPACE_FAIL, error});

export const getStudentWorkspace = (institutionId, assessmentId, userToken) => dispatch => {
  dispatch(getStudentWorkspaceStart());
  axios.get(`/Institutions/${institutionId}/AssessmentsAttendances/${assessmentId}/Stats/Self`, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getStudentWorkspaceSuccess(result));
      dispatch(loader.hideLoader());
    })
    .catch(error => {
      dispatch(getStudentWorkspaceFail(error));
      dispatch(loader.hideLoader());
    })
};

const getStudentAssignmentMarkStart = () => ({type: actionTypes.GET_STUDENTS_ASSIGNMENT_START});
const getStudentAssignmentMarkSuccess = studentAssignmentMark => ({type: actionTypes.GET_STUDENTS_ASSIGNMENT_SUCCESS, payload: studentAssignmentMark});
const getStudentAssignmentMarkFail = error => ({type: actionTypes.GET_STUDENTS_ASSIGNMENT_FAIL, error});

export const getStudentAssignmentMark = (institutionId, assessmentId, userToken) => dispatch => {
  dispatch(getStudentAssignmentMarkStart());
  dispatch(loader.showLoader());
  return axios.get(`/Institutions/${institutionId}/AssessmentAssignments/${assessmentId}/StudentReview`, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getStudentAssignmentMarkSuccess(result));
      dispatch(loader.hideLoader());
    })
    .catch(error => {
      dispatch(getStudentAssignmentMarkFail(error));
      dispatch(loader.hideLoader());
    })

};

const getStudentWorkspaceMarkStart = () => ({type: actionTypes.GET_STUDENTS_WORKSPACE_MARK_START});
const getStudentWorkspaceMarkSuccess = studentWorkspaceMark => ({type: actionTypes.GET_STUDENTS_WORKSPACE_MARK_SUCCESS, payload: studentWorkspaceMark});
const getStudentWorkspaceMarkFail = error => ({type: actionTypes.GET_STUDENTS_WORKSPACE_MARK_FAIL, error});

export const getStudentWorkspaceMark = (institutionId, assessmentId, userToken) => dispatch => {
  dispatch(getStudentWorkspaceMarkStart());
  dispatch(loader.showLoader());
  axios.get(`/Institutions/${institutionId}/AssessmentsWorkspaces/${assessmentId}/StudentReview`, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getStudentWorkspaceMarkSuccess(result));
      dispatch(loader.hideLoader());
    })
    .catch(error => {
      dispatch(getStudentWorkspaceMarkFail(error));
      dispatch(loader.hideLoader());
    })

};

export const clearCurrentAssessment = () => ({type: actionTypes.CLEAR_CURRENT_ASSESSMENT});


const updateAssignmentStart = () => ({
  type: actionTypes.UPDATE_ASSIGNMENT_START
});
const updateAssignmentSuccess = updatedAssignment => ({
  type: actionTypes.UPDATE_ASSIGNMENT_SUCCESS,
  updatedAssignment,
});
const updateAssignmentFail = () => ({
  type: actionTypes.UPDATE_ASSIGNMENT_FAIL
});

export const updateAssignment = (userToken, id, data, assignmentId) => dispatch => {
  dispatch(updateAssignmentStart());
  dispatch(loader.showLoader());
  return axios.put(`/Institutions/${id}/AssessmentAssignments/${assignmentId}`, data, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      dispatch(updateAssignmentSuccess());
      dispatch(loader.hideLoader());
      Toast.fire({
        type: 'success',
        title: 'Assignment has been updated'
      })
      return {status: true};
    })
    .catch(err => {
      dispatch(updateAssignmentFail());
      dispatch(loader.hideLoader());
        Toast.fire({
            type: 'error',
            title: 'Failed!',
            text: err,
            timer: 3000
        });
      return {status: false};
    })
};


const getWorkspaceInfoStart = () => ({type: actionTypes.GET_WORKSPACE_INFO_START});
const getWorkspaceInfoSuccess = wokspace => ({type: actionTypes.GET_WORKSPACE_INFO_SUCCESS, payload: wokspace});
const getWorkspaceInfoFail = error => ({type: actionTypes.GET_WORKSPACE_INFO_FAIL, error});

export const getWorkspaceInfo = (institutionId, workspaceId, userToken) => dispatch => {
  dispatch(getWorkspaceInfoStart());
  dispatch(loader.showLoader());
  return axios.get(`/Institutions/${institutionId}/AssessmentsWorkspaces/${workspaceId}`, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getWorkspaceInfoSuccess(result));
      dispatch(loader.hideLoader());
      return result;
    })
    .catch(error => {
      dispatch(getWorkspaceInfoFail(error));
      dispatch(loader.hideLoader());
    })
};


const updateworkspaceStart = () => ({
  type: actionTypes.UPDATE_WORKSPACE_START
});
const updateworkspaceSuccess = updatedworkspace => ({
  type: actionTypes.UPDATE_WORKSPACE_SUCCESS,
  updatedworkspace,
});
const updateworkspaceFail = () => ({
  type: actionTypes.UPDATE_WORKSPACE_FAIL
});

export const updateWorkspace = (userToken, id, data, workspaceId) => dispatch => {
  dispatch(updateworkspaceStart());
  dispatch(loader.showLoader());
  return axios.put(`/Institutions/${id}/AssessmentsWorkspaces/${workspaceId}`, data, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      dispatch(updateworkspaceSuccess());
      dispatch(loader.hideLoader());
      Toast.fire({
        type: 'success',
        title: 'Workspace has been updated'
      })
      return {status: true};
    })
    .catch(err => {
      dispatch(updateworkspaceFail());
      dispatch(loader.hideLoader());
      Toast.fire({
        type: 'error',
        title: 'Failed!',
        text: err,
        timer: 3000
      });
      return {status: false};
    })
};

const getAttendanceInfoStart = () => ({type: actionTypes.GET_ATTENDANCE_INFO_START});
const getAttendanceInfoSuccess = wokspace => ({type: actionTypes.GET_ATTENDANCE_INFO_SUCCESS, payload: wokspace});
const getAttendanceInfoFail = error => ({type: actionTypes.GET_ATTENDANCE_INFO_FAIL, error});

export const getAttendanceInfo = (institutionId, AttendanceId, userToken) => dispatch => {
  dispatch(getAttendanceInfoStart());
  dispatch(loader.showLoader());
  return axios.get(`/Institutions/${institutionId}/AssessmentsAttendances/${AttendanceId}`, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getAttendanceInfoSuccess(result));
      dispatch(loader.hideLoader());
      return result;
    })
    .catch(error => {
      dispatch(getAttendanceInfoFail(error));
      dispatch(loader.hideLoader());
    })
};



const updateAttendanceStart = () => ({
  type: actionTypes.UPDATE_ATTENDANCE_START
});
const updateAttendanceSuccess = updatedAttendance => ({
  type: actionTypes.UPDATE_ATTENDANCE_SUCCESS,
  updatedAttendance,
});
const updateAttendanceFail = () => ({
  type: actionTypes.UPDATE_ATTENDANCE_FAIL
});

export const updateAttendance = (userToken, id, data, AttendanceId) => dispatch => {
  dispatch(updateAttendanceStart());
  dispatch(loader.showLoader());
  return axios.put(`/Institutions/${id}/AssessmentsAttendances/${AttendanceId}`, data, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      dispatch(updateAttendanceSuccess());
      dispatch(loader.hideLoader());
      Toast.fire({
        type: 'success',
        title: 'Attendance has been updated'
      })
      return {status: true};
    })
    .catch(err => {
      dispatch(updateAttendanceFail());
      dispatch(loader.hideLoader());
      Toast.fire({
        type: 'error',
        title: 'Failed!',
        text: err,
        timer: 3000
      });
      return {status: false};
    })
};

const patchAssignmentStart = () => ({type: actionTypes.PATCH_ASSIGNMENT_START});
const patchAssignmentSuccess = () => ({type: actionTypes.PATCH_ASSIGNMENT_SUCCESS});
const patchAssignmentFail = () => ({type: actionTypes.PATCH_ASSIGNMENT_FAIL});

export const patchAssignment = (userToken, institutionId, data) => dispatch => {
  dispatch(patchAssignmentStart());
  dispatch(loader.showLoader());
  return axios.patch(`/Institutions/${institutionId}/AssessmentAssignments/Submit`, data, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      dispatch(patchAssignmentSuccess());
      dispatch(loader.hideLoader());
      return {status: true}
    })
    .catch(err => {
      dispatch(patchAssignmentFail());
    })
};

const patchWorkspaceStart = () => ({type: actionTypes.PATCH_WORKSPACE_START});
const patchWorkspaceSuccess = () => ({type: actionTypes.PATCH_WORKSPACE_SUCCESS});
const patchWorkspaceFail = () => ({type: actionTypes.PATCH_WORKSPACE_FAIL});

export const patchWorkspace = (userToken, institutionId, data) => dispatch => {
  dispatch(patchWorkspaceStart());
  dispatch(loader.showLoader());
  axios.patch(`/Institutions/${institutionId}/AssessmentsWorkspaces/Submit`, data, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      dispatch(patchWorkspaceSuccess());
      dispatch(loader.hideLoader());
      Toast.fire({
        type: 'success',
        title: 'Workspace has been submitted'
      })
      return {status: true}
    })
    .catch(err => {
      dispatch(patchWorkspaceFail());
        dispatch(loader.hideLoader());
        Toast.fire({
            type: 'error',
            title: 'Failed!',
            text: err,
            timer: 3000
        });
    })
};

const getStudentMarkStart = () => ({type: actionTypes.GET_CURRENT_STUDENT_MARK_START});
const getStudentMarkFail = () => ({type: actionTypes.GET_CURRENT_STUDENT_MARK_FAIL});
const getStudentMarkSuccess = () => ({type: actionTypes.GET_CURRENT_STUDENT_MARK_SUCCESS});

export const getStudentMark = (userToken, institutionId, params) => dispatch => {
  dispatch(getStudentMarkStart());
  return axios.get(`/Institutions/${institutionId}/Assessments/Mark`,{
    headers: getHeaders(userToken),
    params
  })
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getStudentMarkSuccess(result));
      return result;
    })
    .catch(error => {
      dispatch(getStudentMarkFail(error));
    })
};

const setStudentMarkStart = () => ({type: actionTypes.SET_CURRENT_STUDENT_MARK_START});
const setStudentMarkFail = () => ({type: actionTypes.SET_CURRENT_STUDENT_MARK_FAIL});
const setStudentMarkSuccess = () => ({type: actionTypes.SET_CURRENT_STUDENT_MARK_SUCCESS});

export const setStudentMark = (userToken, institutionId, data) => dispatch => {
  dispatch(setStudentMarkStart());
  return axios.post(`/Institutions/${institutionId}/Assessments/Mark`,data, {
    headers: getHeaders(userToken),
  })
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(setStudentMarkSuccess(result));
      return result;
    })
    .catch(error => {
      dispatch(setStudentMarkFail(error));
    })
};

export const setCurrentAssessment = assessment => ({type: actionTypes.SET_CURRENT_ASSESSMENT, assessment});

const getStudentAttendanceStatsStart = () => ({type: actionTypes.GET_STUDENT_ATTENDANCE_STATS_START});
const getStudentAttendanceStatsSuccess = stats => ({type: actionTypes.GET_STUDENT_ATTENDANCE_STATS_SUCCESS, payload:{stats}});
const getStudentAttendanceStatsFail = () => ({type: actionTypes.GET_STUDENT_ATTENDANCE_STATS_FAIL});

export const getStudentAttendanceStats = (token, institutionId, userId, assessmentId) => dispatch => {
  dispatch(getStudentAttendanceStatsStart());
  axios.get(`/Institutions/${institutionId}/AssessmentsAttendances/${assessmentId}/${userId}`,{
    headers: getHeaders(token),
  }).then(response => {
    if (!response.data.ok) {
      throw new Error(response.data.message)
    }
    return response.data.result;
  })
    .then(result => {
      dispatch(getStudentAttendanceStatsSuccess(result));
      return result;
    })
    .catch(error => {
      dispatch(getStudentAttendanceStatsFail(error));
    })
};

const getAssignmentsAttachmentsStart = () => ({type: actionTypes.GET_ASSIGNMENT_ATTACHMENTS_START});
const getAssignmentsAttachmentsSuccess = assignmentsAttachments => ({type: actionTypes.GET_ASSIGNMENT_ATTACHMENTS_SUCCESS, payload: assignmentsAttachments});
const getAssignmentsAttachmentsFail = error => ({type: actionTypes.GET_ASSIGNMENT_ATTACHMENTS_FAIL, error});

export const getAssignmentsAttachments = (institutionId, assessmentId, studentId, userToken) => dispatch => {
  dispatch(getAssignmentsAttachmentsStart());
  axios.get(`/Institutions/${institutionId}/AssessmentAssignments/${assessmentId}/user/${studentId}/attachments`, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getAssignmentsAttachmentsSuccess(result));
      return result;
    })
    .catch(error => {
      dispatch(getAssignmentsAttachmentsFail(error));
    })
};

const getAttendanceDateStart = () => ({type: actionTypes.GET_STUDENT_ATTENDANCE_DATE_START});
const getAttendanceDateSuccess = attendanceDate => ({type: actionTypes.GET_STUDENT_ATTENDANCE_DATE_SUCCESS, payload: attendanceDate});
const getAttendanceDateFail = error => ({type: actionTypes.GET_STUDENT_ATTENDANCE_DATE_FAIL, error});

export const getAttendanceDate = (assessmentId, institutionId, userToken) => dispatch => {
  dispatch(getAttendanceDateStart());
  return axios.get(`/Institutions/${institutionId}/AssessmentsAttendances/${assessmentId}/Dates`, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getAttendanceDateSuccess(result));
      return result;
    })
    .catch(error => {
      dispatch(getAttendanceDateFail(error));
    })
};

const createUserAttendanceStart = () => ({
  type: actionTypes.CREATE_USER_ATTENDANCE_START
});
const createUserAttendanceSuccess = updatedAttendance => ({
  type: actionTypes.CREATE_USER_ATTENDANCE_SUCCESS,
  updatedAttendance,
});
const createUserAttendanceFail = () => ({
  type: actionTypes.CREATE_USER_ATTENDANCE_FAIL
});

export const createUserAttendance = (userToken, id, data) => dispatch => {
  dispatch(createUserAttendanceStart());
  dispatch(loader.showLoader());
  return axios.post(`/Institutions/${id}/AssessmentsAttendances/UserAttendances`, data, {headers: getHeaders(userToken)})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      dispatch(createUserAttendanceSuccess());
      dispatch(loader.hideLoader());
      Toast.fire({
        type: 'success',
        title: 'Attendance has been updated'
      })
      return {status: true};
    })
    .catch(err => {
      dispatch(createUserAttendanceFail());
      dispatch(loader.hideLoader());
      Toast.fire({
        type: 'error',
        title: 'Failed!',
        text: err,
        timer: 3000
      });
      return {status: false};
    })
};

const getUserAttendanceStart = () => ({type: actionTypes.GET_USER_ATTENDANCE_START});
const getUserAttendanceSuccess = attendanceUserMark => ({type: actionTypes.GET_USER_ATTENDANCE_SUCCESS, payload: {attendanceUserMark}});
const getUserAttendanceFail = error => ({type: actionTypes.GET_USER_ATTENDANCE_FAIL, error});

export const getUserAttendance = (userToken, id, assessmentId, attendanceDate) => dispatch => {
  dispatch(getUserAttendanceStart());
  return axios.get(`/Institutions/${id}/AssessmentsAttendances/${assessmentId}/UserAttendances`, {
    headers: getHeaders(userToken),
    params: {attendanceDate},
  })
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      return response.data.result;
    })
    .then(result => {
      dispatch(getUserAttendanceSuccess(result));
      return result;
    })
    .catch(error => {
      dispatch(getUserAttendanceFail(error));
    })
};

const changeGroupLeaderStart = () => ({type: actionTypes.CHANGE_GROUP_LEADER_START});
const changeGroupLeaderSuccess = () => ({type: actionTypes.CHANGE_GROUP_LEADER_SUCCESS});
const changeGroupLeaderFail = () => ({type: actionTypes.CHANGE_GROUP_LEADER_FAIL});

export const changeGroupLeader = (id, userToken, assessmentId, userId) => dispatch => {
  dispatch(changeGroupLeaderStart());
  return axios.put(`/Institutions/${id}/GroupsForAssessment/Leader`, {assessmentId, userId}, {
    headers: getHeaders(userToken),
  }).then(response => {
    if (!response.data.ok) {
      throw new Error(response.data.message)
    }
    return response.data.result;
  })
    .then(result => {
      dispatch(changeGroupLeaderSuccess(result));
      Toast.fire({
        type: 'success',
        title: 'The leader has been changed'
      })
      return true;
    })
    .catch(error => {
      dispatch(changeGroupLeaderFail(error));
      Toast.fire({
        type: 'error',
        title: 'Failed!',
        text: 'The leader has not been changed',
        timer: 3000
      });
      return false;
    })
};

