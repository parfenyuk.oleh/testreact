import Swal from 'sweetalert2';

import axios from '../../axios-instance';
import Qs from 'qs';

import * as actions from './index'
import * as actionTypes from '../actionTypes/index';
import {showLoader} from './index';
import {hideLoader} from './loader';
import {TABS} from '../../constants/assignments';

const getAllAssignmentsStart = () => ({type: actionTypes.GET_ALL_ASSIGNMENTS_START});
const getAllAssignmentsSuccess = (data, isArchive) => ({
  type: actionTypes.GET_ALL_ASSIGNMENTS_SUCCESS,
  data,
  isArchive,
});
const getAllAssignmentsFail = () => ({type: actionTypes.GET_ALL_ASSIGNMENTS_FAIL});

export const getAllAssignments = (token,
                                  institutionId,
                                  isArchive,
                                  groupIds = null,
                                  skip = 0,
                                  take = 10,
                                  search = '') => dispatch => {
  dispatch(getAllAssignmentsStart());
  axios.get(`/Institutions/${institutionId}/Assignments`, {
    headers: {Authorization: token},
    params: {
      isArchive,
      groupIds, skip, take, search,
    },
    paramsSerializer: params => Qs.stringify(params, {arrayFormat: 'repeat'}),
  })
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      dispatch(getAllAssignmentsSuccess(response.data.result, isArchive));
    })
    .catch(err => {
      dispatch(getAllAssignmentsFail());
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 3000,
      });
    })
};

const createNewAssignmentStart = () => ({type: actionTypes.CREATE_NEW_ASSIGNMENT_START});
const createNewAssignmentSuccess = (updatedAssignment) => ({
  type: actionTypes.CREATE_NEW_ASSIGNMENT_SUCCESS,
  updatedAssignment,
});
const createNewAssignmentFail = () => ({type: actionTypes.CREATE_NEW_ASSIGNMENT_FAIL});

export const createNewAssignment = (token, institutionId, data) => dispatch => {
  dispatch(createNewAssignmentStart());
  return axios.post(`/Institutions/${institutionId}/Assignments`, data, {headers: {Authorization: token}})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message)
      }
      dispatch(createNewAssignmentSuccess());
      Swal({
        title: 'Created!',
        text: 'Assignment has been created',
        type: 'success',
        timer: 3000,
      });
      dispatch(getAssignmentsAmount(token, institutionId));
      return {status: true};
    })
    .catch(err => {
      dispatch(createNewAssignmentFail());
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 3000,
      });
    })
};

export const clearAssignments = () => ({type: actionTypes.CLEAR_ASSIGNMENTS});

const deleteAssignmentStart = () => ({type: actionTypes.DELETE_ASSIGNMENT_START});
const deleteAssignmentSuccess = assignmentId => (
  {
    type: actionTypes.DELETE_ASSIGNMENT_SUCCESS,
    assignmentId,
  });
const deleteAssignmentFail = () => ({type: actionTypes.DELETE_ASSIGNMENT_FAIL});

export const deleteAssignment = (token, institutionId, assignmentId) => dispatch => {
  dispatch(deleteAssignmentStart());
  axios.delete(`/Institutions/${institutionId}/Assignments/${assignmentId}`,
    {headers: {Authorization: token}})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      Swal({
        title: 'Created!',
        text: 'Assignment has been deleted',
        type: 'success',
        timer: 3000,
      });
      dispatch(deleteAssignmentSuccess(assignmentId));
      dispatch(getAssignmentsAmount(token, institutionId));
    })
    .catch(err => {
      dispatch(deleteAssignmentFail());
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 3000,
      });
    })
};

const updateAssignmentStart = () => ({type: actionTypes.UPDATE_ASSIGNMENT_START});
const updateAssignmentSuccess = (updatedAssignment) => (
  {
    type: actionTypes.UPDATE_ASSIGNMENT_SUCCESS,
    updatedAssignment,
  });
const updateAssignmentFail = () => ({type: actionTypes.UPDATE_ASSIGNMENT_FAIL});

export const updateAssignment = (token, institutionId, data, restore, params) => dispatch => {
  dispatch(updateAssignmentStart());
  axios.put(`/Institutions/${institutionId}/Assignments/${data.id}`, data, {headers: {Authorization: token}})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      // if (restore) {
      //   dispatch(restoreAssignment(token, institutionId, data.id));
      // }
      dispatch(updateAssignmentSuccess(response.data.result));
      dispatch(clearAssignments());
      dispatch(getAllAssignments(token, institutionId, TABS.active, null, 0, params.active * 10));
      dispatch(getAllAssignments(token, institutionId, TABS.archive, null, 0, params.archive * 10));
      dispatch(getAssignmentsAmount(token, institutionId));
      Swal({
        title: 'Updated!',
        text: 'Assignment  was updated',
        type: 'success',
        timer: 3000,
      });
      return {ok: true};
    })
    .catch(err => {
      dispatch(updateAssignmentFail());
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 3000,
      });
      return {ok: false}
    })
};

const getAssignmentsAmountStart = () => ({type: actionTypes.GET_ASSIGNMENTS_AMOUNT_START});
const getAssignmentsAmountSuccess = (amounts) => ({type: actionTypes.GET_ASSIGNMENTS_AMOUNT_SUCCESS, amounts});
const getAssignmentsAmountFail = () => ({type: actionTypes.GET_ASSIGNMENTS_AMOUNT_FAIL});

export const getAssignmentsAmount = (token, institutionId) => dispatch => {
  dispatch(getAssignmentsAmountStart());
  axios.get(`/Institutions/${institutionId}/AssignmentsAmount`, {headers: {Authorization: token}})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      dispatch(getAssignmentsAmountSuccess(response.data.result));
    })
    .catch(err => {
      dispatch(getAssignmentsAmountFail());
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 3000,
      });
    })
};

const getSubmissionsStart = () => ({type: actionTypes.GET_ALL_SUBMISSIONS_START});
const getSubmissionsSuccess = (submissions) => ({type: actionTypes.GET_ALL_SUBMISSIONS_SUCCESS, submissions});
const getSubmissionsFail = () => ({type: actionTypes.GET_ALL_SUBMISSIONS_FAIL});

export const getAllSubmissions = (token,
                                  institutionId,
                                  assignmentId,
                                  skip = 0,
                                  take = 10) => dispatch => {
  dispatch(getSubmissionsStart());
  dispatch(actions.showLoader());
  axios.get(`/Institutions/${institutionId}/Assignments/${assignmentId}/Submissions`,
    {
      headers: {Authorization: token},
      params: {skip, take},
      paramsSerializer: params => Qs.stringify(params, {arrayFormat: 'repeat'}),
    })
    .then(response => {
      dispatch(actions.hideLoader());
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      dispatch(getSubmissionsSuccess(response.data.result));
    })
    .catch(err => {
      dispatch(getSubmissionsFail());
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 3000,
      });
    })
};

export const clearSubmissions = () => ({type: actionTypes.CLEAR_SUBMISSIONS});

const updateSubmissionStart = () => ({type: actionTypes.UPDATE_SUBMISSION_START});
const updateSubmissionSuccess = () => ({type: actionTypes.UPDATE_SUBMISSION_SUCCESS});
const updateSubmissionLocal = payload => ({type: actionTypes.UPDATE_SUBMISSION_LOCAL, payload});
const updateSubmissionFail = () => ({type: actionTypes.UPDATE_SUBMISSION_FAIL});

export const updateSubmission = (token, userId, field, assignmentId, value, institutionId) => dispatch => {
  let fieldUrlPart = field.charAt(0).toUpperCase() + field.substr(1);
  dispatch(updateSubmissionStart());
  dispatch(updateSubmissionLocal({
    userId,
    field,
    value,
  }));
  value = value ? value : 0;
  axios.patch(`/Institutions/${institutionId}/Assignments/${assignmentId}/${fieldUrlPart}/${userId}`, {[field] : value},
    { headers: {Authorization: token},})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      dispatch(updateSubmissionSuccess())
    })
    .catch(err => {
      dispatch(updateSubmissionFail());
    })
};


const archiveAssignmentStart = () => ({type: actionTypes.ARCHIVE_ASSIGNMENT_START});
const archiveAssignmentSuccess = assignmentId => (
  {
    type: actionTypes.ARCHIVE_ASSIGNMENT_SUCCESS,
    assignmentId,
  });
const archiveAssignmentFail = () => ({type: actionTypes.ARCHIVE_ASSIGNMENT_FAIL});

export const archiveAssignment = (token, institutionId, assignmentId) => dispatch => {
  dispatch(archiveAssignmentStart());
  axios.patch(`/Institutions/${institutionId}/Assignments/${assignmentId}/Archive`, {},
    {headers: {Authorization: token}})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      Swal({
        title: 'Success!',
        text: 'Assignment was successfully archived',
        type: 'success',
        timer: 3000,
      });
      dispatch(archiveAssignmentSuccess(assignmentId));
      dispatch(getAssignmentsAmount(token, institutionId));
    })
    .catch(err => {
      dispatch(archiveAssignmentFail());
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 3000,
      });
    })
};

const restoreAssignmentStart = () => ({type: actionTypes.RESTORE_ASSIGNMENT_START});
const restoreAssignmentSuccess = (assignmentId) => ({
  type: actionTypes.RESTORE_ASSIGNMENT_SUCCESS,
  assignmentId,
});
const restoreAssignmentFail = () => ({type: actionTypes.RESTORE_ASSIGNMENT_FAIL});

export const restoreAssignment = (token, institutionId, assignmentId) => dispatch => {
  dispatch(restoreAssignmentStart());
  axios.patch(`/Institutions/${institutionId}/Assignments/${assignmentId}/Restore`, {},
    {headers: {Authorization: token}})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      Swal({
        title: 'Success!',
        text: 'Assignment was successfully restored',
        type: 'success',
        timer: 3000,
      });
      dispatch(restoreAssignmentSuccess(assignmentId));
      dispatch(getAssignmentsAmount(token, institutionId));
    })
    .catch(err => {
      dispatch(restoreAssignmentFail());
    })
};

const setReviewStart = () => ({type: actionTypes.SET_REVIEW_START});
const setReviewSuccess = payload => ({type: actionTypes.SET_REVIEW_SUCCESS, payload});
const setReviewFail = () => ({type: actionTypes.SET_REVIEW_FAIL});

export const setReview = (token, institutionId, assignmentId, data) => dispatch => {
  dispatch(setReviewStart());
  axios.post(`Institutions/${institutionId}/Assignments/${assignmentId}/Submissions/${data.id}`, data,
    {headers: {Authorization: token}})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }

      dispatch(setReviewSuccess({
        ...response.data.result,
        id: data.id,
      }));

      Swal({
        title: 'Success!',
        text: 'Mark has set successfully',
        type: 'success',
        timer: 3000,
      });

    })
    .catch(err => {
      dispatch(setReviewFail());
    })
};

const getWorkspaceInfoStart = () => ({type: actionTypes.GET_WORKSPACE_INFO_START});
const getWorkspaceInfoSuccess = (workspace) => ({type: actionTypes.GET_WORKSPACE_INFO_SUCCESS, workspace});
const getWorkspaceInfoFail = () => ({type: actionTypes.GET_WORKSPACE_INFO_FAIL});

export const getWorkspaceInfo = (institutionId, token, assessmentId, groupForAssessmentId) => dispatch => {
  dispatch(getWorkspaceInfoStart());
  axios.get(`/Institutions/${institutionId}/AssessmentsWorkspaces/${assessmentId}/Group/${groupForAssessmentId}`,
    {headers: {Authorization: token}})
    .then(response => {
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      dispatch(getWorkspaceInfoSuccess(response.data.result));
    })
    .catch(err => {
      dispatch(getWorkspaceInfoFail())
    })
};

const changeAssignmentStatusStart = () => ({type: actionTypes.CHANGE_ASSIGNMENT_STATUS_START});
const changeAssignmentStatusSuccess = (assignmentId, assignmentStatus) => ({
  type: actionTypes.CHANGE_ASSIGNMENT_STATUS_SUCCESS,
  assignmentId,
  assignmentStatus,
});
const changeAssignmentStatusFail = () => ({type: actionTypes.CHANGE_ASSIGNMENT_STATUS_FAIL});

export const changeAssignmentStatus = (token, assessmentId, institutionId, assignmentStatus) => dispatch => {
  dispatch(changeAssignmentStatusStart());
  dispatch(showLoader());
  axios.put(`/Institutions/${institutionId}/AssessmentsWorkspaces/${assessmentId}`,
    {assignmentStatus},
    {headers: {Authorization: token}})
    .then(response => {

      dispatch(hideLoader());
      if (!response.data.ok) {
        throw new Error(response.data.message);
      }
      Swal({
        title: 'Success!',
        text: 'Assignment has submitted successfully',
        type: 'success',
        timer: 3000,
      });
      dispatch(changeAssignmentStatusSuccess(assessmentId, assignmentStatus));
    })
    .catch(err => {
      dispatch(changeAssignmentStatusFail())
    })
};