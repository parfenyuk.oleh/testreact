import * as actionTypes from '../actionTypes/index';
import axios from '../../axios-instance';
import Swal from 'sweetalert2';

export const clearAttendancesList = () => ({type: actionTypes.CLEAT_ATTENDANCES_LIST});

const getAttendancesListStart = () => ({type: actionTypes.GET_ATTENDANCES_START});
const getAttendancesListSuccess = attendances => ({
  type: actionTypes.GET_ATTENDANCES_SUCCESS,
  payload: {
    attendances,
  },
});
const getAttendancesListFail = () => ({type: actionTypes.GET_ATTENDANCES_FAIL});

export const getAttendancesList = (token, institutionId, dateTime, subjectId = null, attendanceMark = 0, skip = 0, take = 1000) => dispatch => {
  dispatch(getAttendancesListStart());

  axios.get(`/Institutions/${institutionId}/Attendances`, {
    headers: {Authorization: token},
    params: {skip, take, dateTime, subjectId, attendanceMark},
  }).then(response => {
    if (!response.data.ok) {
      throw new Error('Not correct attendance');
    }
    dispatch(getAttendancesListSuccess(response.data.result.attendances))
  }).catch(e => {
    dispatch(getAttendancesListFail(e));
  })
};

export const updateAttendanceLocal = (userId, field, value) => ({
  type: actionTypes.UPDATE_ATTENDANCE_LOCAL,
  payload: {
    field,
    value,
    userId,
  },
});

const saveAttendancesStart = () => ({type: actionTypes.SAVE_ATTENDANCES_START});
const saveAttendancesSuccess = () => ({type: actionTypes.SAVE_ATTENDANCES_SUCCESS});
const saveAttendancesFail = () => ({type: actionTypes.SAVE_ATTENDANCES_FAIL});

export const saveAttendances = (institutionId, token, attendances, subjectId) => dispatch => {
  dispatch(saveAttendancesStart());
  axios.post(`/Institutions/${institutionId}/Attendances`, {attendances, subjectId},
    {
      headers: {Authorization: token},
    })
    .then(response => {
      if (!response.data.ok) {
        throw new Error('Ooops something went wrong');
      }
      Swal({
        title: 'Saved!',
        text: 'Attendances have been saved successfully!',
        type: 'success',
        timer: 3000,
      });
      dispatch(saveAttendancesSuccess())
    }).catch(err => {
    Swal({
      title: 'Failed!',
      text: err,
      type: 'error',
      timer: 4000,
    });
    dispatch(saveAttendancesFail(err));
  })
};

export const markAll = mark => ({type: actionTypes.MARK_ALL, payload: mark});