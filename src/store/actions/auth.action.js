import * as actionTypes from '../actionTypes';
import * as loader from './loader';
import axios from '../../axios-instance';
import {history} from '../../helpers';
import {institution} from './user.action';
import Swal from 'sweetalert2';
import {hideLoader} from './loader';
import {refreshToken} from './superadmin';

const signInStart = () => ({type: actionTypes.SIGNIN_START});
const signInSuccess = user => ({type: actionTypes.SIGNIN_SUCCESS, payload: user});
const signInFail = error => ({type: actionTypes.SIGNIN_FAIL, error});

const signUpStart = () => ({type: actionTypes.SIGN_UP_START});
const signUpSuccess = user => ({type: actionTypes.SIGN_UP_SUCCESS, payload: user});
const signUpFail = error => ({type: actionTypes.SIGN_UP_FAIL, error});

const getDataByTokenStart = () => ({type: actionTypes.GET_DATA_BY_TOKEN_START});
const getDataByTokenSuccess = user => ({type: actionTypes.GET_DATA_BY_TOKEN_SUCCESS, user});
const getDataByTokenFail = error => ({type: actionTypes.GET_DATA_BY_TOKEN_FAIL, error});

const logoutStart = () => ({type: actionTypes.LOGOUT_START});
const logoutSuccess = user => ({type: actionTypes.LOGOUT_SUCCESS});
const logoutFail = error => ({type: actionTypes.LOGOUT_FAIL, error});

export const signIn = (email, password) => dispatch => {
  dispatch(signInStart());
  dispatch(loader.showLoader());
  axios.post('/Authorization/login', {email, password}, {crossdomain: true})
    .then(response => {
      if (response.data.ok) {
        return response.data.result
      }
      throw new Error(response.data.message);
    })
    .then(result => {
      dispatch(signInSuccess(result));
      localStorage.setItem('user', JSON.stringify(result));
      console.log(result);
      if(result.institutionId === 0){
        history.push('/registration-flow');
        dispatch(loader.hideLoader());
      }else {
        history.push('/');
        dispatch(institution(`${result.token}`, result.institutionId))
      }

    })
    .catch(err => {
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 4000,
      });
      dispatch(loader.hideLoader());
      dispatch(signInFail());
    });
};

export const signUp = (data) => dispatch => {
  dispatch(signUpStart());
  dispatch(loader.showLoader());
  axios.post('/Authorization/register', {
    firstName: data.firstName,
    lastName: data.lastName,
    email: data.email,
    password: data.password,
  })
    .then(response => {
      if (response.data.ok) {
        return response.data.result
      }
      throw new Error(response.data.message);
    })
    .then(result => {
      dispatch(loader.hideLoader());
      dispatch(signUpSuccess(result));
      localStorage.setItem('user', JSON.stringify(result));
      localStorage.setItem('registration', JSON.stringify({
        email: data.email,
        password: data.password,
      }));
      history.push('/registration-flow');
    })
    .catch(err => {
      dispatch(loader.hideLoader());
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 4000,
      });
      dispatch(signUpFail());
    });
};

export const getDataByToken = (refreshTokenValue, accessToken) => dispatch => {
  dispatch(loader.showLoader());
  dispatch(getDataByTokenStart());

  return axios.get('/Users/info', {headers: {Authorization: `Bearer ${accessToken}`}})
    .then(response => {
      if (response.data.message) {
        throw response
      }
      return response.data.result
    })
    .then(result => {
      const newUserData = {...JSON.parse(localStorage.getItem('user')), ...result};
      localStorage.setItem('user', JSON.stringify(newUserData));
      dispatch(getDataByTokenSuccess(newUserData));
      dispatch(institution(accessToken, result.institutionId));
    })
    .catch(err => {
      dispatch(hideLoader());
      dispatch(getDataByTokenFail(err));

    });
};

export const logout = () => dispatch => {
  dispatch(logoutStart());
  localStorage.removeItem('user');
  history.push('/');
  dispatch(logoutSuccess());
};



