import * as actionTypes from '../actionTypes';
import axios from '../../axios-instance';
import Qs from 'qs';
import Swal from 'sweetalert2';

const getDashboardAssignmentsStart = () => ({type: actionTypes.GET_DASHBOARD_ASSIGNMENTS_START});
const getDashboardAssignmentsSuccess = assignments => ({
  type: actionTypes.GET_DASHBOARD_ASSIGNMENTS_SUCCESS,
  payload: {
    assignments,
  },
});
const getDashboardAssignmentsFail = () => ({type: actionTypes.GET_DASHBOARD_ASSIGNMENTS_FAIL})

export const getDashboardAssignments = (institutionId, token, subjectIds) => dispatch => {
  dispatch(getDashboardAssignmentsStart());

  return axios.get(`Institutions/${institutionId}/Dashboard/Assignments`, {
    headers: {Authorization: `Bearer ${token}`},
    params: {subjectIds},
    paramsSerializer: params => Qs.stringify(params, {arrayFormat: 'repeat'}),
  }).then(response => {
    if (response.data.ok) {
      dispatch(getDashboardAssignmentsSuccess(response.data.result));
      return response.data.result;
    }
    throw new Error('Oooopps something went wrong');
  }).catch(err => {
    dispatch(getDashboardAssignmentsFail());
    Swal({
      title: 'Error!',
      text: err,
      type: 'error',
      timer: 3000,
    })
  })
};

const getTeacherDataStart = () => ({type: actionTypes.GET_TEACHER_DATA_START});
const getTeacherDataSuccess = data => ({
  type: actionTypes.GET_TEACHER_DATA_SUCCESS,
  payload: {data},
});
const getTeacherDataFail = () => ({type: actionTypes.GET_TEACHER_DATA_FAIL});

export const getTeacherData = (institutionId, token, subjectId, assignmentId, startDate = null, endDate = null) =>
  dispatch => {
    dispatch(getTeacherDataStart());

    axios.get(`Institutions/${institutionId}/Dashboard/Teacher`, {
      headers: {Authorization: `Bearer ${token}`},
      params: {
        subjectId,
        assignmentId,
        startDate,
        endDate,
      },
    }).then(response => {
      dispatch(getTeacherDataSuccess(response.data.result));
    }).catch(err => {
      dispatch(getTeacherDataFail());
    })
  };


const getStudentDataStart = () => ({type: actionTypes.GET_STUDENT_DATA_START});
const getStudentDataSuccess = data => ({
  type: actionTypes.GET_STUDENT_DATA_SUCCESS,
  payload: {data},
});
const getStudentDataFail = () => ({type: actionTypes.GET_STUDENT_DATA_FAIL});

export const getStudentData = (institutionId, token, subjectId, startDate = null, endDate = null) =>
  dispatch => {
    dispatch(getStudentDataStart());

    axios.get(`Institutions/${institutionId}/Dashboard/Student`, {
      headers: {Authorization: `Bearer ${token}`},
      params: {
        subjectId,
        startDate,
        endDate,
      },
    }).then(response => {
      dispatch(getStudentDataSuccess(response.data.result));
    }).catch(err => {
      dispatch(getStudentDataFail());
    })
  };

