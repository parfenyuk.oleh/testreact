import { resourcesConstants } from '../actionTypes/resources.constants';
import axios from 'axios';
import Swal from "sweetalert2";
import {hideLoaderDocument, showLoaderDocument} from "./loader";

const documentLoadingStart = () => ({type: resourcesConstants.LOADING_RESOURCE_START});
const documentLoadingSuccess = () =>({type: resourcesConstants.LOADING_RESOURCE_SUCCESS});
const documentLoadingFail = error => ({type: resourcesConstants.LOADING_RESOURCE_FAIL, error});

export const documentLoadApi = (file, token, source) => dispatch => {

    const data = new FormData();
    data.append('file', file);

    dispatch(showLoaderDocument());

    return axios.post(`${process.env.API_URL}/Files/UploadResource`,data,{
        headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token,
        },
        cancelToken: source.token
    })
        .then(response => {
            if (!response.data.ok) {
                throw response.data.message
            }
            return response.data.result
        })
        .then(result => {
            dispatch(hideLoaderDocument());

            return result;
        })
        .catch(err => {
            if (axios.isCancel(err)) {
                console.log('Request canceled');
            }

            dispatch(hideLoaderDocument());
            return 'error'
        });
};

export const cancelRequest = (file, token, isLoading, source) => {
    console.log('ok');
};