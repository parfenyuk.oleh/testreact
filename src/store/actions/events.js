import * as actionTypes from '../actionTypes/index';
import axios from '../../axios-instance';
import Swal from 'sweetalert2';

const createNewEventStart = () => ({type: actionTypes.CREATE_NEW_EVENT_START});
const createNewEventSuccess = () => ({type: actionTypes.CREATE_NEW_EVENT_SUCCESS});
const createNewEventFail = () => ({type: actionTypes.CREATE_NEW_EVENT_FAIL});

export const createNewEvent = (token, institutionId, model) => dispatch => {
  dispatch(createNewEventStart());

  return axios.post(`/Institutions/${institutionId}/Events`, model, {
    headers: {Authorization: token},
  })
    .then(response => {
      if (!response.data.ok) {
        throw new Error('Ooops something went wrong');
      }
      Swal({
        title: 'Saved!',
        text: 'Event have been created successfully!',
        type: 'success',
        timer: 3000,
      });
      dispatch(createNewEventSuccess());
      return response.data.result;
    }).catch(err => {
    Swal({
      title: 'Failed!',
      text: err,
      type: 'error',
      timer: 4000,
    });
    dispatch(createNewEventFail(err));
  });
};


const getAllEventsStart = () => ({type: actionTypes.GET_ALL_EVENTS_START});
const getAllEventsSuccess = list => ({
  type: actionTypes.GET_ALL_EVENTS_SUCCESS,
  payload: {
    list,
  },
});
const getAllEventsFail = () => ({type: actionTypes.GET_ALL_EVENTS_FAIL});

export const getAllEvents = (token, institutionId, startDate, endDate) => dispatch => {
  dispatch(getAllEventsStart());

  axios.get(`/Institutions/${institutionId}/Events`, {
    headers: {Authorization: token},
    params: {startDate, endDate},
  }).then(response => {
    if (!response.data.ok) {
      throw new Error('Ooops something went wrong');
    }
    dispatch(getAllEventsSuccess(response.data.result))
  }).catch(err => {
    dispatch(getAllEventsFail(err));
  });
};

const deleteEventStart = () => ({type: actionTypes.DELETE_EVENT_START});
const deleteEventSuccess = () => ({type: actionTypes.DELETE_EVENT_SUCCESS});
const deleteEventFail = () => ({type: actionTypes.DELETE_EVENT_FAIL});

export const deleteEvent = (token, institutionId, eventId) => dispatch => {
  dispatch(deleteEventStart());

  return axios.delete(`/Institutions/${institutionId}/Events/${eventId}`, {
    headers: {Authorization: token},
  })
    .then(response => {
      if (!response.data.ok) {
        throw new Error('Ooops something went wrong');
      }
      Swal({
        title: 'Saved!',
        text: 'Event have been removed successfully!',
        type: 'success',
        timer: 3000,
      });
      dispatch(deleteEventSuccess());
      return response.data.result;
    }).catch(err => {
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 4000,
      });
      dispatch(deleteEventFail(err));
    });
};


const updateEventStart = () => ({type: actionTypes.UPDATE_EVENT_START});
const updateEventSuccess = () => ({type: actionTypes.UPDATE_EVENT_SUCCESS});
const updateEventFail = () => ({type: actionTypes.UPDATE_EVENT_FAIL});

export const updateEvent = (token, institutionId, model) => dispatch => {
  dispatch(updateEventStart());

  return axios.put(`/Institutions/${institutionId}/Events/${model.id}`, model, {
    headers: {Authorization: token},
  })
    .then(response => {
      if (!response.data.ok) {
        throw new Error('Ooops something went wrong');
      }
      Swal({
        title: 'Saved!',
        text: 'Event have been updated!',
        type: 'success',
        timer: 3000,
      });
      dispatch(updateEventSuccess());
      return response.data.result;
    }).catch(err => {
      Swal({
        title: 'Failed!',
        text: err,
        type: 'error',
        timer: 4000,
      });
      dispatch(updateEventFail(err));
    });
};
