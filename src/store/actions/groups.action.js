import axios from "../../axios-instance";
import {groupsConstants, noticeConstants, userConstants} from "../actionTypes";
import Swal from "sweetalert2";

const getAllGroupsStart = () => ({type: groupsConstants.GET_ALL_GROUPS_START});
const getAllGroupsSuccess = data =>({type: groupsConstants.GET_ALL_GROUPS_SUCCESS, payload: data});
const getAllGroupsFail = error => ({type: groupsConstants.GET_ALL_GROUPS_FAIL, error});

const getMemberActiveGroupStart = () => ({type: groupsConstants.GET_MEMBER_ACTIVE_GROUP_START});
const getMemberActiveGroupSuccess = data =>({type: groupsConstants.GET_MEMBER_ACTIVE_GROUP_SUCCESS, payload: data});
const getMemberActiveGroupFail = error => ({type: groupsConstants.GET_MEMBER_ACTIVE_GROUP_FAIL, error});

const addForumItemStart = () => ({type: groupsConstants.ADD_GROUP_ITEM_START});
const addForumItemSuccess = data =>({type: groupsConstants.ADD_GROUP_ITEM_SUCCESS, payload: data});
const addForumItemFail = error => ({type: groupsConstants.ADD_GROUP_ITEM_FAIL, error});

const addGroupMemberStart = () => ({type: groupsConstants.ADD_MEMBER_ITEM_START});
const addGroupMemberSuccess = data =>({type: groupsConstants.ADD_MEMBER_ITEM_SUCCESS, payload: data});
const addGroupMemberFail = error => ({type: groupsConstants.ADD_MEMBER_ITEM_FAIL, error});

const deleteGroupItemStart = () => ({type: groupsConstants.DELETE_GROUP_ITEM_START});
const deleteGroupItemSuccess = () =>({type: groupsConstants.DELETE_GROUP_ITEM_SUCCESS});
const deleteGroupItemFail = error => ({type: groupsConstants.DELETE_GROUP_ITEM_FAIL, error});

const deleteGroupMemberStart = () => ({type: groupsConstants.DELETE_GROUP_MEMBER_START});
const deleteGroupMemberSuccess = () =>({type: groupsConstants.DELETE_GROUP_MEMBER_SUCCESS});
const deleteGroupMemberFail = error => ({type: groupsConstants.DELETE_GROUP_MEMBER_FAIL, error});

export const getAllGroups = (token, id) => dispatch => {
    dispatch(getAllGroupsStart());
    return axios.get(`/Institutions/${id}/Groups`, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(getAllGroupsSuccess(result));

            return result;
        })
        .catch(err => {
            dispatch(getAllGroupsFail(err));
        });
};

export const getMemberActiveGroup = (token, id, groupId) => dispatch => {
    dispatch(getMemberActiveGroupStart());
    return axios.get(`/Institutions/${id}/Groups/${groupId}/Members`, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(getMemberActiveGroupSuccess(result));

            return result;
        })
        .catch(err => {
            dispatch(getMemberActiveGroupFail(err));
        });
};

export const addGroupItem = (token, id, result) => (dispatch, getState) => {

    dispatch(addForumItemStart());

    return axios.post(`/Institutions/${id}/Groups`, result, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            let groupList = getState().groups.list;
            groupList.push(result);
            dispatch(addForumItemSuccess(groupList));

            Swal({
                title: "Saved!",
                text: "Forum is added",
                type: "success",
                timer: 3000
            });

            return result;
        })
        .catch(err => {

            Swal({
                title: "Oops...",
                text: err.data.message,
                type: "error",
                timer: 3000
            });

            dispatch(addForumItemFail(err));

            return err;
        });
};

export const addGroupMember = (token, id, groupId, membersIds) => (dispatch, getState) => {

    dispatch(addGroupMemberStart());

    return axios.post(`/Institutions/${id}/Groups/${groupId}/Members`, membersIds, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(addGroupMemberSuccess());
        })
        .catch(err => {
            Swal({
                title: "Error!",
                text: err.data.message,
                type: "error",
                timer: 3000
            });
            dispatch(addGroupMemberFail(err));
        });
};

export const deleteGroupItem = (token, id, groupId) => dispatch => {

    dispatch(deleteGroupItemStart());
    return axios.delete(`/Institutions/${id}/Groups/${groupId}`, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }

            return response.data.result
        })
        .then(result => {
            dispatch(deleteGroupItemSuccess());

            return result;
        })
        .catch(err => {
            dispatch(deleteGroupItemFail(err));

            return err;
        });
};

export const deleteGroupMember = (token, id, groupId, userId) => dispatch => {

    dispatch(deleteGroupMemberStart());
    return axios.delete(`/Institutions/${id}/Groups/${groupId}/Members/${userId}`, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }

            return response.data.result
        })
        .then(result => {
            dispatch(deleteGroupMemberSuccess());

            return result;
        })
        .catch(err => {
            dispatch(deleteGroupMemberFail(err));

            return err;
        });
};