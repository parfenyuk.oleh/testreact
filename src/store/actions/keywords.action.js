import axios from '../../axios-instance';
import {keywordsConstants} from '../actionTypes/keywords.constant';
import Swal from 'sweetalert2';

const getKeywordsListStart = () => ({type: keywordsConstants.GET_KEYWORD_LIST_START});
const getKeywordsListSuccess = list => ({type: keywordsConstants.GET_KEYWORD_LIST_SUCCESS, payload: list});
const getKeywordsListFail = error => ({type: keywordsConstants.GET_KEYWORD_LIST_FAIL, error});

const addKeywordsItemStart = () => ({type: keywordsConstants.ADD_KEYWORD_ITEM_START});
const addKeywordsItemSuccess = list => ({type: keywordsConstants.ADD_KEYWORD_ITEM_SUCCESS, payload: list});
const addKeywordsItemFail = error => ({type: keywordsConstants.ADD_KEYWORD_ITEM_FAIL, error});

const deleteKeywordsItemStart = () => ({type: keywordsConstants.DELETE_KEYWORD_ITEM_START});
const deleteKeywordsItemSuccess = () => ({type: keywordsConstants.DELETE_KEYWORD_ITEM_SUCCESS});
const deleteKeywordsItemFail = () => ({type: keywordsConstants.DELETE_KEYWORD_ITEM_FAIL});

export const clearKeywordsList = () => ({type: keywordsConstants.CLEAR_KEYWORD_LIST});

export const getKeywordsList = (token, institutionId, skip = 0, take = 10, search = '') => dispatch => {

  dispatch(getKeywordsListStart());

  return axios.get(`/Institutions/${institutionId}/Keywords`, {
    headers: {Authorization: token},
    params: {
      skip,
      take,
      search
    }
  })
    .then(response => {
      if (response.data.message) {
        throw response
      }

      return response.data.result
    })
    .then(result => {
      if (search) dispatch(clearKeywordsList());
      dispatch(getKeywordsListSuccess(result));
    })
    .catch(error => {
      dispatch(getKeywordsListFail(error));
    })
};

export const addKeywordsItem = (token, institutionId, result) => dispatch => {
  dispatch(addKeywordsItemStart());

  return axios.post(`/Institutions/${institutionId}/Keywords`, result, {
    headers: {Authorization: token},
  }).then(result => {
    if(!result.data.ok){
      throw new Error('Key word should be without white spaces');
    }
    dispatch(addKeywordsItemSuccess());

    Swal({
      title: "Created!",
      text: "New keywords was created",
      type: "success",
      timer: 3000
    });
  }).catch(err => {
    Swal({
      title: "Ooops..!",
      text: err,
      type: "error",
      timer: 3000
    });
    dispatch(addKeywordsItemFail());
  })
};

export const deleteKeywordsItem = (token, institutionId, keywordId) => dispatch => {
  dispatch(deleteKeywordsItemStart());

  return axios.delete(`/Institutions/${institutionId}/Keywords/${keywordId}`, {
    headers: {Authorization: token}
  })
    .then(response => {
      if (response.data.ok) {
        dispatch(deleteKeywordsItemSuccess());
      } else {
        dispatch(deleteKeywordsItemFail());
      }
    })
};