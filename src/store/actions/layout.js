import * as actionsTypes from '../actionTypes/layout'

export const showModal = modal => ({type: actionsTypes.SHOW_LAYOUT_MODAL, modal});
export const hideModal = modal => ({type: actionsTypes.HIDE_LAYOUT_MODAL, modal});