import * as actions from '../actionTypes/index';

export const showLoader = () => ({type: actions.SHOW_LOADER});
export const hideLoader = () => ({type: actions.HIDE_LOADER});
export const showLoaderDocument = () => ({type: actions.SHOW_DOCUMENT_LOADER});
export const hideLoaderDocument = () => ({type: actions.HIDE_DOCUMENT_LOADER});