import axios from '../../axios-instance';
import {milestonesConstants} from '../actionTypes/milestones.constant';
import Swal from 'sweetalert2';

const getMilestonesListStart = () => ({type: milestonesConstants.GET_MILESTONES_LIST_START});
const getMilestonesListSuccess = list => ({type: milestonesConstants.GET_RESOURCES_LIST_SUCCESS, payload: list});
const getMilestonesListFail = error => ({type: milestonesConstants.GET_MILESTONES_LIST_FAIL, error});

const addMilestoneStart = () => ({type: milestonesConstants.ADD_MILESTONE_START});
const addMilestoneSuccess = list => ({type: milestonesConstants.ADD_MILESTONE_SUCCESS});
const addMilestoneFail = error => ({type: milestonesConstants.ADD_MILESTONE_FAIL, error});

const deleteMilestoneStart = () => ({type: milestonesConstants.DELETE_MILESTONE_START});
const deleteMilestoneSuccess = milestoneId => ({type: milestonesConstants.DELETE_RESOURCE_SUCCESS, milestoneId});
const deleteMilestoneFail = () => ({type: milestonesConstants.DELETE_MILESTONE_FAIL});

const updateMilestoneStart = () => ({type: milestonesConstants.UPDATE_MILESTONE_START});
const updateMilestoneSuccess = () => ({type: milestonesConstants.UPDATE_RESOURCE_SUCCESS});
const updateMilestoneFail = () => ({type: milestonesConstants.UPDATE_MILESTONE_FAIL});

export const clearResourcesList = () => ({type: milestonesConstants.CLEAR_MILESTONES_LIST});

export const getMilestonesList = (token, institutionId, startDate, endDate) => dispatch => {
    dispatch(getMilestonesListStart());

    return axios.get(`/Institutions/${institutionId}/Milestones`, {
        headers: {Authorization: token},
        params: {
            startDate,
            endDate,
        },
    })
        .then(response => {
            if (response.data.message) {
                throw response
            }

            return response.data.result
        })
        .then(result => {
            const user = JSON.parse(localStorage.getItem('user'));

            result.users.sort(function(a, b) {
                if (a.userId === user.id) {
                    return -1;
                }
                return 0;
            });

            dispatch(getMilestonesListSuccess(result));

            return result;
        })
        .catch(error => {
            dispatch(getMilestonesListFail(error));
        })
};

export const addMilestone = (token, institutionId, result) => dispatch => {
    dispatch(addMilestoneStart());

    return axios.post(`/Institutions/${institutionId}/Milestones`, result, {
        headers: {Authorization: token},
    }).then(response => {
        if (response.data.message) {
            throw response
        }

        return response.data.result;
    }).then(() => {
        dispatch(addMilestoneSuccess());

        Swal({
            title: 'Created!',
            text: 'New Milestone was created',
            type: 'success',
            timer: 3000
        });
    })
        .catch(e => {
            Swal({
                title: 'Ooops...',
                text: e.data.message,
                type: 'error',
                timer: 3000
            });

            dispatch(addMilestoneFail());
        })
};

export const deleteMilestone = (token, institutionId, milestoneId) => dispatch => {
    dispatch(deleteMilestoneStart());

    return axios.delete(`/Institutions/${institutionId}/Milestones/${milestoneId}`, {
        headers: { Authorization: token }})
        .then(response => {
            if(response.data.ok){
                dispatch(deleteMilestoneSuccess(milestoneId));
            }else{
                dispatch(deleteMilestoneFail());
            }
        })
};

export const updateMilestone = (token, institutionId, milestoneId, result) => dispatch => {
    dispatch(updateMilestoneStart());
    console.log(result);
    return axios.put(`/Institutions/${institutionId}/Milestones/${milestoneId}`, result, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(updateMilestoneSuccess());

            return result;
        })
        .catch(err => {
            Swal({
                title: 'Ooops...',
                text: err.data.message,
                type: 'error',
                timer: 3000
            });

            dispatch(updateMilestoneFail(err));

            return err;
        });
};