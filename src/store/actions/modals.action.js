import * as actionTypes from '../actionTypes/index';

export const showModal = (modalType, titles, action) => ({
    type: actionTypes.SHOW_MODAL,
    modalType,
    titles,
    action
});

export const hideModal = () => ({type: actionTypes.HIDE_MODAL});

export const closeWithConfirm = () => ({type: actionTypes.WITH_CONFIRM});
