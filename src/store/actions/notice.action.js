import axios from "../../axios-instance";
import {noticeConstants} from "../actionTypes/notice.constants";
import ROLES from "../../constants/roles";
import * as actionTypes from "../actionTypes";
import Swal from "sweetalert2";

const addNoticeItemStart = () => ({type: noticeConstants.ADD_NOTICE_ITEM_START});
const addNoticeItemSuccess = list =>({type: noticeConstants.ADD_NOTICE_ITEM_SUCCESS});
const addNoticeItemFail = error => ({type: noticeConstants.ADD_NOTICE_ITEM_FAIL, error});

const getNoticeListStart = () => ({ type: noticeConstants.GET_NOTICE_LIST_START });
const getNoticeListSuccess = (list) => ({ type: noticeConstants.GET_NOTICE_LIST_SUCCESS, payload: list });
const getNoticeListFail = () => ({ type: noticeConstants.GET_NOTICE_LIST_FAIL });

const deleteNoticeItemStart = () => ({ type: noticeConstants.DELETE_NOTICE_ITEM_START });
const deleteNoticeItemSuccess = (list) => ({ type: noticeConstants.DELETE_NOTICE_ITEM_SUCCESS, payload: list });
const deleteNoticeItemFail = () => ({ type: noticeConstants.DELETE_NOTICE_ITEM_FAIL });

const getNoticeDetailStart = () => ({ type: noticeConstants.GET_NOTICE_DETAIL_START });
const getNoticeDetailSuccess = (item) => ({ type: noticeConstants.GET_NOTICE_DETAIL_SUCCESS, payload: item });
const getNoticeDetailFail = () => ({ type: noticeConstants.GET_NOTICE_DETAIL_FAIL });

const updateNoticeStart = () => ({ type: noticeConstants.UPDATE_NOTICE_DETAIL_START });
const updateNoticeSuccess = () => ({ type: noticeConstants.UPDATE_NOTICE_DETAIL_SUCCESS });
const updateNoticeFail = () => ({ type: noticeConstants.UPDATE_NOTICE_DETAIL_FAIL });

const getNoticeRepliesStart = () => ({ type: noticeConstants.GET_NOTICE_REPLIES_START });
const getNoticeRepliesSuccess = (list) => ({ type: noticeConstants.GET_NOTICE_REPLIES_SUCCESS, payload: list });
const getNoticeRepliesFail = () => ({ type: noticeConstants.GET_NOTICE_REPLIES_FAIL });

const addNoticeRepliesStart = () => ({ type: noticeConstants.ADD_NOTICE_REPLIES_START });
const addNoticeRepliesSuccess = (list) => ({ type: noticeConstants.ADD_NOTICE_REPLIES_SUCCESS, payload: list });
const addNoticeRepliesFail = () => ({ type: noticeConstants.ADD_NOTICE_REPLIES_FAIL });

const deleteNoticeRepliesStart = () => ({ type: noticeConstants.DELETE_NOTICE_REPLIES_START });
const deleteNoticeRepliesSuccess = () => ({ type: noticeConstants.DELETE_NOTICE_REPLIES_SUCCESS });
const deleteNoticeRepliesFail = () => ({ type: noticeConstants.DELETE_NOTICE_REPLIES_FAIL });

const updateNoticeRepliesStart = () => ({ type: noticeConstants.UPDATE_NOTICE_REPLIES_START });
const updateNoticeRepliesSuccess = () => ({ type: noticeConstants.UPDATE_NOTICE_REPLIES_SUCCESS });
const updateNoticeRepliesFail = () => ({ type: noticeConstants.UPDATE_NOTICE_REPLIES_FAIL });

export const addNoticeItem = (token, id, notice) => (dispatch, getState) => {
    dispatch(addNoticeItemStart());

    return axios.post(`/Institutions/${id}/Notices`, notice, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(addNoticeItemSuccess());

            Swal({
                title: "Saved!",
                text: "Notice is added",
                type: "success",
                timer: 3000
            });
        })
        .catch(err => {
            dispatch(addNoticeItemFail(err));
        });
};

export const getNoticeList = (token, institutionId, url = null, skip, take) => dispatch => {

    dispatch(getNoticeListStart());
    url ? url = `/Institutions/${institutionId}/Notices${url}` : url = `/Institutions/${institutionId}/Notices`;

    return axios.get(url,{
        headers: { Authorization: token},
    })
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
           dispatch(getNoticeListSuccess(result));
        })
        .catch(error => {
            dispatch(getNoticeListFail(error));
        })
};

export const clearNoticeList = () => ({type: noticeConstants.CLEAR_NOTICE_LIST});

export const deleteNoticeItem = (token, id, noticeId) => dispatch => {

    dispatch(deleteNoticeItemStart());
    console.log(token, id, noticeId);
    return axios.delete(`/Institutions/${id}/Notices/${noticeId}`, { headers: { Authorization: token }})
        .then(response => {
            console.log(response);
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(deleteNoticeItemSuccess());

            return result;
        })
        .catch(err => {
            dispatch(deleteNoticeItemFail(err));

            return err;
        });
};

export const getNoticeDetail = (token, institutionId, noticeId) => dispatch => {
    dispatch(getNoticeDetailStart());

    return axios.get(`/Institutions/${institutionId}/Notices/${noticeId}`, {
        headers: { Authorization: token},
    })
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(getNoticeDetailSuccess(result));
        })
        .catch(error => {
            dispatch(getNoticeDetailFail(error));
        })
};

export const updateNotice = (token, id, noticeId, message) => dispatch => {

    dispatch(updateNoticeStart());
    return axios.put(`/Institutions/${id}/Notices/${noticeId}`, message, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(updateNoticeSuccess());

            return result;
        })
        .catch(err => {
            dispatch(updateNoticeFail(err));

            return err;
        });
};

export const getNoticeReplies = (token, institutionId, noticeId, skip, take = 10) => dispatch => {
    dispatch(getNoticeRepliesStart());

    return axios.get(`/Institutions/${institutionId}/Notices/${noticeId}/Replies`, {
        headers: { Authorization: token},
        params: {
            skip,
            take
            }
        })
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(getNoticeRepliesSuccess(result));
        })
        .catch(error => {
            dispatch(getNoticeRepliesFail(error));
        })
};

export const addNoticeReplies = (token, id, noticeId, message) => (dispatch, getState) => {

    dispatch(addNoticeRepliesStart());

    return axios.post(`/Institutions/${id}/Notices/${noticeId}/Replies`, message, { headers: { Authorization: token }})
        .then(response => {
            console.log(response);
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            let noticeList = getState().notice.repliesList;
            let forumDetail = getState().notice.item;
            forumDetail['repliesCount'] = forumDetail.repliesCount + 1;

            noticeList.unshift(result);
            dispatch(addNoticeRepliesSuccess(noticeList));
        })
        .catch(err => {
            Swal({
                title: "Error!",
                text: "The field Message must be a string with a maximum length of 500",
                type: "error",
            });
            dispatch(addNoticeRepliesFail(err));
        });
};

export const deleteNoticeReplies = (token, id, noticeId, replyId) => (dispatch, getState) => {

    dispatch(deleteNoticeRepliesStart());
    return axios.delete(`/Institutions/${id}/Notices/${noticeId}/Replies/${replyId}`, { headers: { Authorization: token }})
        .then(response => {

            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            let forumDetail = getState().notice.item;
            forumDetail['repliesCount'] = forumDetail.repliesCount - 1;

            dispatch(deleteNoticeRepliesSuccess(forumDetail));
            return result;
        })
        .catch(err => {
            dispatch(deleteNoticeRepliesFail(err));

            return err;
        });
};

export const updateNoticeReplies = (token, id, noticeId, replyId, message) => dispatch => {

    dispatch(updateNoticeRepliesStart());
    return axios.put(`/Institutions/${id}/Notices/${noticeId}/Replies/${replyId}`, message, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(updateNoticeRepliesSuccess());

            return result;
        })
        .catch(err => {
            dispatch(updateNoticeRepliesFail(err));

            return err;
        });
};