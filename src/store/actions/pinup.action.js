import axios from "../../axios-instance";
import {pinupConstants} from "../actionTypes/pinup.constants";
import Swal from "sweetalert2";
import * as loaderActions from "./loader";
import {getAllSubjects} from "./admin.action";
import * as actionTypes from "../actionTypes";

const getPinupListStart = () => ({type: pinupConstants.GET_PINUP_LIST_START});
const getPinupListSuccess = list => ({type: pinupConstants.GET_PINUP_LIST_SUCCESS, payload: list});
const getPinupListFail = error => ({type: pinupConstants.GET_PINUP_LIST_FAIL, error});

const addPinupItemStart = () => ({type: pinupConstants.ADD_PINUP_ITEM_START});
const addPinupItemSuccess = list => ({type: pinupConstants.ADD_PINUP_ITEM_SUCCESS, payload: list});
const addPinupItemFail = error => ({type: pinupConstants.ADD_PINUP_ITEM_FAIL, error});

const deletePinupItemStart = () => ({type: pinupConstants.DELETE_PINUP_ITEM_START});
const deletePinupItemSuccess = () => ({type: pinupConstants.DELETE_PINUP_ITEM_SUCCESS});
const deletePinupItemFail = () => ({type: pinupConstants.DELETE_PINUP_ITEM_FAIL});

const updatePinupItemStart = () => ({type: pinupConstants.UPDATE_PINUP_ITEM_START});
const updatePinupItemSuccess = () => ({type: pinupConstants.UPDATE_PINUP_ITEM_SUCCESS});
const updatePinupItemFail = () => ({type: pinupConstants.UPDATE_PINUP_ITEM_FAIL});

export const clearPinupList = () => ({type: pinupConstants.CLEAR_PINUP_LIST});

export const getPinupList = (token, institutionId, privacyType = 1, skip, take = 10) => dispatch => {

    dispatch(getPinupListStart());

    return axios.get(`/Institutions/${institutionId}/PinUps`, {
        headers: {Authorization: token},
        params: {
            privacyType,
            skip,
            take,
        }
    })
        .then(response => {
            if (response.data.message) {
                throw response
            }

            return response.data.result
        })
        .then(result => {
            dispatch(getPinupListSuccess(result));
        })
        .catch(error => {
            dispatch(getPinupListFail(error));
        })
};

export const addPinupItem = (token, institutionId, result) => dispatch => {
    dispatch(addPinupItemStart());

    return axios.post(`/Institutions/${institutionId}/PinUps`, result, {
        headers: {Authorization: token},
    }).then(result => {
        console.log(result);
        dispatch(addPinupItemSuccess());

        Swal({
            title: "Created!",
            text: "New pin-up was created",
            type: "success",
            timer: 3000
        });
    }).catch(e => {
        dispatch(addPinupItemFail());
    })
};

export const deletePinupItem = (token, institutionId, pinupId) => dispatch => {
    dispatch(deletePinupItemStart());

    return axios.delete(`/Institutions/${institutionId}/PinUps/${pinupId}`, {
        headers: { Authorization: token }})
        .then(response => {
            console.log(response);
            if(response.data.ok){
                dispatch(deletePinupItemSuccess());
            }else{
                dispatch(deletePinupItemFail());
            }
        })
};

export const updatePinupItem = (token, institutionId, pinupId, result) => dispatch => {

    dispatch(updatePinupItemStart());
    return axios.put(`/Institutions/${institutionId}/PinUps/${pinupId}`, result, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(updatePinupItemSuccess());

            return result;
        })
        .catch(err => {
            dispatch(updatePinupItemFail(err));

            return err;
        });
};