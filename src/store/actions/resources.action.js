import axios from "../../axios-instance";
import {resourcesConstants} from "../actionTypes/resources.constants";
import Swal from "sweetalert2";

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

const getResourcesGroupListStart = () => ({type: resourcesConstants.GET_RESOURCES_GROUP_LIST_START});
const getResourcesGroupListSuccess = list => ({type: resourcesConstants.GET_RESOURCES_GROUP_LIST_SUCCESS, payload: list});
const getResourcesFail = error => ({type: resourcesConstants.GET_RESOURCES_GROUP_LIST_FAIL, error});

const getResourcesStart = () => ({type: resourcesConstants.GET_RESOURCES_START});
const getResourcesSuccess = list => ({type: resourcesConstants.GET_RESOURCES_SUCCESS, payload: list});
const getResourcesPaginationSuccess= list => ({type: resourcesConstants.GET_RESOURCES_PAGINATION_SUCCESS, payload: list});
const getResourcesGroupListFail = error => ({type: resourcesConstants.GET_RESOURCES_FAIL, error});

const addResourcesGroupItemStart = () => ({type: resourcesConstants.ADD_RESOURCES_GROUP_ITEM_START});
const addResourcesGroupItemSuccess = list => ({type: resourcesConstants.ADD_RESOURCES_GROUP_ITEM_SUCCESS, payload: list});
const addResourcesGroupItemFail = error => ({type: resourcesConstants.ADD_RESOURCES_GROUP_ITEM_FAIL, error});

const addResourceStart = () => ({type: resourcesConstants.ADD_RESOURCE_START});
const addResourceSuccess = list => ({type: resourcesConstants.ADD_RESOURCE_SUCCESS, payload: list});
const addResourceFail = error => ({type: resourcesConstants.ADD_RESOURCE_FAIL, error});

const deleteResourcesGroupItemStart = () => ({type: resourcesConstants.DELETE_RESOURCES_GROUP_ITEM_START});
const deleteResourcesGroupItemSuccess = () => ({type: resourcesConstants.DELETE_RESOURCES_GROUP_ITEM_SUCCESS});
const deleteResourcesGroupItemFail = () => ({type: resourcesConstants.DELETE_RESOURCES_GROUP_ITEM_FAIL});

const deleteResourceStart = () => ({type: resourcesConstants.DELETE_RESOURCE_START});
const deleteResourceSuccess = () => ({type: resourcesConstants.DELETE_RESOURCE_SUCCESS});
const deleteResourceFail = () => ({type: resourcesConstants.DELETE_RESOURCE_FAIL});

const updateResourcesGroupItemStart = () => ({type: resourcesConstants.UPDATE_RESOURCES_GROUP_ITEM_START});
const updateResourcesGroupItemSuccess = () => ({type: resourcesConstants.UPDATE_RESOURCES_GROUP_ITEM_SUCCESS});
const updateResourcesGroupItemFail = () => ({type: resourcesConstants.UPDATE_RESOURCES_GROUP_ITEM_FAIL});

export const clearResourcesList = () => ({type: resourcesConstants.CLEAR_RESOURCES_LIST});

export const getResourcesGroupList = (token, institutionId) => dispatch => {

    dispatch(getResourcesGroupListStart());

    return axios.get(`/Institutions/${institutionId}/ResourcesGroups`, {
        headers: {Authorization: token}
    })
        .then(response => {
            if (response.data.message) {
                throw response
            }

            return response.data.result
        })
        .then(result => {
            dispatch(getResourcesGroupListSuccess(result));

            return result;
        })
        .catch(error => {
            dispatch(getResourcesGroupListFail(error));
        })
};

export const addResourcesGroupItem = (token, institutionId, result) => dispatch => {
    dispatch(addResourcesGroupItemStart());

    return axios.post(`/Institutions/${institutionId}/ResourcesGroups`, result, {
        headers: {Authorization: token},
    }).then(result => {
        dispatch(addResourcesGroupItemSuccess());
        Toast.fire({
            type: 'success',
            title: 'New Resource group was created'
        })
    }).catch(e => {
        dispatch(addResourcesGroupItemFail());
    })
};

export const deleteResourcesGroupItem = (token, institutionId, groupId) => dispatch => {
    dispatch(deleteResourcesGroupItemStart());

    return axios.delete(`/Institutions/${institutionId}/ResourcesGroups/${groupId}`, {
        headers: { Authorization: token }})
        .then(response => {
            if(response.data.ok){
                dispatch(deleteResourcesGroupItemSuccess());
            }else{
                dispatch(deleteResourcesGroupItemFail());
            }
        })
};

export const updateResourcesGroupItem = (token, institutionId, groupId, result) => dispatch => {
    dispatch(updateResourcesGroupItemStart());
    console.log(groupId, result);
    return axios.put(`/Institutions/${institutionId}/ResourcesGroups/${groupId}`, result, { headers: { Authorization: token }})
        .then(response => {
            console.log(response);
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(updateResourcesGroupItemSuccess());

            return result;
        })
        .catch(err => {
            dispatch(updateResourcesGroupItemFail(err));

            return err;
        });
};

export const addResource = (token, institutionId, resourcesGroupId, result) => dispatch => {
    dispatch(addResourceStart());

    return axios.post(`/Institutions/${institutionId}/ResourcesGroups/${resourcesGroupId}/Resources`, result, {
        headers: {Authorization: token},
    }).then(result => {
        console.log('addResource', result);
        dispatch(addResourceSuccess());
        Toast.fire({
            type: 'success',
            title: 'New Resource was created'
        })
    }).catch(e => {
        dispatch(addResourceFail());
    })
};

export const getResources = (token, institutionId, resourcesGroupId, url, pagination) => dispatch => {
    dispatch(getResourcesStart());

    return axios.get(`/Institutions/${institutionId}/ResourcesGroups/${resourcesGroupId}/Resources${url}`, {
        headers: {Authorization: token}
    })
        .then(response => {
            if (response.data.message) {
                throw response
            }

            return response.data.result
        })
        .then(result => {

            pagination ? dispatch(getResourcesPaginationSuccess(result)) : dispatch(getResourcesSuccess(result));

            return result;
        })
        .catch(error => {
            dispatch(getResourcesFail(error));
        })
};

export const deleteResource = (token, institutionId, resourcesGroupId, resourceId) => dispatch => {
    dispatch(deleteResourceStart());

    return axios.delete(`/Institutions/${institutionId}/ResourcesGroups/${resourcesGroupId}/Resources/${resourceId}`, {
        headers: { Authorization: token }})
        .then(response => {
            if(response.data.ok){
                dispatch(deleteResourceSuccess());
            }else{
                dispatch(deleteResourceFail());
            }
        })
};

export const updateResource = (token, institutionId, resourcesGroupId, resourceId, result) => dispatch => {
    dispatch(updateResourcesGroupItemStart());
    return axios.put(`/Institutions/${institutionId}/ResourcesGroups/${resourcesGroupId}/Resources/${resourceId}`, result, { headers: { Authorization: token }})
        .then(response => {
            console.log(response);
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(updateResourcesGroupItemSuccess());

            return result;
        })
        .catch(err => {
            dispatch(updateResourcesGroupItemFail(err));

            return err;
        });
};