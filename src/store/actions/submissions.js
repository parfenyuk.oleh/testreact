import * as actions from '../actionTypes/index';

export const showModal = modal => ({type: actions.SHOW_MODAL, modal});
export const hideModal = modal => ({type: actions.HIDE_MODAL, modal});