import * as actionTypes from '../actionTypes';
import * as loader from './loader';
import axios from '../../axios-instance';
import {institution} from './user.action';
import {history} from '../../helpers/index';
import {getDataByToken, logout, signIn} from './auth.action';

/*
 Get all universities actions
  */

const getAllUniversitiesStart = () => ({
  type: actionTypes.GET_ALL_UNIVERSITIES_START,
});

const getAllUniversitiesSuccess = universities => ({
  type: actionTypes.GET_ALL_UNIVERSITIES_SUCCESS,
  universities,
});

const getAllUniversitiesFail = error => ({
  type: actionTypes.GET_ALL_UNIVERSITIES_FAIL,
  error: error,
});

export const getAllUniversities = token => dispatch => {
  dispatch(getAllUniversitiesStart());
  dispatch(loader.showLoader());
  axios.get('/Institutions', {
    params: {
      take: 1000,
      skip: 0,
    },
    headers:
      {
        Authorization: `Bearer ${token}`,
      },
  })
    .then(result => {
      dispatch(getAllUniversitiesSuccess(result.data.result));
      dispatch(loader.hideLoader());
    });
};


/*
  Delete university actions
 */

const deleteUniversityStart = () => ({
  type: actionTypes.DELETE_UNIVERSITY_START,
});

const deleteUniversitySuccess = id => ({
  type: actionTypes.DELETE_UNIVERSITY_SUCCESS,
  id,
});

// const deleteUniversityFail = error => ({
//   type: actionTypes.DELETE_UNIVERSITY_FAIL,
//   error: error
// });

export const deleteUniversity = id => dispatch => {
  dispatch(deleteUniversityStart());
  dispatch(deleteUniversitySuccess(id))
};


/*
 Update university actions
 */

const updateUniversityStart = () => ({
  type: actionTypes.UPDATE_UNIVERSITY_START,
});

const updateUniversitySuccess = () => ({
  type: actionTypes.UPDATE_UNIVERSITY_SUCCESS,
});

const updateUniversityFail = error => ({
  type: actionTypes.UPDATE_UNIVERSITY_FAIL,
  error: error,
});

// for local changes in browser
export const updateUniversityRedux = (id, field, value) => ({
  type: actionTypes.UPDATE_UNIVERSITY_REDUX,
  id,
  value,
  field,
});

// for server changes
export const updateUniversity = (item, token) => dispatch => {
  dispatch(updateUniversityStart());
  dispatch(loader.showLoader());
  axios.put(`Institutions/${item.id}`, item, {
    headers:
      {
        Authorization: `Bearer ${token}`,
      },
  })
    .then(result => {
      if (result.data.ok) {
        dispatch(updateUniversitySuccess());
        dispatch(institution(`Bearer ${token}`, item.id));
      }
      else
        dispatch(updateUniversityFail(result.date.message));
      dispatch(loader.hideLoader());
    })
    .catch(error => {
      dispatch(updateUniversityFail(error));
      dispatch(loader.hideLoader());
    });
};


/*
  Add university actions
 */

const addUniversityStart = () => ({
  type: actionTypes.ADD_UNIVERSITY_START,
});

const addUniversitySuccess = value => ({
  type: actionTypes.ADD_UNIVERSITY_SUCCESS,
  value,
});

const addUniversityFail = error => ({
  type: actionTypes.ADD_UNIVERSITY_FAIL,
  error: error,
});

export const addUniversity = (value, token, refreshToken) => dispatch => {
  dispatch(addUniversityStart());
  dispatch(loader.showLoader());
  return axios.post('/Institutions', value, {
    headers:
      {
        Authorization: `Bearer ${token}`,
      },
  })
    .then(result => {
      if (result.data.ok) {
        dispatch(addUniversitySuccess(result.data.result));
        dispatch(logout());
        const user = JSON.parse(localStorage.getItem('registration'));
        dispatch(signIn(user.email, user.password));
        localStorage.removeItem('registration');
        return result.data;
      }

      else
        dispatch(addUniversityFail(result.data.message));

      dispatch(loader.hideLoader());
    }).catch(error => {
      dispatch(loader.hideLoader());
      dispatch(addUniversityFail(error));
    });
};

const refreshTokenStart = () => ({
  type: actionTypes.REFRESH_TOKEN_START,
});

const refreshTokenSuccess = value => ({
  type: actionTypes.REFRESH_TOKEN_SUCCESS,
});

const refreshTokenFail = error => ({
  type: actionTypes.REFRESH_TOKEN_FAIL,
  error: error,
});

export const refreshToken = (refreshTokenValue) => dispatch => {

  dispatch(refreshTokenStart());
  return axios.post('/Authorization/refreshToken', refreshTokenValue, {
    headers: {
      'Content-Type': 'application/json; charset=utf-8'
    }
  })
    .then(result => {
      if (result.data.ok) {

        dispatch(refreshTokenSuccess());

        return result.data.result;
      } else {
        dispatch(refreshTokenFail(result.data.message));
      }

      return result;
    }).catch(error => {
      dispatch(refreshTokenFail(error));
      return null;
    });
};


const connectUserUniversityStart = () => ({
  type: actionTypes.CONNECT_USER_UNIVERSITY_START,
});

const connectUserUniversitySuccess = () => ({
  type: actionTypes.CONNECT_USER_UNIVERSITY_SUCCESS,
});

const connectUserUniversityFail = error => ({
  type: actionTypes.CONNECT_USER_UNIVERSITY_FAIL,
  error,
});

export const connectUserUniversity = (id, email, token) => dispatch => {
  dispatch(connectUserUniversityStart());
  dispatch(loader.showLoader());
  axios.post(`Institutions/${id}/Admins`, {email}, {
    headers:
      {
        Authorization: `Bearer ${token}`,
      },
  }).then(result => {
    if (result.data.ok)
      dispatch(connectUserUniversitySuccess());
    else
      dispatch(connectUserUniversityFail(result.data.message));
    dispatch(loader.hideLoader());
  }).catch(error => {
    dispatch(connectUserUniversityFail(error));
    dispatch(loader.hideLoader());
  })
};