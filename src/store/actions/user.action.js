import * as actionTypes from '../actionTypes';
import { userConstants } from '../actionTypes/user.constants';
import * as loader from './loader';
import axios from '../../axios-instance';
import Swal from 'sweetalert2'
import {hideLoader} from './loader';
import {noticeConstants} from '../actionTypes';

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

const institutionStart = () => ({type: userConstants.GET_INSTITUTIONS_START});
const institutionSuccess = data =>({type: userConstants.GET_INSTITUTIONS_SUCCESS, payload: data});
const institutionFail = error => ({type: userConstants.GET_INSTITUTIONS_FAIL, error});

const getAllSubjectsStart = () => ({type: actionTypes.GET_ALL_SUBJECTS_START});
const getAllSubjectsSuccess = subjects => ({type: actionTypes.GET_ALL_SUBJECTS_SUCCESS, subjects});
const getAllSubjectsError = error => ({type: actionTypes.GET_ALL_SUBJECTS_FAIL, error });

const getForumPostsStart = () => ({type: userConstants.GET_FORUM_POST_START});
const getForumPostsSuccess = (data, list) =>({type: userConstants.GET_FORUM_POST_SUCCESS, payload: {data, list}});
const getForumPostsFail = error => ({type: userConstants.GET_FORUM_POST_FAIL, error});

const addForumPostStart = () => ({type: userConstants.ADD_FORUM_POST_START});
const addForumPostSuccess = data =>({type: userConstants.ADD_FORUM_POST_SUCCESS});
const addForumPostFail = error => ({type: userConstants.ADD_FORUM_POST_FAIL, error});

const getForumPostDetailStart = () => ({type: userConstants.GET_FORUM_POST_DETAIL_START});
const getForumPostDetailSuccess = data =>({type: userConstants.GET_FORUM_POST_DETAIL_SUCCESS, payload: data});
const getForumPostDetailFail = error => ({type: userConstants.GET_FORUM_POST_DETAIL_FAIL, error});

const getForumRepliesStart = () => ({type: userConstants.GET_FORUM_REPLIES_START});
const getForumRepliesSuccess = data =>({type: userConstants.GET_FORUM_REPLIES_SUCCESS, payload: data});
const getForumRepliesFail = error => ({type: userConstants.GET_FORUM_REPLIES_FAIL, error});

const addForumRepliesStart = () => ({type: userConstants.ADD_FORUM_REPLIES_START});
const addForumRepliesSuccess = forumsList =>({type: userConstants.ADD_FORUM_REPLIES_SUCCESS, payload: forumsList});
const addForumRepliesFail = error => ({type: userConstants.ADD_FORUM_REPLIES_FAIL, error});

const updateForumRepliesStart = () => ({type: userConstants.UPDATE_FORUM_REPLIES_START});
const updateForumRepliesSuccess = (id, message) =>({type: userConstants.UPDATE_FORUM_REPLIES_SUCCESS, id, message});
const updateForumRepliesFail = error => ({type: userConstants.UPDATE_FORUM_REPLIES_FAIL, error});

const updateForumStart = () => ({type: userConstants.UPDATE_FORUM_START});
const updateForumSuccess = () =>({type: userConstants.UPDATE_FORUM_SUCCESS});
const updateForumFail = error => ({type: userConstants.UPDATE_FORUM_FAIL, error});

const deleteForumRepliesStart = () => ({type: userConstants.DELETE_FORUM_REPLIES_START});
const deleteForumRepliesSuccess = (count, deleteId) =>({type: userConstants.DELETE_FORUM_REPLIES_SUCCESS, count, deleteId});
const deleteForumRepliesFail = error => ({type: userConstants.DELETE_FORUM_REPLIES_FAIL, error});

const deleteForumItemStart = () => ({type: userConstants.DELETE_FORUM_ITEM_START});
const deleteForumItemSuccess = () =>({type: userConstants.DELETE_FORUM_ITEM_SUCCESS});
const deleteForumItemFail = error => ({type: userConstants.DELETE_FORUM_ITEM_FAIL, error});

export const clearForumReplies = () => ({type: userConstants.CLEAR_FORUM_REPLIES});

export const institution = (token, id) => dispatch => {
    dispatch(institutionStart());
    return axios.get(`/Institutions/${id}`, { headers: { Authorization: `Bearer ${token}` }})
        .then(response => {
          dispatch(hideLoader());
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(institutionSuccess(result));

            return result
        })
        .catch(err => {
            dispatch(institutionFail(err));
        });
};

export const updateInstitution = (token, id, data) => dispatch => {
  dispatch(institutionStart());
  return axios.put(`/Institutions/${id}`, data, { headers: { Authorization: `Bearer ${token}` }})
    .then(response => {
      dispatch(hideLoader());
      if (response.data.message) {
        throw response
      }
      return response.data.result
    })
    .then(result => {

      dispatch(institutionSuccess(result));

      return result
    })
    .catch(err => {
      dispatch(institutionFail(err));
    });
};

export const getForumPosts = (token, id, skip, take, list, requestUrl) => dispatch => {
    dispatch(getForumPostsStart());

    return axios.get(`/Institutions/${id}/Forums?${requestUrl}take=${take}&skip=${skip}`, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            if (list === undefined) {
                list = [];
            }

            if(requestUrl.length !== 0 && list === undefined) {
                list = [];
            }

            dispatch(getForumPostsSuccess(result, list));
        })
        .catch(err => {
            dispatch(getForumPostsFail(err));
        });
};

export const addForumPosts = (token, id, result) => (dispatch, getState) => {

    dispatch(addForumPostStart());

    return axios.post(`/Institutions/${id}/Forums`, result, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(addForumPostSuccess());

          Toast.fire({
            type: 'success',
            title: 'Forum is added'
          })

            return result;
        })
        .catch(err => {

          Toast.fire({
            type: 'error',
            title: 'Failed!',
            text: err.data.message,
            timer: 3000
          });

            dispatch(addForumPostFail(err));

            return err;
        });
};

export const getAllSubjects = (token, id, relation) => dispatch => {

        dispatch(getAllSubjectsStart());

        axios.get(`/Institutions/${id}/Subjects`, {
          headers:
            { Authorization: token },
          params: {relation},
        })
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(getAllSubjectsSuccess(result));
        })
        .catch(err => {
            dispatch(getAllSubjectsError(err));
        });
};

export const getForumPostDetail = (token, id, forumId) => dispatch => {
    dispatch(getForumPostDetailStart());
    return axios.get(`/Institutions/${id}/Forums/${forumId}`, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(getForumPostDetailSuccess(result));
        })
        .catch(err => {
            dispatch(getForumPostDetailFail(err));
        });
};

export const getForumReplies = (token, id, forumId, skip, take = 10) => dispatch => {
    dispatch(getForumRepliesStart());
    return axios.get(`/Institutions/${id}/Forums/${forumId}/Replies`, {
        headers: { Authorization: token },
        params: {
            skip,
            take,
        },
    })
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(getForumRepliesSuccess(result));
        })
        .catch(err => {
            dispatch(getForumRepliesFail(err));
        });
};

export const addForumReplies = (token, id, forumId, message) => (dispatch, getState) => {

    dispatch(addForumRepliesStart());

    return axios.post(`/Institutions/${id}/Forums/${forumId}/Replies`, message, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            let forumsList = getState().user.forumReplies;
            let forumDetail = getState().user.item;
            forumDetail['repliesCount'] = forumDetail.repliesCount + 1;

            forumsList.unshift(result);
            dispatch(addForumRepliesSuccess(forumsList));
        })
        .catch(err => {
          Toast.fire({
            type: 'error',
            title: 'Failed!',
            text: 'The field Message must be a string with a maximum length of 500',
            timer: 3000
          });
            dispatch(addForumRepliesFail(err));
        });
};

export const updateForumReplies = (token, id, forumId, replyId, message) => dispatch => {

    dispatch(updateForumRepliesStart());
    return axios.put(`/Institutions/${id}/Forums/${forumId}/Replies/${replyId}`, message, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(updateForumRepliesSuccess(replyId, message.message));

            return result;
        })
        .catch(err => {
            dispatch(updateForumRepliesFail(err));

            return err;
        });
};

export const updateForum = (token, id, forumId, result) => dispatch => {

    dispatch(updateForumStart());
    return axios.put(`/Institutions/${id}/Forums/${forumId}`, result, { headers: { Authorization: token }})
        .then(response => {
          console.log(response);
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(updateForumSuccess());

            return result;
        })
        .catch(err => {
            dispatch(updateForumFail(err));

            return err;
        });
};

export const deleteForumReplies = (token, id, forumId, replyId) => (dispatch, getState) => {

    dispatch(deleteForumRepliesStart());
    return axios.delete(`/Institutions/${id}/Forums/${forumId}/Replies/${replyId}`, { headers: { Authorization: token }})
        .then(response => {

            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            let forumDetail = getState().user.item;
            forumDetail['repliesCount'] = forumDetail.repliesCount - 1;

            dispatch(deleteForumRepliesSuccess(forumDetail, replyId));

            return result;
        })
        .catch(err => {
            dispatch(deleteForumRepliesFail(err));

            return err;
        });
};

export const deleteForumItem = (token, id, forumId) => dispatch => {

    dispatch(deleteForumItemStart());
    return axios.delete(`/Institutions/${id}/Forums/${forumId}`, { headers: { Authorization: token }})
        .then(response => {
            if (response.data.message) {
                throw response
            }
            return response.data.result
        })
        .then(result => {
            dispatch(deleteForumItemSuccess());

            return result;
        })
        .catch(err => {
            dispatch(deleteForumItemFail(err));

            return err;
        });
};
