import * as actionTypes from '../actionTypes/index';
import ROLES from '../../constants/roles';

const initialState = {
  members: [],
  students: [],
  admins: [],
  subject: [],
  teacher: [],
  subjectStudents: [],
  subjectTeachers: [],
  subjectMixed: [],
  loading: false,
  amount: {},
};

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.CREATE_NEW_SUBJECT_START :
      return {...state};
    case actionTypes.CREATE_NEW_SUBJECT_SUCCESS :
      return {...state};
    case actionTypes.CREATE_NEW_SUBJECT_FAIL :
      return {...state};

    case actionTypes.CREATE_NEW_MEMBER_START :
      return {...state};
    case actionTypes.CREATE_NEW_MEMBER_SUCCESS : {
      let newState = {...state};
      if (action.newMember.role === ROLES.TEACHER) {
        newState.teacher.unshift(action.newMember);
      }
      if (action.newMember.role === ROLES.STUDENT) {
        newState.students.unshift(action.newMember);
      }

      if (action.newMember.role === ROLES.ADMIN) {
        newState.admins.unshift(action.newMember);
      }
      return newState;
    }
    case actionTypes.CREATE_NEW_MEMBER_FAIL :
      return {...state};

    case actionTypes.GET_ALL_SUBJECTS_START :
      return {...state};
    case actionTypes.GET_ALL_SUBJECTS_SUCCESS :
      return {
        ...state,
        subject: action.subjects,
      };
    case actionTypes.GET_ALL_SUBJECTS_FAIL :
      return {...state};

    case actionTypes.GET_ALL_MEMBERS_START :
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_ALL_MEMBERS_SUCCESS : {
      if (!action.search) {
        return {
          ...state,
          // members: action.members,
          students: state.students.concat(action.students),
          teacher: state.teacher.concat(action.teacher),
          admins: state.admins.concat(action.admins),
          loading: false,
        };
      } else {
        return {
          ...state,
          students: action.students,
          teacher: action.teacher,
          admins: action.admins,
          loading: false,
        }
      }
    }
    case actionTypes.CLEAR_ADMINS_TEACHERS_STUDENTS :
      return {
        ...state,
        students: [],
        teacher: [],
        admins: [],
        loading: false,
      }
    case actionTypes.GET_ALL_TYPES_MEMBERS_SUCCESS :
      return {
        ...state,
        members: action.members,
        loading: false,
      };
    case actionTypes.GET_ALL_MEMBERS_FAIL :
      return {...state};

    case actionTypes.UPDATE_MEMBER_START :
      return {...state};
    case actionTypes.UPDATE_MEMBER_SUCCESS :
      return {...state};
    case actionTypes.UPDATE_MEMBER_FAIL :
      return {...state};

    case actionTypes.UPDATE_BY_ADMIN_MEMBER_START :
      return {...state};
    case actionTypes.UPDATE_BY_ADMIN_MEMBER_SUCCESS :
      return {
        ...state,
        teacher: state.teacher.map(item => item.id === action.member.id ? action.member : item),
        students: state.students.map(item => item.id === action.member.id ? action.member : item),
        admins: state.admins.map(item => item.id === action.member.id ? action.member : item),
      };
    case actionTypes.UPDATE_BY_ADMIN_MEMBER_FAIL :
      return {...state};

    case actionTypes.GET_ASSIGNED_MEMBERS_TO_SUBJECT_START :
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_ASSIGNED_MEMBERS_TO_SUBJECT_SUCCESS :
      return {
        ...state,
        subjectStudents: state.subjectStudents.concat(action.students),
        subjectTeachers: state.subjectTeachers.concat(action.teachers),
        subjectMixed: state.subjectMixed.concat(action.mixed),
        loading: false,
      };
    case actionTypes.GET_ASSIGNED_MEMBERS_TO_SUBJECT_FAIL :
      return {
        ...state,
        loading: true,
      };
    case actionTypes.CLEAR_SUBJECT_MIXED_FIELD : {
      return {
        ...state,
        subjectMixed: [],
      }
    }
    case actionTypes.CLEAR_MEMBER_LIST : {
      return {
        ...state,
        subjectStudents: [],
        subjectTeachers: [],
      }
    }
    case actionTypes.DELETE_MEMBER_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.DELETE_MEMBER_SUCCESS : {
      const students = state.students.filter(item => item.id !== action.memberId);
      const teacher = state.teacher.filter(item => item.id !== action.memberId);
      const admins = state.admins.filter(item => item.id !== action.memberId);
      return {
        ...state,
        students,
        teacher,
        admins,
        loading: false,
      };
    }
    case actionTypes.DELETE_MEMBER_FAIL :
      return {
        ...state,
        loading: false,
      };

    case actionTypes.DELETE_MEMBER_FROM_SUBJECT_START:
      return {...state};
    case actionTypes.DELETE_MEMBER_FROM_SUBJECT_SUCCESS : {
      const subjectMixed = state.subjectMixed.filter(item => item.id !== action.memberId);
      const subjectTeachers = state.subjectTeachers.filter(item => item.id !== action.memberId);
      const subjectStudents = state.subjectStudents.filter(item => item.id !== action.memberId);
      return {
        ...state,
        subjectMixed,
        subjectTeachers,
        subjectStudents,
      };
    }
    case actionTypes.DELETE_MEMBER_FROM_SUBJECT_FAIL :
      return {...state};

    case actionTypes.ADD_MEMBER_TO_SUBJECT_START :
      return {...state};
    case actionTypes.ADD_MEMBER_TO_SUBJECT_SUCCESS : {

      const subjectMixed = [
        ...state.students.filter(item => action.userIds.indexOf(item.id) !== -1),
        ...state.teacher.filter(item => action.userIds.indexOf(item.id) !== -1),
        ...state.subjectMixed,
      ];
      return {
        ...state,
        subjectMixed,
      }
    }
    case actionTypes.ADD_MEMBER_TO_SUBJECT_FAIL :
      return {...state};

    case actionTypes.GET_MEMBER_AMOUNT_START :
      return {...state};
    case actionTypes.GET_MEMBER_AMOUNT_SUCCESS :
      return {
        ...state,
        amount: action.amount,
      };
    case actionTypes.GET_MEMBER_AMOUNT_FAIL :
      return {
        ...state,
      };

    case actionTypes.ARCHIVE_SUBJECT_START :
      return {...state};
    case actionTypes.ARCHIVE_SUBJECT_SUCCESS :
      return {
        ...state,
        subject: state.subject.map(item => {
          if (item.id === action.id) item.archievedAt = true;
          return item;
        }),
      };
    case actionTypes.ARCHIVE_SUBJECT_FAIL :
      return {...state};


    case actionTypes.UPDATE_SUBJECT_START:
      return {...state};
    case actionTypes.UPDATE_SUBJECT_SUCCESS: {
      const subject = state.subject.map(item => {
        if (item.id === action.subject.id) {
          return action.subject;
        }
        return item;
      });
      return {
        ...state,
        subject,
      };
    }
    case actionTypes.GET_ASSIGNED_STUDENTS_SUCCESS :
      return {
        ...state,
        subjectStudents: action.students,
        loading: false,
      };
    case actionTypes.GET_ASSIGNED_TEACHER_SUCCESS :
      return {
        ...state,
        subjectTeachers: action.teachers,
        loading: false,
      };

    case actionTypes.UPDATE_SUBJECT_FAIL:
      return {...state};

    case actionTypes.RESET_REDUCER :
      return {
        ...state,
        members: [],
        students: [],
        subject: [],
        teacher: [],
        admins: [],
        subjectStudents: [],
        subjectTeachers: [],
        subjectMixed: [],
        loading: false,
      };

    default:
      return {...state};
  }
};

export default reducer;