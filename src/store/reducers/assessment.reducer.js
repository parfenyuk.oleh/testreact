import * as actionTypes from '../actionTypes/assessmentTypes';

const initialState = {
    assessmentsList: {},
    assessmentsGroups: [],
    currentAssessment: {},
    studentAssessmentsList: [],
    studentAssignment: [],
    studentWorkspace:[],
    studentWorkspaceMark:[],
    studentAssignmentMark: [],
    studentAttendance: [],
    studentAttendanceStats: {},
    assignmentsAttachments: [],
    attendanceDate: [],
    attendanceUserMark: [],
    loading: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
      case actionTypes.GET_USER_ATTENDANCE_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.GET_USER_ATTENDANCE_SUCCESS:
        return {
          ...state,
          attendanceUserMark: action.payload.attendanceUserMark,
          loading: false
        };
      case actionTypes.GET_USER_ATTENDANCE_FAIL:
        return {
          ...state,
          error: action.error,
          loading: false
        };
      case actionTypes.CREATE_USER_ATTENDANCE_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.CREATE_USER_ATTENDANCE_SUCCESS:
        return {
          ...state,
          loading: false
        };
      case actionTypes.CREATE_USER_ATTENDANCE_FAIL:
        return {
          ...state,
          loading: false
        };
      case actionTypes.GET_STUDENT_ATTENDANCE_DATE_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.GET_STUDENT_ATTENDANCE_DATE_SUCCESS:
        return {
          ...state,
          attendanceDate: action.payload,
          loading: false
        };
      case actionTypes.GET_STUDENT_ATTENDANCE_DATE_FAIL:
        return {
          ...state,
          error: action.error,
          loading: false
        };
      case actionTypes.GET_ASSIGNMENT_ATTACHMENTS_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.GET_ASSIGNMENT_ATTACHMENTS_SUCCESS:
        return {
          ...state,
          assignmentsAttachments: action.payload,
          loading: false
        };
      case actionTypes.GET_ASSIGNMENT_ATTACHMENTS_FAIL:
        return {
          ...state,
          error: action.error,
          loading: false
        };
      case actionTypes.PATCH_WORKSPACE_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.PATCH_WORKSPACE_SUCCESS:
        return {
          ...state,
          loading: false
        };
      case actionTypes.PATCH_WORKSPACE_FAIL:
        return {
          ...state,
          loading: false
        };
      case actionTypes.PATCH_ASSIGNMENT_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.PATCH_ASSIGNMENT_SUCCESS:
        return {
          ...state,
          loading: false
        };
      case actionTypes.PATCH_ASSIGNMENT_FAIL:
        return {
          ...state,
          loading: false
        };
      case actionTypes.GET_STUDENTS_WORKSPACE_MARK_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.GET_STUDENTS_WORKSPACE_MARK_SUCCESS:
        return {
          ...state,
          studentWorkspaceMark: action.payload,
          loading: false
        };
      case actionTypes.GET_STUDENTS_WORKSPACE_MARK_FAIL:
        return {
          ...state,
          error: action.error,
          loading: false,
        };
        case actionTypes.GET_STUDENTS_WORKSPACE_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.GET_STUDENTS_WORKSPACE_SUCCESS:
            return {
                ...state,
                studentWorkspace: action.payload,
                loading: false
            };
        case actionTypes.GET_STUDENTS_WORKSPACE_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
      case actionTypes.GET_STUDENTS_ATTENDANCE_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.GET_STUDENTS_ATTENDANCE_SUCCESS:
        return {
          ...state,
          studentAttendance: action.payload,
          loading: false
        };
      case actionTypes.GET_STUDENTS_ATTENDANCE_FAIL:
        return {
          ...state,
          error: action.error,
          loading: false,
        };
      case actionTypes.GET_STUDENTS_ASSIGNMENT_MARK_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.GET_STUDENTS_ASSIGNMENT_MARK_SUCCESS:
        return {
          ...state,
          studentAssignmentMark: action.payload,
          loading: false
        };
      case actionTypes.GET_STUDENTS_ASSIGNMENT_MARK_FAIL:
        return {
          ...state,
          error: action.error,
          loading: false,
        };
        case actionTypes.GET_STUDENTS_ASSIGNMENT_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.GET_STUDENTS_ASSIGNMENT_SUCCESS:
            return {
                ...state,
                studentAssignment: action.payload,
                loading: false
            };
        case actionTypes.GET_STUDENTS_ASSIGNMENT_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case actionTypes.GET_ASSESSMENTS_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.GET_ASSESSMENTS_SUCCESS:
            return {
                ...state,
                assessmentsList: action.payload,
                loading: false
            };
        case actionTypes.GET_ASSESSMENTS_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };

        case actionTypes.GET_STUDENTS_ASSESSMENTS_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.GET_STUDENTS_ASSESSMENTS_SUCCESS:
            return {
                ...state,
                studentAssessmentsList: action.payload,
                loading: false
            };
        case actionTypes.GET_STUDENTS_ASSESSMENTS_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };

        case actionTypes.GET_ASSESSMENT_INFO_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.GET_ASSESSMENT_INFO_SUCCESS:
            return {
                ...state,
                currentAssessment: action.payload,
                loading: false
            };
        case actionTypes.GET_ASSESSMENT_INFO_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };



      case actionTypes.GET_WORKSPACE_INFO_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.GET_WORKSPACE_INFO_SUCCESS:
        return {
          ...state,
          currentAssessment: action.payload,
          loading: false
        };
      case actionTypes.GET_WORKSPACE_INFO_FAIL:
        return {
          ...state,
          error: action.error,
          loading: false,
        };  
        
      case actionTypes.GET_ATTENDANCE_INFO_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.GET_ATTENDANCE_INFO_SUCCESS:
        return {
          ...state,
          currentAssessment: action.payload,
          loading: false
        };
      case actionTypes.GET_ATTENDANCE_INFO_FAIL:
        return {
          ...state,
          error: action.error,
          loading: false,
        };    

        case actionTypes.GET_ASSESSMENTS_GROUPS_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.GET_ASSESSMENTS_GROUPS_SUCCESS:
            return {
                ...state,
                assessmentsGroups: action.payload,
                loading: false
            };
        case actionTypes.GET_ASSESSMENTS_GROUPS_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };

        case actionTypes.ADD_ASSESSMENTS_GROUP_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.ADD_ASSESSMENTS_GROUP_SUCCESS:
            {
                const groups = [...state.assessmentsGroups];
                groups.unshift(action.payload);
                return {
                    ...state,
                    assessmentsGroups: [
                        action.payload,
                        ...state.assessmentsGroups
                    ],
                    loading: false
                };
            }
        case actionTypes.ADD_ASSESSMENTS_GROUP_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };

        case actionTypes.CHANGE_ASSESSMENTS_GROUP_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.CHANGE_ASSESSMENTS_GROUP_SUCCESS:
            return {
                ...state,
                loading: false,
            };
        case actionTypes.CHANGE_ASSESSMENTS_GROUP_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };

        case actionTypes.CREATE_NEW_ASSIGNMENT_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.CREATE_NEW_ASSIGNMENT_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case actionTypes.CREATE_NEW_ASSIGNMENT_FAIL:
            return {
                ...state,
                loading: false
            };

      case actionTypes.UPDATE_ASSIGNMENT_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.UPDATE_ASSIGNMENT_SUCCESS:
        return {
          ...state,
          loading: false
        };
      case actionTypes.UPDATE_ASSIGNMENT_FAIL:
        return {
          ...state,
          loading: false
        };

      case actionTypes.UPDATE_ATTENDANCE_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.UPDATE_ATTENDANCE_SUCCESS:
        return {
          ...state,
          loading: false
        };
      case actionTypes.UPDATE_ATTENDANCE_FAIL:
        return {
          ...state,
          loading: false
        };

      case actionTypes.UPDATE_WORKSPACE_START:
        return {
          ...state,
          loading: true
        };
      case actionTypes.UPDATE_WORKSPACE_SUCCESS:
        return {
          ...state,
          loading: false
        };
      case actionTypes.UPDATE_WORKSPACE_FAIL:
        return {
          ...state,
          loading: false
        };

        case actionTypes.CREATE_NEW_ATTENDANCE_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.CREATE_NEW_ATTENDANCE_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case actionTypes.CREATE_NEW_ATTENDANCE_FAIL:
            return {
                ...state,
                loading: false
            };

        case actionTypes.CREATE_NEW_WORKSPACE_START:
            return {
                ...state,
                loading: true
            };
        case actionTypes.CREATE_NEW_WORKSPACE_SUCCESS:
            return {
                ...state,
                loading: false
            };
        case actionTypes.CREATE_NEW_WORKSPACE_FAIL:
            return {
                ...state,
                loading: false
            };

      case actionTypes.CLEAR_CURRENT_ASSESSMENT:
          return {
            ...state,
            currentAssessment: {}
          }
      case actionTypes.SET_CURRENT_ASSESSMENT:
        return {
          ...state,
          currentAssessment: action.assessment,
        }

      case actionTypes.GET_STUDENT_ATTENDANCE_STATS_START:
        return {
          ...state,
          loading: true,
        };
      case actionTypes.GET_STUDENT_ATTENDANCE_STATS_SUCCESS:
        return {
          ...state,
          studentAttendanceStats: action.payload.stats,
          loading: false,
        };
      case actionTypes.GET_STUDENT_ATTENDANCE_STATS_FAIL:
        return {
          ...state,
          loading: false,
        };

        default:
            return state;
    }
};

export default reducer;
