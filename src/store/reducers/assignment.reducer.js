import * as actionTypes from '../actionTypes/index';
import {STATUSES, TABS} from '../../constants/assignments';
import moment from 'moment';

const initialState = {
  activeAssignments: [],
  archiveAssignments: [],
  allAssignments: [],
  loading: false,
  amounts: {
    amountOfActiveAssignments: 0,
    amountOfArchiveAssignments: 0,
  },
  submissions: [],
  workspace: {},
};

const sortById = (a, b) => (a.id - b.id) * -1;

// const updateAssignmentHelper = (state, action) => {
//   let {activeAssignments, archiveAssignments} = state;
//
//   const now = moment();
//
//   const concat = [...activeAssignments, ...archiveAssignments].map(item => {
//     if (item.id === action.updatedAssignment.id) {
//       return action.updatedAssignment
//     }
//     return item;
//   }).sort(sortById);
//
//   activeAssignments = concat.filter(item => {
//     if (item.status === STATUSES.Done) { return false; }
//     else {
//       return !item.archievedAt;
//     }
//   });
//   archiveAssignments = concat.filter(item => {
//     if (item.status === STATUSES.Done) return true;
//     return moment(item.endDate).isBefore(now) || item.archievedAt
//   });
//
//   return {
//     ...state,
//     activeAssignments,
//     archiveAssignments,
//   }
// };

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.GET_ALL_ASSIGNMENTS_START :
      return {...state, loading: true};
    case actionTypes.GET_ALL_ASSIGNMENTS_SUCCESS : {
      const newState = {...state, loading: false};
      if (action.isArchive === TABS.archive) {
        newState.archiveAssignments = [...newState.archiveAssignments, ...action.data];
      } else if (action.isArchive === TABS.active) {
        newState.activeAssignments = [...newState.activeAssignments, ...action.data];
      } else {
        newState.allAssignments = [...newState.allAssignments, ...action.data];
      }
      return newState;
    }
    case actionTypes.GET_ALL_ASSIGNMENTS_FAIL :
      return {...state, loading: false};

    case actionTypes.CLEAR_ASSIGNMENTS :
      return {
        ...state,
        activeAssignments: [],
        archiveAssignments: [],
        allAssignments: [],
        loading: true,
      };

    case actionTypes.CREATE_NEW_ASSIGNMENT_START:
      return {...state, loading: true};
    case actionTypes.CREATE_NEW_ASSIGNMENT_SUCCESS:
      return {...state, loading: false};
    case actionTypes.CREATE_NEW_ASSIGNMENT_FAIL:
      return {...state, loading: false};

    case actionTypes.DELETE_ASSIGNMENT_START:
      return {...state};
    case actionTypes.DELETE_ASSIGNMENT_SUCCESS: {
      return {
        ...state,
        archiveAssignments: state.archiveAssignments.filter(item => item.id !== action.assignmentId),
      };
    }
    case actionTypes.DELETE_ASSIGNMENT_FAIL:
      return {...state, loading: false};

    case actionTypes.UPDATE_ASSIGNMENT_START :
      return {...state};
    case actionTypes.UPDATE_ASSIGNMENT_SUCCESS :
      return {...state};
    case actionTypes.UPDATE_ASSIGNMENT_FAIL :
      return {...state};

    case actionTypes.GET_ASSIGNMENTS_AMOUNT_START:
      return {...state};
    case actionTypes.GET_ASSIGNMENTS_AMOUNT_SUCCESS: {
      return {
        ...state,
        amounts: action.amounts,
      }
    }
    case actionTypes.GET_ASSIGNMENTS_AMOUNT_FAIL :
      return {...state};

    case actionTypes.CLEAR_SUBMISSIONS :
      return {...state, submissions: []};
    case actionTypes.GET_ALL_SUBMISSIONS_START :
      return {...state, loading: true};
    case actionTypes.GET_ALL_SUBMISSIONS_SUCCESS :
      return {
        ...state,
        submissions: [...state.submissions, ...action.submissions],
        loading: false,
      };
    case  actionTypes.GET_ALL_SUBMISSIONS_FAIL :
      return {...state, loading: false};

    case actionTypes.UPDATE_SUBMISSION_LOCAL : {
      const {submissions} = state;
      const currentSubmission = submissions.find(item => item.userId === action.payload.userId);
      currentSubmission[action.payload.field] = action.payload.value;
      return {
        ...state,
        submissions,
      }
    }

    case actionTypes.ARCHIVE_ASSIGNMENT_START:
      return {...state};
    case actionTypes.ARCHIVE_ASSIGNMENT_SUCCESS: {
      let {activeAssignments, archiveAssignments} = state;
      activeAssignments = activeAssignments.filter(item => {
        if (item.id === action.assignmentId) {
          item.archievedAt = true;
          archiveAssignments.push(item);
          archiveAssignments.sort(sortById);
          return false;
        }
        return true;
      });
      return {
        ...state,
        activeAssignments,
        archiveAssignments,
      };
    }
    case actionTypes.ARCHIVE_ASSIGNMENT_FAIL:
      return {...state, loading: false};

    case actionTypes.RESTORE_ASSIGNMENT_START : {
      return {...state}
    }
    case actionTypes.RESTORE_ASSIGNMENT_SUCCESS : {
      let {activeAssignments, archiveAssignments} = state;
      archiveAssignments = archiveAssignments.filter(item => {
        if (item.id === action.assignmentId) {
          item.archievedAt = false;
          activeAssignments.push(item);
          activeAssignments.sort(sortById);
          return false;
        }
        return true;
      });
      return {
        ...state,
        activeAssignments,
        archiveAssignments,
      }
    }
    case actionTypes.RESTORE_ASSIGNMENT_FAIL : {
      return {...state}
    }

    case actionTypes.GET_WORKSPACE_INFO_START :
      return {...state};
    case actionTypes.GET_WORKSPACE_INFO_SUCCESS : {
      return {
        ...state,
        workspace: action.workspace,
      };
    }
    case actionTypes.GET_WORKSPACE_INFO_FAIL :
      return {...state};

    case actionTypes.SET_REVIEW_SUCCESS : {
      const {submissions} = state;
      const currentItem = submissions.find(item => item.userId === action.payload.id);
      currentItem.review = {
        mark: action.payload.mark,
        description: action.payload.description,
      };
      return {
        ...state,
        submissions,
      };
    }
    case actionTypes.CHANGE_ASSIGNMENT_STATUS_START:
      return state;
    case actionTypes.CHANGE_ASSIGNMENT_STATUS_SUCCESS: {
      const {allAssignments} = state;
      const currentAssignment = allAssignments.find(item => item.id === action.assignmentId);
      currentAssignment.status = action.assignmentStatus;
      return {
        ...state,
        allAssignments,
      }
    }

    default:
      return {...state};
  }
};

export default reducer;