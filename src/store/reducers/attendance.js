import * as actionTypes from '../actionTypes/index';
import {ATTENDANCE_MARKS} from '../../constants/attendance';

const initialState = {
  list: [],
  loading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CLEAT_ATTENDANCES_LIST:
      return {
        ...state,
        list: [],
      };
    case actionTypes.GET_ATTENDANCES_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_ATTENDANCES_SUCCESS:

      return {
        ...state,
        list: [...state.list, ...action.payload.attendances],
        loading: false,
      };
    case actionTypes.GET_ATTENDANCES_FAIL:
      return {
        ...state,
        loading: false
      };
    case actionTypes.UPDATE_ATTENDANCE_LOCAL:{
      const {list} = state;
      const item = list.find(item => item.userId === action.payload.userId);
      item[action.payload.field] = action.payload.value;
      if(action.payload.field === 'attendanceMark' && action.payload.value === ATTENDANCE_MARKS.ABSENT){
        item['participationMarkPresent'] = 0;
      }
      return {
        ...state,
        list,
      };
    }
    case actionTypes.SET_REVIEW_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.SET_REVIEW_SUCCESS:
      return {
        ...state,
        loading: false,
      };
    case actionTypes.SET_REVIEW_FAIL:
      return {
        ...state,
        loading: false,
      };
    case actionTypes.MARK_ALL:{
      const list = state.list.map(item => {
        item.attendanceMark = action.payload;
        if(action.payload === ATTENDANCE_MARKS.ABSENT){
          item.participationMarkPresent = 0;
        }
        return item;
      });
      return {
        ...state,
        list,
      };
    }
    default:
      return {...state};
  }
};

export default reducer;