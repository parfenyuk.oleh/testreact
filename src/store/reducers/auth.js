import * as actionTypes from '../actionTypes/index';

const user = JSON.parse(localStorage.getItem('user'));

const initialState = user ? {loading: true, loggedIn: true, user} : {loading: false, user: {}};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.LOGOUT_START:
      return {
        ...state,
      };
    case actionTypes.LOGOUT_SUCCESS:
      return {
        loading: false,
        user: {},
      };
    case actionTypes.SIGNIN_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.SIGNIN_SUCCESS:
      return {
        ...state,
        user: {
          ...action.payload,
        },
        loggedIn: true,
        loading: false,
      };
    case actionTypes.SIGNIN_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false,
      };

    case actionTypes.GET_DATA_BY_TOKEN_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_DATA_BY_TOKEN_SUCCESS:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.user,
        },
        loading: false,
      };
    case actionTypes.GET_DATA_BY_TOKEN_FAIL:
      return {
        ...state,
        error: action.error,
      };
    default:
      return state;
  }
};

export default reducer;
