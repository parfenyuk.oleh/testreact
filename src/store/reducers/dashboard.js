import * as actionTypes from '../actionTypes/index';

const initialState = {
  assignments: [],
  teacherData: {},
  studentData: {},
  loading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {

    case actionTypes.GET_DASHBOARD_ASSIGNMENTS_START:
      return {...state};
    case actionTypes.GET_DASHBOARD_ASSIGNMENTS_SUCCESS:
      return {
        ...state,
        assignments: action.payload.assignments,
      };
    case actionTypes.GET_DASHBOARD_ASSIGNMENTS_FAIL:
      return {...state};
    case actionTypes.GET_TEACHER_DATA_SUCCESS:
      return {
        ...state,
        teacherData: action.payload.data,
      };
    case actionTypes.GET_STUDENT_DATA_SUCCESS:
      return {
        ...state,
        studentData: action.payload.data,
      };
    default:
      return {...state};
  }
};

export default reducer;