import * as actionTypes from '../actionTypes/index';

const initialState = {
  list: [],
  loading: false,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL_EVENTS_START:
      return {
        ...state,
        loading: true,
      };
    case actionTypes.GET_ALL_EVENTS_SUCCESS:
      return {
        ...state,
        list: action.payload.list,
        loading: false,
      };
    case actionTypes.GET_ALL_EVENTS_FAIL:
      return {
        ...state,
        loading: false,
      };
    default:
      return {...state};
  }
};

export default reducer;