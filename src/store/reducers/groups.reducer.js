import { groupsConstants } from '../actionTypes/groups.constants';

const groups = (state = {loading: false, list: [], activeGroup: []}, action) => {
    switch (action.type) {
        case groupsConstants.GET_ALL_GROUPS_START:
            return {
                ...state,
                loading: true,
            };
        case groupsConstants.GET_ALL_GROUPS_SUCCESS:
            return {
                ...state,
                list: action.payload,
                loading: false,
            };
        case groupsConstants.GET_ALL_GROUPS_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case groupsConstants.ADD_GROUP_ITEM_START:
            return {
                ...state,
                loading: true,
            };
        case groupsConstants.ADD_GROUP_ITEM_SUCCESS:
            return {
                ...state,
                list: action.payload,
                loading: false,
            };
        case groupsConstants.ADD_GROUP_ITEM_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case groupsConstants.GET_MEMBER_ACTIVE_GROUP_START:
            return {
                ...state,
                loading: true,
            };
        case groupsConstants.GET_MEMBER_ACTIVE_GROUP_SUCCESS:
            return {
                ...state,
                activeGroup: action.payload,
                loading: false,
            };
        case groupsConstants.GET_MEMBER_ACTIVE_GROUP_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        default:
            return state
    }
};

export default groups;