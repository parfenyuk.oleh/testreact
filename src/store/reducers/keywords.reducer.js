import { keywordsConstants } from '../actionTypes/keywords.constant';

const keywords = (state = {loading: false, list: []}, action) => {
    switch (action.type) {
        case keywordsConstants.GET_KEYWORD_LIST_START:
            return {
                ...state,
                loading: true,
            };
        case keywordsConstants.CLEAR_KEYWORD_LIST:
            return {
                ...state,
                list: [],
            };
        case keywordsConstants.GET_KEYWORD_LIST_SUCCESS:
            return {
                ...state,
                list: [...state.list, ...action.payload],
                loading: false,
            };
        case keywordsConstants.GET_KEYWORD_LIST_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        default:
            return state
    }
};

export default keywords;