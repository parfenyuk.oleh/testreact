import * as actionsTypes from '../actionTypes/layout';

const initialState = {
  modals: {
    university: false
  }
};

const reducer = (state = initialState, action) => {
  switch(action.type){
    case actionsTypes.SHOW_LAYOUT_MODAL:
    {
      const modals = {...initialState.modal};

      modals[action.modal] = true;
      return {
        ...initialState,
        modals
      }
    }
    case actionsTypes.HIDE_LAYOUT_MODAL:
    {
      const modals = {...initialState.modal};

      modals[action.modal] = false;
      return {
        ...initialState,
        modals
      }
    }
    default:
      return state;
  }
};

export default reducer;
