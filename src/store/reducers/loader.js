import * as actionTypes from '../actionTypes/loader';

const initialState = {show: false, showDocumentLoader: false};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SHOW_LOADER :
            return {
                show: true,
            };
        case actionTypes.HIDE_LOADER :
            return {
                show: false,
            };
        case actionTypes.SHOW_DOCUMENT_LOADER :
            return {
                showDocumentLoader: true,
            };
        case actionTypes.HIDE_DOCUMENT_LOADER :
            return {
                showDocumentLoader: false,
            };
        default:
            return state;
    }
};

export default reducer;