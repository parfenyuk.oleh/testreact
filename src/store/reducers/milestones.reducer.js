import {milestonesConstants} from '../actionTypes/milestones.constant';

const milestones = (state = {loading: false}, action) => {
  switch (action.type) {
    case milestonesConstants.CLEAR_MILESTONES_LIST:
      return {
        ...state,
        list: [],
      };
    case milestonesConstants.GET_MILESTONES_LIST_START:
      return {
        ...state,
        loading: true,
      };
    case milestonesConstants.GET_RESOURCES_LIST_SUCCESS:
      return {
        ...state,
        milestoneList: action.payload.items,
        userList: action.payload.users,
        loading: false,
      };
    case milestonesConstants.GET_MILESTONES_LIST_FAIL:
      return {
        ...state,
        error: action.error,
        loading: false,
      };
    case milestonesConstants.DELETE_MILESTONE_START: {
      return {...state}
    }
    case milestonesConstants.DELETE_RESOURCE_SUCCESS: {
      const milestoneList = state.milestoneList.filter(item => item.id !== action.id);
      return {...state, milestoneList};
    }
    case milestonesConstants.DELETE_MILESTONE_FAIL: {
      return {...state}
    }
    default:
      return state
  }
};

export default milestones;