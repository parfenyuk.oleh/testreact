import * as actionTypes from '../actionTypes/index';

const initialState = {
    show: false,
    type: null,
    titles: null,
    subjectName: null,
    isChanged: false,
    action: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.SHOW_MODAL:
            return {
                ...state,
                show: true,
                type: action.modalType,
                titles: action.titles,
                action: action.action,
                subjectName: action.subjectName
            };
        case actionTypes.WITH_CONFIRM:
            return {
                ...state,
                isChanged: true
            };
        case actionTypes.HIDE_MODAL:
            return {
              show: false,
              type: null,
              titles: null,
              isChanged: false,
              action: {}
            };

        default:
            return{...state};
    }
};
export default reducer;