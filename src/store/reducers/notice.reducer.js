import {noticeConstants} from "../actionTypes";

const notice = (state = {loading: false, list: [], item: null, repliesList: []}, action) => {
    switch (action.type) {
        case noticeConstants.ADD_NOTICE_ITEM_START:
            return {
                ...state,
                loading: true,
            };
        case noticeConstants.ADD_NOTICE_ITEM_SUCCESS:
            return {
                ...state,
                list: [],
                loading: false,
            };
        case noticeConstants.ADD_NOTICE_ITEM_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case noticeConstants.CLEAR_NOTICE_LIST:
            return {
                ...state,
                list: [],
                repliesList: []
            };
        case noticeConstants.GET_NOTICE_LIST_START:
            return {
                ...state,
                loading: true,
            };
        case noticeConstants.GET_NOTICE_LIST_SUCCESS:
            return {
                ...state,
                list: [...state.list, ...action.payload],
                loading: false,
            };
        case noticeConstants.GET_NOTICE_LIST_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case noticeConstants.GET_NOTICE_DETAIL_START:
            return {
                ...state,
                loading: true,
                item: null,
            };
        case noticeConstants.GET_NOTICE_DETAIL_SUCCESS:
            return {
                ...state,
                item: action.payload,
                loading: false,
            };
        case noticeConstants.GET_NOTICE_DETAIL_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case noticeConstants.GET_NOTICE_REPLIES_START:
            return {
                ...state,
                loading: true,
            };
        case noticeConstants.GET_NOTICE_REPLIES_SUCCESS:
            return {
                ...state,
                repliesList: [...state.repliesList, ...action.payload],
                loading: false,
            };
        case noticeConstants.GET_NOTICE_REPLIES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case noticeConstants.ADD_NOTICE_REPLIES_START:
            return {
                ...state,
                loading: true,
            };
        case noticeConstants.ADD_NOTICE_REPLIES_SUCCESS:
            return {
                ...state,
                repliesList: action.payload,
                loading: false,
            };
        case noticeConstants.ADD_NOTICE_REPLIES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        default:
            return state
    }
};

export default notice;