import { pinupConstants } from '../actionTypes/pinup.constants';

const pinup = (state = {loading: false, list: [], pinups: {pinUps: [], pinUpsGroupCount: 0, pinUpsPrivateCount: 0, pinUpsPublicCount: 0}}, action) => {
    switch (action.type) {
        case pinupConstants.GET_PINUP_LIST_START:
            return {
                ...state,
                loading: true,
            };
        case pinupConstants.CLEAR_PINUP_LIST:
            return {
                ...state,
                list: [],
            };
        case pinupConstants.GET_PINUP_LIST_SUCCESS:
            return {
                ...state,
                list: [...state.list, ...action.payload.pinUps],
                pinups: action.payload,
                loading: false,
            };
        case pinupConstants.GET_PINUP_LIST_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        default:
            return state
    }
};

export default pinup;