import { resourcesConstants } from '../actionTypes/resources.constants';

const resources = (state = {loading: false, list: [], groupList: []}, action) => {
    switch (action.type) {
        case resourcesConstants.GET_RESOURCES_GROUP_LIST_START:
            return {
                ...state,
                loading: true,
            };
        case resourcesConstants.CLEAR_RESOURCES_LIST:
            return {
                ...state,
                list: [],
            };
        case resourcesConstants.GET_RESOURCES_GROUP_LIST_SUCCESS:
            return {
                ...state,
                groupList: action.payload.reverse(),
                loading: false,
            };
        case resourcesConstants.GET_RESOURCES_GROUP_LIST_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case resourcesConstants.GET_RESOURCES_START:
            return {
                ...state,
                loading: true,
            };
        case resourcesConstants.GET_RESOURCES_SUCCESS:
            return {
                ...state,
                list: action.payload,
                loading: false,
            };
        case resourcesConstants.GET_RESOURCES_PAGINATION_SUCCESS:
            return {
                ...state,
                list: [...state.list, ...action.payload],
                loading: false,
            };
        case resourcesConstants.GET_RESOURCES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case resourcesConstants.LOADING_RESOURCE_START:
            return {
                ...state,
                loading: false,
            };
        case resourcesConstants.LOADING_RESOURCE_SUCCESS:
            return {
                ...state,
                loading: false,
            };
        case resourcesConstants.LOADING_RESOURCE_FAIL:
            return {
                ...state,
                loading: false,
            };
        default:
            return state
    }
};

export default resources;