import {combineReducers} from 'redux';
import auth from './auth';
import submissions from './submissions';
import loader from './loader';
import superadmin from './superadmin';
import layout from './layout';
import user from './user.reducer';
import notice from './notice.reducer';
import pinup from './pinup.reducer';
import * as actionTypes from '../actionTypes';
import keywords from './keywords.reducer';
import admin from './admin.reducer';
import groups from './groups.reducer';
import assignments from './assignment.reducer';
import assessments from './assessment.reducer';
import resources from './resources.reducer';
import milestones from './milestones.reducer';
import attendance from './attendance';
import events from './events';
import dashboard from './dashboard';
import modals from './modal.reducer';

const appReducer = combineReducers({
  auth,
  submissions,
  loader,
  superadmin,
  layout,
  user,
  keywords,
  admin,
  groups,
  notice,
  pinup,
  assignments,
  resources,
  milestones,
  attendance,
  events,
  dashboard,
  modals,
  assessments,
});


const rootReducer = (state, action) => {
  if (action.type === actionTypes.LOGOUT_SUCCESS) {
    state = undefined
  }

  return appReducer(state, action)
};

export default rootReducer;
