import * as actionsTypes from '../actionTypes/index';

const initialState = {
  modal: {
    archive: false,
    review: false,
    mark: false
  }
};

const reducer = (state = initialState, action) => {
  switch(action.type){
    case actionsTypes.SHOW_MODAL:
    {
      const modal = {...initialState.modal};

      modal[action.modal] = true;
      return {
        ...initialState,
        modal
      }
    }
    case actionsTypes.HIDE_MODAL:
    {
      const modal = {...initialState.modal};

      modal[action.modal] = false;
      return {
        ...initialState,
        modal
      }
    }
    default:
      return state;
  }
};

export default reducer;
