import * as actionTypes from '../actionTypes';

const initialState = {
  universities: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {


    //getALL
    case actionTypes.GET_ALL_UNIVERSITIES_START:
      return state;
    case actionTypes.GET_ALL_UNIVERSITIES_SUCCESS:
      return {
        ...state,
        universities: action.universities
      };
    case actionTypes.GET_ALL_UNIVERSITIES_FAIL:
      return {
        ...state,
        error: action.error
      };


    // DELETE
    case actionTypes.DELETE_UNIVERSITY_START:
      return state;
    case actionTypes.DELETE_UNIVERSITY_SUCCESS: {
      const universities = state.universities.filter(item => item.id !== action.id);
      return {
        ...state,
        universities
      }
    }
    case actionTypes.DELETE_UNIVERSITY_FAIL:
      return{
        ...state,
        error: action.error
      };


    // UPDATE
    case actionTypes.UPDATE_UNIVERSITY_START:
      return state;
    case actionTypes.UPDATE_UNIVERSITY_REDUX: {
      const universities = state.universities.map(item => {
        if(item.id === action.id){
          item[action.field] = action.value;
        }
        return item;
      });
      return {
        ...state,
        universities
      };
    }
    case actionTypes.UPDATE_UNIVERSITY_FAIL:
      return{
        ...state,
        error: action.error
      };


    //ADD
    case actionTypes.ADD_UNIVERSITY_START:
      return state;
    case actionTypes.ADD_UNIVERSITY_SUCCESS: {
      const universities = [...state.universities];
      universities.push(action.value);
      return {
        ...state,
        universities
      }
    }
    case actionTypes.ADD_UNIVERSITY_FAIL:
      return{
        ...state,
        error: action.error
      };

    //CONNECT
    case actionTypes.CONNECT_USER_UNIVERSITY_START:
      return state;
    case actionTypes.CONNECT_USER_UNIVERSITY_SUCCESS:
      return state;
    case actionTypes.CONNECT_USER_UNIVERSITY_FAIL:
      return {
        ...state,
        error: action.error
      };
    default :
      return state;
  }
};

export default reducer;