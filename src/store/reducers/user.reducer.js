import { userConstants } from '../actionTypes/user.constants';
import {noticeConstants} from '../actionTypes';

const initialState = {
    loading: false,
    institution : {},
    forums: [],
    forumLastArray: [],
    forumReplies: []
};

const user = (state = initialState, action) => {
    switch (action.type) {
        case userConstants.GET_INSTITUTIONS_START:
            return {
                ...state,
                loading: true,
            };
        case userConstants.GET_INSTITUTIONS_SUCCESS:
            return {
                ...state,
                institution: action.payload,
                loading: false,
            };
        case userConstants.GET_INSTITUTIONS_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case userConstants.GET_FORUM_POST_START:
            return {
                ...state,
                loading: true,
            };
        case userConstants.GET_FORUM_POST_SUCCESS:
            return {
                ...state,
                forums: [...action.payload.list, ...action.payload.data],
                forumLastArray: action.payload.data,
                loading: false,
            };
        case userConstants.GET_FORUM_POST_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case userConstants.ADD_FORUM_POST_START:
            return {
                ...state,
                loading: true,
            };
        case userConstants.ADD_FORUM_POST_SUCCESS:
            return {
                ...state,
                loading: false,
            };
        case userConstants.ADD_FORUM_POST_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case userConstants.GET_FORUM_POST_DETAIL_START:
            return {
                ...state,
                item: undefined,
                loading: true,
            };
        case userConstants.GET_FORUM_POST_DETAIL_SUCCESS:
            return {
                ...state,
                item: action.payload,
                loading: false,
            };
        case userConstants.GET_FORUM_POST_DETAIL_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case userConstants.CLEAR_FORUM_REPLIES:
            return {
                ...state,
                forumReplies: [],
            };
        case userConstants.GET_FORUM_REPLIES_START:
            return {
                ...state,
                loading: true,
            };
        case userConstants.GET_FORUM_REPLIES_SUCCESS:
            return {
                ...state,
                forumReplies: [...state.forumReplies, ...action.payload],
                loading: false,
            };
        case userConstants.GET_FORUM_REPLIES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case userConstants.ADD_FORUM_REPLIES_START:
            return {
                ...state,
                loading: true,
            };
        case userConstants.ADD_FORUM_REPLIES_SUCCESS:
            return {
                ...state,
                forumReplies: action.payload,
                loading: false,
            };
        case userConstants.ADD_FORUM_REPLIES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
        case userConstants.UPDATE_FORUM_REPLIES_START:
            return {
                ...state,
                loading: true,
            };
        case userConstants.UPDATE_FORUM_REPLIES_SUCCESS:{
            let {forumReplies} = state;

            forumReplies = forumReplies.map(item => {

              if(item.id === action.id){
                item.message = action.message;
              }
              return item;
            });
            return {
                ...state,
                loading: false,
                forumReplies,
            };

        }
        case userConstants.UPDATE_FORUM_REPLIES_FAIL:
            return {
                ...state,
                error: action.error,
                loading: false,
            };
      case userConstants.DELETE_FORUM_REPLIES_SUCCESS:
            return {
              ...state,
              forumReplies: state.forumReplies.filter(item => item.id !== action.deleteId)
            };
        default:
            return state
    }
};

export default user;