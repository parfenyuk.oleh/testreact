import React from 'react';
import { text, select, boolean, object } from '@storybook/addon-knobs';
import { optionalSelect } from '../../../src/components/utils/optionalSelect';
import { action } from '@storybook/addon-actions';

import readme from './README.md';
import AvatarBlock from '../../../src/components/UI/Elements/AvatarBlock';

const sizeOptions = {
    sm: 'sm',
    md: 'md',
    lg: 'lg',
};

const contextOptions = {
    primary: 'primary',
    secondary: 'secondary',
    outline: 'outline',
};

const component = () => (
    <AvatarBlock
        size={optionalSelect('Size', sizeOptions, 'md')}
        visibleInfo={boolean('VisibleCity', false)}
        forum={boolean('Forum Detail', false)}
        createdAt={text('createdAt', '123')}
        activeUntil={text('activeUntil', '123')}
        onClick={action('button_clicked')}
        className={text('ClassName', '')}
        style={object('Style', {})}
        imgSrc={text('ImgSrc', 'https://bizraise.pro/wp-content/uploads/2014/09/no-avatar-300x300.png')}
        cityName={text('City Name', 'Dnipro')}>
        Test test test
    </AvatarBlock>
);

export default [readme, component];