import React from 'react';
import { text, select, boolean, object } from '@storybook/addon-knobs';
import { optionalSelect } from '../../../src/components/utils/optionalSelect';

import readme from './README.md';
import Badge from '../../../src/components/UI/Elements/Badge';

const sizeOptions = {
    circle: 'circle',
};

const component = () => (
    <Badge
        type={optionalSelect('Type', sizeOptions, 'circle')}>
        15
    </Badge>
);

export default [readme, component];