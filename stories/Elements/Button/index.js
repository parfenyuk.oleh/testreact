import React from 'react';
import { text, select, boolean, object } from '@storybook/addon-knobs';
import { optionalSelect } from '../../../src/components/utils/optionalSelect';
import { action } from '@storybook/addon-actions';

import readme from './README.md';
import Button from '../../../src/components/UI/Elements/Button';

const sizeOptions = {
    xs: 'xs',
    sm: 'sm',
    md: 'md',
    lg: 'lg',
    'No Value': ''
};

const contextOptions = {
    primary: 'primary',
    secondary: 'secondary',
    outline: 'outline',
};

const component = () => (
    <Button
        type={text('Type', 'button')}
        size={optionalSelect('Size', sizeOptions, '')}
        context={select('Context', contextOptions, 'primary')}
        group={boolean('Group', false)}
        disabled={boolean('Disabled', false)}
        onClick={action('button_clicked')}
        className={text('ClassName', '')}
        style={object('Style', {})}>
        Great Success
    </Button>
);

export default [readme, component];