import React from 'react';
import { text, select, boolean, object } from '@storybook/addon-knobs';
import { optionalSelect } from '../../../src/components/utils/optionalSelect';
import { action } from '@storybook/addon-actions';

import readme from './README.md';
import Title from '../../../src/components/UI/Elements/Title';

const sizeOptions = {
    h1: 'h1',
    h2: 'h2',
    h3: 'h3',
    h4: 'h4',
};

const component = () => (
    <Title
        size={optionalSelect('Size', sizeOptions, 'h1')}
        style={object('Style', {})}>
        Great Success
    </Title>
);

export default [readme, component];