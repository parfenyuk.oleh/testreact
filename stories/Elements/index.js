import React from 'react';
import { storiesOf } from '@storybook/react';
import { withReadme } from 'storybook-readme';
import { withKnobs } from '@storybook/addon-knobs';
import { withInfo } from '@storybook/addon-info';

import Button from './Button/index';
import Titles from './Titles/index';
import AvatarBlock from './AvatarBlock/index';
import Badges from './Badges/index';

const stories = storiesOf('Elements', module);
const storyWrapper = story => <div style={{ margin: '35px' }}>{story()}</div>;

stories
    .addDecorator((story, context) => withInfo('')(story)(context))
    .addDecorator(storyWrapper)
    .addDecorator(withKnobs)
    .add('Button', withReadme(...Button))
    .add('Titles', withReadme(...Titles))
    .add('AvatarBlock', withReadme(...AvatarBlock))
    .add('Badges', withReadme(...Badges))
