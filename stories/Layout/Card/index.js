import React from 'react';
import { text, number, boolean, object } from '@storybook/addon-knobs';
import { optionalSelect } from '../../../src/components/utils/optionalSelect';
import { action } from '@storybook/addon-actions';

import readme from './README.md';
import Card from '../../../src/components/UI/Layout/Card';

const component = () => (
    <Card
        forum={boolean('Forum', true)}
        imgSrc={text('First Name', 'https://bizraise.pro/wp-content/uploads/2014/09/no-avatar-300x300.png')}
        firstCreatorName={text('First Name', 'Jack')}
        lastCreatorName={text('Last Name', 'Nickolson')}
        createdAt={text('Created At', '10 10')}
        topicName={text('Topic Name', 'Name')}
        subjectName={text('Subject Name', 'Subject')}
        repliesCount={number('Replies Count', 0)}
        onClick={action('button_clicked')}
        className={text('ClassName', '')}
        style={object('Style', {})}>
        Today, many people rely on computers to do homework, work, and create or store useful information.
        Therefore, it is important for the information on the computer to be stored and kept properly.
        It is also extremely important for people on computers to protect their computer from data los to protect
        their computer from data losto protect their computer from data. Therefore…important for people on …
    </Card>
);

export default [readme, component];