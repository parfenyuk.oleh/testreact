import React from 'react';
import { text, number, boolean, object } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import readme from './README.md';
import CommentItem from '../../../src/components/UI/Layout/CommentItem';

const component = () => (
    <CommentItem
        imgSrc={text('Image src', 'https://bizraise.pro/wp-content/uploads/2014/09/no-avatar-300x300.png')}
        firstName={text('First Name', 'Jack')}
        lastName={text('Last Name', 'Jack')}
        date={text('Date', '12 20202 202')}
        onClick={action('button_clicked')}
        className={text('ClassName', '')}
        style={object('Style', {})}>
        This subject consolidates the student's understanding of network security by considering security principles,
        methodologies and technologies from a technical and management perspective used in practice. The subject allows
        students to learn about and discuss various network-based attack techniques used in practice, and methods to defend
        against such attacks using industry standard tools and techniques. Topics include network attacks and defenses, web security,
        firewalls, intrusion detection systems along with security services such as confidentiality, integrity, authentication (CIA) and
        technologies such as IPSec, SSL, PGP and S/MIME.
    </CommentItem>
);

export default [readme, component];