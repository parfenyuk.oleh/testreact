import React from 'react';
import { text, number, boolean, object } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import readme from './README.md';
import Header from '../../../src/components/UI/Layout/Header';

const component = () => (
    <Header
        headerText={text('HeaderText', 'Forum')}
        onClick={action('button_clicked')}
        className={text('ClassName', '')}
        style={object('Style', {})}>
        test
    </Header>
);

export default [readme, component];