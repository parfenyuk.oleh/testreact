import React from 'react';
import { text, number, boolean, object } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import readme from './README.md';
import Reply from '../../../src/components/UI/Layout/Reply';

const component = () => (
    <Reply
        imgSrc={text('Image src', 'https://bizraise.pro/wp-content/uploads/2014/09/no-avatar-300x300.png')}
        placeholderText={text('Placeholder Text', 'Jack')}
        onClick={action('button_clicked')}
        className={text('ClassName', '')}
        style={object('Style', {})}
        message={text('Input message', 'bla bla bla')}>
    </Reply>
);

export default [readme, component];